package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Team;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class gameengine implements Listener {
	public static HashMap<String,Integer> mapvote = new HashMap<String,Integer>();
	public static ArrayList<String> mapvote2 = new ArrayList<String>();
	public static ArrayList<UUID> mapvoteblacklist = new ArrayList<UUID>();
	public static boolean allowmapvote=true;
	public static String votedmap = null;
	public static boolean allowteamchange = true;
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    @SuppressWarnings("unused")
	private bedwars plugin;
	public gameengine(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	public static boolean gamestarted = false;
	public static void sendTitle(Player player, String title, String subtitle) {
		try{
			CraftPlayer craftplayer = (CraftPlayer) player;
			PlayerConnection connection = craftplayer.getHandle().playerConnection;
			IChatBaseComponent titleJSON = ChatSerializer.a("{'text': '" + title + "'}");
			IChatBaseComponent subtitleJSON = ChatSerializer.a("{'text': '" + subtitle + "'}");
			PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleJSON, 0, 0, 0);
			PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, subtitleJSON);
			connection.sendPacket(titlePacket);
			connection.sendPacket(subtitlePacket);
		}catch (NoClassDefFoundError e1){
			
		}
	}
	
	public static void removeTeam(Player p) {
		try {
		Team remove = teams.board.getEntryTeam(p.getName());
		remove.removeEntry(p.getName());
		teams.players.remove(p.getUniqueId());
		}catch(NullPointerException en) {
			
		}
	}
	public static String setTeam(Player p,Team team) {
		String teamname = team.getName();
		Team boardteam = teams.board.getEntryTeam(p.getName());
		try {
		String boardteamname = boardteam.getName();

		int teamsize = bedwars.cfgmain.getInt("teamplayersize");
		if(team.getSize()==teamsize) {
			return "full";
		}else
			if(team.getSize()>=teamsize){
				return "full";
				
			}else
		if(boardteamname.equalsIgnoreCase(teamname)) {
			return "inside";
		}else {
			removeTeam(p);
			team.addEntry(p.getName());
			teams.players.add(p.getUniqueId());
			return "done";
		}
		}catch(NullPointerException en) {
			removeTeam(p);
			team.addEntry(p.getName());
			teams.players.add(p.getUniqueId());
			return "done";
		}
	}
	@SuppressWarnings("unchecked")
	public static String mapvoted() {
		bedwars.chatconsole("§cVoted Maps:"+mapvote.toString());
		if(gameengine.mapvoteblacklist.isEmpty()) {

			ArrayList<String> maps  = new ArrayList<String>();
			maps=(ArrayList<String>) bedwars.cfgmain.getList("maps");
			int mapsvalue = maps.size();

			String finishmapvote = maps.get((new Random()).nextInt(mapsvalue));
			
			allowmapvote=false;
			return finishmapvote;
		}else{
		Integer highestvalue = Collections.max(mapvote.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getValue();
		for(int i = 0;i<gameengine.mapvote.size();i++) {
			String highmap = getKeyByValue(mapvote, highestvalue);
			mapvote2.add(highmap);
		}
		String finishmapvote = mapvote2.get((new Random()).nextInt(mapvote2.size()));
		
		allowmapvote=false;
		bedwars.chatconsole("§cFinal Map:"+finishmapvote);
		return finishmapvote;
		}
		
	}
	public static void removeVote(UUID uuid,String mapvote) {
		mapvoteblacklist.remove(uuid);
		if(gameengine.mapvote.containsKey(mapvote)) {
			Integer oldmapvote = gameengine.mapvote.get(mapvote);
			gameengine.mapvote.put(mapvote, oldmapvote-1);
			
		}
	}
	public static boolean addVote(UUID uuid,String mapvote) {
		if(mapvoteblacklist.contains(uuid)) {
			return false;
		}else {
			
			if(!gameengine.mapvote.containsKey(mapvote)) {
				gameengine.mapvote.put(mapvote, 1);
				
			}else {
				Integer oldmapvote = gameengine.mapvote.get(mapvote);
				gameengine.mapvote.put(mapvote, oldmapvote+1);

				
			}
			mapvoteblacklist.add(uuid);
			
			return true;
		}
	}
	@EventHandler
	public void onMelt(BlockFadeEvent e) {
		if(e.getBlock().getType()==Material.ICE) {
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		UUID uuid = p.getUniqueId();
		if(gametimer.slowdamagelist.containsKey(uuid)) {
			gametimer.slowdamagelist.remove(uuid);
		}
		p.getEnderChest().clear();
		if(timer.runningtimer.equalsIgnoreCase("game")) {

			if(teams.spectator.hasEntry(p.getName())||teams.lobbyuser.hasEntry(p.getName())||teams.saver.hasEntry(p.getName())||teams.saverlobby.hasEntry(p.getName())){
				Team remove = teams.board.getEntryTeam(p.getName());
				remove.removeEntry(p.getName());
			}else {


				Team remove = teams.board.getEntryTeam(p.getName());
				remove.removeEntry(p.getName());
				if(remove.getEntries().size()==0) {
					for(Player pa:Bukkit.getOnlinePlayers()) {
						pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
						centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
						centralmessage.sendCenteredMessage(pa, "§7Team §r"+remove.getDisplayName()+"§r§7 hat verlassen");
						centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
					}
					
				}
				
			}
		}else {
			Team remove = teams.board.getEntryTeam(p.getName());
			remove.removeEntry(p.getName());
		}
	}
	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet()) {
	        if (Objects.equals(value, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
	    return null;
	}


}
