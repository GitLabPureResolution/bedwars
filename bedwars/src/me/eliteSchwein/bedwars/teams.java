package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class teams {
	

	public static ScoreboardManager manager;
	public static Scoreboard board;
	
	public static Team red;
	public static Team blue;
	public static Team yellow;
	public static Team green;
	public static Team purple;
	public static Team magenta;
	public static Team white;
	public static Team black;
	
	public static Inventory redinv;
	public static Inventory blueinv;
	public static Inventory yellowinv;
	public static Inventory greeninv;
	public static Inventory purpleinv;
	public static Inventory magentainv;
	public static Inventory whiteinv;
	public static Inventory blackinv;
	
	public static Team build;
	public static Team spectator;
	public static Team saver;
	public static Team saverlobby;
	public static Team lobbyuser;
	
	public static ArrayList<Team> teams = new ArrayList<Team>();
	
	public static ArrayList<Team> activeteams = new ArrayList<Team>();
	
	public static ArrayList<UUID> players = new ArrayList<UUID>();

	
	public static void enableTeams() {
		disableTeams();

		manager = Bukkit.getScoreboardManager();
    	board = manager.getMainScoreboard();
    	int teamamount = bedwars.cfgmain.getInt("teamamount");
    	red = board.registerNewTeam("ROT");
    	String teamnamered = bedwars.cfgmain.getString("redname");

    	String teamprefixred = bedwars.cfgmain.getString("redprefix");
    	red.setPrefix(teamprefixred.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
    	red.setDisplayName(teamnamered.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
    	activeteams.add(red);
    	blue = board.registerNewTeam("BLAU");
    	String teamnameblue = bedwars.cfgmain.getString("bluename");

    	String teamprefixblue = bedwars.cfgmain.getString("blueprefix");
    	blue.setPrefix(teamprefixblue.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
    	blue.setDisplayName(teamnameblue.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
    	activeteams.add(blue);
    	redinv = Bukkit.getServer().createInventory(null,27, red.getDisplayName()+" §8Chest");
    	blueinv = Bukkit.getServer().createInventory(null,27, blue.getDisplayName()+" §8Chest");
    	if(teamamount==4) {

    		yellow = board.registerNewTeam("GELB");
        	String teamnameyellow = bedwars.cfgmain.getString("yellowname");

        	String teamprefixyellow = bedwars.cfgmain.getString("yellowprefix");
        	yellow.setPrefix(teamprefixyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	yellow.setDisplayName(teamnameyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(yellow);
        	green = board.registerNewTeam("GRUEN");
        	String teamnamegreen = bedwars.cfgmain.getString("greenname");

        	String teamprefixgreen = bedwars.cfgmain.getString("greenprefix");
        	green.setPrefix(teamprefixgreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	green.setDisplayName(teamnamegreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(green);
        	yellowinv = Bukkit.getServer().createInventory(null,27, yellow.getDisplayName()+" §8Chest");
        	greeninv = Bukkit.getServer().createInventory(null,27, green.getDisplayName()+" §8Chest");
    	}else
        	if(teamamount==6) {
        		yellow = board.registerNewTeam("GELB");
            	String teamnameyellow = bedwars.cfgmain.getString("yellowname");

            	String teamprefixyellow = bedwars.cfgmain.getString("yellowprefix");
            	yellow.setPrefix(teamprefixyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	yellow.setDisplayName(teamnameyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	activeteams.add(yellow);
            	green = board.registerNewTeam("GRUEN");
            	String teamnamegreen = bedwars.cfgmain.getString("greenname");
            	
            	String teamprefixgreen = bedwars.cfgmain.getString("greenprefix");
            	green.setPrefix(teamprefixgreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	green.setDisplayName(teamnamegreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	activeteams.add(green);
            	purple = board.registerNewTeam("LILA");
            	String teamnamepurple = bedwars.cfgmain.getString("purplename");

            	String teamprefixpurple = bedwars.cfgmain.getString("purpleprefix");
            	purple.setPrefix(teamprefixpurple.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	purple.setDisplayName(teamnamepurple.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	activeteams.add(purple);
            	magenta = board.registerNewTeam("MAGENTA");
            	String teamnamemagenta = bedwars.cfgmain.getString("magentaname");

            	String teamprefixmagenta = bedwars.cfgmain.getString("magentaprefix");
            	magenta.setPrefix(teamprefixmagenta.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	magenta.setDisplayName(teamnamemagenta.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
            	activeteams.add(magenta);
            	yellowinv = Bukkit.getServer().createInventory(null,27, yellow.getDisplayName()+" §8Chest");
            	greeninv = Bukkit.getServer().createInventory(null,27, green.getDisplayName()+" §8Chest");
            	purpleinv = Bukkit.getServer().createInventory(null,27, purple.getDisplayName()+" §8Chest");
            	magentainv = Bukkit.getServer().createInventory(null,27, magenta.getDisplayName()+" §8Chest");
        	}else
    	if(teamamount==8) {
        	yellow = board.registerNewTeam("GELB");
        	String teamnameyellow = bedwars.cfgmain.getString("yellowname");

        	String teamprefixyellow = bedwars.cfgmain.getString("yellowprefix");
        	yellow.setPrefix(teamprefixyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	yellow.setDisplayName(teamnameyellow.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(yellow);
        	green = board.registerNewTeam("GRUEN");
        	String teamnamegreen = bedwars.cfgmain.getString("greenname");

        	String teamprefixgreen = bedwars.cfgmain.getString("greenprefix");
        	green.setPrefix(teamprefixgreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	green.setDisplayName(teamnamegreen.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(green);
        	purple = board.registerNewTeam("LILA");
        	String teamnamepurple = bedwars.cfgmain.getString("purplename");

        	String teamprefixpurple = bedwars.cfgmain.getString("purpleprefix");
        	purple.setPrefix(teamprefixpurple.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	purple.setDisplayName(teamnamepurple.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(purple);
        	magenta = board.registerNewTeam("MAGENTA");
        	String teamnamemagenta = bedwars.cfgmain.getString("magentaname");

        	String teamprefixmagenta = bedwars.cfgmain.getString("magentaprefix");
        	magenta.setPrefix(teamprefixmagenta.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	magenta.setDisplayName(teamnamemagenta.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(magenta);
        	white = board.registerNewTeam("WEISS");
        	String teamnamewhite = bedwars.cfgmain.getString("whitename");

        	String teamprefixwhite = bedwars.cfgmain.getString("whiteprefix");
        	white.setPrefix(teamprefixwhite.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	white.setDisplayName(teamnamewhite.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(white);
        	black = board.registerNewTeam("SCHWARZ");
        	String teamnameblack = bedwars.cfgmain.getString("blackname");

        	String teamprefixblack = bedwars.cfgmain.getString("blackprefix");
        	black.setPrefix(teamprefixblack.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	black.setDisplayName(teamnameblack.replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß"));
        	activeteams.add(black);
        	
        	yellowinv = Bukkit.getServer().createInventory(null,27, yellow.getDisplayName()+" §8Chest");
        	greeninv = Bukkit.getServer().createInventory(null,27, green.getDisplayName()+" §8Chest");
        	purpleinv = Bukkit.getServer().createInventory(null,27, purple.getDisplayName()+" §8Chest");
        	magentainv = Bukkit.getServer().createInventory(null,27, magenta.getDisplayName()+" §8Chest");
        	whiteinv = Bukkit.getServer().createInventory(null,27, white.getDisplayName()+" §8Chest");
        	blackinv = Bukkit.getServer().createInventory(null,27, black.getDisplayName()+" §8Chest");
    	}
		build = board.registerNewTeam("BauModus");
		saver = board.registerNewTeam("SpeicherModus");
		saverlobby = board.registerNewTeam("SpeicherLobby");
		spectator = board.registerNewTeam("Zuschauer");
		lobbyuser = board.registerNewTeam("Lobbynutzer");
		
	}
	public static void checkteamsusage() {
		teams.clear();
    	int teamamount = bedwars.cfgmain.getInt("teamamount");
    	if(!red.getEntries().isEmpty()) {
    		teams.add(red);
    	}
    	if(!blue.getEntries().isEmpty()) {
    		teams.add(blue);
    	}
    	
    	if(teamamount==4) {
        	if(!yellow.getEntries().isEmpty()) {
        		teams.add(yellow);
        	}
        	if(!green.getEntries().isEmpty()) {
        		teams.add(green);
        	}
    		
    	}

    	if(teamamount==6) {
        	if(!yellow.getEntries().isEmpty()) {
        		teams.add(yellow);
        	}
        	if(!green.getEntries().isEmpty()) {
        		teams.add(green);
        	}
        	if(!purple.getEntries().isEmpty()) {
        		teams.add(purple);
        	}
        	if(!magenta.getEntries().isEmpty()) {
        		teams.add(magenta);
        	}
    		
    	}

    	if(teamamount==8) {
        	if(!yellow.getEntries().isEmpty()) {
        		teams.add(yellow);
        	}
        	if(!green.getEntries().isEmpty()) {
        		teams.add(green);
        	}
        	if(!purple.getEntries().isEmpty()) {
        		teams.add(purple);
        	}
        	if(!magenta.getEntries().isEmpty()) {
        		teams.add(magenta);
        	}
        	if(!white.getEntries().isEmpty()) {
        		teams.add(white);
        	}
        	if(!black.getEntries().isEmpty()) {
        		teams.add(black);
        	}
    		
    	}
		
	}
	public static Team getTeamFromDisplayName(String Displayname) {
		for(Team ta:activeteams) {
			if(ta.getDisplayName().equalsIgnoreCase(Displayname)) {
				return ta;
			}
		}
		return null;
	}
	public static Color getteamleathercolor(Player p) {

		if(red.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("redleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(blue.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("blueleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(green.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("greenleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(yellow.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("yellowleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(magenta.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("magentaleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(purple.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("purpleleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(white.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("whiteleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}else

		if(black.hasEntry(p.getName())) {
			String colorstring = bedwars.cfgmain.getString("blackleathercolor");
			int r=0;
			int g=0;
			int b=0;
			for(String str:colorstring.split(";")) {
				if(str.contains("r")) {
					String rstr=str.replace("r:", "");
					r=Integer.parseInt(rstr);
				}
				if(str.contains("g")) {
					String rstr=str.replace("g:", "");
					g=Integer.parseInt(rstr);
				}
				if(str.contains("b")) {
					String rstr=str.replace("b:", "");
					b=Integer.parseInt(rstr);
				}
			}
			Color color = Color.fromRGB(r, g, b);
			return color;
		}
		return Color.WHITE;
	}
	public static int getteamclaycolor(Player p) {
		if(red.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("redclaycolor");
		}
		if(blue.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("blueclaycolor");
		}
		if(green.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("greenclaycolor");
		}
		if(yellow.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("yellowclaycolor");
		}
		if(purple.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("purpleclaycolor");
		}
		if(magenta.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("magentaclaycolor");
		}
		if(white.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("whiteclaycolor");
		}
		if(black.hasEntry(p.getName())) {
			return bedwars.cfgmain.getInt("blackclaycolor");
		}
		return 0;
	}
	public static void disableTeams() {
		try {
		redinv.clear();
		blueinv.clear();
		yellowinv.clear();
		greeninv.clear();
		purpleinv.clear();
		magentainv.clear();
		whiteinv.clear();
		blackinv.clear();
		}catch(NullPointerException e) {
			
		}
		try {


			manager = Bukkit.getScoreboardManager();
	    	board = manager.getMainScoreboard();
	    	for(Team reset:board.getTeams()) {
	    		reset.unregister();
	    	}
	    	for(Objective reset:board.getObjectives()) {
	    		reset.unregister();
	    	}
		}catch (NullPointerException e) {
			
		}
	}
}
