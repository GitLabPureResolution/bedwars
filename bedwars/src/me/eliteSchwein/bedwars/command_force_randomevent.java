package me.eliteSchwein.bedwars;


import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import me.eliteSCHW31N.bedwars.randomevents.manager;

public class command_force_randomevent implements CommandExecutor{

    HeadDatabaseAPI api = new HeadDatabaseAPI();
	@SuppressWarnings("unused")
	private bedwars plugin;

	public command_force_randomevent(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(p.hasPermission("bedwars.forcerandomevent")) {
				boolean scan = mapengine.forcerandomevn;
				if(args.length==1) {
					ArrayList<String> scanlist = manager.rmds;
					if(scanlist.contains(args[0])) {
						if(timer.runningtimer.equalsIgnoreCase("game")||mapengine.allowrandomevnvote==false) {
							p.sendMessage(bedwars.MessageNoPerm+"Das Spiel läuft schon bereits");
							return true;
						}
						p.sendMessage(bedwars.MessagePerm+"RandomEvent §r§a"+args[0]+" §r§2wird erzwungen");
						mapengine.forcerandomevn=true;
						mapengine.forcerandomevnwhich=args[0];
					}else {
						p.sendMessage(bedwars.MessageNoPerm+"RandomEvent §r§4"+args[0]+" §r§cexsistiert nicht");
					}
					
				}else {
					if(scan==true) {
						p.sendMessage(bedwars.MessagePerm+"RandomEvent wird entzwungen");
						mapengine.forcerandomevn=false;
						mapengine.forcerandomevnwhich=null;
					}else {
						p.sendMessage(bedwars.MessageNoPerm+"RandomEvent wird nicht erzwungen");
					}
				}
			}else {
				p.sendMessage(bedwars.MessageNoPerm+"Du hast nicht das Recht dafür");
			}
			
		}else {
			bedwars.chatconsole("§cPlayer only!");
		}
		return false;
	}

}
