package me.eliteSchwein.bedwars;

import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Anvil {

    public static void openAnvilInventory(final Player player) {
    	
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        FakeAnvil fakeAnvil = new FakeAnvil(entityPlayer);
        int containerId = entityPlayer.nextContainerCounter();
   
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutOpenWindow(containerId, "minecraft:anvil", new ChatMessage("Repairing", new Object[]{}), 0));
   
        entityPlayer.activeContainer = fakeAnvil;
        entityPlayer.activeContainer.windowId = containerId;
        entityPlayer.activeContainer.addSlotListener(entityPlayer);
        entityPlayer.activeContainer = fakeAnvil;
        entityPlayer.activeContainer.windowId = containerId;
   
        Inventory inv = fakeAnvil.getBukkitView().getTopInventory();
        ItemStack moneyitem = new ItemStack(Material.PAPER);
        ItemMeta moneymeta = moneyitem.getItemMeta();
        moneymeta.setDisplayName("Mapname");
        moneyitem.setItemMeta(moneymeta);
        moneyitem.setAmount(1);
        inv.setItem(0, moneyitem); //Remove this if you don't want to add stuff into slots. This is what I needed it for my GUI.
        
   
        }
}
