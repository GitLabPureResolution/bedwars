package me.eliteSchwein.bedwars;

import java.io.File;
import java.io.IOException;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.LocalPlayer;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldedit.session.ClipboardHolder;

@SuppressWarnings("deprecation")
public class schematic {
	public static void setPosition1(Player player,Location location) {
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		player.performCommand("/pos1 "+Integer.toString(x)+","+Integer.toString(y)+","+Integer.toString(z));
		
	}
	public static void setPosition2(Player player,Location location) {
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		player.performCommand("/pos2 "+Integer.toString(x)+","+Integer.toString(y)+","+Integer.toString(z));
		player.performCommand("/copy");
		
	}
	public static void makeWorldGuard(Player player,String mapname) {
		player.setOp(true);
		new BukkitRunnable() {

			@Override
			public void run() {
				player.performCommand("rg delete "+mapname);
				new BukkitRunnable() {

					@Override
					public void run() {
						player.performCommand("rg create "+mapname);
						new BukkitRunnable() {

							@Override
							public void run() {
								player.performCommand("rg flag "+mapname+" passthrough allow");
								new BukkitRunnable() {

									@Override
									public void run() {
										player.performCommand("rg flag "+mapname+" entry allow");
										new BukkitRunnable() {

											@Override
											public void run() {
												player.performCommand("rg flag "+mapname+" block-place allow");
												new BukkitRunnable() {

													@Override
													public void run() {
														player.performCommand("rg flag "+mapname+" block-break allow");
														new BukkitRunnable() {

															@Override
															public void run() {
																player.performCommand("rg flag "+mapname+" item-pickup allow");
																new BukkitRunnable() {

																	@Override
																	public void run() {
																		player.performCommand("rg flag "+mapname+" item-drop allow");
																		new BukkitRunnable() {

																			@Override
																			public void run() {
																				player.performCommand("rg flag "+mapname+" weather-lock clear");
																				new BukkitRunnable() {

																					@Override
																					public void run() {
																						player.performCommand("rg flag "+mapname+" time-lock 2000");
																						new BukkitRunnable() {

																							@Override
																							public void run() {
																								player.performCommand("rg flag "+mapname+" game-mode survival");
																								new BukkitRunnable() {

																									@Override
																									public void run() {
																										player.performCommand("rg flag "+mapname+" item-drop allow");
																										new BukkitRunnable() {

																											@Override
																											public void run() {
																												player.performCommand("rg flag "+mapname+" pvp allow");
																												new BukkitRunnable() {

																													@Override
																													public void run() {
																														player.performCommand("rg flag "+mapname+" sleep deny");
																														new BukkitRunnable() {

																															@Override
																															public void run() {
																																player.performCommand("rg flag "+mapname+" pvp allow");
																																new BukkitRunnable() {

																																	@Override
																																	public void run() {
																																		player.performCommand("rg flag "+mapname+" use allow");
																																		new BukkitRunnable() {

																																			@Override
																																			public void run() {
																																				player.performCommand("rg flag "+mapname+" chest-access allow");
																																				new BukkitRunnable() {

																																					@Override
																																					public void run() {
																																						player.setOp(false);
																																						
																																					}
																																					
																																				}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																																				
																																				
																																			}
																																			
																																		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																																		
																																		
																																	}
																																	
																																}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																																
																																
																															}
																															
																														}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																														
																														
																													}
																													
																												}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																												
																												
																											}
																											
																										}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																										
																										
																									}
																									
																								}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																								
																								
																							}
																							
																						}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																						
																					}
																					
																				}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																				
																			}
																			
																		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																		
																	}
																	
																}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
																
															}
															
														}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
														
													}
													
												}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
												
											}
											
										}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
										
									}
									
								}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
								
							}
							
						}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
						
					}
					
				}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
				
			}
			
		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
		
	}
    public static void save(Player player, String schematicName,String mapname) {
        try {
        	
            File schematic = new File("plugins/bedwars/schematics/"+mapname +"/"+ schematicName+".schematic");
            File schematicpath = new File("plugins/bedwars/schematics/"+mapname);
           if(!schematic.exists()) {
        	   schematicpath.mkdir();
        	   schematicpath.mkdirs();
        	   schematic.createNewFile();
           }

            WorldEditPlugin wep = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
            WorldEdit we = wep.getWorldEdit();

            LocalPlayer localPlayer = wep.wrapPlayer(player);
            LocalSession localSession = we.getSession(localPlayer);
            ClipboardHolder selection = localSession.getClipboard();
            EditSession editSession = localSession.createEditSession(localPlayer);

            Vector min = selection.getClipboard().getMinimumPoint();
            Vector max = selection.getClipboard().getMaximumPoint();

            editSession.enableQueue();
            CuboidClipboard clipboard = new CuboidClipboard(max.subtract(min).add(new Vector(1, 1, 1)), min);
            clipboard.copy(editSession);
            SchematicFormat.MCEDIT.save(clipboard, schematic);
            editSession.flushQueue();

            player.sendMessage("Saved schematic!");
        } catch (IOException | DataException ex) {
            ex.printStackTrace();
        } catch (EmptyClipboardException ex) {
            ex.printStackTrace();
        }
    }


    public static void paste(String schematicName, Location pasteLoc,String mapname) {
        try {
            File dir = new File("plugins/bedwars/schematics/"+mapname+"/"+schematicName+".schematic");

            EditSession editSession = new EditSession(new BukkitWorld(pasteLoc.getWorld()), 999999999);
            editSession.enableQueue();

            SchematicFormat schematic = SchematicFormat.getFormat(dir);
            CuboidClipboard clipboard = schematic.load(dir);

            clipboard.paste(editSession, BukkitUtil.toVector(pasteLoc), true);
            editSession.flushQueue();
        } catch (DataException | IOException ex) {
            ex.printStackTrace();
        } catch (MaxChangedBlocksException ex) {
            ex.printStackTrace();
        }
    }

}