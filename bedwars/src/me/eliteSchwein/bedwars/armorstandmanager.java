package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.EulerAngle;

import me.arcaniax.hdb.api.HeadDatabaseAPI;


public class armorstandmanager {

    static HeadDatabaseAPI api = new HeadDatabaseAPI();
	public static void onEnable() {
		makeTeamStands(bedwars.cfglobbydata);
		bedwars.chatconsole("§cArmorStand online");
	}
	
	public static void makeTeamStands(FileConfiguration cfg) {
		new BukkitRunnable() {
			int max = bedwars.cfgmain.getInt("teamplayersize");
			int timer = 0;
			@Override
			public void run() {
				if(timer==max+1) {
					cancel();
					dynamicchanger();
					me.eliteSchwein.bedwars.timer.onEnable();
				}else {
					timer++;
				}
				for(Team team:teams.activeteams) {
					String teamname = team.getDisplayName();
				// TODO Auto-generated method stub

					bedwars.chatconsole(teamname);
					

            	String configfinder = teamname+" Team AM "+Integer.toString(timer);
            	bedwars.chatconsole(configfinder);
    			
    			
            	
            	try {
            	World w = Bukkit.getWorld(cfg.getString(configfinder+".loc.world"));
            	double x = cfg.getDouble(configfinder+".loc.X");
            	double y = cfg.getDouble(configfinder+".loc.Y");
            	double z = cfg.getDouble(configfinder+".loc.Z");
            	double yaw = cfg.getDouble(configfinder+".HeadYaw");
            	double pitch = cfg.getDouble(configfinder+".HeadPitch");
            	
            	
            	int redchestplate = cfg.getInt(configfinder+".chestplate.red");
            	int bluechestplate = cfg.getInt(configfinder+".chestplate.blue");
            	int greenchestplate = cfg.getInt(configfinder+".chestplate.green");
            	Material chestplate = Material.getMaterial(cfg.getString(configfinder+".chestplate.material"));
            	int chestplatesubid = cfg.getInt(configfinder+".chestplate.subid");
            	
            	int redleggings = cfg.getInt(configfinder+".leggings.red");
            	int blueleggings = cfg.getInt(configfinder+".leggings.blue");
            	int greenleggings = cfg.getInt(configfinder+".leggings.green");
            	Material leggings = Material.getMaterial(cfg.getString(configfinder+".leggings.material"));
            	int leggingssubid = cfg.getInt(configfinder+".leggings.subid");
            	
            	int redboots = cfg.getInt(configfinder+".boots.red");
            	int blueboots = cfg.getInt(configfinder+".boots.blue");
            	int greenboots = cfg.getInt(configfinder+".boots.green");
            	Material boots = Material.getMaterial(cfg.getString(configfinder+".boots.material"));
            	int bootssubid = cfg.getInt(configfinder+".boots.subid");
            	

            	Material materialhand = Material.getMaterial(cfg.getString(configfinder+".handitem.material"));
            	int handsubid = cfg.getInt(configfinder+".handitem.subid");
				
				double HeadX = cfg.getDouble(configfinder+".HeadX");
				double HeadY = cfg.getDouble(configfinder+".HeadY");
				double HeadZ = cfg.getDouble(configfinder+".HeadZ");
				
				double LeftArmX = cfg.getDouble(configfinder+".LeftArmX");
				double LeftArmY = cfg.getDouble(configfinder+".LeftArmY");
				double LeftArmZ = cfg.getDouble(configfinder+".LeftArmZ");
				
				double RightArmX = cfg.getDouble(configfinder+".RightArmX");
				double RightArmY = cfg.getDouble(configfinder+".RightArmY");
				double RightArmZ = cfg.getDouble(configfinder+".RightArmZ");
				
				double BodyX = cfg.getDouble(configfinder+".BodyX");
				double BodyY = cfg.getDouble(configfinder+".BodyY");
				double BodyZ = cfg.getDouble(configfinder+".BodyZ");
				
				double LeftLegX = cfg.getDouble(configfinder+".LeftLegX");
				double LeftLegY = cfg.getDouble(configfinder+".LeftLegY");
				double LeftLegZ = cfg.getDouble(configfinder+".LeftLegZ");
				
				double RightLegX = cfg.getDouble(configfinder+".RightLegX");
				double RightLegY = cfg.getDouble(configfinder+".RightLegY");
				double RightLegZ = cfg.getDouble(configfinder+".RightLegZ");

            	ItemStack itemhand = new ItemStack(materialhand,1,(byte)handsubid);
            	itemhand.setAmount(1);
            	
            	ItemStack chestplateitem = new ItemStack(chestplate,1,(byte)chestplatesubid);
            	if(chestplate==Material.LEATHER_CHESTPLATE) {
            		LeatherArmorMeta chestplatemeta = (LeatherArmorMeta) chestplateitem.getItemMeta();
            		chestplatemeta.setColor(org.bukkit.Color.fromRGB(redchestplate, greenchestplate, bluechestplate));
            		chestplateitem.setItemMeta(chestplatemeta);
            	}
            	chestplateitem.setAmount(1);
            	
            	ItemStack leggingsitem = new ItemStack(leggings,1,(byte)leggingssubid);
            	if(leggings==Material.LEATHER_LEGGINGS) {
            		LeatherArmorMeta leggingsmeta = (LeatherArmorMeta) leggingsitem.getItemMeta();
            		leggingsmeta.setColor(org.bukkit.Color.fromRGB(redleggings, greenleggings, blueleggings));
            		leggingsitem.setItemMeta(leggingsmeta);
            	}
            	leggingsitem.setAmount(1);

            	
            	ItemStack bootsitem = new ItemStack(boots,1,(byte)bootssubid);
            	if(boots==Material.LEATHER_BOOTS) {
            		LeatherArmorMeta bootsmeta = (LeatherArmorMeta) bootsitem.getItemMeta();
            		bootsmeta.setColor(org.bukkit.Color.fromRGB(redboots, greenboots, blueboots));
            		bootsitem.setItemMeta(bootsmeta);
            	}
            	bootsitem.setAmount(1);
            	
            	
            	 Location loc = new Location(w, x, y, z);
            	 loc.setYaw((float) yaw);
            	 loc.setPitch((float) pitch);
 	            ArmorStand am = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
 	            am.setHelmet(new ItemStack(Material.STONE));
 	            am.setArms(true);
 	            am.setBasePlate(false);
 	            am.setHeadPose(new EulerAngle(HeadX, HeadY, HeadZ));
 	            am.setLeftArmPose(new EulerAngle(LeftArmX, LeftArmY, LeftArmZ));
 	            am.setRightArmPose(new EulerAngle(RightArmX, RightArmY, RightArmZ));
 	            am.setBodyPose(new EulerAngle(BodyX, BodyY, BodyZ));
 	            am.setLeftLegPose(new EulerAngle(LeftLegX, LeftLegY, LeftLegZ));
 	            am.setRightLegPose(new EulerAngle(RightLegX, RightLegY, RightLegZ));
 	            am.setItemInHand(itemhand);
 	            am.setChestplate(chestplateitem);
 	            am.setLeggings(leggingsitem);
 	            am.setBoots(bootsitem);
 	            am.setGravity(false);
 	            am.setCustomName(teamname+" "+Integer.toString(timer));
 	            am.setCustomNameVisible(true);
 	            am.setVisible(true);
            	}catch(IllegalArgumentException e) {

            	}}
    			}
            
        
	}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"),5, 5);
		
	}
	
      public static void updateTeamInv() {
    	  for(Player p : Bukkit.getOnlinePlayers()) {
    		 String invname = p.getOpenInventory().getTitle();
    		 if(invname.equalsIgnoreCase("§8>> §3Team-Auswahl")) {
    			 ItemStack teamexit = new ItemStack(Material.BARRIER);
    			 ItemMeta teamexit1 = teamexit.getItemMeta();
					teamexit1.setDisplayName("§cTeam-Auswahl schließen?");
					ArrayList<String> Lore1 = new ArrayList<String>();
					Lore1.add("§aKlick, zum schließen");
					teamexit1.setLore(Lore1);
					teamexit.setItemMeta(teamexit1);
					p.getOpenInventory().setItem(8, teamexit);
    			 ArrayList<String> Color = new ArrayList<String>();

					Color.add("9356");
					Color.add("8924");
					Color.add("9140");
					Color.add("10220");
					Color.add("9788");
					Color.add("9464");
					Color.add("9248");
					Color.add("8777");
					
					for(int i = 0;i<teams.activeteams.size();i++) {
						
						
						ArrayList<String> names = new ArrayList<String>();
						if(teams.activeteams.get(i).getEntries().isEmpty()) {
							
						}else {
							for(String str:teams.activeteams.get(i).getEntries()) {
								names.add(str);
							}
						}
						
						ItemStack teamselector = api.getItemHead(Color.get(i));
						ArrayList<String> Lore = new ArrayList<String>();
						if(names.size()==0) {
							Lore.add("§6│");
							Lore.add("§6└Leer");
							
						}else {
							for(int i2 = 0;i2<names.size()-1;i2++) {
								Lore.add("§6├"+names.get(i2));
							}
							Lore.add("§6└"+names.get(names.size()-1));
							
						}
						ItemMeta teammeta = teamselector.getItemMeta();
						teammeta.setDisplayName(teams.activeteams.get(i).getDisplayName());
						teammeta.setLore(Lore);
						teamselector.setItemMeta(teammeta);
						p.getOpenInventory().setItem(i, teamselector);
						
					}
    			 
    		 }else if(teams.spectator.hasEntry(p.getName())) {
    			
    			 ArrayList<String> Color = new ArrayList<String>();

					Color.add("9356");
					Color.add("8924");
					Color.add("9140");
					Color.add("10220");
					Color.add("9788");
					Color.add("9464");
					Color.add("9248");
					Color.add("8777");
					ItemStack freefly = new ItemStack(Material.COMPASS,1);
					ItemMeta freeflym = freefly.getItemMeta();
					freeflym.setDisplayName("§7>> §fRandom Spieler");
					freefly.setItemMeta(freeflym);
					p.getInventory().setItem(8, freefly);
					for(int i = 0;i<teams.activeteams.size();i++) {
						
						
						ArrayList<String> names = new ArrayList<String>();
						if(teams.activeteams.get(i).getEntries().isEmpty()) {
							
						}else {
							for(String str:teams.activeteams.get(i).getEntries()) {
								names.add(str);
							}
						}
						
						ItemStack teamselector = api.getItemHead(Color.get(i));
						ArrayList<String> Lore = new ArrayList<String>();
						if(names.size()==0) {
							teamselector=api.getItemHead("14901");
							Lore.add("§6│");
							Lore.add("§6└Leer");
							
						}else {
							for(int i2 = 0;i2<names.size()-1;i2++) {
								Lore.add("§6├"+names.get(i2));
							}
							Lore.add("§6└"+names.get(names.size()-1));
							
						}
						ItemMeta teammeta = teamselector.getItemMeta();
						teammeta.setDisplayName(teams.activeteams.get(i).getDisplayName());
						teammeta.setLore(Lore);
						teamselector.setItemMeta(teammeta);
						p.getInventory().setItem(i, teamselector);
						
					}
    			 
    		 }
    		 if(timer.runningtimer.equalsIgnoreCase("Lobby")) {
    	   		  p.updateInventory();
    			 
    		 }
    	  }
      }
		
	public static void dynamicchanger() {
		new BukkitRunnable() {

			@Override
			public void run() {
				updateTeamInv();
				int teamsize = bedwars.cfgmain.getInt("teamplayersize");
				for(Team team:teams.activeteams) {
					String worldname = bedwars.cfglobbydata.getString("§cRot Team AM 1.loc.world");
					World w = Bukkit.getWorld(worldname);
					ArrayList<String> dynteam = new ArrayList<String>();
					for(String st : team.getEntries()) {
						dynteam.add(st);
					}
					for(Entity et:w.getEntities()) {
						if(et instanceof ArmorStand) {
							for(int i = 1;i<(teamsize+1);i++) {
								try {
								String configstand = team.getDisplayName()+" Team AM "+Integer.toString(i);
								String prefix = team.getPrefix();
								double X = et.getLocation().getX();
								double Y = et.getLocation().getY();
								double Z = et.getLocation().getZ();
								double checkX = bedwars.cfglobbydata.getDouble(configstand+".loc.X");
								double checkY = bedwars.cfglobbydata.getDouble(configstand+".loc.Y");
								double checkZ = bedwars.cfglobbydata.getDouble(configstand+".loc.Z");
								if(X==checkX&&Y==checkY&&Z==checkZ) {
								ArmorStand am = (ArmorStand) et;
								try {
									String tpl = dynteam.get(i-1);
									am.setCustomName(prefix+tpl);
									ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
						            SkullMeta meta = (SkullMeta)skull.getItemMeta();
						            meta.setOwner(tpl);
						            meta.setDisplayName(tpl);
						            skull.setItemMeta(meta);
						            am.setHelmet(skull);
									
									
								}catch(NullPointerException | IndexOutOfBoundsException en) {
									am.setCustomName(prefix+"Frei");
									ItemStack skull = api.getItemHead("920");
						            am.setHelmet(skull);
									
								}
								}
								}catch(NullPointerException | ConcurrentModificationException en1) {
									
								}
							}
							
						}
					}
				}
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 2, 10);
	}

}
