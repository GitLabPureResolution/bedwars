package me.eliteSchwein.bedwars;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftFish;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import me.eliteSCHW31N.bedwars.randomevents.manipulator;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.Blocks;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockAction;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class event_extra implements Listener {
	public static ConcurrentHashMap<Location, ArrayList<UUID>> teamchestsopened = new ConcurrentHashMap<Location, ArrayList<UUID>>();
	public static File filealertstring = new File("plugins/bedwars", "alertstring.yml");
	public static FileConfiguration alertstring = YamlConfiguration.loadConfiguration(filealertstring);
	public static boolean alternatetnt = false;
	public static int tnttime=50;
	public static float tntpower=3;

	HeadDatabaseAPI api = new HeadDatabaseAPI();
    private bedwars plugin;
	public event_extra(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	public static ArrayList<Player> homepowder = new ArrayList<Player>();
	@EventHandler
	public void onStringPlace(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(p.getItemInHand().getType()==Material.STRING) {
			Location loc = e.getClickedBlock().getLocation().add(0, 1, 0);
			for(Team team:gametimer.startedteams) {
				if(team.hasEntry(p.getName())) {
					team.getName();
					int x = loc.getBlockX();
					int y = loc.getBlockY();
					int z = loc.getBlockZ();
					alertstring.set("X."+x+".Y."+y+".Z."+z, team.getName());
					try {
						alertstring.save(filealertstring);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					return;
				}
			}
		}
	}
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		
		if(homepowder.contains(e.getPlayer())) {
			homepowder.remove(e.getPlayer());
		}
		if(e.getPlayer().getLocation().getBlock().getType()==Material.TRIPWIRE) {
			Location loc = e.getPlayer().getLocation();

				for(Team team:gametimer.startedteams) {
					if(team.hasEntry(p.getName())) {
						team.getName();

						int x = loc.getBlockX();
						int y = loc.getBlockY();
						int z = loc.getBlockZ();
						String name = alertstring.getString("X."+x+".Y."+y+".Z."+z);
						if(name.equalsIgnoreCase(team.getName())) {
							
						}else {
							for(Team team1:gametimer.startedteams) {
								if(team1.getName().equalsIgnoreCase(name)) {
									for(String entry:team1.getEntries()) {
										Player pen = Bukkit.getPlayer(entry);
										bedwars.sendTitle(pen, "§cALARM", null);
									}
								}
							}
						}
						
					}
				}
			
			
		}
		
	}
	@EventHandler
	public void onFishing(EntityDamageByEntityEvent e) {
		if(e.getDamager() instanceof CraftFish && e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
//			Projectile pr = (Projectile)e.getDamager();
			CraftFish cf = (CraftFish) e.getDamager();
			if(cf.getShooter() instanceof Player) {
				Player po = (Player) cf.getShooter();
				Team teama = teams.board.getEntryTeam(po.getName());
				if(teama.hasEntry(p.getName())) {
					e.setCancelled(true);
				}
			}
		}
	}
	@EventHandler
	public void onAtack(EntityDamageByEntityEvent e) {
		Entity et = e.getEntity();
		Entity eto = e.getDamager();
		if(et instanceof Player&&eto instanceof Player) {
			Player p = (Player) et;
			Player pd = (Player) eto;
			for(Team team:gametimer.startedteams) {
				if(team.hasEntry(p.getName())) {
					if(team.hasEntry(pd.getName())) {
//						p.sendMessage(bedwars.MessageNoPerm+"Das ist dein Mitspieler");
						e.setCancelled(true);
						e.setDamage(0);
					}
				}
			}
			
		}
		
	}
	@EventHandler
	public void onQuit1(PlayerQuitEvent e) {
		Player p = (Player) e.getPlayer();
		UUID uuid = p.getUniqueId();
		for(Location loc:teamchestsopened.keySet()) {
			ArrayList<UUID> uuids = teamchestsopened.get(loc);
			if(uuids.contains(uuid)) {
				uuids.remove(uuid);
				if(uuids.isEmpty()) {
					teamchestsopened.remove(loc);
				}else {
					teamchestsopened.replace(loc, uuids);
					p.playSound(loc, Sound.CHEST_CLOSE, 1, 1);
					
				}
				return;
			}
			
		}
		
		
	}
	@EventHandler
	public void onInvClose1(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		UUID uuid = p.getUniqueId();
		for(Location loc:teamchestsopened.keySet()) {
			ArrayList<UUID> uuids = teamchestsopened.get(loc);
			if(uuids.contains(uuid)) {
				uuids.remove(uuid);
				if(uuids.isEmpty()) {
					teamchestsopened.remove(loc);
				}else {
					teamchestsopened.replace(loc, uuids);
					p.playSound(loc, Sound.CHEST_CLOSE, 1, 1);
					
				}
				return;
			}
			
		}
		
		
	}
	@EventHandler
	  public void onInteract(PlayerInteractEvent event)
	  {
		Player player = event.getPlayer();
		if(event.getAction()==Action.RIGHT_CLICK_AIR||event.getAction()==Action.RIGHT_CLICK_BLOCK) {
			if(event.getAction()==Action.RIGHT_CLICK_BLOCK) {
				if(event.getClickedBlock().getType()==Material.ENDER_CHEST) {

					event.setCancelled(true);
					for(Team teamA:teams.activeteams) {
						if(teamA.hasEntry(player.getName())) {
							if(teamchestsopened.containsKey(event.getClickedBlock().getLocation())) {
								ArrayList<UUID> testteama = teamchestsopened.get(event.getClickedBlock().getLocation());
								if(!testteama.contains(player.getUniqueId())) {
									testteama.add(player.getUniqueId());
									teamchestsopened.replace(event.getClickedBlock().getLocation(), testteama);
									for(Player ppa:Bukkit.getOnlinePlayers()) {
										Block b = event.getClickedBlock();
										
								    	ppa.playSound(b.getLocation(), Sound.CHEST_OPEN, 1, 1);
									}
								}
							}else {
								ArrayList<UUID> newteam = new ArrayList<UUID>();
								newteam.add(player.getUniqueId());
								teamchestsopened.put(event.getClickedBlock().getLocation(), newteam);
								for(Player ppa:Bukkit.getOnlinePlayers()) {
									Block b = event.getClickedBlock();
									
							    	ppa.playSound(b.getLocation(), Sound.CHEST_OPEN, 1, 1);
								}
								new BukkitRunnable() {
									
									@Override
									public void run() {
										if(teamchestsopened.containsKey(event.getClickedBlock().getLocation())) {
											for(Player ppa:Bukkit.getOnlinePlayers()) {
												Block b = event.getClickedBlock();
										    	BlockPosition pos = new BlockPosition(b.getLocation().getBlockX(), b.getLocation().getBlockY(), b.getLocation().getBlockZ());
												PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(pos, Blocks.CHEST, 1, 1);
										    	((CraftPlayer) ppa).getHandle().playerConnection.sendPacket(packet);
											}
										}else {
											cancel();
											for(Player ppa:Bukkit.getOnlinePlayers()) {
												Block b = event.getClickedBlock();
										    	BlockPosition pos = new BlockPosition(b.getLocation().getBlockX(), b.getLocation().getBlockY(), b.getLocation().getBlockZ());
												PacketPlayOutBlockAction packet = new PacketPlayOutBlockAction(pos, Blocks.CHEST, 1, 0);
										    	((CraftPlayer) ppa).getHandle().playerConnection.sendPacket(packet);
										    	ppa.playSound(b.getLocation(), Sound.CHEST_CLOSE, 1, 1);
											}
											
											
										}
										
										
									}
								}.runTaskTimer(plugin, 1,1);
							}
							
//							player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 1);
							if(teams.red.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.redinv);
							}
							if(teams.blue.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.blueinv);
							}
							if(teams.green.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.greeninv);
							}
							if(teams.yellow.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.yellowinv);
							}
							if(teams.purple.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.purpleinv);
							}
							if(teams.magenta.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.magentainv);
							}
							if(teams.white.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.whiteinv);
							}
							if(teams.black.getDisplayName().equalsIgnoreCase(teamA.getDisplayName())) {
								player.openInventory(teams.blackinv);
							}
							return;
						}
					}
				}
			}
			if(event.getAction()==Action.RIGHT_CLICK_BLOCK&&event.getClickedBlock().getType()==Material.BED) {
				if(!player.isSneaking()) {
					event.setCancelled(true);
				}
			}else if(event.getAction()==Action.RIGHT_CLICK_BLOCK&&event.getClickedBlock().getType()==Material.BED_BLOCK) {
				if(!player.isSneaking()) {
					event.setCancelled(true);
				}
			}
			if(event.getItem()==null) {
				
			}else if(event.getItem().getType()==Material.AIR) {
				
			}else 
			if(event.getItem().getType()==Material.TNT) {
				event.setCancelled(true);
				World world = player.getWorld();
				Location loc = new Location(player.getWorld(),player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ());
				loc.setDirection(player.getLocation().getDirection());
				TNTPrimed tnt = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			      Vector playerDirection = loc.getDirection();
			      Vector smallerVector = playerDirection.multiply(1.0D);
			      tnt.setVelocity(smallerVector);
			      tnt.setCustomName("§cGRANATE!!!!");
			      tnt.setCustomNameVisible(true);
			      tnt.setYield(tntpower);
			      tnt.setFuseTicks(tnttime);
			      manipulator.putlaunchedTnt(tnt);
			      int amount = player.getItemInHand().getAmount();
			      if(amount==1) {
			    	  player.setItemInHand(new ItemStack(Material.AIR));
			      }
			      player.getItemInHand().setAmount(amount-1);
			}else

			if(event.getItem().getType()==Material.SULPHUR) {
				if(homepowder.contains(player)) {
					player.sendMessage(bedwars.MessageNoPerm+"Warte Kurz");
					return;
					
				}
				homepowder.add(player);

			      
				new BukkitRunnable() {
					int value = 9;
					@Override
					public void run() {
						
						if(homepowder.contains(player)==false) {
							cancel();
							player.sendMessage(bedwars.MessageNoPerm+"Bleib stehen zum Telepotieren");
							
						}
						if(value==0) {
							cancel();
							homepowder.remove(player);

							if(teams.red.hasEntry(player.getName())) {
								player.teleport(mapengine.redspawn);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								player.teleport(mapengine.bluespawn);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								player.teleport(mapengine.yellowspawn);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								player.teleport(mapengine.greenspawn);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								player.teleport(mapengine.purplespawn);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								player.teleport(mapengine.magentaspawn);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								player.teleport(mapengine.whitespawn);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								player.teleport(mapengine.blackspawn);
								
							}
							for(ItemStack item:player.getInventory().getContents()) {
								if(item==null) {
									
								}else
								if(item.getType()==Material.SULPHUR) {
									int amount = item.getAmount();
									if(amount==1) {
										player.getInventory().removeItem(item);
									}
									item.setAmount(amount-1);
								}
							}
						}
						if(value==1) {
							ringer(player.getLocation(), 1, 2, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 2, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 2, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 2, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 2, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 2, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 2, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 2, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 2, null);
								
							}
						}
						if(value==2) {
							ringer(player.getLocation(), 1, 1.75, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 1.75, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 1.75, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 1.75, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 1.75, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 1.75, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 1.75, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 1.75, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 1.75, null);
								
							}
						}
						if(value==3) {
							ringer(player.getLocation(), 1, 1.5, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 1.5, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 1.5, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 1.5, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 1.5, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 1.5, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 1.5, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 1.5, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 1.5, null);
								
							}
						}
						if(value==4) {
							ringer(player.getLocation(), 1, 1.25, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 1.25, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 1.25, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 1.25, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 1.25, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 1.25, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 1.25, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 1.25, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 1.25, null);
								
							}
						}
						if(value==5) {
							ringer(player.getLocation(), 1, 1, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 1, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 1, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 1, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 1, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 1, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 1, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 1, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 1, null);
								
							}
						}
						if(value==6) {
							ringer(player.getLocation(), 1, 0.75, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 0.75, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 0.75, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 0.75, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 0.75, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 0.75, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 0.75, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 0.75, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 0.75, null);
								
							}
						}
						if(value==7) {
							ringer(player.getLocation(), 1, 0.5, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 0.5, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 0.5, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 0.5, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 0.5, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 0.5, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 0.5, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 0.5, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 0.5, null);
								
							}
						}
						if(value==8) {
							ringer(player.getLocation(), 1, 0.25, null);
							if(teams.red.hasEntry(player.getName())) {
								ringer(mapengine.redspawn, 1, 0.25, null);
								
							}else
							if(teams.blue.hasEntry(player.getName())) {
								ringer(mapengine.bluespawn, 1, 0.25, null);
								
							}else
							if(teams.yellow.hasEntry(player.getName())) {
								ringer(mapengine.yellowspawn, 1, 0.25, null);
								
							}else
							if(teams.green.hasEntry(player.getName())) {
								ringer(mapengine.greenspawn, 1, 0.25, null);
								
							}else
							if(teams.purple.hasEntry(player.getName())) {
								ringer(mapengine.purplespawn, 1, 0.25, null);
								
							}else
							if(teams.magenta.hasEntry(player.getName())) {
								ringer(mapengine.magentaspawn, 1, 0.25, null);
								
							}else
							if(teams.white.hasEntry(player.getName())) {
								ringer(mapengine.whitespawn, 1, 0.25, null);
								
							}else
							if(teams.black.hasEntry(player.getName())) {
								ringer(mapengine.blackspawn, 1, 0.25, null);
								
							}
						}
						value--;
						
						
					}
				}.runTaskTimer(plugin, 2, 10);	

				event.setCancelled(true);
			}
		}
	  }
	@EventHandler
	public void noteamdamagebow(EntityDamageByEntityEvent e) {
		try {
		Entity killer = (Entity) ((Arrow) e.getDamager()).getShooter();
		Entity innocent = e.getEntity();
		if(killer instanceof Player && innocent instanceof Player) {
			for(Team tm:teams.activeteams) {
				Player killerp = (Player) killer;
				Player innocentp = (Player) innocent;
				
				if(tm.hasEntry(killerp.getName())&&tm.hasEntry(innocentp.getName())) {
					e.setCancelled(true);
					killerp.playSound(killerp.getLocation(), Sound.IRONGOLEM_HIT, 1, 1);
					return;
				}
			}
		}}catch(ClassCastException en) {
			
		}
	}
	@EventHandler
	public void noteamdamage(EntityDamageByEntityEvent e) {
		Entity killer = e.getDamager();
		Entity innocent = e.getEntity();
		if(killer instanceof Player && innocent instanceof Player) {
			for(Team tm:teams.activeteams) {
				Player killerp = (Player) killer;
				Player innocentp = (Player) innocent;
				
				if(tm.hasEntry(killerp.getName())&&tm.hasEntry(innocentp.getName())) {
					e.setCancelled(true);
					killerp.playSound(killerp.getLocation(), Sound.IRONGOLEM_HIT, 1, 1);
					return;
				}
			}
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onTNT(EntityExplodeEvent e) {
		if(e.getEntityType()==EntityType.PRIMED_TNT) {
			TNTPrimed tnt = (TNTPrimed) e.getEntity();
			manipulator.removelaunchedTnt(tnt);
			if(alternatetnt==true) {
				return;
			}
			e.getLocation().getWorld().playEffect(e.getLocation(), Effect.EXPLOSION_HUGE, 1000);
			e.getLocation().getWorld().playSound(e.getLocation(), Sound.ZOMBIE_WOODBREAK, 1,1);
			e.getLocation().getWorld().playSound(e.getLocation(), Sound.ZOMBIE_WOODBREAK, 1,1);
			e.getLocation().getWorld().playSound(e.getLocation(), Sound.ZOMBIE_WOODBREAK, 1,1);
			e.getLocation().getWorld().playSound(e.getLocation(), Sound.ZOMBIE_WOODBREAK, 1,1);
			e.getLocation().getWorld().playSound(e.getLocation(), Sound.ZOMBIE_WOODBREAK, 1,1);
			 for(Block b : e.blockList()) {
				  Material checkmaterial = b.getType();
				  if(bedwars.tntblocks.contains(checkmaterial)) {
					  float x = (float) (0.0D + Math.random() - Math.random());
		                float y = (float) Math.random();
		                float z = (float) (0.0D + Math.random() - Math.random());
		 
		                FallingBlock falling = b.getWorld().spawnFallingBlock(b.getLocation(), b.getType(), b.getData());
		                falling.setVelocity(new Vector(x, y, z));
		                falling.setDropItem(true);
		                falling.setCustomName(b.getLocation().getWorld().getName()+","+b.getLocation().getBlockX()+","+b.getLocation().getBlockY()+","+b.getLocation().getBlockZ()+","+b.getType().toString()+","+b.getData());
		                falling.setCustomNameVisible(false);
		 
		                b.setType(Material.AIR);
				  }else {
					  e.setCancelled(true);
				  }
				 
			 }
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onHit(EntityChangeBlockEvent e) {
		try {
			if ((e.getEntityType() == EntityType.FALLING_BLOCK)) {
				int id = e.getBlock().getTypeId();
					Location loc = e.getBlock().getLocation();
					new BukkitRunnable() {
						
						@Override
						public void run() {
							for(Player pp:Bukkit.getOnlinePlayers()) {
								pp.playEffect(loc, Effect.STEP_SOUND, id);
							}
							loc.getBlock().setType(Material.AIR);
							
						}
					}.runTaskLater(plugin, 5);
				}
			}catch(NullPointerException en) {
        	
        }
	}
	@EventHandler
	  public void onInteractET(PlayerInteractAtEntityEvent event)
	  {
		Player player = event.getPlayer();
			if(event.getPlayer().getItemInHand().getType()==Material.TNT) {
				World world = player.getWorld();
				Location loc = new Location(player.getWorld(),player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ());
				loc.setDirection(player.getLocation().getDirection());
				TNTPrimed tnt = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			      Vector playerDirection = loc.getDirection();
			      Vector smallerVector = playerDirection.multiply(3D);
			      tnt.setVelocity(smallerVector);
			      tnt.setCustomName("§cGRANATE!!!!");
			      tnt.setCustomNameVisible(true);
			      tnt.setYield(tntpower);
			      tnt.setFuseTicks(tnttime);
			}
		
	  }
	@EventHandler
	public void onBurn(BlockBurnEvent e) {
		e.setCancelled(true);
	}
	@EventHandler
	public void onSleep(PlayerBedEnterEvent e) {
		e.setCancelled(true);
	}
	
	
	
	
	

	public static void ringer(Location L, double r,double yadd, Material material) {
    	double PI = (double) 3.141592653589793238462643383279502884197169399375105820974944592307816406286208995;
    	double x = (double) L.getX();
    	double y = (double) L.getZ();
    	for (int faktor = 0; faktor <= 150; faktor++) {
    	float faktordezimal = (float) faktor / 150;
    	float xp = (float) r * (float) Math.cos(faktordezimal * 2 * PI);
    	float yp = (float) r * (float) Math.sin(faktordezimal * 2 * PI);
    	Location tile = new Location(L.getWorld(), x + xp, L.getY()+yadd, y + yp);
    	
    		try {
    			if (tile.getBlock().getType() == Material.AIR) {
    				double x1 = tile.getX();
    				double y1= tile.getY();
    				double z1 = tile.getZ();
    				PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(EnumParticle.FIREWORKS_SPARK,true, (float) x1, (float) y1, (float) z1, 0, 0, 0, 0, 1);
		            for(Player ppa : Bukkit.getOnlinePlayers()) {
		                ((CraftPlayer)ppa).getHandle().playerConnection.sendPacket(packet);
						
		            }
        	    	
        	    	}	
    		}catch (NullPointerException en) {
				double x1 = tile.getX();
				double y1= tile.getY();
				double z1 = tile.getZ();
    			if (tile.getBlock().getType() == Material.AIR) {PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(EnumParticle.FIREWORKS_SPARK,true, (float) x1, (float) y1, (float) z1, 0, 0, 0, 0, 1);
    			for(Player ppa : Bukkit.getOnlinePlayers()) {
	                ((CraftPlayer)ppa).getHandle().playerConnection.sendPacket(packet);
					
	            }}
    		}
    		
    	
    	}
    	
    }

}
