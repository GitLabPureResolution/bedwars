package me.eliteSchwein.bedwars;


import java.io.IOException;
import java.util.ArrayList;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class event_makelobby implements Listener{
    HeadDatabaseAPI api = new HeadDatabaseAPI();
	public static String playerwhouse = null;
	public static boolean LobbySpawn =false;
    public event_makelobby(bedwars plugin) {
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onArmorstand(PlayerInteractAtEntityEvent e) {
		Player p = e.getPlayer();
		Entity et = e.getRightClicked();
		if(et instanceof ArmorStand) {
			try {
			if(p.getCustomName().contains("teamselected")&&e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("Team")&&e.getPlayer().getItemInHand().getType()!=Material.AIR) {
			e.setCancelled(true);
			String armorstandname = p.getItemInHand().getItemMeta().getDisplayName();
			ArmorStand am = (ArmorStand) et;
			am.setCustomName(armorstandname);
			am.setCustomNameVisible(true);
			
			double X = am.getLocation().getX();
			double Y = am.getLocation().getY();
			double Z = am.getLocation().getZ();
			
			String cfgx = bedwars.cfglobbydata.getString("X.teamstand."+(int)X);
			String cfgy = bedwars.cfglobbydata.getString("Y.teamstand."+(int)Y);
			String cfgz = bedwars.cfglobbydata.getString("Z.teamstand."+(int)Z);
			if(cfgx!=null&&cfgy!=null&&cfgz!=null&&p.isSneaking()==false) {
				
				p.sendMessage(bedwars.MessageNoPerm+"Dieser Armorstand ist belegt");
				p.sendMessage(bedwars.MessageNoPerm+"sneake zum entfernen");
				return;
			}

			if(cfgx!=null&&cfgy!=null&&cfgz!=null&&p.isSneaking()==true) {
				bedwars.cfglobbydata.set("X.teamstand."+(int)X, null);
				bedwars.cfglobbydata.set("Y.teamstand."+(int)Y, null);
				bedwars.cfglobbydata.set("Z.teamstand."+(int)Z, null);
				Location chunkholer = new Location(p.getWorld(),X,Y,Z);
				save.chunkloader.remove(chunkholer);
				ItemStack color = api.getItemHead("6118");
				am.setHelmet(color);
				p.sendMessage(bedwars.MessageNoPerm+"Dieser Armorstand ist nun weg");
				return;
			}

			Location chunkholer = new Location(p.getWorld(),X,Y,Z);
			save.chunkloader.add(chunkholer);
			bedwars.cfglobbydata.set("X.teamstand."+(int)X, armorstandname);
			bedwars.cfglobbydata.set("Y.teamstand."+(int)Y, armorstandname);
			bedwars.cfglobbydata.set("Z.teamstand."+(int)Z, armorstandname);
			

			
			float HeadYaw = am.getLocation().getYaw();
			float HeadPitch = am.getLocation().getPitch();
			
			double HeadX = am.getHeadPose().getX();
			double HeadY = am.getHeadPose().getY();
			double HeadZ = am.getHeadPose().getZ();

			double LeftArmX = am.getLeftArmPose().getX();
			double LeftArmY = am.getLeftArmPose().getY();
			double LeftArmZ = am.getLeftArmPose().getZ();
			
			double RightArmX = am.getRightArmPose().getX();
			double RightArmY = am.getRightArmPose().getY();
			double RightArmZ = am.getRightArmPose().getZ();

			double BodyX = am.getBodyPose().getX();
			double BodyY = am.getBodyPose().getY();
			double BodyZ = am.getBodyPose().getZ();

			double LeftLegX = am.getLeftLegPose().getX();
			double LeftLegY = am.getLeftLegPose().getY();
			double LeftLegZ = am.getLeftLegPose().getZ();
			
			double RightLegX = am.getRightLegPose().getX();
			double RightLegY = am.getRightLegPose().getY();
			double RightLegZ = am.getRightLegPose().getZ();
			
			World world = am.getWorld();
			
			String material = am.getItemInHand().getType().toString();
			int subid = am.getItemInHand().getData().getData();
			
			bedwars.cfglobbydata.set(armorstandname+".chestplate.material", "LEATHER_CHESTPLATE");
			bedwars.cfglobbydata.set(armorstandname+".chestplate.subid", 0);
			bedwars.cfglobbydata.set(armorstandname+".chestplate.red", 127);
			bedwars.cfglobbydata.set(armorstandname+".chestplate.green", 127);
			bedwars.cfglobbydata.set(armorstandname+".chestplate.blue", 127);
			bedwars.cfglobbydata.set(armorstandname+".leggings.material", "LEATHER_LEGGINGS");
			bedwars.cfglobbydata.set(armorstandname+".leggings.subid", 0);
			bedwars.cfglobbydata.set(armorstandname+".leggings.red", 127);
			bedwars.cfglobbydata.set(armorstandname+".leggings.green", 127);
			bedwars.cfglobbydata.set(armorstandname+".leggings.blue", 127);
			bedwars.cfglobbydata.set(armorstandname+".boots.material", "LEATHER_BOOTS");
			bedwars.cfglobbydata.set(armorstandname+".boots.subid", 0);
			bedwars.cfglobbydata.set(armorstandname+".boots.red", 127);
			bedwars.cfglobbydata.set(armorstandname+".boots.green", 127);
			bedwars.cfglobbydata.set(armorstandname+".boots.blue", 127);
			bedwars.cfglobbydata.set(armorstandname+".HeadYaw", HeadYaw);
        	bedwars.cfglobbydata.set(armorstandname+".HeadPitch", HeadPitch);
        	bedwars.cfglobbydata.set(armorstandname+".HeadX", HeadX);
        	bedwars.cfglobbydata.set(armorstandname+".HeadY", HeadY);
        	bedwars.cfglobbydata.set(armorstandname+".HeadZ", HeadZ);
        	bedwars.cfglobbydata.set(armorstandname+".LeftArmX", LeftArmX);
        	bedwars.cfglobbydata.set(armorstandname+".LeftArmY", LeftArmY);
        	bedwars.cfglobbydata.set(armorstandname+".LeftArmZ", LeftArmZ);
        	bedwars.cfglobbydata.set(armorstandname+".RightArmX", RightArmX);
        	bedwars.cfglobbydata.set(armorstandname+".RightArmY", RightArmY);
        	bedwars.cfglobbydata.set(armorstandname+".RightArmZ", RightArmZ);
        	bedwars.cfglobbydata.set(armorstandname+".BodyX", BodyX);
        	bedwars.cfglobbydata.set(armorstandname+".BodyY", BodyY);
        	bedwars.cfglobbydata.set(armorstandname+".BodyZ", BodyZ);
        	bedwars.cfglobbydata.set(armorstandname+".LeftLegX", LeftLegX);
        	bedwars.cfglobbydata.set(armorstandname+".LeftLegY", LeftLegY);
        	bedwars.cfglobbydata.set(armorstandname+".LeftLegZ", LeftLegZ);
        	bedwars.cfglobbydata.set(armorstandname+".RightLegX", RightLegX);
        	bedwars.cfglobbydata.set(armorstandname+".RightLegY", RightLegY);
        	bedwars.cfglobbydata.set(armorstandname+".RightLegZ", RightLegZ);
        	bedwars.cfglobbydata.set(armorstandname+".loc.world", world.getName());
        	bedwars.cfglobbydata.set(armorstandname+".loc.X", X);
        	bedwars.cfglobbydata.set(armorstandname+".loc.Y", Y);
        	bedwars.cfglobbydata.set(armorstandname+".loc.Z", Z);
        	bedwars.cfglobbydata.set(armorstandname+".handitem.material", material);
        	bedwars.cfglobbydata.set(armorstandname+".handitem.subid", subid);
        	try {
			 
			
			
			
			
			
			
			if(armorstandname.contains(teams.red.getDisplayName())) {
				ItemStack color = api.getItemHead("9405");
				am.setHelmet(color);
							
			}else if(armorstandname.contains(teams.blue.getDisplayName())) {
				ItemStack color = api.getItemHead("8973");
				am.setHelmet(color);
						
			}else  if(armorstandname.contains(teams.yellow.getDisplayName())) {
				ItemStack color = api.getItemHead("9189");
				am.setHelmet(color);
					
			}else  if(armorstandname.contains(teams.green.getDisplayName())) {
				ItemStack color = api.getItemHead("10269");
				am.setHelmet(color);
					
			}else if(armorstandname.contains(teams.magenta.getDisplayName())) {
				ItemStack color = api.getItemHead("9837");
				am.setHelmet(color);
					
			}else if(armorstandname.contains(teams.purple.getDisplayName())) {
				ItemStack color = api.getItemHead("9513");
				am.setHelmet(color);
					
			}else if(armorstandname.contains(teams.white.getDisplayName())) {
				ItemStack color = api.getItemHead("9297");
				am.setHelmet(color);
					
			}else
			if(armorstandname.contains(teams.black.getDisplayName())) {
				ItemStack color = api.getItemHead("8840");
				am.setHelmet(color);
			}
        	}catch(NullPointerException en) {
        		
        	}
        	try {
				saveConfig();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}}catch(NullPointerException en){
			
		}}
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		try {
		if(playerwhouse.equalsIgnoreCase(p.getName())) {
			if(p.getCustomName().equalsIgnoreCase("general")&&a==Action.RIGHT_CLICK_AIR||p.getCustomName().equalsIgnoreCase("general")&&a==Action.RIGHT_CLICK_BLOCK) {
				if(a==Action.RIGHT_CLICK_BLOCK&&teams.saverlobby.hasEntry(p.getName())||a==Action.RIGHT_CLICK_AIR&&teams.saverlobby.hasEntry(p.getName())) {

					int slot = e.getPlayer().getInventory().getHeldItemSlot();
					e.setCancelled(true);
					e.getMaterial();
					
						if(slot==8) {
							p.getInventory().clear();
							teams.saverlobby.removeEntry(p.getName());
							teams.build.addEntry(p.getName());
							
						}
					else
						if(slot==7) {
							
								
						}
					else
						if(slot==4) {
									
						}
					else
						if(slot==3) {			
						}
					else
						if(slot==2) {
							
						}
					else
						if(slot==1) {
							
						}
					else
						if(slot==0) {
							p.setCustomName("selectteamconfigurator");
							ArrayList<String> teams = new ArrayList<String>();
							teams.add("red");
							teams.add("blue");
							teams.add("green");
							teams.add("yellow");
							teams.add("purple");
							teams.add("magenta");
							teams.add("white");
							teams.add("black");
							ArrayList<String> teamskulls = new ArrayList<String>();
							teamskulls.add("9356");
							teamskulls.add("8924");
							teamskulls.add("10220");
							teamskulls.add("9140");
							teamskulls.add("9464");
							teamskulls.add("9788");
							teamskulls.add("9248");
							teamskulls.add("8777");
							int teamamount = bedwars.cfgmain.getInt("teamamount");

							for(int is = 0;is<9;is++) {
								ItemStack spacer = new ItemStack(Material.AIR,1);
								p.getInventory().setItem(is, spacer);
								
							}
							for(int i = 0;i<(teamamount);i++) {
								String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
								String functioncustomname = teams.get(i)+"team";
								ArrayList<String> lore = new ArrayList<String>();
								lore.add(functioncustomname);
								ItemStack team = api.getItemHead(teamskulls.get(i));
								ItemMeta teammeta = team.getItemMeta();
								teammeta.setDisplayName(teamname+" Team Konfiguration");
								teammeta.setLore(lore);
								team.setItemMeta(teammeta);
								p.getInventory().setItem(i, team);
								
							}
							ItemStack generell = api.getItemHead("516");
							ItemMeta generellmeta = generell.getItemMeta();
							generellmeta.setDisplayName("§rGenerelle Einstellungen");
							generell.setItemMeta(generellmeta);
							p.getInventory().setItem(8, generell);
								
						}

					
					
				}
			}else if(p.getCustomName().equalsIgnoreCase("useless")) {
				e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m==Material.BED) {
					Block b = e.getClickedBlock();
		            int radius = 2;
		            for (int x = b.getX(); x < b.getX()+ radius; x++)
		            {
		                for (int y = b.getY(); y < b.getY()+ radius; y++)
		                {
		                    for (int z = b.getZ(); z < b.getZ()+ radius; z++)
		                    {
		                       Block locb = b.getWorld().getBlockAt(x, y, z);
		                       Material locm = locb.getType();
		                       String locmn = locm.name();
		                       if(locmn.contains("BED")) {
		                           p.sendMessage(locm.name());
		                           p.sendMessage(Integer.toString(x));
		                           p.sendMessage(Integer.toString(y));
		                           p.sendMessage(Integer.toString(z));
		                    	   
		                       }
		                    }
		                }
		            }
				}
			}else if(p.getCustomName().equalsIgnoreCase("selectteamconfigurator")&&a==Action.RIGHT_CLICK_AIR||p.getCustomName().equalsIgnoreCase("selectteamconfigurator")&&a==Action.RIGHT_CLICK_BLOCK) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot!=8&&m!=Material.AIR) {
						p.setCustomName("teamselected");
						ArrayList<String> teamfunctions = new ArrayList<String>();
						String teamname = e.getItem().getItemMeta().getDisplayName().replace("Team", "").replaceAll(" ", "").replace("Konfiguration", "");
						teamfunctions.add(teamname+" Team AM 1");
						teamfunctions.add(teamname+" Team AM 2");
						teamfunctions.add(teamname+" Team AM 3");
						teamfunctions.add(teamname+" Team AM 4");
						teamfunctions.add(teamname+" Team AM 5");
						teamfunctions.add(teamname+" Team AM 6");
						ArrayList<String> teamskulls = new ArrayList<String>();
						if(bedwars.cfglobbydata.getString(teamfunctions.get(0))==null) {
							teamskulls.add("10724");
							
						}else {
							teamskulls.add("9378");
							
						}
						if(bedwars.cfglobbydata.getString(teamfunctions.get(1))==null) {
							teamskulls.add("10723");
							
						}else {
							teamskulls.add("9377");
							
						}
						if(bedwars.cfglobbydata.getString(teamfunctions.get(2))==null) {
							teamskulls.add("10722");
							
						}else {
							teamskulls.add("9376");
							
						}
						if(bedwars.cfglobbydata.getString(teamfunctions.get(3))==null) {
							teamskulls.add("10721");
							
						}else {
							teamskulls.add("9375");
							
						}
						if(bedwars.cfglobbydata.getString(teamfunctions.get(4))==null) {
							teamskulls.add("10720");
							
						}else {
							teamskulls.add("9374");
							
						}
						if(bedwars.cfglobbydata.getString(teamfunctions.get(5))==null) {
							teamskulls.add("10719");
							
						}else {
							teamskulls.add("9373");
							
						}
						for(int is = 0;is<9;is++) {
							ItemStack spacer = new ItemStack(Material.AIR,1);
							p.getInventory().setItem(is, spacer);
							
							
						}
						for(int i = 0;i<bedwars.cfgmain.getInt("teamplayersize");i++) {
//							String teamname = bedwars.cfgmain.getString(teamfunctions.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
//							String functioncustomname = teamfunctions.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							ItemStack team = api.getItemHead(teamskulls.get(i));
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName(teamfunctions.get(i));
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							p.getInventory().setItem(i, team);
							
						}
						ItemStack generell = api.getItemHead("516");
						ItemMeta generellmeta = generell.getItemMeta();
						generellmeta.setDisplayName("§rTeam Auswahl");
						generell.setItemMeta(generellmeta);
						p.getInventory().setItem(8, generell);
						
						
					}
					if(slot==8) {
						p.setCustomName("general");
						event_makemap.playerwhouse=p.getName();
						teams.saverlobby.addEntry(p.getName());
						p.getInventory().clear();
						ItemStack teamselector =  api.getItemHead("10358");
						ItemMeta teamselectormeta = teamselector.getItemMeta();
						teamselectormeta.setDisplayName("§rTeam auswählen");
						teamselector.setItemMeta(teamselectormeta);
						teamselector.setAmount(1);
						p.getInventory().setItem(0, teamselector);
						
						ItemStack save = api.getItemHead("584");
						ItemMeta savemeta = save.getItemMeta();
						savemeta.setDisplayName("§rFertig?");
						save.setItemMeta(savemeta);
						p.getInventory().setItem(8, save);
						
						
						
						
						
					}
				}
				
			}else if(p.getCustomName().contains("team")&&a==Action.RIGHT_CLICK_AIR) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot==8) {
						p.setCustomName("selectteamconfigurator");
						ArrayList<String> teams = new ArrayList<String>();
						teams.add("red");
						teams.add("blue");
						teams.add("green");
						teams.add("yellow");
						teams.add("purple");
						teams.add("magenta");
						teams.add("white");
						teams.add("black");
						ArrayList<String> teamskulls = new ArrayList<String>();
						teamskulls.add("9356");
						teamskulls.add("8924");
						teamskulls.add("10220");
						teamskulls.add("9140");
						teamskulls.add("9464");
						teamskulls.add("9788");
						teamskulls.add("9248");
						teamskulls.add("8777");
						int teamamount = bedwars.cfgmain.getInt("teamamount");

						for(int is = 0;is<9;is++) {
							ItemStack spacer = new ItemStack(Material.AIR,1);
							p.getInventory().setItem(is, spacer);
							
						}
						for(int i = 0;i<(teamamount);i++) {
							String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
							String functioncustomname = teams.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							lore.add(functioncustomname);
							ItemStack team = api.getItemHead(teamskulls.get(i));
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName(teamname+" Team Konfiguration");
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							p.getInventory().setItem(i, team);
							
						}
						ItemStack generell = api.getItemHead("516");
						ItemMeta generellmeta = generell.getItemMeta();
						generellmeta.setDisplayName("§rGenerelle Einstellungen");
						generell.setItemMeta(generellmeta);
						p.getInventory().setItem(8, generell);
					}
				}
			}else if(p.getCustomName().contains("team")&&a==Action.RIGHT_CLICK_BLOCK) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot==8) {p.setCustomName("selectteamconfigurator");
					ArrayList<String> teams = new ArrayList<String>();
					teams.add("red");
					teams.add("blue");
					teams.add("green");
					teams.add("yellow");
					teams.add("purple");
					teams.add("magenta");
					teams.add("white");
					teams.add("black");
					ArrayList<String> teamskulls = new ArrayList<String>();
					teamskulls.add("9356");
					teamskulls.add("8924");
					teamskulls.add("10220");
					teamskulls.add("9140");
					teamskulls.add("9464");
					teamskulls.add("9788");
					teamskulls.add("9248");
					teamskulls.add("8777");
					int teamamount = bedwars.cfgmain.getInt("teamamount");

					for(int is = 0;is<9;is++) {
						ItemStack spacer = new ItemStack(Material.AIR,1);
						p.getInventory().setItem(is, spacer);
						
					}
					for(int i = 0;i<(teamamount);i++) {
						String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
						String functioncustomname = teams.get(i)+"team";
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(functioncustomname);
						ItemStack team = api.getItemHead(teamskulls.get(i));
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname+" Team Konfiguration");
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						p.getInventory().setItem(i, team);
						
					}
					ItemStack generell = api.getItemHead("516");
					ItemMeta generellmeta = generell.getItemMeta();
					generellmeta.setDisplayName("§rGenerelle Einstellungen");
					generell.setItemMeta(generellmeta);
					p.getInventory().setItem(8, generell);
					}
					
				}
				
			}
			
			
		}}catch(NullPointerException en) {
			
		}
		
	}
	public static void saveConfig() throws IOException {
		bedwars.cfglobbydata.save(bedwars.filelobbydata);
	}
}
