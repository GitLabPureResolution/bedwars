package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import me.arcaniax.hdb.api.HeadDatabaseAPI;


public class event_makemap implements Listener{
    HeadDatabaseAPI api = new HeadDatabaseAPI();
    public static HashMap<Player,String> teamlocator = new HashMap<Player,String>();
	public static ArrayList<Location> tier1location = new ArrayList<Location>();
	public static ArrayList<Location> tier2location = new ArrayList<Location>();
	public static ArrayList<Location> tier3location = new ArrayList<Location>();
	public static HashMap<Location,String> bedlocation = new HashMap<Location,String>();
	public static HashMap<Location,String> shoplocation = new HashMap<Location,String>();
	public static HashMap<Location,String> teleportlocation = new HashMap<Location,String>();
	boolean saveinuse = false;
	public static String playerwhouse = null;
	public static boolean pos1exists = false;
	public static boolean pos2exists = false;
	private bedwars plugin;
	public event_makemap(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	@EventHandler
	public void onArmorClick(PlayerInteractAtEntityEvent e) {
		
		Entity et = e.getRightClicked();
		if(et instanceof ArmorStand) {
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onArmorKill(EntityDamageByEntityEvent e) {
		
		Entity et = e.getEntity();
		if(et instanceof ArmorStand) {
			e.setCancelled(true);
		}
	}
	@SuppressWarnings("unused")
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		try {
		if(playerwhouse.equalsIgnoreCase(p.getName())) {
			if(teams.saver.hasEntry(p.getName())){
				for(World w : Bukkit.getWorlds()) {
					 for(Entity et : w.getEntities()) {
								if(et instanceof ArmorStand) {
								String nametag = et.getCustomName();
								if(nametag.contains("teleport")||nametag.contains("shop")||nametag.contains("bed")||nametag.contains("Pos")||nametag.contains("Spawner")) {
									 Vector direction = getVector(et).subtract(getVector(p)).normalize();

						                
									    double x = direction.getX();
									    double y = direction.getY();
									    double z = direction.getZ();
						                double pitch = Math.toRadians(p.getLocation().getPitch()*(-1));
									 
									    // Now change the angle
									    Location changed = et.getLocation().clone();
									    changed.setYaw(180 - toDegree(Math.atan2(x, z)));
//									    changed.setPitch(90 - toDegree(Math.acos(y)));
									    changed.setPitch(0);

						                EulerAngle a = new EulerAngle(pitch,0,0);
						                
						                ((ArmorStand) et).setHeadPose(a);
						                
									    et.teleport(changed);
								}
							
								}
						}
				 }
			}
		}}catch (NullPointerException en) {
			
		}
	}
	@SuppressWarnings("unused")
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		try {
		if(playerwhouse.equalsIgnoreCase(p.getName())) {
			if(p.getCustomName().equalsIgnoreCase("general")&&a==Action.RIGHT_CLICK_AIR||p.getCustomName().equalsIgnoreCase("general")&&a==Action.RIGHT_CLICK_BLOCK) {
				if(a==Action.RIGHT_CLICK_BLOCK&&teams.saver.hasEntry(p.getName())||a==Action.RIGHT_CLICK_AIR&&teams.saver.hasEntry(p.getName())) {

					int slot = e.getPlayer().getInventory().getHeldItemSlot();
					e.setCancelled(true);
					Material m = e.getMaterial();
					
						if(slot==6) {
							save.start(p);
							p.setCustomName("saving");
							
						}
					else
						if(slot==5) {
								
						}
					else
						if(slot==4) {
									
						}
					else
						if(slot==3&&a==Action.RIGHT_CLICK_BLOCK) {
							if(tier3location.contains(e.getClickedBlock().getLocation())) {

								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.remove(chunkholer);
								p.sendMessage(bedwars.MessageNoPerm+"Entfernt");
								tier3location.remove(e.getClickedBlock().getLocation());
								for(Entity et : e.getClickedBlock().getLocation().getWorld().getNearbyEntities(e.getClickedBlock().getLocation().add(0.50, 0, 0.50),0.5,0.5,0.5)) {
									if(et instanceof ArmorStand) {
										et.remove();
									}
								}
							}else {

								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.add(chunkholer);
								ItemStack isplace = api.getItemHead("7928");
								Location amloc = e.getClickedBlock().getLocation().add(0.50, 0, 0.50);
								amloc.setPitch(p.getLocation().getPitch()-180);
								amloc.setYaw(p.getLocation().getYaw()-180);
								ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
								amplace.setHelmet(isplace);
								amplace.setGravity(false);
								amplace.setCustomName(bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
								amplace.setCustomNameVisible(false);
								amplace.setCanPickupItems(false);
								amplace.setBasePlate(false);
								tier3location.add(e.getClickedBlock().getLocation());
								

								p.sendMessage(bedwars.MessagePerm+"Addiert");
								p.sendMessage(tier3location.toString());
								
							}				
						}
					else
						if(slot==2&&a==Action.RIGHT_CLICK_BLOCK) {
							if(tier2location.contains(e.getClickedBlock().getLocation())) {


								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.remove(chunkholer);
								p.sendMessage(bedwars.MessageNoPerm+"Entfernt");
								tier2location.remove(e.getClickedBlock().getLocation());
								for(Entity et : e.getClickedBlock().getLocation().getWorld().getNearbyEntities(e.getClickedBlock().getLocation().add(0.50, 0, 0.50),0.5,0.5,0.5)) {
									if(et instanceof ArmorStand) {
										et.remove();
									}
								}
							}else {

								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.add(chunkholer);
								ItemStack isplace = api.getItemHead("514");
								Location amloc = e.getClickedBlock().getLocation().add(0.50, 0, 0.50);
								amloc.setPitch(p.getLocation().getPitch()-180);
								amloc.setYaw(p.getLocation().getYaw()-180);
								ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
								amplace.setHelmet(isplace);
								amplace.setGravity(false);
								amplace.setCustomName(bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
								amplace.setCustomNameVisible(false);
								amplace.setCanPickupItems(false);
								amplace.setBasePlate(false);
								tier2location.add(e.getClickedBlock().getLocation());
								

								p.sendMessage(bedwars.MessagePerm+"Addiert");
								p.sendMessage(tier2location.toString());
								
							}	
						}
					else
						if(slot==1&&a==Action.RIGHT_CLICK_BLOCK) {
							if(tier1location.contains(e.getClickedBlock().getLocation())) {


								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.remove(chunkholer);
								p.sendMessage(bedwars.MessageNoPerm+"Entfernt");
								tier1location.remove(e.getClickedBlock().getLocation());
								for(Entity et : e.getClickedBlock().getLocation().getWorld().getNearbyEntities(e.getClickedBlock().getLocation().add(0.50, 0, 0.50),0.5,0.5,0.5)) {
									if(et instanceof ArmorStand) {
										et.remove();
									}
								}
							}else {

								Location chunkholer = e.getClickedBlock().getLocation();
								save.chunkloader.add(chunkholer);
								ItemStack isplace = api.getItemHead("10998");
								Location amloc = e.getClickedBlock().getLocation().add(0.50, 0, 0.50);
								amloc.setPitch(p.getLocation().getPitch()-180);
								amloc.setYaw(p.getLocation().getYaw()-180);
								ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
								amplace.setHelmet(isplace);
								amplace.setGravity(false);
								amplace.setCustomName(bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
								amplace.setCustomNameVisible(false);
								amplace.setCanPickupItems(false);
								amplace.setBasePlate(false);
								tier1location.add(e.getClickedBlock().getLocation());
								

								p.sendMessage(bedwars.MessagePerm+"Addiert");
								p.sendMessage(tier1location.toString());
								
							}
						}
					else
						if(slot==0) {
							p.setCustomName("selectteamconfigurator");
							ArrayList<String> teams = new ArrayList<String>();
							teams.add("red");
							teams.add("blue");
							teams.add("green");
							teams.add("yellow");
							teams.add("purple");
							teams.add("magenta");
							teams.add("white");
							teams.add("black");
							ArrayList<String> teamskulls = new ArrayList<String>();
							teamskulls.add("9356");
							teamskulls.add("8924");
							teamskulls.add("10220");
							teamskulls.add("9140");
							teamskulls.add("9464");
							teamskulls.add("9788");
							teamskulls.add("9248");
							teamskulls.add("8777");
							int teamamount = bedwars.cfgmain.getInt("teamamount");

							for(int is = 0;is<9;is++) {
								ItemStack spacer = new ItemStack(Material.AIR,1);
								p.getInventory().setItem(is, spacer);
								
							}
							for(int i = 0;i<(teamamount);i++) {
								String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
								String functioncustomname = teams.get(i)+"team";
								ArrayList<String> lore = new ArrayList<String>();
								lore.add(functioncustomname);
								ItemStack team = api.getItemHead(teamskulls.get(i));
								ItemMeta teammeta = team.getItemMeta();
								teammeta.setDisplayName(teamname+" Team Konfiguration");
								teammeta.setLore(lore);
								team.setItemMeta(teammeta);
								p.getInventory().setItem(i, team);
								
							}
							ItemStack generell = api.getItemHead("516");
							ItemMeta generellmeta = generell.getItemMeta();
							generellmeta.setDisplayName("§rGenerelle Einstellungen");
							generell.setItemMeta(generellmeta);
							p.getInventory().setItem(8, generell);
								
						}

					
					
				}
				if(a==Action.RIGHT_CLICK_AIR&&teams.saver.hasEntry(p.getName())) {
					int slot = e.getPlayer().getInventory().getHeldItemSlot();
					if(slot==8) {
						p.sendMessage(bedwars.MessagePerm+"SpielBereich Pos2 gesetzt/geupdated");
						Location locplace = e.getPlayer().getLocation();
						if(pos2exists==true) {
							
							for(Entity et : locplace.getWorld().getEntities()) {
								if(et instanceof ArmorStand) {
									
									String nametag = et.getCustomName();
									if(nametag.contains("Pos2")) {

										Location chunkholer = et.getLocation();
										save.chunkloader.remove(chunkholer);
										et.teleport(locplace);

										save.chunkloader.add(locplace);
										return;
									}
								}
								
							}
						}else {
							
							ItemStack pos2 = api.getItemHead("10241");
							ItemMeta pos2meta = pos2.getItemMeta();
							pos2meta.setDisplayName("§rSpielbereicht Pos.2 (gesetzt)");
							pos2.setItemMeta(pos2meta);
							p.getInventory().setItem(8, pos2);
							ItemStack isplace = api.getItemHead("10349");
							ArmorStand amplace = (ArmorStand) locplace.getWorld().spawnEntity(locplace, EntityType.ARMOR_STAND);
							amplace.setHelmet(isplace);
							amplace.setGravity(false);
							amplace.setCustomName("SpielBereich Pos2");
							amplace.setCustomNameVisible(true);
							amplace.setCanPickupItems(false);
							amplace.setBasePlate(false);
							amplace.setVisible(false);
							pos2exists=true;
							
						}
						
							
						
						
						
					}else
					if(slot==7) {
						p.sendMessage(bedwars.MessagePerm+"SpielBereich Pos1 gesetzt/geupdated");
						Location locplace = e.getPlayer().getLocation();
						if(pos1exists==true) {
							
							for(Entity et : locplace.getWorld().getEntities()) {
								if(et instanceof ArmorStand) {
									
									String nametag = et.getCustomName();
									if(nametag.contains("Pos1")) {

										Location chunkholer = et.getLocation();
										save.chunkloader.remove(chunkholer);
										et.teleport(locplace);
										save.chunkloader.add(locplace);
										return;
									}
								}
								
							}
						}else {
							ItemStack pos1 = api.getItemHead("10242");
							ItemMeta pos1meta = pos1.getItemMeta();
							pos1meta.setDisplayName("§rSpielbereicht Pos.1 (gesetzt)");
							pos1.setItemMeta(pos1meta);
							p.getInventory().setItem(7, pos1);
							ItemStack isplace = api.getItemHead("10350");
							ArmorStand amplace = (ArmorStand) locplace.getWorld().spawnEntity(locplace, EntityType.ARMOR_STAND);
							amplace.setHelmet(isplace);
							amplace.setGravity(false);
							amplace.setCustomName("SpielBereich Pos1");
							amplace.setCustomNameVisible(true);
							amplace.setCanPickupItems(false);
							amplace.setBasePlate(false);
							amplace.setVisible(false);
							pos1exists=true;
							
						}
						
							
						
						
						
					}
				}
			}else if(p.getCustomName().equalsIgnoreCase("useless")) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m==Material.BED) {
					Block b = e.getClickedBlock();
		            int radius = 2;
		            for (int x = b.getX(); x < b.getX()+ radius; x++)
		            {
		                for (int y = b.getY(); y < b.getY()+ radius; y++)
		                {
		                    for (int z = b.getZ(); z < b.getZ()+ radius; z++)
		                    {
		                       Block locb = b.getWorld().getBlockAt(x, y, z);
		                       Material locm = locb.getType();
		                       String locmn = locm.name();
		                       if(locmn.contains("BED")) {
		                           p.sendMessage(locm.name());
		                           p.sendMessage(Integer.toString(x));
		                           p.sendMessage(Integer.toString(y));
		                           p.sendMessage(Integer.toString(z));
		                    	   
		                       }
		                    }
		                }
		            }
				}
			}else if(p.getCustomName().equalsIgnoreCase("selectteamconfigurator")&&a==Action.RIGHT_CLICK_AIR||p.getCustomName().equalsIgnoreCase("selectteamconfigurator")&&a==Action.RIGHT_CLICK_BLOCK) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot!=8&&m!=Material.AIR) {
						p.setCustomName(p.getItemInHand().getItemMeta().getLore().get(0));
						ArrayList<String> teamfunctions = new ArrayList<String>();
						teamfunctions.add("Bett");
						teamfunctions.add("Shop");
						teamfunctions.add("Spawn");
						ArrayList<String> teamskulls = new ArrayList<String>();
						teamskulls.add("10376");
						teamskulls.add("10359");
						teamskulls.add("10307");
						for(int is = 0;is<9;is++) {
							ItemStack spacer = new ItemStack(Material.AIR,1);
							p.getInventory().setItem(is, spacer);
							
							
						}
						for(int i = 0;i<teamfunctions.size();i++) {
//							String teamname = bedwars.cfgmain.getString(teamfunctions.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
//							String functioncustomname = teamfunctions.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							ItemStack team = api.getItemHead(teamskulls.get(i));
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName(teamfunctions.get(i));
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							p.getInventory().setItem(i, team);
							
						}
						ItemStack generell = api.getItemHead("516");
						ItemMeta generellmeta = generell.getItemMeta();
						generellmeta.setDisplayName("§rTeam Auswahl");
						generell.setItemMeta(generellmeta);
						p.getInventory().setItem(8, generell);
					}
					if(slot==8) {
						p.setCustomName("general");
						event_makemap.playerwhouse=p.getName();
						teams.saver.addEntry(p.getName());
						p.getInventory().clear();
						ItemStack teamselector =  api.getItemHead("10358");
						ItemMeta teamselectormeta = teamselector.getItemMeta();
						teamselectormeta.setDisplayName("§rTeam auswählen");
						teamselector.setItemMeta(teamselectormeta);
						teamselector.setAmount(1);
						p.getInventory().setItem(0, teamselector);
						
						ItemStack tier1 = api.getItemHead("1114");
						ItemMeta tier1meta = tier1.getItemMeta();
						tier1meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
						tier1.setItemMeta(tier1meta);
						p.getInventory().setItem(1, tier1);
						
						ItemStack tier2 = api.getItemHead("99");
						ItemMeta tier2meta = tier2.getItemMeta();
						tier2meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
						tier2.setItemMeta(tier2meta);
						p.getInventory().setItem(2, tier2);
						
						ItemStack tier3 = api.getItemHead("98");
						ItemMeta tier3meta = tier3.getItemMeta();
						tier3meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
						tier3.setItemMeta(tier3meta);
						p.getInventory().setItem(3, tier3);
						
						ItemStack save = api.getItemHead("584");
						ItemMeta savemeta = save.getItemMeta();
						savemeta.setDisplayName("§rSpeichern");
						save.setItemMeta(savemeta);
						p.getInventory().setItem(6, save);
						
						if(pos1exists==false) {
							ItemStack pos1 = api.getItemHead("10350");
							ItemMeta pos1meta = pos1.getItemMeta();
							pos1meta.setDisplayName("§rSpielbereicht Pos.1");
							pos1.setItemMeta(pos1meta);
							p.getInventory().setItem(7, pos1);
							
							
						}else {
							ItemStack pos1 = api.getItemHead("10242");
							ItemMeta pos1meta = pos1.getItemMeta();
							pos1meta.setDisplayName("§rSpielbereicht Pos.1 (gesetzt)");
							pos1.setItemMeta(pos1meta);
							p.getInventory().setItem(7, pos1);
							
						}
						if(pos2exists==false) {
							
							ItemStack pos2 = api.getItemHead("10349");
							ItemMeta pos2meta = pos2.getItemMeta();
							pos2meta.setDisplayName("§rSpielbereicht Pos.2");
							pos2.setItemMeta(pos2meta);
							p.getInventory().setItem(8, pos2);
						}else {
							
							ItemStack pos2 = api.getItemHead("10241");
							ItemMeta pos2meta = pos2.getItemMeta();
							pos2meta.setDisplayName("§rSpielbereicht Pos.2 (gesetzt)");
							pos2.setItemMeta(pos2meta);
							p.getInventory().setItem(8, pos2);
							
							
						}
						
						
					}
				}
				
			}else if(p.getCustomName().contains("team")&&a==Action.RIGHT_CLICK_AIR) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot==8) {
						p.setCustomName("selectteamconfigurator");
						ArrayList<String> teams = new ArrayList<String>();
						teams.add("red");
						teams.add("blue");
						teams.add("green");
						teams.add("yellow");
						teams.add("purple");
						teams.add("magenta");
						teams.add("white");
						teams.add("black");
						ArrayList<String> teamskulls = new ArrayList<String>();
						teamskulls.add("9356");
						teamskulls.add("8924");
						teamskulls.add("10220");
						teamskulls.add("9140");
						teamskulls.add("9464");
						teamskulls.add("9788");
						teamskulls.add("9248");
						teamskulls.add("8777");
						int teamamount = bedwars.cfgmain.getInt("teamamount");

						for(int is = 0;is<9;is++) {
							ItemStack spacer = new ItemStack(Material.AIR,1);
							p.getInventory().setItem(is, spacer);
							
						}
						for(int i = 0;i<(teamamount);i++) {
							String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
							String functioncustomname = teams.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							lore.add(functioncustomname);
							ItemStack team = api.getItemHead(teamskulls.get(i));
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName(teamname+" Team Konfiguration");
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							p.getInventory().setItem(i, team);
							
						}
						ItemStack generell = api.getItemHead("516");
						ItemMeta generellmeta = generell.getItemMeta();
						generellmeta.setDisplayName("§rGenerelle Einstellungen");
						generell.setItemMeta(generellmeta);
						p.getInventory().setItem(8, generell);
					}
				}
			}else if(p.getCustomName().contains("team")&&a==Action.RIGHT_CLICK_BLOCK) {
				int slot = e.getPlayer().getInventory().getHeldItemSlot();
				e.setCancelled(true);
				Material m = e.getMaterial();
				if(m!=Material.AIR) {
					if(slot==8) {
						p.setCustomName("selectteamconfigurator");
						ArrayList<String> teams = new ArrayList<String>();
						teams.add("red");
						teams.add("blue");
						teams.add("green");
						teams.add("yellow");
						teams.add("purple");
						teams.add("magenta");
						teams.add("white");
						teams.add("black");
						ArrayList<String> teamskulls = new ArrayList<String>();
						teamskulls.add("9356");
						teamskulls.add("8924");
						teamskulls.add("10220");
						teamskulls.add("9140");
						teamskulls.add("9464");
						teamskulls.add("9788");
						teamskulls.add("9248");
						teamskulls.add("8777");
						int teamamount = bedwars.cfgmain.getInt("teamamount");

						for(int is = 0;is<9;is++) {
							ItemStack spacer = new ItemStack(Material.AIR,1);
							p.getInventory().setItem(is, spacer);
							
						}
						for(int i = 0;i<(teamamount);i++) {
							String teamname = bedwars.cfgmain.getString(teams.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
							String functioncustomname = teams.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							lore.add(functioncustomname);
							ItemStack team = api.getItemHead(teamskulls.get(i));
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName(teamname+" Team Konfiguration");
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							p.getInventory().setItem(i, team);
							
						}
						ItemStack generell = api.getItemHead("516");
						ItemMeta generellmeta = generell.getItemMeta();
						generellmeta.setDisplayName("§rGenerelle Einstellungen");
						generell.setItemMeta(generellmeta);
						p.getInventory().setItem(8, generell);
					}
					String redbed = "9404";
					String bluebed = "8972";
					String greenbed = "10268";
					String yellowbed = "9188";
					String purplebed = "9512";
					String magentabed = "9836";
					String whitebed = "9296";
					String blackbed = "8839";
					
					String redshop = "9387";
					String blueshop = "8955";
					String greenshop = "10251";
					String yellowshop = "9171";
					String purpleshop = "9495";
					String magentashop = "9819";
					String whiteshop = "9279";
					String blackshop = "8822";
					
					String redteleport = "9335";
					String blueteleport = "8903";
					String greenteleport = "10199";
					String yellowteleport = "9119";
					String purpleteleport = "9443";
					String magentateleport = "9767";
					String whiteteleport = "9227";
					String blackteleport = "8793";
					
					String defaultbed ="10376";
					String defaultshop ="10359";
					String defaultteleport ="10307";
					
					String bed ="10376";
					String shop ="10359";
					String teleport ="10307";
					
					String bedlocator ="";
					String shoplocator ="";
					String teleportlocator ="";
					if(p.getCustomName().contains("red")) {
						bed=redbed;
						shop=redshop;
						teleport=redteleport;
						bedlocator="redbed";
						shoplocator="redshop";
						teleportlocator="redteleport";
					}
					if(p.getCustomName().contains("blue")) {
						bed=bluebed;
						shop=blueshop;
						teleport=blueteleport;
						bedlocator="bluebed";
						shoplocator="blueshop";
						teleportlocator="blueteleport";
					}
					if(p.getCustomName().contains("green")) {
						bed=greenbed;
						shop=greenshop;
						teleport=greenteleport;
						bedlocator="greenbed";
						shoplocator="greenshop";
						teleportlocator="greenteleport";
					}
					if(p.getCustomName().contains("yellow")) {
						bed=yellowbed;
						shop=yellowshop;
						teleport=yellowteleport;
						bedlocator="yellowbed";
						shoplocator="yellowshop";
						teleportlocator="yellowteleport";
					}
					if(p.getCustomName().contains("purple")) {
						bed=purplebed;
						shop=purpleshop;
						teleport=purpleteleport;
						bedlocator="purplebed";
						shoplocator="purpleshop";
						teleportlocator="purpleteleport";
					}
					if(p.getCustomName().contains("magenta")) {
						bed=magentabed;
						shop=magentashop;
						teleport=magentateleport;
						bedlocator="magentabed";
						shoplocator="magentashop";
						teleportlocator="magentateleport";
					}
					if(p.getCustomName().contains("white")) {
						bed=whitebed;
						shop=whiteshop;
						teleport=whiteteleport;
						bedlocator="whitebed";
						shoplocator="whiteshop";
						teleportlocator="whiteteleport";
					}
					if(p.getCustomName().contains("black")) {
						bed=blackbed;
						shop=blackshop;
						teleport=blackteleport;
						bedlocator="blackbed";
						shoplocator="blackshop";
						teleportlocator="blackteleport";
					}
						if(slot==0&&!p.getItemInHand().getItemMeta().getDisplayName().contains("gesetzt")) {
							
//							String teamname = bedwars.cfgmain.getString(teamfunctions.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
//							String functioncustomname = teamfunctions.get(i)+"team";
							ArrayList<String> lore = new ArrayList<String>();
							ItemStack team = api.getItemHead(bed);
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName("Bett (gesetzt)");
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							Block b = e.getClickedBlock();
				            int radius = 2;
				            for (int x = b.getX(); x < b.getX()+ radius; x++)
				            {
				                for (int y = b.getY(); y < b.getY()+ radius; y++)
				                {
				                    for (int z = b.getZ(); z < b.getZ()+ radius; z++)
				                    {
				                       Block locb = b.getWorld().getBlockAt(x, y, z);
				                       Material locm = locb.getType();
				                       String locmn = locm.name();
				                       if(locmn.contains("BED")) {
				                    	   ItemStack isplace = api.getItemHead(bed);
											Location amloc = new Location(e.getClickedBlock().getWorld(),x,y,z).add(0.50, -0.5, 0.50);
											amloc.setPitch(p.getLocation().getPitch()-180);
											amloc.setYaw(p.getLocation().getYaw()-180);
											ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
											amplace.setHelmet(isplace);
											amplace.setGravity(false);
											amplace.setCustomName(bedlocator);
											amplace.setCustomNameVisible(false);
											amplace.setCanPickupItems(false);
											amplace.setBasePlate(false);
				                    	   
				                       }
				                    }
				                }
				            }
							p.getInventory().setItem(0, team);
							
						
						
					}
						if(slot==0&&p.isSneaking()) {
							ArrayList<String> lore = new ArrayList<String>();
							ItemStack team = api.getItemHead(defaultbed);
							ItemMeta teammeta = team.getItemMeta();
							teammeta.setDisplayName("Bett");
							teammeta.setLore(lore);
							team.setItemMeta(teammeta);
							 for(World w : Bukkit.getWorlds()) {
								 for(Entity et : w.getEntities()) {
										if(et instanceof ArmorStand) {
											
											String nametag = et.getCustomName();
											if(nametag.equalsIgnoreCase(bedlocator)) {
												et.remove();
											}
										}
										
									}
							 }
							p.getInventory().setItem(0, team);
							
						}
					if(slot==1&&!p.getItemInHand().getItemMeta().getDisplayName().contains("gesetzt")) {
						
//						String teamname = bedwars.cfgmain.getString(teamfunctions.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
//						String functioncustomname = teamfunctions.get(i)+"team";
						ArrayList<String> lore = new ArrayList<String>();
						ItemStack team = api.getItemHead(shop);
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName("Shop (gesetzt)");
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						p.getInventory().setItem(1, team);
						ItemStack isplace = api.getItemHead(shop);
						Location amloc = e.getClickedBlock().getLocation().add(0.50, 1, 0.50);
						amloc.setPitch(p.getLocation().getPitch()-180);
						amloc.setYaw(p.getLocation().getYaw()-180);
						ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
						amplace.setHelmet(isplace);
						amplace.setGravity(false);
						amplace.setCustomName(shoplocator);
						amplace.setCustomNameVisible(false);
						amplace.setCanPickupItems(false);
						amplace.setBasePlate(false);
						
					
					
					}
					if(slot==1&&p.isSneaking()) {
						ArrayList<String> lore = new ArrayList<String>();
						ItemStack team = api.getItemHead(defaultshop);
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName("Shop");
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						 for(World w : Bukkit.getWorlds()) {
							 for(Entity et : w.getEntities()) {
									if(et instanceof ArmorStand) {
										
										String nametag = et.getCustomName();
										if(nametag.equalsIgnoreCase(shoplocator)) {
											et.remove();
										}
									}
									
								}
						 }
						p.getInventory().setItem(1, team);
						
					}
					if(slot==2&&!p.getItemInHand().getItemMeta().getDisplayName().contains("gesetzt")) {
						
//						String teamname = bedwars.cfgmain.getString(teamfunctions.get(i)+"name").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß");
//						String functioncustomname = teamfunctions.get(i)+"team";
						ArrayList<String> lore = new ArrayList<String>();
						ItemStack team = api.getItemHead(teleport);
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName("Spawn (gesetzt)");
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						p.getInventory().setItem(2, team);
						ItemStack isplace = api.getItemHead(teleport);
						Location amloc = e.getClickedBlock().getLocation().add(0.50, 0, 0.50);
						amloc.setPitch(p.getLocation().getPitch()-180);
						amloc.setYaw(p.getLocation().getYaw()-180);
						ArmorStand amplace = (ArmorStand) e.getClickedBlock().getLocation().getWorld().spawnEntity(amloc, EntityType.ARMOR_STAND);
						amplace.setHelmet(isplace);
						amplace.setGravity(false);
						amplace.setCustomName(teleportlocator);
						amplace.setCustomNameVisible(false);
						amplace.setCanPickupItems(false);
						amplace.setBasePlate(false);
						
					
					
					}
					if(slot==2&&p.isSneaking()) {
						ArrayList<String> lore = new ArrayList<String>();
						ItemStack team = api.getItemHead(defaultteleport);
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName("Spawn");
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						 for(World w : Bukkit.getWorlds()) {
							 for(Entity et : w.getEntities()) {
									if(et instanceof ArmorStand) {
										
										String nametag = et.getCustomName();
										if(nametag.equalsIgnoreCase(teleportlocator)) {
											et.remove();
										}
									}
									
								}
						 }
						p.getInventory().setItem(2, team);
						
					}
					
					
					
				}
				
			}
			
			
		}}catch(NullPointerException en) {
			
		}
		
	}
	@EventHandler
	public void onAnvil(InventoryClickEvent e) {
		try {
		Player p = (Player) e.getWhoClicked();
		
		if(playerwhouse.equalsIgnoreCase(p.getName())) {
			e.setCancelled(true);
			int slot = e.getSlot();
			String slottyp = e.getSlotType().toString();
			
			if(slot==2&&slottyp.equalsIgnoreCase("result")) {
				p.playSound(p.getLocation(), Sound.ANVIL_USE, 1, 1);
				p.closeInventory();
				String mapname = e.getCurrentItem().getItemMeta().getDisplayName().replace("§0", "");
				save.finish(p, mapname);
				return;
				
				
			}
			if(slottyp.equalsIgnoreCase("crafting")) {
				p.closeInventory();
				new BukkitRunnable() {
					
					@Override
					public void run() {
						
						
						Anvil.openAnvilInventory(p);
						for(int i =0 ; i<p.getInventory().getSize();i++) {
							try {
							if(p.getInventory().getItem(i).getType()==Material.PAPER) {
								p.getInventory().setItem(i, new ItemStack(Material.AIR));
							}
							}catch (NullPointerException en) {
								
							}
						}
					}
				}.runTaskLater(plugin, 5);
				
			}
			
		}}catch(NullPointerException en) {
			
		}
	}
	private float toDegree(double angle) {
	    return (float) Math.toDegrees(angle);
	}
	 
	private Vector getVector(Entity entity) {
	    if (entity instanceof Player)
	        return ((Player) entity).getEyeLocation().toVector();
	    else
	        return entity.getLocation().toVector();
	}
}
