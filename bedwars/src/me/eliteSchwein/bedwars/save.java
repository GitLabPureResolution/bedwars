package me.eliteSchwein.bedwars;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class save {
	public static ArrayList<Location> chunkloader = new ArrayList<Location>();
    static HeadDatabaseAPI api = new HeadDatabaseAPI();
	public static ArrayList<Location> bedstand = new ArrayList<Location>();
	public static HashMap<String,Location> bedlocator = new HashMap<String,Location>();
	public static ArrayList<Location> tier3location = new ArrayList<Location>();
	public static ArrayList<Location> tier2location = new ArrayList<Location>();
	public static ArrayList<Location> tier1location = new ArrayList<Location>();
	public static ArrayList<Location> Posstand = new ArrayList<Location>();
	public static HashMap<String,Location> Poslocator = new HashMap<String,Location>();
	public static ArrayList<Location> shopstand = new ArrayList<Location>();
	public static HashMap<String,Location> shoplocator = new HashMap<String,Location>();
	public static ArrayList<Location> spawnstand = new ArrayList<Location>();
	public static HashMap<String,Location> spawnlocator = new HashMap<String,Location>();
	
	public static int teamsize = bedwars.cfgmain.getInt("teamamount");
	public static void startchunkloader() {
		new BukkitRunnable() {

			@Override
			public void run() {
				for(Location loc:chunkloader) {
					loc.getChunk().load(true);
				}
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 5);
	}
	public static void start(Player player) {
		ArrayList<String> Loadingbar = new ArrayList<String>();
		Loadingbar.add("11290");
		Loadingbar.add("11273");
		Loadingbar.add("11221");
		Loadingbar.add("10998");
		Loadingbar.add("514");
		Loadingbar.add("7928");
		Loadingbar.add("10350");
		Loadingbar.add("10349");
		Loadingbar.add("923");
		ArrayList<String> Loadingbartitle = new ArrayList<String>();
		Loadingbartitle.add("Betten");
		Loadingbartitle.add("Shops");
		Loadingbartitle.add("Spawnpunkte");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
		Loadingbartitle.add("Spielbereich Pos.1");
		Loadingbartitle.add("Spielbereich Pos.2");
		Loadingbartitle.add("Status");
		for(int i = 0;i<Loadingbar.size();i++) {
			String teamname = Loadingbartitle.get(i);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(" ");
			ItemStack team = api.getItemHead(Loadingbar.get(i));
			ItemMeta teammeta = team.getItemMeta();
			teammeta.setDisplayName(teamname);
			teammeta.setLore(lore);
			team.setItemMeta(teammeta);
			player.getInventory().setItem(i, team);
		}
		new BukkitRunnable() {
			int timer = 0;

			@Override
			public void run() {
				if(timer==0) {
					
					if(scanBeds(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10268");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9404");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						
						player.sendMessage(bedwars.MessageNoPerm+"Bitte Betten fixen");
					}
					timer++;
					
				}else
				if(timer==1) {
					if(scanShop(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10251");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9387");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Bitte Shops fixen");
					}
					timer++;
					
				}else
				if(timer==2) {
					if(scanSpawn(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10199");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9335");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Bitte Spawns fixen");
					}
					timer++;
						
				}else
				if(timer==3) {
					if(scantier1(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10192");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9328");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Zu wenige Spawner!");
					}
					timer++;
						
				}else
				if(timer==4) {
					if(scantier2(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10192");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9328");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Zu wenige Spawner!");
					}
					timer++;
						
				}else
				if(timer==5) {
					if(scantier3(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10192");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9328");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Zu wenige Spawner!");
					}
					timer++;
					
				}else
				if(timer==6) {
					if(scanpos(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10242");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						
						String team1name = Loadingbartitle.get(timer);
						ArrayList<String> lore1 = new ArrayList<String>();
						lore1.add(" ");
						ItemStack team1 = api.getItemHead("10241");
						ItemMeta team1meta = team1.getItemMeta();
						team1meta.setDisplayName(team1name);
						team1meta.setLore(lore1);
						team1.setItemMeta(team1meta);
						player.getInventory().setItem(timer+1, team1);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9378");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);

						String team1name = Loadingbartitle.get(timer);
						ArrayList<String> lore1 = new ArrayList<String>();
						lore1.add(" ");
						ItemStack team1 = api.getItemHead("9377");
						ItemMeta team1meta = team1.getItemMeta();
						team1meta.setDisplayName(team1name);
						team1meta.setLore(lore1);
						team1.setItemMeta(team1meta);
						player.getInventory().setItem(timer+1, team1);
						cancel();
						failedScan(player);
						bedstand.clear();
						shopstand.clear();
						spawnstand.clear();
						Posstand.clear();
						bedlocator.clear();
						shoplocator.clear();
						spawnlocator.clear();
						Poslocator.clear();
						tier1location.clear();
						tier2location.clear();
						tier3location.clear();
						player.sendMessage(bedwars.MessageNoPerm+"Spielbereich Fehlerhaft makiert");
					}
					timer++;
						
				}else if(timer==7){
					player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
					ItemStack save = api.getItemHead("845");
					ItemMeta savemeta = save.getItemMeta();
					savemeta.setDisplayName("§rStatus (Gescannt ohne Fehler)");
					save.setItemMeta(savemeta);
					player.getInventory().setItem(8, save);
					player.sendMessage(bedwars.MessagePerm+"Scan war erfolgreich!");
					cancel();
					new BukkitRunnable() {

						@Override
						public void run() {
							Anvil.openAnvilInventory(player);
							
						}
						
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 20);
					
				}else {
					timer++;
				}
				
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 20, 20);
		
	}
	public static boolean scanBeds(Player player) {
		int minandmax = teamsize*2;
		World w = player.getWorld();
		for(Entity et : w.getEntities()) {
			if(et instanceof ArmorStand) {
				String nametag = et.getCustomName();
				if(nametag.contains("bed")) {
					if(bedlocator.containsKey(nametag)) {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						bedstand.add(loc);
						bedlocator.put(nametag+"1", loc);
					}else {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						bedstand.add(loc);
						bedlocator.put(nametag, loc);
					}
					
					
				}
			}
			
		}
		if(bedstand.size()==minandmax) {

			return true;
		}else {
			return false;
			
		}
		
	}
	public static boolean scanShop(Player player) {
		int minandmax = teamsize;
		World w = player.getWorld();
		for(Entity et : w.getEntities()) {
			if(et instanceof ArmorStand) {
				String nametag = et.getCustomName();
				if(nametag.contains("shop")) {
					if(shoplocator.containsKey(nametag)) {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						shopstand.add(loc);
						shoplocator.put(nametag+"1", loc);
					}else {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						shopstand.add(loc);
						shoplocator.put(nametag, loc);
					}
				}
			}
			
		}
		if(shopstand.size()==minandmax) {

			return true;
		}else {
			return false;
			
		}
		
	}
	public static boolean scanSpawn(Player player) {
		int minandmax = teamsize;
		World w = player.getWorld();
		for(Entity et : w.getEntities()) {
			if(et instanceof ArmorStand) {
				String nametag = et.getCustomName();
				if(nametag.contains("teleport")) {
					if(spawnlocator.containsKey(nametag)) {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						spawnstand.add(loc);
						spawnlocator.put(nametag+"1", loc);
					}else {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						spawnstand.add(loc);
						spawnlocator.put(nametag, loc);
					}
				}
			}
			
		}
		if(spawnstand.size()==minandmax) {

			return true;
		}else {
			return false;
			
		}
		
	}
	public static boolean scantier3(Player player) {
		int min = 1;
		for(Location loc : event_makemap.tier3location) {
			tier3location.add(loc);
		}
		if(tier3location.size()>=min||tier3location.size()==min) {
			return true;
			
		}else {
			return false;
			
		}
		
	}
	public static boolean scantier2(Player player) {
		int min = 1;
		for(Location loc : event_makemap.tier2location) {
			tier2location.add(loc);
		}
		if(tier2location.size()>=min||tier2location.size()==min) {
			return true;
			
		}else {
			return false;
			
		}
		
	}
	public static boolean scantier1(Player player) {
		int min = teamsize;
		for(Location loc : event_makemap.tier1location) {
			tier1location.add(loc);
		}
		if(tier1location.size()>=min||tier1location.size()==min) {
			return true;
			
		}else {
			return false;
			
		}
		
	}
	public static boolean scanpos(Player player) {
		int minandmax = 2;
		World w = player.getWorld();
		for(Entity et : w.getEntities()) {
			if(et instanceof ArmorStand) {
				String nametag = et.getCustomName();
				if(nametag.contains("Pos")) {
					if(Poslocator.containsKey(nametag)) {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						Posstand.add(loc);
						Poslocator.put(nametag+"1", loc);
					}else {
						double x = et.getLocation().getX();
						double y = et.getLocation().getY()+1;
						double z = et.getLocation().getZ();
						Location loc = new Location(w,x,y,z);
						Posstand.add(loc);
						Poslocator.put(nametag, loc);
					}
					
					
				}
			}
			
		}
		if(Posstand.size()==minandmax) {

			return true;
		}else {
			return false;
			
		}
		
	}
	
	public static void finish(Player player,String mapname) {
		ArrayList<String> Loadingbar = new ArrayList<String>();
		Loadingbar.add("11290");
		Loadingbar.add("11273");
		Loadingbar.add("11221");
		Loadingbar.add("10998");
		Loadingbar.add("514");
		Loadingbar.add("7928");
		Loadingbar.add("10350");
		Loadingbar.add("10349");
		Loadingbar.add("923");
		ArrayList<String> Loadingbartitle = new ArrayList<String>();
		Loadingbartitle.add("Betten");
		Loadingbartitle.add("Shops");
		Loadingbartitle.add("Spawnpunkte");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
		Loadingbartitle.add("Spielbereich Pos.1");
		Loadingbartitle.add("Spielbereich Pos.2");
		Loadingbartitle.add("Status");
		new BukkitRunnable() {
			int timer = 0;

			@Override
			public void run() {
				try {
				if(timer==0) {
					schematic.setPosition1(player, bedlocator.get("redbed"));
					schematic.setPosition2(player, bedlocator.get("redbed1"));
					
					timer++;
				}if(timer==1) {
					schematic.save(player, "redbed", mapname);
					
					timer++;
				}if(timer==2) {
					schematic.setPosition1(player, bedlocator.get("bluebed"));
					schematic.setPosition2(player, bedlocator.get("bluebed1"));
					
					timer++;
				}if(timer==3) {
					schematic.save(player, "bluebed", mapname);
					
					timer++;
				}if(timer==4) {
					schematic.setPosition1(player, bedlocator.get("greenbed"));
					schematic.setPosition2(player, bedlocator.get("greenbed1"));
					
					timer++;
				}if(timer==5) {
					schematic.save(player, "greenbed", mapname);
					
					timer++;
				}if(timer==6) {
					schematic.setPosition1(player, bedlocator.get("yellowbed"));
					schematic.setPosition2(player, bedlocator.get("yellowbed1"));
					
					timer++;
				}if(timer==7) {
					schematic.save(player, "yellowbed", mapname);
					
					timer++;
				}if(timer==8) {
					schematic.setPosition1(player, bedlocator.get("purplebed"));
					schematic.setPosition2(player, bedlocator.get("purplebed1"));
					
					timer++;
				}if(timer==9) {
					schematic.save(player, "purplebed", mapname);
					
					timer++;
				}if(timer==10) {
					schematic.setPosition1(player, bedlocator.get("magentabed"));
					schematic.setPosition2(player, bedlocator.get("magentabed1"));
					
					timer++;
				}if(timer==11) {
					schematic.save(player, "magentabed", mapname);
					
					timer++;
				}if(timer==12) {
					schematic.setPosition1(player, bedlocator.get("whitebed"));
					schematic.setPosition2(player, bedlocator.get("whitebed1"));
					
					timer++;
				}if(timer==13) {
					schematic.save(player, "whitebed", mapname);
					
					timer++;
				}if(timer==14) {
					schematic.setPosition1(player, bedlocator.get("blackbed"));
					schematic.setPosition2(player, bedlocator.get("blackbed1"));
					
					timer++;
				}if(timer==15) {
					schematic.save(player, "blackbed", mapname);
					
					timer++;
				}if(timer==16) {
					cancel();
					String teamname = Loadingbartitle.get(0);
					ArrayList<String> lore = new ArrayList<String>();
					lore.add(" ");
					ItemStack team = api.getItemHead("8972");
					ItemMeta teammeta = team.getItemMeta();
					teammeta.setDisplayName(teamname);
					teammeta.setLore(lore);
					team.setItemMeta(teammeta);
					player.getInventory().setItem(0, team);
					new BukkitRunnable() {

						@Override
						public void run() {
							schematic.setPosition1(player, Poslocator.get("SpielBereich Pos1"));
							schematic.setPosition2(player, Poslocator.get("SpielBereich Pos2"));
							new BukkitRunnable() {

								@Override
								public void run() {
									schematic.makeWorldGuard(player, mapname);
									
									String teamname = Loadingbartitle.get(6);
									ArrayList<String> lore = new ArrayList<String>();
									lore.add(" ");
									ItemStack team = api.getItemHead("8946");
									ItemMeta teammeta = team.getItemMeta();
									teammeta.setDisplayName(teamname);
									teammeta.setLore(lore);
									team.setItemMeta(teammeta);
									player.getInventory().setItem(6, team);
									
									String teamname1 = Loadingbartitle.get(7);
									ArrayList<String> lore1 = new ArrayList<String>();
									lore1.add(" ");
									ItemStack team1 = api.getItemHead("8945");
									ItemMeta teammeta1 = team1.getItemMeta();
									teammeta1.setDisplayName(teamname1);
									teammeta1.setLore(lore1);
									team1.setItemMeta(teammeta1);
									player.getInventory().setItem(7, team1);
									MapConfig(player, mapname);
									
								}
							}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 3);
							
						}
						
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 5);
				}}catch(NullPointerException en) {
					cancel();
					new BukkitRunnable() {

						@Override
						public void run() {
							schematic.setPosition1(player, Poslocator.get("SpielBereich Pos1"));
							schematic.setPosition2(player, Poslocator.get("SpielBereich Pos2"));
							new BukkitRunnable() {

								@Override
								public void run() {
									schematic.makeWorldGuard(player, mapname);
									
									String teamname = Loadingbartitle.get(6);
									ArrayList<String> lore = new ArrayList<String>();
									lore.add(" ");
									ItemStack team = api.getItemHead("8946");
									ItemMeta teammeta = team.getItemMeta();
									teammeta.setDisplayName(teamname);
									teammeta.setLore(lore);
									team.setItemMeta(teammeta);
									player.getInventory().setItem(6, team);
									
									String teamname1 = Loadingbartitle.get(7);
									ArrayList<String> lore1 = new ArrayList<String>();
									lore1.add(" ");
									ItemStack team1 = api.getItemHead("8945");
									ItemMeta teammeta1 = team1.getItemMeta();
									teammeta1.setDisplayName(teamname1);
									teammeta1.setLore(lore1);
									team1.setItemMeta(teammeta1);
									player.getInventory().setItem(7, team1);
									MapConfig(player, mapname);
									
								}
							}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 3);
							
						}
						
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 5);
					String teamname = Loadingbartitle.get(0);
					ArrayList<String> lore = new ArrayList<String>();
					lore.add(" ");
					ItemStack team = api.getItemHead("8972");
					ItemMeta teammeta = team.getItemMeta();
					teammeta.setDisplayName(teamname);
					teammeta.setLore(lore);
					team.setItemMeta(teammeta);
					player.getInventory().setItem(0, team);
				}
				
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 2);
		
	}
	public static void MapConfig(Player player,String mapname) {
		File filemapdata = new File("plugins/bedwars/mapconfigs/"+mapname , "config.yml");
        File filemapdatapath = new File("plugins/bedwars/mapconfigs/"+mapname);

    	FileConfiguration cfgmapdata = YamlConfiguration.loadConfiguration(filemapdata);
       if(!filemapdata.exists()) {
    	   filemapdatapath.mkdir();
    	   filemapdatapath.mkdirs();
    	   try {
    		   filemapdata.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       ArrayList<String> teams = new ArrayList<String>();
       teams.add("red");
       teams.add("blue");
       teams.add("green");
       teams.add("yellow");
       teams.add("purple");
       teams.add("magenta");
       teams.add("white");
       teams.add("black");
		ArrayList<String> Loadingbar = new ArrayList<String>();
		Loadingbar.add("11290");
		Loadingbar.add("8955");
		Loadingbar.add("8903");
		Loadingbar.add("8896");
		Loadingbar.add("8896");
		Loadingbar.add("8896");
		Loadingbar.add("10350");
		Loadingbar.add("10349");
		Loadingbar.add("6119");
		ArrayList<String> Loadingbartitle = new ArrayList<String>();
		Loadingbartitle.add("Betten");
		Loadingbartitle.add("Shops");
		Loadingbartitle.add("Spawnpunkte");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
		Loadingbartitle.add(bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
		Loadingbartitle.add("Spielbereich Pos.1");
		Loadingbartitle.add("Spielbereich Pos.2");
		Loadingbartitle.add("Status (gespeichert/fertig)");

 	   ArrayList<Double> xt1 = new ArrayList<Double>();
 	   ArrayList<Double> yt1 = new ArrayList<Double>();
 	   ArrayList<Double> zt1 = new ArrayList<Double>();

 	   ArrayList<Double> xt2 = new ArrayList<Double>();
 	   ArrayList<Double> yt2 = new ArrayList<Double>();
 	   ArrayList<Double> zt2 = new ArrayList<Double>();

 	   ArrayList<Double> xt3 = new ArrayList<Double>();
 	   ArrayList<Double> yt3 = new ArrayList<Double>();
 	   ArrayList<Double> zt3 = new ArrayList<Double>();
       for(String str : teams) {
    	   try {
           
           cfgmapdata.set(str+".bed.x1",bedlocator.get(str+"bed1").getBlockX());
           cfgmapdata.set(str+".bed.y1",bedlocator.get(str+"bed1").getBlockY());
           cfgmapdata.set(str+".bed.z1",bedlocator.get(str+"bed1").getBlockZ());
           cfgmapdata.set(str+".bed.x",bedlocator.get(str+"bed").getBlockX());
           cfgmapdata.set(str+".bed.y",bedlocator.get(str+"bed").getBlockY());
           cfgmapdata.set(str+".bed.z",bedlocator.get(str+"bed").getBlockZ());
           
           cfgmapdata.set(str+".shop.x",shoplocator.get(str+"shop").getBlockX());
           cfgmapdata.set(str+".shop.y",shoplocator.get(str+"shop").getBlockY());
           cfgmapdata.set(str+".shop.z",shoplocator.get(str+"shop").getBlockZ());
           
           String teamname = Loadingbartitle.get(1);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(" ");
			ItemStack team = api.getItemHead(Loadingbar.get(1));
			ItemMeta teammeta = team.getItemMeta();
			teammeta.setDisplayName(teamname);
			teammeta.setLore(lore);
			team.setItemMeta(teammeta);
			player.getInventory().setItem(1, team);
           
           cfgmapdata.set(str+".spawn.x", spawnlocator.get(str+"teleport").getBlockX());
           cfgmapdata.set(str+".spawn.y", spawnlocator.get(str+"teleport").getBlockY());
           cfgmapdata.set(str+".spawn.z", spawnlocator.get(str+"teleport").getBlockZ());
           
           String teamname1 = Loadingbartitle.get(2);
			ArrayList<String> lore1 = new ArrayList<String>();
			lore1.add(" ");
			ItemStack team1 = api.getItemHead(Loadingbar.get(2));
			ItemMeta teammeta1 = team1.getItemMeta();
			teammeta1.setDisplayName(teamname1);
			teammeta1.setLore(lore1);
			team1.setItemMeta(teammeta1);
			player.getInventory().setItem(2, team1);
    	   }catch(NullPointerException en) {
    		   
    	   }
    	   
       }
       for(Location loc : tier1location) {
           xt1.add((double)loc.getBlockX()+0.5);
           yt1.add((double)loc.getBlockY()+0.5);
           zt1.add((double)loc.getBlockZ()+0.5);
       }
       cfgmapdata.set("tier1spawner.x", xt1);
       cfgmapdata.set("tier1spawner.y", yt1);
       cfgmapdata.set("tier1spawner.z", zt1);
       
       String teamnamet1 = Loadingbartitle.get(3);
		ArrayList<String> loret1 = new ArrayList<String>();
		loret1.add(" ");
		ItemStack teamt1 = api.getItemHead(Loadingbar.get(3));
		ItemMeta teammetat1 = teamt1.getItemMeta();
		teammetat1.setDisplayName(teamnamet1);
		teammetat1.setLore(loret1);
		teamt1.setItemMeta(teammetat1);
		player.getInventory().setItem(3, teamt1);
	       for(Location loc : tier2location) {
	           xt2.add((double)loc.getBlockX()+0.5);
	           yt2.add((double)loc.getBlockY()+0.5);
	           zt2.add((double)loc.getBlockZ()+0.5);
	       }
	       cfgmapdata.set("tier2spawner.x", xt2);
	       cfgmapdata.set("tier2spawner.y", yt2);
	       cfgmapdata.set("tier2spawner.z", zt2);
	       
	       String teamnamet2 = Loadingbartitle.get(4);
			ArrayList<String> loret2 = new ArrayList<String>();
			loret2.add(" ");
			ItemStack teamt2 = api.getItemHead(Loadingbar.get(4));
			ItemMeta teammetat2 = teamt2.getItemMeta();
			teammetat2.setDisplayName(teamnamet2);
			teammetat2.setLore(loret2);
			teamt2.setItemMeta(teammetat2);
			player.getInventory().setItem(4, teamt2);

		       for(Location loc : tier3location) {
		           xt3.add((double)loc.getBlockX()+0.5);
		           yt3.add((double)loc.getBlockY()+0.5);
		           zt3.add((double)loc.getBlockZ()+0.5);
		       }
		       cfgmapdata.set("tier3spawner.x", xt3);
		       cfgmapdata.set("tier3spawner.y", yt3);
		       cfgmapdata.set("tier3spawner.z", zt3);
		       
		       String teamnamet3 = Loadingbartitle.get(5);
				ArrayList<String> loret3 = new ArrayList<String>();
				loret3.add(" ");
				ItemStack teamt3 = api.getItemHead(Loadingbar.get(5));
				ItemMeta teammetat3 = teamt3.getItemMeta();
				teammetat3.setDisplayName(teamnamet3);
				teammetat3.setLore(loret3);
				teamt3.setItemMeta(teammetat3);
				player.getInventory().setItem(5, teamt3);
       cfgmapdata.set("gamepos1.x", (double)Poslocator.get("SpielBereich Pos1").getBlockX()+0.5);
       cfgmapdata.set("gamepos1.y", (double)Poslocator.get("SpielBereich Pos1").getBlockY()+1.5);
       cfgmapdata.set("gamepos1.z", (double)Poslocator.get("SpielBereich Pos1").getBlockZ()+0.5);
       
       cfgmapdata.set("gamepos2.x", (double)Poslocator.get("SpielBereich Pos2").getBlockX()+0.5);
       cfgmapdata.set("gamepos2.y", (double)Poslocator.get("SpielBereich Pos2").getBlockY()+1.5);
       cfgmapdata.set("gamepos2.z", (double)Poslocator.get("SpielBereich Pos2").getBlockZ()+0.5);
       
       new BukkitRunnable() {

		@Override
		public void run() {
			try {
				cfgmapdata.save(filemapdata);

				player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1, 1);
		           String teamname = Loadingbartitle.get(8);
					ArrayList<String> lore = new ArrayList<String>();
					lore.add(" ");
					ItemStack team = api.getItemHead(Loadingbar.get(8));
					ItemMeta teammeta = team.getItemMeta();
					teammeta.setDisplayName(teamname);
					teammeta.setLore(lore);
					team.setItemMeta(teammeta);
					player.getInventory().setItem(8, team);
					new BukkitRunnable() {

						@Override
						public void run() {

							player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 1, 1);
							player.setCustomName(player.getName());
							event_makemap.pos1exists=false;
							event_makemap.pos2exists=false;
							event_makemap.tier1location.clear();;
							event_makemap.tier2location.clear();
							event_makemap.tier3location.clear();
							event_makemap.shoplocation.clear();
							event_makemap.teamlocator.clear();
							event_makemap.teleportlocation.clear();
							event_makemap.bedlocation.clear();
							event_makemap.playerwhouse=null;
							me.eliteSchwein.bedwars.teams.saver.removeEntry(player.getName());
							player.getInventory().clear();
							 for(World w : Bukkit.getWorlds()) {
								 for(Entity et : w.getEntities()) {
										if(et instanceof ArmorStand) {
											
											String nametag = et.getCustomName();
											if(nametag.contains("shop")||nametag.contains("teleport")||nametag.contains("bed")||nametag.contains("Pos")||nametag.contains("Spawner")) {
												et.remove();
											}
										}
										
									}
							 }
							
						}
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 30);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	   
       }.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 10);
       
       
	}
	public static void failedScan(Player player) {
		new BukkitRunnable() {

			@Override
			public void run() {
				for(int is = 0;is<9;is++) {
					ItemStack spacer = new ItemStack(Material.AIR,1);
					player.getInventory().setItem(is, spacer);
					
				}
				player.setCustomName("general");
				event_makemap.playerwhouse=player.getName();
				teams.saver.addEntry(player.getName());
				player.getInventory().clear();
				ItemStack teamselector =  api.getItemHead("10358");
				ItemMeta teamselectormeta = teamselector.getItemMeta();
				teamselectormeta.setDisplayName("§rTeam auswählen");
				teamselector.setItemMeta(teamselectormeta);
				teamselector.setAmount(1);
				player.getInventory().setItem(0, teamselector);
				
				ItemStack tier1 = api.getItemHead("1114");
				ItemMeta tier1meta = tier1.getItemMeta();
				tier1meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
				tier1.setItemMeta(tier1meta);
				player.getInventory().setItem(1, tier1);
				
				ItemStack tier2 = api.getItemHead("99");
				ItemMeta tier2meta = tier2.getItemMeta();
				tier2meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
				tier2.setItemMeta(tier2meta);
				player.getInventory().setItem(2, tier2);
				
				ItemStack tier3 = api.getItemHead("98");
				ItemMeta tier3meta = tier3.getItemMeta();
				tier3meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
				tier3.setItemMeta(tier3meta);
				player.getInventory().setItem(3, tier3);
				
				ItemStack save = api.getItemHead("584");
				ItemMeta savemeta = save.getItemMeta();
				savemeta.setDisplayName("§rSpeichern");
				save.setItemMeta(savemeta);
				player.getInventory().setItem(6, save);
				
				if(event_makemap.pos1exists==false) {
					ItemStack pos1 = api.getItemHead("10350");
					ItemMeta pos1meta = pos1.getItemMeta();
					pos1meta.setDisplayName("§rSpielbereicht Pos.1");
					pos1.setItemMeta(pos1meta);
					player.getInventory().setItem(7, pos1);
					
					
				}else {
					ItemStack pos1 = api.getItemHead("10242");
					ItemMeta pos1meta = pos1.getItemMeta();
					pos1meta.setDisplayName("§rSpielbereicht Pos.1 (gesetzt)");
					pos1.setItemMeta(pos1meta);
					player.getInventory().setItem(7, pos1);
					
				}
				if(event_makemap.pos2exists==false) {
					
					ItemStack pos2 = api.getItemHead("10349");
					ItemMeta pos2meta = pos2.getItemMeta();
					pos2meta.setDisplayName("§rSpielbereicht Pos.2");
					pos2.setItemMeta(pos2meta);
					player.getInventory().setItem(8, pos2);
				}else {
					
					ItemStack pos2 = api.getItemHead("10241");
					ItemMeta pos2meta = pos2.getItemMeta();
					pos2meta.setDisplayName("§rSpielbereicht Pos.2 (gesetzt)");
					pos2.setItemMeta(pos2meta);
					player.getInventory().setItem(8, pos2);
					
					
				}
			}
			
		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 20);
		
	}
}
