package me.eliteSchwein.bedwars;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class mapengine  implements Listener {
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    @SuppressWarnings("unused")
	private bedwars plugin;
	public mapengine(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	public static boolean tier3vote = false;
	public static boolean randomevnvote = true;
	public static boolean allowtier3vote = true;
	public static boolean allowrandomevnvote = true;
	public static ArrayList<UUID> tier3votedn = new ArrayList<UUID>();
	public static ArrayList<UUID> tier3votedy = new ArrayList<UUID>();
	public static ArrayList<UUID> randomevnvotedn = new ArrayList<UUID>();
	public static ArrayList<UUID> randomevnvotedy = new ArrayList<UUID>();
	
	public static Location redbed1 = new Location(null, 0, 0, 0);
	public static Location redbed2 = new Location(null, 0, 0, 0);
	public static Location bluebed1 = new Location(null, 0, 0, 0);
	public static Location bluebed2 = new Location(null, 0, 0, 0);
	public static Location greenbed1 = new Location(null, 0, 0, 0);
	public static Location greenbed2 = new Location(null, 0, 0, 0);
	public static Location yellowbed1 = new Location(null, 0, 0, 0);
	public static Location yellowbed2 = new Location(null, 0, 0, 0);
	public static Location purplebed1 = new Location(null, 0, 0, 0);
	public static Location purplebed2 = new Location(null, 0, 0, 0);
	public static Location magentabed1 = new Location(null, 0, 0, 0);
	public static Location magentabed2 = new Location(null, 0, 0, 0);
	public static Location whitebed1 = new Location(null, 0, 0, 0);
	public static Location whitebed2 = new Location(null, 0, 0, 0);
	public static Location blackbed1 = new Location(null, 0, 0, 0);
	public static Location blackbed2 = new Location(null, 0, 0, 0);
	
	public static Location redshop = new Location(null, 0, 0, 0);
	public static Location blueshop = new Location(null, 0, 0, 0);
	public static Location greenshop = new Location(null, 0, 0, 0);
	public static Location yellowshop = new Location(null, 0, 0, 0);
	public static Location purpleshop = new Location(null, 0, 0, 0);
	public static Location magentashop = new Location(null, 0, 0, 0);
	public static Location whiteshop = new Location(null, 0, 0, 0);
	public static Location blackshop = new Location(null, 0, 0, 0);
	
	public static Location redspawn = new Location(null, 0, 0, 0);
	public static Location bluespawn = new Location(null, 0, 0, 0);
	public static Location greenspawn = new Location(null, 0, 0, 0);
	public static Location yellowspawn = new Location(null, 0, 0, 0);
	public static Location purplespawn = new Location(null, 0, 0, 0);
	public static Location magentaspawn = new Location(null, 0, 0, 0);
	public static Location whitespawn = new Location(null, 0, 0, 0);
	public static Location blackspawn = new Location(null, 0, 0, 0);
	
	public static ArrayList<Location> tier1spawner = new ArrayList<Location>();
	public static ArrayList<Location> tier2spawner = new ArrayList<Location>();
	public static ArrayList<Location> tier3spawner = new ArrayList<Location>();
	
	public static Location gamepos1 = new Location(null, 0, 0, 0);
	public static Location gamepos2 = new Location(null, 0, 0, 0);
	
	public static boolean forcerandomevn = false;
	public static String forcerandomevnwhich = null;
	
	@SuppressWarnings("unchecked")
	public static void loadmap(String mapname) {

		File filemapdata = new File("plugins/bedwars/mapconfigs/"+mapname , "config.yml");
        FileConfiguration cfgmapdata = YamlConfiguration.loadConfiguration(filemapdata);
       
        	ArrayList<Double> x = (ArrayList<Double>) cfgmapdata.getList("tier3spawner.x");
        	int size = x.size();
        	ArrayList<Double> y = (ArrayList<Double>) cfgmapdata.getList("tier3spawner.y");
        	ArrayList<Double> z = (ArrayList<Double>) cfgmapdata.getList("tier3spawner.z");
        	String worldname = null;
        	for(Player p : Bukkit.getOnlinePlayers()) {
        		worldname=p.getWorld().getName();
        	}
        	for(int i = 0;i<size;i++) {
        		tier3spawner.add(new Location(Bukkit.getWorld(worldname),x.get(i),y.get(i),z.get(i)));
        	}
        
       
    	ArrayList<Double> x2 = (ArrayList<Double>) cfgmapdata.getList("tier2spawner.x");
    	int size2 = x2.size();
    	ArrayList<Double> y2 = (ArrayList<Double>) cfgmapdata.getList("tier2spawner.y");
    	ArrayList<Double> z2 = (ArrayList<Double>) cfgmapdata.getList("tier2spawner.z");
    	String worldname2 = null;
    	for(Player p : Bukkit.getOnlinePlayers()) {
    		worldname2=p.getWorld().getName();
    	}
    	for(int i = 0;i<size2;i++) {
    		tier2spawner.add(new Location(Bukkit.getWorld(worldname2),x2.get(i),y2.get(i),z2.get(i)));
    	}

    	ArrayList<Double> x1 = (ArrayList<Double>) cfgmapdata.getList("tier1spawner.x");
    	int size1 = x1.size();
    	ArrayList<Double> y1 = (ArrayList<Double>) cfgmapdata.getList("tier1spawner.y");
    	ArrayList<Double> z1 = (ArrayList<Double>) cfgmapdata.getList("tier1spawner.z");
    	String worldname1 = null;
    	for(Player p : Bukkit.getOnlinePlayers()) {
    		worldname1=p.getWorld().getName();
    	}
    	for(int i = 0;i<size1;i++) {
    		tier1spawner.add(new Location(Bukkit.getWorld(worldname1),x1.get(i),y1.get(i),z1.get(i)));
    	}
        try {
    	try {
    	redbed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("red.bed.x"),cfgmapdata.getInt("red.bed.y"),cfgmapdata.getInt("red.bed.z"));
    	redbed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("red.bed.x1"),cfgmapdata.getInt("red.bed.y1"),cfgmapdata.getInt("red.bed.z1"));
    	bluebed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("blue.bed.x"),cfgmapdata.getInt("blue.bed.y"),cfgmapdata.getInt("blue.bed.z"));
    	bluebed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("blue.bed.x1"),cfgmapdata.getInt("blue.bed.y1"),cfgmapdata.getInt("blue.bed.z1"));
    	greenbed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("green.bed.x"),cfgmapdata.getInt("green.bed.y"),cfgmapdata.getInt("green.bed.z"));
    	greenbed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("green.bed.x1"),cfgmapdata.getInt("green.bed.y1"),cfgmapdata.getInt("green.bed.z1"));
    	yellowbed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("yellow.bed.x"),cfgmapdata.getInt("yellow.bed.y"),cfgmapdata.getInt("yellow.bed.z"));
    	yellowbed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("yellow.bed.x1"),cfgmapdata.getInt("yellow.bed.y1"),cfgmapdata.getInt("yellow.bed.z1"));
    	purplebed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("purple.bed.x"),cfgmapdata.getInt("purple.bed.y"),cfgmapdata.getInt("purple.bed.z"));
    	purplebed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("purple.bed.x1"),cfgmapdata.getInt("purple.bed.y1"),cfgmapdata.getInt("purple.bed.z1"));
    	magentabed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("magenta.bed.x"),cfgmapdata.getInt("magenta.bed.y"),cfgmapdata.getInt("magenta.bed.z"));
    	magentabed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("magenta.bed.x1"),cfgmapdata.getInt("magenta.bed.y1"),cfgmapdata.getInt("magenta.bed.z1"));
    	whitebed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("white.bed.x"),cfgmapdata.getInt("white.bed.y"),cfgmapdata.getInt("white.bed.z"));
    	whitebed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("white.bed.x1"),cfgmapdata.getInt("white.bed.y1"),cfgmapdata.getInt("white.bed.z1"));
    	blackbed1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("black.bed.x"),cfgmapdata.getInt("black.bed.y"),cfgmapdata.getInt("black.bed.z"));
    	blackbed2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("black.bed.x1"),cfgmapdata.getInt("black.bed.y1"),cfgmapdata.getInt("black.bed.z1"));
    	}catch (NullPointerException en) {
    		
    	}
    	try {
        	redshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("red.shop.x"),cfgmapdata.getInt("red.shop.y"),cfgmapdata.getInt("red.shop.z"));
        	redshop.add(0.5, 0, 0.5);
        	blueshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("blue.shop.x"),cfgmapdata.getInt("blue.shop.y"),cfgmapdata.getInt("blue.shop.z"));
        	blueshop.add(0.5, 0, 0.5);
        	greenshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("green.shop.x"),cfgmapdata.getInt("green.shop.y"),cfgmapdata.getInt("green.shop.z"));
        	greenshop.add(0.5, 0, 0.5);
        	yellowshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("yellow.shop.x"),cfgmapdata.getInt("yellow.shop.y"),cfgmapdata.getInt("yellow.shop.z"));
        	yellowshop.add(0.5, 0, 0.5);
        	purpleshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("purple.shop.x"),cfgmapdata.getInt("purple.shop.y"),cfgmapdata.getInt("purple.shop.z"));
        	purpleshop.add(0.5, 0, 0.5);
        	magentashop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("magenta.shop.x"),cfgmapdata.getInt("magenta.shop.y"),cfgmapdata.getInt("magenta.shop.z"));
        	magentashop.add(0.5, 0, 0.5);
        	whiteshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("white.shop.x"),cfgmapdata.getInt("white.shop.y"),cfgmapdata.getInt("white.shop.z"));
        	whiteshop.add(0.5, 0, 0.5);
        	blackshop=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("black.shop.x"),cfgmapdata.getInt("black.shop.y"),cfgmapdata.getInt("black.shop.z"));
        	blackshop.add(0.5, 0, 0.5);
        }catch (NullPointerException en) {
        		
        }
    	try {
        	redspawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("red.spawn.x"),cfgmapdata.getInt("red.spawn.y"),cfgmapdata.getInt("red.spawn.z"));
        	redspawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.red.getDisplayName(), redspawn.clone());
        	bluespawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("blue.spawn.x"),cfgmapdata.getInt("blue.spawn.y"),cfgmapdata.getInt("blue.spawn.z"));
        	bluespawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.blue.getDisplayName(), bluespawn.clone());
        	greenspawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("green.spawn.x"),cfgmapdata.getInt("green.spawn.y"),cfgmapdata.getInt("green.spawn.z"));
        	greenspawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.green.getDisplayName(), greenspawn.clone());
        	yellowspawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("yellow.spawn.x"),cfgmapdata.getInt("yellow.spawn.y"),cfgmapdata.getInt("yellow.spawn.z"));
        	yellowspawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.yellow.getDisplayName(), yellowspawn.clone());
        	purplespawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("purple.spawn.x"),cfgmapdata.getInt("purple.spawn.y"),cfgmapdata.getInt("purple.spawn.z"));
        	purplespawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.purple.getDisplayName(), purplespawn.clone());
        	magentaspawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("magenta.spawn.x"),cfgmapdata.getInt("magenta.spawn.y"),cfgmapdata.getInt("magenta.spawn.z"));
        	magentaspawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.magenta.getDisplayName(), magentaspawn.clone());
        	whitespawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("white.spawn.x"),cfgmapdata.getInt("white.spawn.y"),cfgmapdata.getInt("white.spawn.z"));
        	whitespawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.white.getDisplayName(), whitespawn.clone());
        	blackspawn=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getInt("black.spawn.x"),cfgmapdata.getInt("black.spawn.y"),cfgmapdata.getInt("black.spawn.z"));
        	blackspawn.add(0.5, 0, 0.5);
        	spectator.specstands.put("Team "+teams.black.getDisplayName(), blackspawn.clone());
        }catch (NullPointerException en) {
        		
        }
        }catch(NullPointerException en) {
        	
        }
    	gamepos1=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getDouble("gamepos1.x"),cfgmapdata.getDouble("gamepos1.y"),cfgmapdata.getDouble("gamepos1.z"));
    	gamepos2=new Location(Bukkit.getWorld(worldname1),cfgmapdata.getDouble("gamepos2.x"),cfgmapdata.getDouble("gamepos2.y"),cfgmapdata.getDouble("gamepos2.z"));
        setbeds();
	}
	
	
	
	public static void setbeds() {
		String mapname = gameengine.votedmap;
		try {
		if(redbed2.getWorld()!=null) {
			if(teams.red.getEntries().size()!=0) {
				schematic.paste("redbed", redbed1, mapname);
			}
		}
		if(bluebed2.getWorld()!=null) {
			if(teams.blue.getEntries().size()!=0) {
			schematic.paste("bluebed", bluebed1, mapname);
			}
		}
		if(greenbed2.getWorld()!=null) {
			if(teams.green.getEntries().size()!=0) {
			schematic.paste("greenbed", greenbed1, mapname);
			}
		}
		if(yellowbed2.getWorld()!=null) {
			if(teams.yellow.getEntries().size()!=0) {
			schematic.paste("yellowbed", yellowbed1, mapname);
			}
		}
		if(purplebed2.getWorld()!=null) {
			if(teams.purple.getEntries().size()!=0) {
			schematic.paste("purplebed", purplebed1, mapname);
			}
		}
		if(magentabed2.getWorld()!=null) {
			if(teams.magenta.getEntries().size()!=0) {
			schematic.paste("magentabed", magentabed1, mapname);
			}
		}
		if(whitebed2.getWorld()!=null) {
			if(teams.white.getEntries().size()!=0) {
			schematic.paste("whitebed", whitebed1, mapname);
			}
		}
		if(blackbed2.getWorld()!=null) {
			if(teams.black.getEntries().size()!=0) {
			schematic.paste("blackbed", blackbed1, mapname);
			}
		}}catch(NullPointerException en) {
			
		}
	}
	public static void resetmap() {
		bedwars.setSquare(gamepos1, gamepos2);
	
	}
	public static void tier3finalvote() {
		ArrayList<UUID> onlineplayer = new ArrayList<UUID>();
		for(Player p:Bukkit.getOnlinePlayers()) {
			onlineplayer.add(p.getUniqueId());
		}
		int votesize = tier3votedn.size();
		int playersize = onlineplayer.size();
		int halfplayersize = playersize/2;
		if(votesize>=halfplayersize) {
			tier3vote=true;
		}else {
			tier3vote=false;
		}
	}
	public static void randomevnfinalvote() {
		if(forcerandomevn==true) {
			bedwars.chatconsole("§3§m--[§r§bRandom Event Forced!!§r§3§m]--");
			randomevnvote=true;
			return;
		}
		ArrayList<UUID> onlineplayer = new ArrayList<UUID>();
		for(Player p:Bukkit.getOnlinePlayers()) {
			onlineplayer.add(p.getUniqueId());
		}
		int votesize = randomevnvotedy.size();
		int playersize = onlineplayer.size();
		int halfplayersize = playersize/2;
		if(votesize>=halfplayersize) {
			bedwars.chatconsole("§2§m--[§r§aRandom Event Voted§r§2§m]--");
			randomevnvote=true;
		}else {
			randomevnvote=false;
			bedwars.chatconsole("§4§m--[§r§cRandom Event Unvoted§r§4§m]--");
		}
	}
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		e.setCancelled(true);
	}
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		UUID puuid = p.getUniqueId();
		if(tier3votedn.contains(puuid)) {
			tier3votedn.remove(puuid);
		}
		if(tier3votedy.contains(puuid)) {
			tier3votedy.remove(puuid);
		}
		
	}
	
}
