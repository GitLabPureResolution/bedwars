package me.eliteSchwein.bedwars;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_8_R3.WorldServer;

public class spectator {
	public static ConcurrentHashMap<Integer, String> entityIds = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, Location> specstands = new ConcurrentHashMap<>();
	public static void SpecTest(Player p) {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Inventory ses;
		ses = p.getServer().createInventory(null,18, "§8>>§aSpec Menü");
		

		new BukkitRunnable() {

			@Override
			public void run() {
				p.openInventory(ses);
				
			}
			
		}.runTaskLater(plugin, 2);
		
		
	}
public static void removeArmorStand(Player player, ConcurrentHashMap<Integer, ?> map){


    Iterator<Integer> it = map.keySet().iterator();
    while(it.hasNext()){
        Integer item = it.next();
        if(map.get(item).equals(player.getUniqueId().toString())){
            PacketPlayOutEntityDestroy pa = new PacketPlayOutEntityDestroy(item);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(pa);
            it.remove();

        }
    }
}
public static void addArmorStand(Player p, Location loc, ConcurrentHashMap<Integer, String> map, String name){



    WorldServer s = ((CraftWorld)p.getWorld()).getHandle();
    EntityArmorStand stand = new EntityArmorStand(s);

    stand.setLocation(loc.getX(), loc.getY() + 0.5, loc.getZ(), 0, 0);
    stand.setCustomName(name);
    stand.setCustomNameVisible(true);
    stand.setGravity(true);
    stand.setSmall(true);
    stand.setInvisible(true);
    
    PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
    ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
    
    map.put(stand.getId(), p.getUniqueId().toString());
}
public static void moveArmorStand(Player player, ConcurrentHashMap<Integer, String> map, String name,Location loc) {
	for(Integer i : map.keySet()){
        if(map.get(i).equals(player.getUniqueId().toString())){
            PacketPlayOutEntityDestroy pa = new PacketPlayOutEntityDestroy(i);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(pa);
            map.remove(i);
        	WorldServer s = ((CraftWorld)player.getWorld()).getHandle();
        	EntityArmorStand stand = new EntityArmorStand(s);
        	
        	
        	stand.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
            stand.setCustomName(name);
            stand.setCustomNameVisible(true);
            stand.setGravity(true);
            stand.setSmall(true);
            stand.setInvisible(true);
            
            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
            
            map.put(stand.getId(), player.getUniqueId().toString());
        }
	}
}
public static void editArmorStand(Player player, ConcurrentHashMap<Integer, ?> map, String name){

    for(Integer i : map.keySet()){
        if(map.get(i).equals(player.getUniqueId().toString())){
            DataWatcher daw = new DataWatcher(null);
            daw.a(2, name);
            PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(i, daw, true);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
        }
    }

}
}
