package me.eliteSchwein.bedwars;

import java.io.IOException;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class savelobby {
	
    static HeadDatabaseAPI api = new HeadDatabaseAPI();
	
	public static int teamsize = bedwars.cfgmain.getInt("teamamount");
	public static int teammembers = bedwars.cfgmain.getInt("teamplayersize");
	
	public static void start(Player player) {
		ArrayList<String> Loadingbar = new ArrayList<String>();
		Loadingbar.add("8840");
		Loadingbar.add("8829");
		ArrayList<String> Loadingbartitle = new ArrayList<String>();
		Loadingbartitle.add("ArmorStands");
		Loadingbartitle.add("LobbySpawn");
		for(int i = 0;i<Loadingbar.size();i++) {
			String teamname = Loadingbartitle.get(i);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(" ");
			ItemStack team = api.getItemHead(Loadingbar.get(i));
			ItemMeta teammeta = team.getItemMeta();
			teammeta.setDisplayName(teamname);
			teammeta.setLore(lore);
			team.setItemMeta(teammeta);
			player.getInventory().setItem(i, team);
		}
		new BukkitRunnable() {
			int timer = 0;

			@Override
			public void run() {
				if(timer==0) {
					
					if(scanArmorStands(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10269");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9404");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						bedwars.cfglobbydata.set("", null);
						cancel();
						failedScan(player);
						
						player.sendMessage(bedwars.MessageNoPerm+"Bitte ArmorStands fixen");
					}
					timer++;
					
				}else
				if(timer==1) {
					if(scanSpawn(player)==true) {
						player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("10258");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						for(Entity eta : player.getWorld().getEntities()) {
							if(eta instanceof ArmorStand) {
								if(eta.getCustomName().equalsIgnoreCase("LobbySpawn")) {
									Location lobby = eta.getLocation();
									double x = lobby.getX();
									double y = lobby.getY();
									double z = lobby.getZ();
									float yaw = lobby.getYaw();
									float pitch = lobby.getPitch();
									bedwars.cfglobbydata.set("Lobby.X", x);
									bedwars.cfglobbydata.set("Lobby.Y", y);
									bedwars.cfglobbydata.set("Lobby.Z", z);
									bedwars.cfglobbydata.set("Lobby.YAW", yaw);
									bedwars.cfglobbydata.set("Lobby.PITCH", pitch);
								}
							}
						}
					}else {
						player.playSound(player.getLocation(), Sound.ZOMBIE_METAL, 1, 1);
						String teamname = Loadingbartitle.get(timer);
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(" ");
						ItemStack team = api.getItemHead("9387");
						ItemMeta teammeta = team.getItemMeta();
						teammeta.setDisplayName(teamname);
						teammeta.setLore(lore);
						team.setItemMeta(teammeta);
						player.getInventory().setItem(timer, team);
						bedwars.cfglobbydata.set("", null);
						cancel();
						failedScan(player);
						player.sendMessage(bedwars.MessageNoPerm+"Bitte Spawn fixen");
					}
					timer++;
					
				}else if(timer==2){
					player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
					ItemStack save = api.getItemHead("845");
					ItemMeta savemeta = save.getItemMeta();
					savemeta.setDisplayName("§rStatus (Gescannt ohne Fehler)");
					save.setItemMeta(savemeta);
					player.getInventory().setItem(8, save);
					player.sendMessage(bedwars.MessagePerm+"Scan war erfolgreich!");
					try {
						bedwars.cfglobbydata.save(bedwars.filelobbydata);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					cancel();
					new BukkitRunnable() {

						@Override
						public void run() {
							try {
								event_makelobby.saveConfig();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							player.getInventory().clear();
							teams.saverlobby.removeEntry(player.getName());
							teams.build.addEntry(player.getName());
							
						}
						
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 20);
					
				}else {
					timer++;
				}
				
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 20, 20);
		
	}
	public static boolean scanArmorStands(Player player) {
		int minandmax = teamsize*teammembers;
		for(String key : bedwars.cfglobbydata.getConfigurationSection("X.teamstand").getKeys(false)) {
			String remove1 = key.replaceAll(",", "");
			String remove2 = remove1.replaceAll(" ", "");
			String remove3 = remove2.replace("[", "");
			String remove4 = remove3.replace("]", "");
			if(remove4.length()==minandmax) {
				return true;
			}else {
				return false;
			}
		}
		return false;
		
		
	}
	public static boolean scanSpawn(Player player) {
		int minandmax = 1;
		int amount = 0;
		World w = player.getWorld();
		for(Entity et : w.getEntities()) {
			if(et instanceof ArmorStand) {
				String nametag = et.getCustomName();
				if(nametag.contains("LobbySpawn")) {
					amount=amount+1;
				}
			}
			
		}
		if(amount==minandmax) {

			return true;
		}else {
			return false;
			
		}
		
	}
	
	public static void failedScan(Player player) {
		new BukkitRunnable() {

			@Override
			public void run() {
				for(int is = 0;is<9;is++) {
					ItemStack spacer = new ItemStack(Material.AIR,1);
					player.getInventory().setItem(is, spacer);
					
				}
				player.setCustomName("general");
				event_makelobby.playerwhouse=player.getName();
				teams.saverlobby.addEntry(player.getName());
				player.getInventory().clear();
				ItemStack teamselector =  api.getItemHead("10358");
				ItemMeta teamselectormeta = teamselector.getItemMeta();
				teamselectormeta.setDisplayName("§rTeam auswählen");
				teamselector.setItemMeta(teamselectormeta);
				teamselector.setAmount(1);
				player.getInventory().setItem(0, teamselector);
				
				ItemStack save = api.getItemHead("584");
				ItemMeta savemeta = save.getItemMeta();
				savemeta.setDisplayName("§rSpeichern");
				save.setItemMeta(savemeta);
				player.getInventory().setItem(8, save);
				if(event_makelobby.LobbySpawn==true) {
					ItemStack pos2 = api.getItemHead("10258");
					ItemMeta pos2meta = pos2.getItemMeta();
					pos2meta.setDisplayName("§rLobbySpawn (gesetzt)");
					pos2.setItemMeta(pos2meta);
					player.getInventory().setItem(7, pos2);
						
					}else {
						
						ItemStack pos2 = api.getItemHead("10848");
						ItemMeta pos2meta = pos2.getItemMeta();
						pos2meta.setDisplayName("§rLobbySpawn");
						pos2.setItemMeta(pos2meta);
						player.getInventory().setItem(7, pos2);
					}
			}
			
		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 20);
		
	}
}
