package me.eliteSchwein.bedwars;


import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.arcaniax.hdb.api.HeadDatabaseAPI;



public class command_makelobby implements CommandExecutor{

    HeadDatabaseAPI api = new HeadDatabaseAPI();
	@SuppressWarnings("unused")
	private bedwars plugin;

	public command_makelobby(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
		if((sender instanceof org.bukkit.entity.Player)){
			final org.bukkit.entity.Player p = (org.bukkit.entity.Player)sender;
			if(p.hasPermission("bedwars.makelobby")) {
				if(event_makemap.playerwhouse==null) {
				if(teams.saverlobby.hasEntry(p.getName())) {
					p.sendMessage(bedwars.MessageNoPerm+"Du bist raus aus dem Lobby Speicher Modus");
					teams.saverlobby.removeEntry(p.getName());
					teams.build.addEntry(p.getName());
					p.getInventory().clear();
					event_makelobby.playerwhouse=null;
				}else {
					p.setCustomName("general");
					event_makelobby.playerwhouse=p.getName();
					teams.saverlobby.addEntry(p.getName());
					p.getInventory().clear();
					ItemStack teamselector =  api.getItemHead("10358");
					ItemMeta teamselectormeta = teamselector.getItemMeta();
					teamselectormeta.setDisplayName("§rTeam auswählen");
					teamselector.setItemMeta(teamselectormeta);
					teamselector.setAmount(1);
					p.getInventory().setItem(0, teamselector);
					
					ItemStack save = api.getItemHead("584");
					ItemMeta savemeta = save.getItemMeta();
					savemeta.setDisplayName("§rFertig?");
					save.setItemMeta(savemeta);
					p.getInventory().setItem(8, save);
					
				
				
				}
				
				
				
				}else if(p.getName().equalsIgnoreCase(event_makelobby.playerwhouse)) {
					if(teams.saverlobby.hasEntry(p.getName())) {
						p.sendMessage(bedwars.MessageNoPerm+"Du bist raus aus dem Lobby Speicher Modus");
						teams.saverlobby.removeEntry(p.getName());
						teams.build.addEntry(p.getName());
						p.getInventory().clear();
					}
					event_makelobby.playerwhouse=null;
				}else {
					p.sendMessage(bedwars.MessageNoPerm+"Speicher Modus ist belegt");
					
					
				}
			}else{
				p.sendMessage(bedwars.MessageNoPerm+"Du hast das Recht dafür nicht");
			}
		}else{
			bedwars.chatconsole("§cOnly Player command!");
		}
		return false;
	}

}

