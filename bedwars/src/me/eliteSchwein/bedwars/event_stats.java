package me.eliteSchwein.bedwars;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Team;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class event_stats implements Listener{
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    @SuppressWarnings("unused")
	private bedwars plugin;
	public event_stats(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	public static HashMap<String,Long> stats = new HashMap<String,Long>();
	public static HashMap<String,Double> statskd = new HashMap<String,Double>();
	public static void setbedbreak(Player p) {
				long oldplayed = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert");
				event_stats.stats.put(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert", newplayed);
		
	}
	public static void addwin(Player p) {


		long oldplayed = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen");
		long newplayed = oldplayed +1;
		event_stats.stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen");
		event_stats.stats.put(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen", newplayed);


		long oldplayed1 = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gespielt");
		long newplayed1 = oldplayed1-newplayed;
		event_stats.stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Verloren");
		event_stats.stats.put(p.getUniqueId().toString()+"Bedwars.Global.Verloren", newplayed1);
		
	}
	public static void setlose(Player p) {


		long oldplayed = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen");


		long oldplayed1 = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gespielt");
		long newplayed1 = oldplayed1-oldplayed;
		event_stats.stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Verloren");
		event_stats.stats.put(p.getUniqueId().toString()+"Bedwars.Global.Verloren", newplayed1);
		
	}
	public static void addmoney(Player p,int amount) {


		long oldplayed = event_stats.stats.get(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient");
		long newplayed = oldplayed +(long)amount;
		event_stats.stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient");
		event_stats.stats.put(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient", newplayed);
	}
	@EventHandler
	public void kill(EntityDeathEvent e) {
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			Entity Entity_target = e.getEntity();
			Entity Entity_murder = e.getEntity().getKiller();
			if(Entity_target instanceof Player&&Entity_murder instanceof Player) {
				boolean teamt =false;
				boolean teamm = false;
				for(Team teamtt : gametimer.startedteams) {
					if(teamtt.hasEntry(Entity_target.getName())) {
						teamt=true;
					}
					if(teamtt.hasEntry(Entity_murder.getName())) {
						teamm=true;
					}
				}
				if(teamt==true&&teamm==true) {

					long oldplayed = event_stats.stats.get(Entity_target.getUniqueId().toString()+"Bedwars.Tode");
					long newplayed = oldplayed +1;
					event_stats.stats.remove(Entity_target.getUniqueId().toString()+"Bedwars.Tode");
					event_stats.stats.put(Entity_target.getUniqueId().toString()+"Bedwars.Tode", newplayed);
					
					long oldkd = event_stats.stats.get(Entity_target.getUniqueId().toString()+"Bedwars.Kills");
					double newkd = (double)oldkd/(double)newplayed;
					event_stats.statskd.remove(Entity_target.getUniqueId().toString()+"Bedwars.KD");
					event_stats.statskd.put(Entity_target.getUniqueId().toString()+"Bedwars.KD", newkd);
					

					long oldplayed1 = event_stats.stats.get(Entity_murder.getUniqueId().toString()+"Bedwars.Kills");
					long newplayed1 = oldplayed1 +1;
					event_stats.stats.remove(Entity_murder.getUniqueId().toString()+"Bedwars.Kills");
					event_stats.stats.put(Entity_murder.getUniqueId().toString()+"Bedwars.Kills", newplayed1);
					
					long oldkd1 = event_stats.stats.get(Entity_target.getUniqueId().toString()+"Bedwars.Tode");
					double newkd1 = (double)oldkd1/(double)newplayed1;
					event_stats.statskd.remove(Entity_murder.getUniqueId().toString()+"Bedwars.KD");
					event_stats.statskd.put(Entity_murder.getUniqueId().toString()+"Bedwars.KD", newkd1);
				}
			}
			
		}
	}
	public static void kdsett(UUID uuid) {
		long death = event_stats.stats.get(uuid.toString()+"Bedwars.Tode");
		
		long kill = event_stats.stats.get(uuid.toString()+"Bedwars.Kills");
		
		double kd = (double)kill/(double)death;
		if(Double.isNaN(kd)) {
			kd=0;
		}
		if(Double.isInfinite(kd)) {
			kd=0;
		}
		me.eliteSchwein.PRcore_addon.stats.setstatsdouble(uuid, "Bedwars.KD", kd);
	}
	@EventHandler
	public void invclick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		try {
		String title = e.getClickedInventory().getTitle();
		if(title.contains("'s Stats")) {
			e.setCancelled(true);
			String itemname = e.getCurrentItem().getItemMeta().getDisplayName();
			if(itemname.equalsIgnoreCase("§cExit")) {
				p.closeInventory();
			}
			
		}}catch(NullPointerException en) {
			
		}
	}
	@EventHandler
	public void leave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		setlose(p);
		if(teams.spectator.hasEntry(p.getName())) {
			
		}else if(timer.runningtimer.equalsIgnoreCase("game")) {
			long oldrage = stats.get(p.getUniqueId().toString()+"Bedwars.Global.Verlassen");
			long newrage = oldrage+1;
			stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Verlassen");
			stats.put(p.getUniqueId().toString()+"Bedwars.Global.Verlassen",newrage);
			
		}
		kdsett(p.getUniqueId());
//		me.eliteSchwein.PRcore_addon.stats.setstatsdouble(p.getUniqueId(), "Bedwars.KD", statskd.get(p.getUniqueId().toString()+"Bedwars.KD"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Kills", stats.get(p.getUniqueId().toString()+"Bedwars.Kills"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Tode", stats.get(p.getUniqueId().toString()+"Bedwars.Tode"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Bed_Zerstoert", stats.get(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Global.Gespielt", stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gespielt"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Global.Gewonnen", stats.get(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Global.Verloren", stats.get(p.getUniqueId().toString()+"Bedwars.Global.Verloren"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Global.Verlassen", stats.get(p.getUniqueId().toString()+"Bedwars.Global.Verlassen"));
		me.eliteSchwein.PRcore_addon.stats.setstats(p.getUniqueId(), "Bedwars.Global.Coins_Verdient", stats.get(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient"));

		
		
		stats.remove(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Kills");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Tode");
		statskd.remove(p.getUniqueId().toString()+"Bedwars.KD");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Gespielt");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Verloren");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Verlassen");
		stats.remove(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient");
	}
	@EventHandler
	public void join(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Global").size()==0){
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Bed_Zerstoert", "&a->_Betten_zerstört:_&c", 0, 1);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Kills", "&a->_Kills:_&c", 0, 1);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Tode", "&a->_Tode:_&c", 0, 1);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.KD", "&a->_KD:_&c", 0, 1);
			
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Gespielt", "&a->_Generell_gespielt:_&c", 0, 1);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Spacer1", "&e-------------------", 0, 2); //SPACER1
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Gewonnen", "&a->_Gewonnen:_&c", 0, 3);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Verloren", "&a->_Verloren:_&c", 0, 4);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Verlassen", "&a->_RageQuit:_&c", 0, 5);
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Spacer2", "&e-------------------", 0, 6); //SPACER2
			me.eliteSchwein.PRcore_addon.stats.setstatsmessage(p.getUniqueId(), "Bedwars.Global.Coins_Verdient", "&a->_Coins_verdient:_&c",0, 7);
			
		}
		statskd.put(p.getUniqueId().toString()+"Bedwars.KD", 0.0);
		stats.put(p.getUniqueId().toString()+"Bedwars.Bed_Zerstoert", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Bed_Zerstoert"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Kills", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Kills"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Tode", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Tode"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Global.Gespielt", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Global.Gespielt"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Global.Gewonnen", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Global.Gewonnen"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Global.Verloren", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Global.Verloren"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Global.Verlassen", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Global.Verlassen"));
		stats.put(p.getUniqueId().toString()+"Bedwars.Global.Coins_Verdient", me.eliteSchwein.PRcore_addon.stats.getstats(p.getUniqueId(), "Bedwars.Global.Coins_Verdient"));

		kdsett(p.getUniqueId());
	}

}
