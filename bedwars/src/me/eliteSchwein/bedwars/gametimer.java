package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Team;

import me.eliteSCHW31N.bedwars.randomevents.manager;
import me.eliteSCHW31N.bedwars.randomevents.manipulator;
import me.eliteSchwein.PRcore.PRcore;
import me.eliteSchwein.PRcore_addon.PRcore_addon;


public class gametimer {
	public static boolean endgame = false;
	public static int timeg = 0;
	public static int timegm = 0;
	public static int timegs = 0;
	public static int times1 = 0;
	public static int times2 = 0;
	public static int times3 = 0;
	public static int timersidebar = 0;
	public static ArrayList<Location> modiloc = new ArrayList<Location>();
	public static Material t1material = Material.AIR;
	public static Material t2material = Material.AIR;
	public static Material t3material = Material.AIR;
	public static int t1materialsub = 0;
	public static int t2materialsub = 0;
	public static int t3materialsub = 0;
	public static String t1materialname = null;
	public static String t2materialname = null;
	public static String t3materialname = null;
	public static boolean redteam = false;
	public static boolean blueteam = false;
	public static boolean greenteam = false;
	public static boolean yellowteam = false;
	public static boolean purpleteam = false;
	public static boolean magentateam = false;
	public static boolean whiteteam = false;
	public static boolean blackteam = false;
	public static ArrayList<Team> startedteams = new ArrayList<Team>();
	public static ArrayList<Team> restteams = new ArrayList<Team>();
	public static ArrayList<Team> liveteams = new ArrayList<Team>();
	public static Objective sidebar = null;
	public static Team winteam = null;
	public static ArrayList<String> villagercheck = new ArrayList<String>();
	public static ConcurrentHashMap<UUID, Integer> slowdamagelist = new ConcurrentHashMap<UUID, Integer>();
	public static ArrayList<FireworkEffect> winrmdlist = new ArrayList<FireworkEffect>();
	public static int dropamounttier1 = 1;
	public static int dropamounttier2 = 1;
	public static int dropamounttier3 = 1;
	public static boolean restartaftergame = false;
	
	public static void timeslowdamage() {
		for(UUID uuid:slowdamagelist.keySet()) {
			Player p = Bukkit.getPlayer(uuid);
			int timerrr = slowdamagelist.get(uuid);
			if(timerrr==1) {
				p.damage(2);
				slowdamagelist.remove(uuid);
			}else {
				p.damage(2);
				slowdamagelist.replace(uuid, timerrr-1);
			}
		}
	}
	public static void updatesidebar() {
		timeslowdamage();
		String anibar = "§7§m--[§r§f§lBEDWARS§7§m]--";
		if(timersidebar==0) {
			anibar="§7§m--[§r§f§lBEDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==1) {
			anibar="§7§m--[§r§6§lB§f§lEDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==2) {
			anibar="§7§m--[§r§6§lBE§f§lDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==3) {
			anibar="§7§m--[§r§6§lBED§f§lWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==4) {
			anibar="§7§m--[§r§6§lBEDW§f§lARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==5) {
			anibar="§7§m--[§r§6§lBEDWA§f§lRS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==6) {
			anibar="§7§m--[§r§6§lBEDWAR§f§lS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==7) {
			anibar="§7§m--[§r§6§lBEDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==8) {
			anibar="§7§m--[§r§f§lB§6§lEDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==9) {
			anibar="§7§m--[§r§f§lBE§6§lDWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==10) {
			anibar="§7§m--[§r§f§lBED§6§lWARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==11) {
			anibar="§7§m--[§r§f§lBEDW§6§lARS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==12) {
			anibar="§7§m--[§r§f§lBEDWA§6§lRS§7§m]--";
			timersidebar++;
		}else
		if(timersidebar==13) {
			anibar="§7§m--[§r§f§lBEDWAR§6§lS§7§m]--";
			timersidebar=0;
		}
//		teams.board.clearSlot(DisplaySlot.SIDEBAR);
		teams.board.getObjective("sidebar").unregister();
		int secondstominutes = timeg;
//		bedwars.chatconsole(""+secondstominutes);
		double stmc = ((double)secondstominutes)/60;
//		bedwars.chatconsole(""+stmc);
		int stmm = (int)stmc;
		timegm = stmm;
		bedwars.chatconsole("§cGame Minutes: "+stmm);
		int stms = secondstominutes-(stmm*60);
		timegs = stms;
		bedwars.chatconsole("§cGame Seconds: "+stms);
		sidebar = teams.board.registerNewObjective("sidebar", "dummy");
		sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
		sidebar.setDisplayName(anibar);


		Score map;

			map= sidebar.getScore("§7┌§3 Map: §l"+gameengine.votedmap);
		map.setScore(-1);
		Score tier3;
		if(mapengine.tier3vote==true) {

			tier3= sidebar.getScore("§7├§c "+bedwars.cfgmoney.getString("moneytyp.tier3")+" aus");
		}else {

			tier3= sidebar.getScore("§7├§a "+bedwars.cfgmoney.getString("moneytyp.tier3")+" an");
		}
		tier3.setScore(-2);
		Score randomevn;
		if(mapengine.randomevnvote==false) {

			randomevn= sidebar.getScore("§7└§c Random Event:");
		}else {

			randomevn= sidebar.getScore("§7└§a Random Event:");
		}
		randomevn.setScore(-3);
		Score spacer;

		if(mapengine.randomevnvote==false) {

			spacer= sidebar.getScore("§7    └§c aus");
		}else {

			spacer= sidebar.getScore("§7    └§a§l"+manager.selected);
		}
		spacer.setScore(-4);
//		Score time;
		String time=null;
		if(timegm<=9) {
			if(timegs<=9) {
				time = Integer.toString(timegm)+":0"+Integer.toString(timegs);
				
			}else {
				time = Integer.toString(timegm)+":"+Integer.toString(timegs);
				
			}
		}else {
		if(timegs<=9) {
			time = Integer.toString(timegm)+":0"+Integer.toString(timegs);
			
		}else {
			time = Integer.toString(timegm)+":"+Integer.toString(timegs);
			
		}}

		for(Team team:restteams) {
			for(String entry:team.getEntries()) {
				Player teamp = Bukkit.getPlayer(entry);
				String displayn=team.getDisplayName();
				if(teamp.getGameMode()!=GameMode.SPECTATOR) {
					bedwars.sendActionBar(teamp, "§r§4Dein Team: "+displayn+ "§r§e Zeit: §6§l"+time);
				}
			}
		}
//		time.setScore(-4);
		if(startedteams.contains(teams.red)) {
			if(teams.red.getSize()==0) {
				restteams.remove(teams.red);
				Score red = sidebar.getScore("§f☤ "+teams.red.getDisplayName());
				red.setScore(teams.red.getSize());
			}else
			if(liveteams.contains(teams.red)) {
				Score red = sidebar.getScore("§a✔ "+teams.red.getDisplayName());
				red.setScore(teams.red.getSize());
				
			}else {
				Score red = sidebar.getScore("§c✘ "+teams.red.getDisplayName());
				red.setScore(teams.red.getSize());
				
			}
		}
		if(startedteams.contains(teams.blue)) {
			if(teams.blue.getSize()==0) {

				restteams.remove(teams.blue);
				Score blue = sidebar.getScore("§f☤ "+teams.blue.getDisplayName());
				blue.setScore(teams.blue.getSize());
			}else
			if(liveteams.contains(teams.blue)) {
				Score blue = sidebar.getScore("§a✔ "+teams.blue.getDisplayName());
				blue.setScore(teams.blue.getSize());
				
			}else {
				Score blue = sidebar.getScore("§c✘ "+teams.blue.getDisplayName());
				blue.setScore(teams.blue.getSize());
				
			}
		}
		if(startedteams.contains(teams.green)) {

			if(teams.green.getSize()==0) {

				restteams.remove(teams.green);
				Score green = sidebar.getScore("§f☤ "+teams.green.getDisplayName());
				green.setScore(teams.green.getSize());
			}else
			if(liveteams.contains(teams.green)) {
				Score green = sidebar.getScore("§a✔ "+teams.green.getDisplayName());
				green.setScore(teams.green.getSize());
				
			}else {
				Score green = sidebar.getScore("§c✘ "+teams.green.getDisplayName());
				green.setScore(teams.green.getSize());
				
			}
		}
		if(startedteams.contains(teams.yellow)) {

			if(teams.yellow.getSize()==0) {

				restteams.remove(teams.yellow);
				Score yellow = sidebar.getScore("§f☤ "+teams.yellow.getDisplayName());
				yellow.setScore(teams.yellow.getSize());
			}else
			if(liveteams.contains(teams.yellow)) {
				Score yellow = sidebar.getScore("§a✔ "+teams.yellow.getDisplayName());
				yellow.setScore(teams.yellow.getSize());
				
			}else {
				Score yellow = sidebar.getScore("§c✘ "+teams.yellow.getDisplayName());
				yellow.setScore(teams.yellow.getSize());
				
			}
		}
		if(startedteams.contains(teams.purple)) {
			if(teams.purple.getSize()==0) {

				restteams.remove(teams.purple);
				Score purple = sidebar.getScore("§f☤ "+teams.purple.getDisplayName());
				purple.setScore(teams.purple.getSize());
			}else
			if(liveteams.contains(teams.purple)) {
				Score purple = sidebar.getScore("§a✔ "+teams.purple.getDisplayName());
				purple.setScore(teams.purple.getSize());
				
			}else {
				Score purple = sidebar.getScore("§c✘ "+teams.purple.getDisplayName());
				purple.setScore(teams.purple.getSize());
				
			}
		}
		if(startedteams.contains(teams.magenta)) {
			if(teams.magenta.getSize()==0) {

				restteams.remove(teams.magenta);
				Score magenta = sidebar.getScore("§f☤ "+teams.magenta.getDisplayName());
				magenta.setScore(teams.magenta.getSize());
			}else
			if(liveteams.contains(teams.magenta)) {
				Score magenta = sidebar.getScore("§a✔ "+teams.magenta.getDisplayName());
				magenta.setScore(teams.magenta.getSize());
				
			}else {
				Score magenta = sidebar.getScore("§c✘ "+teams.magenta.getDisplayName());
				magenta.setScore(teams.magenta.getSize());
				
			}
		}
		if(startedteams.contains(teams.white)) {
			if(teams.white.getSize()==0) {

				restteams.remove(teams.white);
				Score white = sidebar.getScore("§f☤ "+teams.white.getDisplayName());
				white.setScore(teams.white.getSize());
			}else
			if(liveteams.contains(teams.white)) {
				Score white = sidebar.getScore("§a✔ "+teams.white.getDisplayName());
				white.setScore(teams.white.getSize());
				
			}else {
				Score white = sidebar.getScore("§c✘ "+teams.white.getDisplayName());
				white.setScore(teams.white.getSize());
				
			}
		}
		if(startedteams.contains(teams.black)) {
			if(teams.black.getSize()==0) {

				restteams.remove(teams.black);
				Score black = sidebar.getScore("§f☤ "+teams.black.getDisplayName());
				black.setScore(teams.black.getSize());
			}else
			if(liveteams.contains(teams.black)) {
				Score black = sidebar.getScore("§a✔ "+teams.black.getDisplayName());
				black.setScore(teams.black.getSize());
				
			}else {
				Score black = sidebar.getScore("§c✘ "+teams.black.getDisplayName());
				black.setScore(teams.black.getSize());
				
			}
		}
	}
	public static void makesidebar() {
		int secondstominutes = timeg;
//		bedwars.chatconsole(""+secondstominutes);
		double stmc = ((double)secondstominutes)/60;
//		bedwars.chatconsole(""+stmc);
		int stmm = (int)stmc;
		timegm = stmm;
		bedwars.chatconsole("§cGame Minutes: "+stmm);
		int stms = secondstominutes-(stmm*60);
		timegs = stms;
		bedwars.chatconsole("§cGame Seconds: "+stms);
		sidebar = teams.board.registerNewObjective("sidebar", "dummy");
		sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
		sidebar.setDisplayName("§7§m--[§r§f§lBEDWARS§7§m]--");
		

		Score map;

			map= sidebar.getScore("§7┌§3 Map: §l"+gameengine.votedmap);
		map.setScore(-1);
		Score tier3;
		if(mapengine.tier3vote==true) {

			tier3= sidebar.getScore("§7├§c "+bedwars.cfgmoney.getString("moneytyp.tier3")+" aus");
		}else {

			tier3= sidebar.getScore("§7├§a "+bedwars.cfgmoney.getString("moneytyp.tier3")+" an");
		}
		tier3.setScore(-2);
		Score randomevn;
		if(mapengine.randomevnvote==false) {

			randomevn= sidebar.getScore("§7└§c Random Event:");
		}else {

			randomevn= sidebar.getScore("§7└§a Random Event:");
		}
		randomevn.setScore(-3);
		Score spacer;

		if(mapengine.randomevnvote==false) {

			spacer= sidebar.getScore("§7    └§c aus");
		}else {

			spacer= sidebar.getScore("§7    └§a§l"+manager.selected);
		}
		spacer.setScore(-4);
//		Score time;
		String time=null;
		if(timegm<=9) {
			if(timegs<=9) {
				time = Integer.toString(timegm)+":0"+Integer.toString(timegs);
				
			}else {
				time = Integer.toString(timegm)+":"+Integer.toString(timegs);
				
			}
		}else {
		if(timegs<=9) {
			time = Integer.toString(timegm)+":0"+Integer.toString(timegs);
			
		}else {
			time = Integer.toString(timegm)+":"+Integer.toString(timegs);
			
		}}

		for(Team team:restteams) {
			for(String entry:team.getEntries()) {
				Player teamp = Bukkit.getPlayer(entry);
				String displayn=team.getDisplayName();
				if(teamp.getGameMode()!=GameMode.SPECTATOR) {
					bedwars.sendActionBar(teamp, "§r§4Dein Team: "+displayn+ "§r§e Zeit: §6§l"+time);
				}
			}
		}
//		time.setScore(-4);
		if(startedteams.contains(teams.red)) {
			if(redteam=true) {
				Score red = sidebar.getScore("§a✔ "+teams.red.getDisplayName());
				red.setScore(teams.red.getSize());
				
			}else {
				Score red = sidebar.getScore("§c✘ "+teams.red.getDisplayName());
				red.setScore(teams.red.getSize());
				
			}
		}
		if(startedteams.contains(teams.blue)) {
			if(blueteam=true) {
				Score blue = sidebar.getScore("§a✔ "+teams.blue.getDisplayName());
				blue.setScore(teams.blue.getSize());
				
			}else {
				Score blue = sidebar.getScore("§c✘ "+teams.blue.getDisplayName());
				blue.setScore(teams.blue.getSize());
				
			}
		}
		if(startedteams.contains(teams.green)) {
			if(greenteam=true) {
				Score green = sidebar.getScore("§a✔ "+teams.green.getDisplayName());
				green.setScore(teams.green.getSize());
				
			}else {
				Score green = sidebar.getScore("§c✘ "+teams.green.getDisplayName());
				green.setScore(teams.green.getSize());
				
			}
		}
		if(startedteams.contains(teams.yellow)) {
			if(yellowteam=true) {
				Score yellow = sidebar.getScore("§a✔ "+teams.yellow.getDisplayName());
				yellow.setScore(teams.yellow.getSize());
				
			}else {
				Score yellow = sidebar.getScore("§c✘ "+teams.yellow.getDisplayName());
				yellow.setScore(teams.yellow.getSize());
				
			}
		}
		if(startedteams.contains(teams.purple)) {
			if(purpleteam=true) {
				Score purple = sidebar.getScore("§a✔ "+teams.purple.getDisplayName());
				purple.setScore(teams.purple.getSize());
				
			}else {
				Score purple = sidebar.getScore("§c✘ "+teams.purple.getDisplayName());
				purple.setScore(teams.purple.getSize());
				
			}
		}
		if(startedteams.contains(teams.magenta)) {
			if(magentateam=true) {
				Score magenta = sidebar.getScore("§a✔ "+teams.magenta.getDisplayName());
				magenta.setScore(teams.magenta.getSize());
				
			}else {
				Score magenta = sidebar.getScore("§c✘ "+teams.magenta.getDisplayName());
				magenta.setScore(teams.magenta.getSize());
				
			}
		}
		if(startedteams.contains(teams.white)) {
			if(whiteteam=true) {
				Score white = sidebar.getScore("§a✔ "+teams.white.getDisplayName());
				white.setScore(teams.white.getSize());
				
			}else {
				Score white = sidebar.getScore("§c✘ "+teams.white.getDisplayName());
				white.setScore(teams.white.getSize());
				
			}
		}
		if(startedteams.contains(teams.black)) {
			if(blackteam=true) {
				Score black = sidebar.getScore("§a✔ "+teams.black.getDisplayName());
				black.setScore(teams.black.getSize());
				
			}else {
				Score black = sidebar.getScore("§c✘ "+teams.black.getDisplayName());
				black.setScore(teams.black.getSize());
				
			}
		}
		
	}
	public static void registerTeams() {
		try {
		if(teams.red.getEntries().size()==0) {
		}else {
			for(String entry:teams.red.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.red);
			restteams.add(teams.red);
			liveteams.add(teams.red);
			redteam=true;
		}
		if(teams.blue.getEntries().size()==0) {
		}else {
			for(String entry:teams.blue.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.blue);
			restteams.add(teams.blue);
			liveteams.add(teams.blue);
			blueteam=true;
		}
		if(teams.green.getEntries().size()==0) {
		}else {
			for(String entry:teams.green.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.green);
			restteams.add(teams.green);
			liveteams.add(teams.green);
			greenteam=true;
		}
		if(teams.yellow.getEntries().size()==0) {
		}else {
			for(String entry:teams.yellow.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.yellow);
			restteams.add(teams.yellow);
			liveteams.add(teams.yellow);
			yellowteam=true;
		}
		if(teams.purple.getEntries().size()==0) {
		}else {
			for(String entry:teams.purple.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.purple);
			restteams.add(teams.purple);
			liveteams.add(teams.purple);
			purpleteam=true;
		}
		if(teams.magenta.getEntries().size()==0) {
		}else {
			for(String entry:teams.magenta.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.magenta);
			restteams.add(teams.magenta);
			liveteams.add(teams.magenta);
			magentateam=true;
		}
		if(teams.white.getEntries().size()==0) {
		}else {
			for(String entry:teams.white.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.white);
			restteams.add(teams.white);
			liveteams.add(teams.white);
			whiteteam=true;
		}
		if(teams.black.getEntries().size()==0) {
		}else {
			for(String entry:teams.black.getEntries()) {
				Player ep=Bukkit.getPlayer(entry);
				long oldplayed = event_stats.stats.get(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				long newplayed = oldplayed +1;
				event_stats.stats.remove(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt");
				event_stats.stats.put(ep.getUniqueId().toString()+"Bedwars.Global.Gespielt", newplayed);
			}
			startedteams.add(teams.black);
			restteams.add(teams.black);
			liveteams.add(teams.black);
			blackteam=true;
		}}catch(NullPointerException en){
			
		}
	}

	
	
	
	
	
	
	public static void startgame() {
		makeVillager();
		holdVillager();
		registerTeams();
        FireworkEffect effect1 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(39, 174, 96)).with(Type.STAR).build();
        FireworkEffect effect2 = FireworkEffect.builder().flicker(false).trail(true).withColor(Color.fromRGB(39, 174, 96)).build();
        FireworkEffect effect3 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(39, 174, 96)).with(Type.BURST).build();
        FireworkEffect effect4 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(39, 174, 96)).with(Type.CREEPER).build();
        FireworkEffect effect5 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(39, 174, 96)).with(Type.BALL_LARGE).build();
        
        FireworkEffect effect11 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.WHITE).with(Type.STAR).build();
        FireworkEffect effect21 = FireworkEffect.builder().flicker(false).trail(true).withColor(Color.WHITE).with(Type.BALL).build();
        FireworkEffect effect31 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.WHITE).with(Type.BURST).build();
        FireworkEffect effect41 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.WHITE).with(Type.CREEPER).build();
        FireworkEffect effect51 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.WHITE).with(Type.BALL_LARGE).build();
        
        FireworkEffect effect12 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(81, 206, 0)).with(Type.STAR).build();
        FireworkEffect effect22 = FireworkEffect.builder().flicker(false).trail(true).withColor(Color.fromRGB(81, 206, 0)).with(Type.BALL).build();
        FireworkEffect effect32 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(81, 206, 0)).with(Type.BURST).build();
        FireworkEffect effect42 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(81, 206, 0)).with(Type.CREEPER).build();
        FireworkEffect effect52 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(81, 206, 0)).with(Type.BALL_LARGE).build();
        
        FireworkEffect effect13 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(251, 206, 0)).with(Type.STAR).build();
        FireworkEffect effect23 = FireworkEffect.builder().flicker(false).trail(true).withColor(Color.fromRGB(251, 206, 0)).with(Type.BALL).build();
        FireworkEffect effect33 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(251, 206, 0)).with(Type.BURST).build();
        FireworkEffect effect43 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.fromRGB(251, 206, 0)).with(Type.CREEPER).build();
        FireworkEffect effect53 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.fromRGB(251, 206, 0)).with(Type.BALL_LARGE).build();
        
        FireworkEffect effect14 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.AQUA).with(Type.STAR).build();
        FireworkEffect effect24 = FireworkEffect.builder().flicker(false).trail(true).withColor(Color.AQUA).with(Type.BALL).build();
        FireworkEffect effect34 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.AQUA).with(Type.BURST).build();
        FireworkEffect effect44 = FireworkEffect.builder().flicker(true).trail(true).withColor(Color.AQUA).with(Type.CREEPER).build();
        FireworkEffect effect54 = FireworkEffect.builder().flicker(false).trail(false).withColor(Color.AQUA).with(Type.BALL_LARGE).build();
        
        winrmdlist.add(effect1);
        winrmdlist.add(effect2);
        winrmdlist.add(effect3);
        winrmdlist.add(effect4);
        winrmdlist.add(effect5);
        winrmdlist.add(effect11);
        winrmdlist.add(effect21);
        winrmdlist.add(effect31);
        winrmdlist.add(effect41);
        winrmdlist.add(effect51);
        winrmdlist.add(effect12);
        winrmdlist.add(effect22);
        winrmdlist.add(effect32);
        winrmdlist.add(effect42);
        winrmdlist.add(effect52);
        winrmdlist.add(effect13);
        winrmdlist.add(effect23);
        winrmdlist.add(effect33);
        winrmdlist.add(effect43);
        winrmdlist.add(effect53);
        winrmdlist.add(effect14);
        winrmdlist.add(effect24);
        winrmdlist.add(effect34);
        winrmdlist.add(effect44);
        winrmdlist.add(effect54);
		me.eliteSchwein.PRcore.PRcore.Motdstate(false);
		
		t1material = Material.getMaterial(bedwars.cfgmoney.getString("material.tier1"));
		t2material = Material.getMaterial(bedwars.cfgmoney.getString("material.tier2"));
		t3material = Material.getMaterial(bedwars.cfgmoney.getString("material.tier3"));

		t1materialsub = bedwars.cfgmoney.getInt("subid.tier1");
		t2materialsub = bedwars.cfgmoney.getInt("subid.tier2");
		t3materialsub = bedwars.cfgmoney.getInt("subid.tier3");

		t1materialname = bedwars.cfgmoney.getString("moneytyp.tier1");
		t2materialname = bedwars.cfgmoney.getString("moneytyp.tier2");
		t3materialname = bedwars.cfgmoney.getString("moneytyp.tier3");
		String timeunitg = bedwars.cfgmain.getString("timeunit");
		if(timeunitg.equalsIgnoreCase("SECONDS")) {
			int rawtimeg = bedwars.cfgmain.getInt("time");
			timeg=rawtimeg;
		}
		if(timeunitg.equalsIgnoreCase("MINUTES")) {
			int rawtimeg = bedwars.cfgmain.getInt("time");
			timeg=rawtimeg*60;
			
		}
		if(timeunitg.equalsIgnoreCase("HOURS")) {
			int rawtimeg = bedwars.cfgmain.getInt("time");
			timeg=rawtimeg*60*60;
			
		}
		
		String timeunits1 = bedwars.cfgmain.getString("timetier1unit");
		if(timeunits1.equalsIgnoreCase("SECONDS")) {
			int rawtimes1 = bedwars.cfgmain.getInt("timetier1");
			times1=rawtimes1-1;
		}
		if(timeunits1.equalsIgnoreCase("MINUTES")) {
			int rawtimes1 = bedwars.cfgmain.getInt("timetier1");
			times1=rawtimes1*60-1;
			
		}
		if(timeunits1.equalsIgnoreCase("HOURS")) {
			int rawtimes1 = bedwars.cfgmain.getInt("timetier1");
			times1=rawtimes1*60*60-1;
			
		}
		
		String timeunits2 = bedwars.cfgmain.getString("timetier2unit");
		if(timeunits2.equalsIgnoreCase("SECONDS")) {
			int rawtimes2 = bedwars.cfgmain.getInt("timetier2");
			times2=rawtimes2-1;
		}
		if(timeunits2.equalsIgnoreCase("MINUTES")) {
			int rawtimes2 = bedwars.cfgmain.getInt("timetier2");
			times2=rawtimes2*60-1;
			
		}
		if(timeunits2.equalsIgnoreCase("HOURS")) {
			int rawtimes2 = bedwars.cfgmain.getInt("timetier2");
			times2=rawtimes2*60*60-1;
			
		}
		
		String timeunits3 = bedwars.cfgmain.getString("timetier3unit");
		if(timeunits3.equalsIgnoreCase("SECONDS")) {
			int rawtimes3 = bedwars.cfgmain.getInt("timetier3");
			times3=rawtimes3-1;
		}
		if(timeunits3.equalsIgnoreCase("MINUTES")) {
			int rawtimes3 = bedwars.cfgmain.getInt("timetier3");
			times3=rawtimes3*60-1;
			
		}
		if(timeunits3.equalsIgnoreCase("HOURS")) {
			int rawtimes3 = bedwars.cfgmain.getInt("timetier3");
			times3=rawtimes3*60*60-1;
			
		}
		timer.runningtimer="game";
		dropamounttier1=1;
		dropamounttier2=1;
		dropamounttier3=1;
		event_bed.respawntime=5;
		event_extra.alternatetnt=false;
		event_extra.tntpower=3;
		event_extra.tnttime=50;
		manager.useRDM(manager.selected, true);
		for(Player p: Bukkit.getOnlinePlayers()) {
			p.getInventory().clear();
			if(p.getGameMode()==GameMode.SPECTATOR) {
				
			}else {
				try {
				p.setGameMode(GameMode.SURVIVAL);
				if(teams.red.hasEntry(p.getName())) {
					p.teleport(mapengine.redspawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.blue.hasEntry(p.getName())) {
					p.teleport(mapengine.bluespawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.green.hasEntry(p.getName())) {
					p.teleport(mapengine.greenspawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.yellow.hasEntry(p.getName())) {
					p.teleport(mapengine.yellowspawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.purple.hasEntry(p.getName())) {
					p.teleport(mapengine.purplespawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.magenta.hasEntry(p.getName())) {
					p.teleport(mapengine.magentaspawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.white.hasEntry(p.getName())) {
					p.teleport(mapengine.whitespawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}
				if(teams.black.hasEntry(p.getName())) {
					p.teleport(mapengine.blackspawn);

					if(!manipulator.respawneffects.isEmpty()) {
						for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
							p.addPotionEffect(rdmeventeffect);
						}
					}
				}}catch(NullPointerException en) {
					
				}
			}
		}
		makesidebar();
		new BukkitRunnable() {
			int value1 = times1;
			int value2 = times2;
			int value3 = times3;
			



			@Override
			public void run() {

				Date date = new Date();
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.setTime(date);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				if(hour==0) {
					restartaftergame=true;
				}
				for(World w:Bukkit.getWorlds()) {
					w.setTime(2000);
				}
				if(restteams.size()==1) {
					winteam=restteams.get(0);
					endgame=true;
				}
				if(restteams.size()==0) {
					winteam=null;
					endgame=true;
				}

				bedwars.chatconsole("§3Tier 1 Counter: "+value1);
				if(value1==0) {
					for(int i = 0;i<dropamounttier1;i++) {
						for(Location loc:mapengine.tier1spawner) {
							if(modiloc.contains(loc)) {
								
							}else {
								loc.add(0, 0.5, 0);
								modiloc.add(loc);
								
							}
							
							ItemStack item3s = new ItemStack(t1material, 1, (short)t1materialsub);
							ItemMeta item3 = item3s.getItemMeta();
							ArrayList<String> Text3 = new ArrayList<String>();
					        Text3.add(" ");
					        item3.setLore(Text3);
							item3.setDisplayName(t1materialname);
							item3s.setItemMeta(item3);
							item3s.setAmount(1);
							loc.getWorld().dropItemNaturally(loc, item3s);
						}
					}
					
					value1=times1;
				}else {

					value1--;
				}

				bedwars.chatconsole("§3Tier 2 Counter: "+value2);
				if(value2==0) {
					for(int i = 0;i<dropamounttier2;i++) {
						for(Location loc:mapengine.tier2spawner) {
							if(modiloc.contains(loc)) {
								
							}else {
								loc.add(0, 0.5, 0);
								modiloc.add(loc);
							
							}
						
							ItemStack item3s = new ItemStack(t2material, 1, (short)t2materialsub);
							ItemMeta item3 = item3s.getItemMeta();
							ArrayList<String> Text3 = new ArrayList<String>();
							Text3.add(" ");
							item3.setLore(Text3);
							item3.setDisplayName(t2materialname);
							item3s.setItemMeta(item3);
							item3s.setAmount(1);
							loc.getWorld().dropItemNaturally(loc, item3s);
						}
					}
					value2=times2;
				}else {

					value2--;
				}
				if(mapengine.tier3vote==false) {


					bedwars.chatconsole("§3Tier 3 Counter: "+value3);
					if(value3==0) {

						for(int i = 0;i<dropamounttier3;i++) {
							for(Location loc:mapengine.tier3spawner) {
								if(modiloc.contains(loc)) {
								
								}else {
									loc.add(0, 0.5, 0);
									modiloc.add(loc);
								
								}
							
								ItemStack item3s = new ItemStack(t3material, 1, (short)t3materialsub);
								ItemMeta item3 = item3s.getItemMeta();
								ArrayList<String> Text3 = new ArrayList<String>();
					        	Text3.add(" ");
					        	item3.setLore(Text3);
								item3.setDisplayName(t3materialname);
								item3s.setItemMeta(item3);
								item3s.setAmount(1);
								loc.getWorld().dropItemNaturally(loc, item3s);
							}
						}
						value3=times3;
					}else {

						value3--;
					}
				}
				if(endgame==true) {
					cancel();
					for (Player allplayer :  Bukkit.getOnlinePlayers()) {
						allplayer.getInventory().clear();
//						allplayer.updateInventory();
						allplayer.setExp(0.99f);
						}
						int winmoney = bedwars.cfgmain.getInt("winmoney");
//						int losemoney = bedwars.cfgmain.getInt("winmoney");
						if(winteam==null) {
							for(Player p:Bukkit.getOnlinePlayers()) {

								bedwars.sendTitle(p, "§bAlle Teams haben verlassen", null);
							}
							
						}else {
							for(Player p:Bukkit.getOnlinePlayers()) {
								if(winteam.hasEntry(p.getName())) {
									bedwars.sendTitle(p, "§bWinner: "+winteam.getDisplayName(), "§2+ "+Integer.toString(winmoney)+"PRc");
									PRcore_addon.setMoney(false, p.getName(), winmoney, true);
									event_stats.addmoney(p, winmoney);
									event_stats.addwin(p);
									
								}else {
									bedwars.sendTitle(p, "§bWinner: "+winteam.getDisplayName(), null);
								}
							}
						}
						
					new BukkitRunnable() {
						int endvalue=8;
						int rlvalue=3;

						@Override
						public void run() {
							timer.runningtimer="gameend";
							
							if(endvalue==0) {

								if(winteam==null) {
									PRcore.hub("§7Runde Ende : §bAlle Teams haben verlassen");
									
								}else {
									PRcore.hub("§7Runde Ende : §bWinner "+winteam.getDisplayName());
									
								}
								if(rlvalue==0) {
									cancel();
									if(restartaftergame==true) {
										Bukkit.shutdown();
									}else {
										Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "reload");
									}
								}else {
									rlvalue--;
								}
							}else {
								if(winteam!=null) {
									for(String etr:winteam.getEntries()) {
										Player poao = Bukkit.getPlayer(etr);
										Location loc = poao.getLocation();
										FireworkEffect effefw1 = winrmdlist.get((new Random()).nextInt(winrmdlist.size()));
//										FireworkEffect effefw2 = winrmdlist.get((new Random()).nextInt(winrmdlist.size()));
//										FireworkEffect effefw3 = winrmdlist.get((new Random()).nextInt(winrmdlist.size()));
										Firework fw1 = (Firework) poao.getWorld().spawnEntity(loc, EntityType.FIREWORK);
							            FireworkMeta fwm1 = fw1.getFireworkMeta();
							            fwm1.addEffect(effefw1);
							            fwm1.setPower(2);
							            fw1.setFireworkMeta(fwm1);
							            
//							            Firework fw2 = (Firework) poao.getWorld().spawnEntity(loc, EntityType.FIREWORK);
//							            FireworkMeta fwm2 = fw2.getFireworkMeta();
//							            fwm2.addEffect(effefw2);
//							            fwm2.setPower(1);
//							            fw2.setFireworkMeta(fwm2);
//							            
//							            Firework fw3 = (Firework) poao.getWorld().spawnEntity(loc, EntityType.FIREWORK);
//							            FireworkMeta fwm3 = fw3.getFireworkMeta();
//							            fwm3.addEffect(effefw3);
//							            fwm3.setPower(2);
//							            fw3.setFireworkMeta(fwm3);
									}
								}
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									float exp = allplayer.getExp();
									float removeexp = (float)1/endvalue;
									float newexp = exp-removeexp;
									allplayer.setExp(newexp);
									allplayer.setLevel(endvalue);
								}
								endvalue--;
							}
							
							
						}
						
					}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 1, 20);
					
				}
				
				if(timeg==0) {
					cancel();
					gametimer.slowdamagelist.clear();
					updatesidebar();
					for (Player allplayer :  Bukkit.getOnlinePlayers()) {
					allplayer.setExp(0.99f);
					allplayer.getInventory().clear();
//					allplayer.updateInventory();
					}

					new BukkitRunnable() {
						int endvalue=5;
						int rlvalue=3;

						@Override
						public void run() {
							try {
							timer.runningtimer="gameend";
							if(endvalue==0) {
								PRcore.hub("§7Runde Ende : §4Unentschieden");
								if(rlvalue==0) {
									cancel();
									gametimer.slowdamagelist.clear();
									Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "reload");
								}else {
									rlvalue--;
								}
							}else {
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									float exp = allplayer.getExp();
									float removeexp = (float)1/endvalue;
									float newexp = exp-removeexp;
									allplayer.setExp(newexp);
									allplayer.setLevel(endvalue);
								}
								endvalue--;
							}
							
							}catch (NullPointerException en) {
								PRcore.hub("§7Runde Ende : §4Unentschieden");
								cancel();
								gametimer.slowdamagelist.clear();
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "reload");
								
							}
						}
						
					}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 1, 20);
					int halfmoney = bedwars.cfgmain.getInt("winmoney")/3;
					for(Player p:Bukkit.getOnlinePlayers()) {
						for(Team team:startedteams) {
							if(team.hasEntry(p.getName())) {
								bedwars.sendTitle(p, "§4Unentschieden", "§2+ "+Integer.toString(halfmoney)+"PRc");
								PRcore_addon.setMoney(false, p.getName(), halfmoney, true);
								return;
								
							}
						}

						bedwars.sendTitle(p, "§4Unentschieden", null);
					}
				}else {
					updatesidebar();
					
					
					timeg--;
				}
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 1, 20);
		
		
	}
	public static void killVillager() {
		for(World w : Bukkit.getWorlds()) {
			for(Entity et : w.getEntities()) {
				if(et instanceof Player) {
					
				}else {
					et.remove();
				}
			}
		}
		
	}
	public static void makeVillager() {
		new BukkitRunnable() {

			@Override
			public void run() {
				try {
					if(mapengine.redshop.getWorld()!=null) {
						
						Villager vl = (Villager) mapengine.redshop.getWorld().spawnEntity(mapengine.redshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("redprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.blueshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.blueshop.getWorld().spawnEntity(mapengine.blueshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("blueprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.greenshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.greenshop.getWorld().spawnEntity(mapengine.greenshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("greenprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.yellowshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.yellowshop.getWorld().spawnEntity(mapengine.yellowshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("yellowprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.purpleshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.purpleshop.getWorld().spawnEntity(mapengine.purpleshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("purpleprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.magentashop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.magentashop.getWorld().spawnEntity(mapengine.magentashop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("magentaprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.whiteshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.whiteshop.getWorld().spawnEntity(mapengine.whiteshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("whiteprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					if(mapengine.blackshop.getWorld()!=null) {
						Villager vl = (Villager) mapengine.blackshop.getWorld().spawnEntity(mapengine.blackshop, EntityType.VILLAGER);
						vl.setCustomName(bedwars.cfgmain.getString("blackprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
						vl.setCustomNameVisible(true);
					}
					}catch(NullPointerException en) {
						
					}
				
			}
			
		}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 30);
	}
	public static boolean isInBorder(Location center, Location notCenter, double d) {
		int x = center.getBlockX(), z = center.getBlockZ();
		int x1 = notCenter.getBlockX(), z1 = notCenter.getBlockZ();
		 
		if (x1 >= (x + d) || z1 >= (z + d) || x1 <= (x - d) || z1 <= (z - d)) {
		return false;
		}
		return true;
		}
	public static void forceVillager() {
		new BukkitRunnable() {

			@Override
			public void run() {

				if(endgame==true) {
					cancel();
				}
				for(World w : Bukkit.getWorlds()) {
					for(Entity et : w.getEntities()) {
						if(et instanceof Villager) {

							if(!villagercheck.contains(et.getCustomName())) {

								if(mapengine.redshop.getWorld()!=null) {
								
								Villager vl = (Villager) mapengine.redshop.getWorld().spawnEntity(mapengine.redshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("redprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.blueshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.blueshop.getWorld().spawnEntity(mapengine.blueshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("blueprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.greenshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.greenshop.getWorld().spawnEntity(mapengine.greenshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("greenprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.yellowshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.yellowshop.getWorld().spawnEntity(mapengine.yellowshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("yellowprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.purpleshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.purpleshop.getWorld().spawnEntity(mapengine.purpleshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("purpleprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.magentashop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.magentashop.getWorld().spawnEntity(mapengine.magentashop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("magentaprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.whiteshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.whiteshop.getWorld().spawnEntity(mapengine.whiteshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("whiteprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
							if(!villagercheck.contains(et.getCustomName())) {


								if(mapengine.blackshop.getWorld()!=null) {
								Villager vl = (Villager) mapengine.blackshop.getWorld().spawnEntity(mapengine.blackshop, EntityType.VILLAGER);
								vl.setCustomName(bedwars.cfgmain.getString("blackprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop");
								vl.setCustomNameVisible(true);
								}
							}
						}
					}
				}
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 20, 20);
		
	}
	public static void holdVillager() {
//		forceVillager();
		new BukkitRunnable() {

			@Override
			public void run() {
				if(endgame==true) {
					cancel();
				}
				for(World w : Bukkit.getWorlds()) {
					for(Entity et : w.getEntities()) {
						if(et instanceof Villager) {
				try {
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("redprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.redshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.redshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("blueprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.blueshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.blueshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("greenprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.greenshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.greenshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("yellowprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.yellowshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.yellowshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("purpleprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.purpleshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.purpleshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("magentaprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.magentashop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.magentashop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("whiteprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.whiteshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.whiteshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
					
					if(et.getCustomName().equalsIgnoreCase(bedwars.cfgmain.getString("blackprefix").replaceAll("&", "§").replace("§auml;", "ä").replace("§Auml;", "Ä").replace("§ouml;", "ö").replace("§Ouml;", "Ö").replace("§uuml;", "ü").replace("§Uuml;", "Ü").replace("§szlig;", "ß")+"Shop")&&isInBorder(mapengine.blackshop, et.getLocation(), 0.2)==false) {
						et.teleport(mapengine.blackshop);
						if(!villagercheck.contains(et.getCustomName())) {
							villagercheck.add(et.getCustomName());
						}
					}
				}catch(NullPointerException en) {
					
				}
				 
				
			}}}}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 1);
	}
	
}
