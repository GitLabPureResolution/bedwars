package me.eliteSchwein.bedwars;


import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.arcaniax.hdb.api.HeadDatabaseAPI;



public class command_makemap implements CommandExecutor{

    HeadDatabaseAPI api = new HeadDatabaseAPI();
	@SuppressWarnings("unused")
	private bedwars plugin;

	public command_makemap(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
		if((sender instanceof org.bukkit.entity.Player)){
			final org.bukkit.entity.Player p = (org.bukkit.entity.Player)sender;
			if(p.hasPermission("bedwars.makemap")) {
				timer.killmanlobby=true;
				if(event_makemap.playerwhouse==null) {
				if(teams.saver.hasEntry(p.getName())) {
					p.sendMessage(bedwars.MessageNoPerm+"Du bist raus aus dem Speicher Modus");
					teams.saver.removeEntry(p.getName());
					teams.build.addEntry(p.getName());
					p.getInventory().clear();
					event_makemap.playerwhouse=null;
				}else {
					p.setCustomName("general");
					event_makemap.playerwhouse=p.getName();
					p.sendMessage(bedwars.MessagePerm+"Du bist nun im Speicher Modus");
					teams.saver.addEntry(p.getName());
					p.getInventory().clear();
					
					ItemStack teamselector = api.getItemHead("10358");
					ItemMeta teamselectormeta = teamselector.getItemMeta();
					teamselectormeta.setDisplayName("§rTeam auswählen");
					teamselector.setItemMeta(teamselectormeta);
					teamselector.setAmount(1);
					p.getInventory().setItem(0, teamselector);
					
					ItemStack tier1 = api.getItemHead("1114");
					ItemMeta tier1meta = tier1.getItemMeta();
					tier1meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier1")+" Spawner");
					tier1.setItemMeta(tier1meta);
					p.getInventory().setItem(1, tier1);
					
					ItemStack tier2 = api.getItemHead("99");
					ItemMeta tier2meta = tier2.getItemMeta();
					tier2meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier2")+" Spawner");
					tier2.setItemMeta(tier2meta);
					p.getInventory().setItem(2, tier2);
					
					ItemStack tier3 = api.getItemHead("98");
					ItemMeta tier3meta = tier3.getItemMeta();
					tier3meta.setDisplayName("§r"+bedwars.cfgmoney.getString("moneytyp.tier3")+" Spawner");
					tier3.setItemMeta(tier3meta);
					p.getInventory().setItem(3, tier3);
					
					ItemStack save = api.getItemHead("584");
					ItemMeta savemeta = save.getItemMeta();
					savemeta.setDisplayName("§rSpeichern");
					save.setItemMeta(savemeta);
					p.getInventory().setItem(6, save);
					
					ItemStack pos1 = api.getItemHead("10350");
					ItemMeta pos1meta = pos1.getItemMeta();
					pos1meta.setDisplayName("§rSpielbereicht Pos.1");
					pos1.setItemMeta(pos1meta);
					p.getInventory().setItem(7, pos1);
					
					ItemStack pos2 = api.getItemHead("10349");
					ItemMeta pos2meta = pos2.getItemMeta();
					pos2meta.setDisplayName("§rSpielbereicht Pos.2");
					pos2.setItemMeta(pos2meta);
					p.getInventory().setItem(8, pos2);
					
					
				}
				
				
				
				}else if(p.getName().equalsIgnoreCase(event_makemap.playerwhouse)) {
					if(teams.saver.hasEntry(p.getName())) {
						p.sendMessage(bedwars.MessageNoPerm+"Du bist raus aus dem Speicher Modus");
						teams.saver.removeEntry(p.getName());
						teams.build.addEntry(p.getName());
						p.getInventory().clear();
					}
					event_makemap.playerwhouse=null;
				}else {
					p.sendMessage(bedwars.MessageNoPerm+"Speicher Modus ist belegt");
					
					
				}
			}else{
				p.sendMessage(bedwars.MessageNoPerm+"Du hast das Recht dafür nicht");
			}
		}else{
			bedwars.chatconsole("§cOnly Player command!");
		}
		return false;
	}

}

