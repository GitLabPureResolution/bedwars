package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import me.eliteSCHW31N.bedwars.randomevents.manipulator;

public class event_inventory_shop implements Listener{
 
    final Enchantment PROTECTION = Enchantment.PROTECTION_ENVIRONMENTAL;
    final Enchantment FIRE_PROTECTION = Enchantment.PROTECTION_FIRE;
    final Enchantment FEATHER_FALLING = Enchantment.PROTECTION_FALL;
    final Enchantment BLAST_PROTECTION = Enchantment.PROTECTION_EXPLOSIONS;
    final Enchantment PROJECTILE_PROTECTION = Enchantment.PROTECTION_PROJECTILE;
    final Enchantment RESPIRATION = Enchantment.OXYGEN;
    final Enchantment AQUA_AFFINITY = Enchantment.WATER_WORKER;
    final Enchantment SHARPNESS = Enchantment.DAMAGE_ALL;
    final Enchantment SMITE = Enchantment.DAMAGE_UNDEAD;
    final Enchantment BANE = Enchantment.DAMAGE_ARTHROPODS;
    final Enchantment KNOCKBACK = Enchantment.KNOCKBACK;
    final Enchantment FIRE_ASPECT = Enchantment.FIRE_ASPECT;
    final Enchantment LOOTING = Enchantment.LOOT_BONUS_MOBS;
    final Enchantment EFFICIENCY = Enchantment.DIG_SPEED;
    final Enchantment SILK_TOUCH = Enchantment.SILK_TOUCH;
    final Enchantment UNBREAKING = Enchantment.DURABILITY;
    final Enchantment FORTUNE = Enchantment.LOOT_BONUS_BLOCKS;
    final Enchantment POWER = Enchantment.ARROW_DAMAGE;
    final Enchantment PUNCH = Enchantment.ARROW_KNOCKBACK;
    final Enchantment FLAME = Enchantment.ARROW_FIRE;
    final Enchantment INFINITE = Enchantment.ARROW_INFINITE;
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    private bedwars plugin;
	public event_inventory_shop(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			Player p = (Player) e.getPlayer();

			String invname = e.getInventory().getTitle();
			if(invname.equalsIgnoreCase("§8>>§3SHOP")) {
				p.setCustomName(p.getName());
			}
		}
		
	}
	@SuppressWarnings({ "unused" })
	@EventHandler
	public void onInventory(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			try {
			String invname = e.getClickedInventory().getTitle();
			if(invname.equalsIgnoreCase("§8>>§3SHOP")) {
				Material m = e.getCurrentItem().getType();
				String ms = e.getCurrentItem().getItemMeta().getDisplayName();
//				if(ms==null) {
//					return;
//				}
				e.setCancelled(true);
				((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(),Sound.WOOD_CLICK,1,1);
				if(m==Material.SKULL&&ms.contains("Zurück")||m==Material.SKULL_ITEM&&ms.contains("Zurück")) {
					e.getWhoClicked().setCustomName(e.getWhoClicked().getName());
					e.getClickedInventory().clear();
					ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
					ItemMeta swordm = sword.getItemMeta();
					swordm.setDisplayName("Waffen");
					ArrayList<String> swordl = new ArrayList<String>();
					swordl.add(" ");
					swordm.setLore(swordl);
					sword.setItemMeta(swordm);
					sword.setAmount(1);
					e.getClickedInventory().setItem(1, sword);
					ItemStack block = new ItemStack(Material.SANDSTONE);
					ItemMeta blockm = block.getItemMeta();
					blockm.setDisplayName("Blöcke");
					ArrayList<String> blockl = new ArrayList<String>();
					blockl.add(" ");
					blockm.setLore(blockl);
					block.setItemMeta(blockm);
					block.setAmount(1);
					e.getClickedInventory().setItem(3, block);
					ItemStack food = new ItemStack(Material.COOKED_CHICKEN);
					ItemMeta foodm = food.getItemMeta();
					foodm.setDisplayName("Essen");
					ArrayList<String> foodl = new ArrayList<String>();
					foodl.add(" ");
					foodm.setLore(foodl);
					food.setItemMeta(foodm);
					food.setAmount(1);
					e.getClickedInventory().setItem(5, food);
					ItemStack pickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
					ItemMeta pickaxem = pickaxe.getItemMeta();
					pickaxem.setDisplayName("Spitzhacke");
					ArrayList<String> pickaxel = new ArrayList<String>();
					pickaxel.add(" ");
					pickaxem.setLore(pickaxel);
					pickaxe.setItemMeta(pickaxem);
					pickaxe.setAmount(1);
					e.getClickedInventory().setItem(7, pickaxe);
					ItemStack extra = new ItemStack(Material.EYE_OF_ENDER);
					ItemMeta extram = extra.getItemMeta();
					extram.setDisplayName("Extra");
					ArrayList<String> extral = new ArrayList<String>();
					extral.add(" ");
					extram.setLore(extral);
					extra.setItemMeta(extram);
					extra.setAmount(1);
					e.getClickedInventory().setItem(13, extra);
					ItemStack potion = new ItemStack(Material.EXP_BOTTLE);
					ItemMeta potionm = potion.getItemMeta();
					potionm.setDisplayName("Tränke");
					ArrayList<String> potionl = new ArrayList<String>();
					potionl.add(" ");
					potionm.setLore(potionl);
					potion.setItemMeta(potionm);
					potion.setAmount(1);
					e.getClickedInventory().setItem(11, potion);
					ItemStack armor = new ItemStack(Material.LEATHER_CHESTPLATE);
					ItemMeta armorm = armor.getItemMeta();
					armorm.setDisplayName("Rüstung");
					ArrayList<String> armorl = new ArrayList<String>();
					armorl.add(" ");
					armorm.setLore(armorl);
					armor.setItemMeta(armorm);
					armor.setAmount(1);
					e.getClickedInventory().setItem(15, armor);
					
				}else
				if(m==Material.SANDSTONE&&ms.contains("Blöcke")) {
					e.getWhoClicked().setCustomName("Blöcke");
					e.getClickedInventory().clear();
					String tstainedclay = bedwars.cfgmoney.getString("block.stainedclay.typ");
					int tstainedclayamount = bedwars.cfgmoney.getInt("block.stainedclay.shopamount");
					int tstainedclaysamount = bedwars.cfgmoney.getInt("block.stainedclay.amount");
					Material mtstainedclay = Material.getMaterial(bedwars.cfgmoney.getString("material."+tstainedclay));
					ItemStack itstainedclay = new ItemStack(mtstainedclay);
					ItemMeta itmstainedclay = itstainedclay.getItemMeta();
					itmstainedclay.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tstainedclay));
					itstainedclay.setItemMeta(itmstainedclay);
					itstainedclay.setAmount(tstainedclaysamount);
					ItemStack stainedclay = new ItemStack(Material.STAINED_CLAY,1,(short)teams.getteamclaycolor(p));
					ItemMeta stainedclaym = stainedclay.getItemMeta();
//					stainedclaym.setDisplayName("Sandstein");
					stainedclay.setItemMeta(stainedclaym);
					stainedclay.setAmount(tstainedclayamount);
					e.getClickedInventory().setItem(0, stainedclay);
					e.getClickedInventory().setItem(9, itstainedclay);
					String tladder = bedwars.cfgmoney.getString("block.ladder.typ");
					int tladderamount = bedwars.cfgmoney.getInt("block.ladder.shopamount");
					int tladdersamount = bedwars.cfgmoney.getInt("block.ladder.amount");
					Material mtladder = Material.getMaterial(bedwars.cfgmoney.getString("material."+tladder));
					ItemStack itladder = new ItemStack(mtladder);
					ItemMeta itmladder = itladder.getItemMeta();
					itmladder.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tladder));
					itladder.setItemMeta(itmladder);
					itladder.setAmount(tladdersamount);
					ItemStack ladder = new ItemStack(Material.LADDER);
					ItemMeta ladderm = ladder.getItemMeta();
//					ladderm.setDisplayName("Eisblock");
					ladder.setItemMeta(ladderm);
					ladder.setAmount(tladderamount);
					e.getClickedInventory().setItem(1, ladder);
					e.getClickedInventory().setItem(10, itladder);
					String ticeblock = bedwars.cfgmoney.getString("block.iceblock.typ");
					int ticeblockamount = bedwars.cfgmoney.getInt("block.iceblock.shopamount");
					int ticeblocksamount = bedwars.cfgmoney.getInt("block.iceblock.amount");
					Material mticeblock = Material.getMaterial(bedwars.cfgmoney.getString("material."+ticeblock));
					ItemStack iticeblock = new ItemStack(mticeblock);
					ItemMeta itmiceblock = iticeblock.getItemMeta();
					itmiceblock.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+ticeblock));
					iticeblock.setItemMeta(itmiceblock);
					iticeblock.setAmount(ticeblocksamount);
					ItemStack iceblock = new ItemStack(Material.PACKED_ICE);
					ItemMeta iceblockm = iceblock.getItemMeta();
//					iceblockm.setDisplayName("Eisblock");
					iceblock.setItemMeta(iceblockm);
					iceblock.setAmount(ticeblockamount);
					e.getClickedInventory().setItem(2, iceblock);
					e.getClickedInventory().setItem(11, iticeblock);
					String tironblock = bedwars.cfgmoney.getString("block.ironblock.typ");
					int tironblockamount = bedwars.cfgmoney.getInt("block.ironblock.shopamount");
					int tironblocksamount = bedwars.cfgmoney.getInt("block.ironblock.amount");
					Material mtironblock = Material.getMaterial(bedwars.cfgmoney.getString("material."+tironblock));
					ItemStack itironblock = new ItemStack(mtironblock);
					ItemMeta itmironblock = itironblock.getItemMeta();
					itmironblock.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tironblock));
					itironblock.setItemMeta(itmironblock);
					itironblock.setAmount(tironblocksamount);
					ItemStack ironblock = new ItemStack(Material.IRON_BLOCK);
					ItemMeta ironblockm = ironblock.getItemMeta();
//					ironblockm.setDisplayName("Eisenblock");
					ironblock.setItemMeta(ironblockm);
					ironblock.setAmount(tironblockamount);
					e.getClickedInventory().setItem(3, ironblock);
					e.getClickedInventory().setItem(12, itironblock);
					String tglassblock = bedwars.cfgmoney.getString("block.glassblock.typ");
					int tglassblockamount = bedwars.cfgmoney.getInt("block.glassblock.shopamount");
					int tglassblocksamount = bedwars.cfgmoney.getInt("block.glassblock.amount");
					Material mtglassblock = Material.getMaterial(bedwars.cfgmoney.getString("material."+tglassblock));
					ItemStack itglassblock = new ItemStack(mtglassblock);
					ItemMeta itmglassblock = itglassblock.getItemMeta();
					itmglassblock.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tglassblock));
					itglassblock.setItemMeta(itmglassblock);
					itglassblock.setAmount(tglassblocksamount);
					ItemStack glassblock = new ItemStack(Material.GLASS);
					ItemMeta glassblockm = glassblock.getItemMeta();
//					glassblockm.setDisplayName("Glas");
					glassblock.setItemMeta(glassblockm);
					glassblock.setAmount(tglassblockamount);
					e.getClickedInventory().setItem(4, glassblock);
					e.getClickedInventory().setItem(13, itglassblock);
					String tchest = bedwars.cfgmoney.getString("block.chest.typ");
					int tchestamount = bedwars.cfgmoney.getInt("block.chest.shopamount");
					int tchestsamount = bedwars.cfgmoney.getInt("block.chest.amount");
					Material mtchest = Material.getMaterial(bedwars.cfgmoney.getString("material."+tchest));
					ItemStack itchest = new ItemStack(mtchest);
					ItemMeta itmchest = itchest.getItemMeta();
					itmchest.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tchest));
					itchest.setItemMeta(itmchest);
					itchest.setAmount(tchestsamount);
					ItemStack chest = new ItemStack(Material.CHEST);
					ItemMeta chestm = chest.getItemMeta();
//					chestm.setDisplayName("Kiste");
					chest.setItemMeta(chestm);
					chest.setAmount(tchestamount);
					e.getClickedInventory().setItem(5, chest);
					e.getClickedInventory().setItem(14, itchest);
					String tenderchest = bedwars.cfgmoney.getString("block.enderchest.typ");
					int tenderchestamount = bedwars.cfgmoney.getInt("block.enderchest.shopamount");
					int tenderchestsamount = bedwars.cfgmoney.getInt("block.enderchest.amount");
					Material mtenderchest = Material.getMaterial(bedwars.cfgmoney.getString("material."+tenderchest));
					ItemStack itenderchest = new ItemStack(mtenderchest);
					ItemMeta itmenderchest = itenderchest.getItemMeta();
					itmenderchest.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tenderchest));
					itenderchest.setItemMeta(itmenderchest);
					itenderchest.setAmount(tenderchestsamount);
					ItemStack enderchest = new ItemStack(Material.ENDER_CHEST);
					ItemMeta enderchestm = enderchest.getItemMeta();
//					enderchestm.setDisplayName("Ender Kiste");
					enderchest.setItemMeta(enderchestm);
					enderchest.setAmount(tenderchestamount);
					e.getClickedInventory().setItem(6, enderchest);
					e.getClickedInventory().setItem(15, itenderchest);
					
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else
				if(m==Material.COOKED_CHICKEN&&ms.contains("Essen")) {
					e.getWhoClicked().setCustomName("Essen");
					e.getClickedInventory().clear();
					String tfish = bedwars.cfgmoney.getString("food.fish.typ");
					int tfishamount = bedwars.cfgmoney.getInt("food.fish.shopamount");
					int tfishsamount = bedwars.cfgmoney.getInt("food.fish.amount");
					Material mtfish = Material.getMaterial(bedwars.cfgmoney.getString("material."+tfish));
					ItemStack itfish = new ItemStack(mtfish);
					ItemMeta itmfish = itfish.getItemMeta();
					itmfish.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tfish));
					itfish.setItemMeta(itmfish);
					itfish.setAmount(tfishsamount);
					ItemStack fish = new ItemStack(Material.COOKED_FISH);
					ItemMeta fishm = fish.getItemMeta();
//					fishm.setDisplayName("Fisch");
					fish.setItemMeta(fishm);
					fish.setAmount(tfishamount);
					e.getClickedInventory().setItem(0, fish);
					e.getClickedInventory().setItem(9, itfish);
					String tapple = bedwars.cfgmoney.getString("food.apple.typ");
					int tappleamount = bedwars.cfgmoney.getInt("food.apple.shopamount");
					int tapplesamount = bedwars.cfgmoney.getInt("food.apple.amount");
					Material mtapple = Material.getMaterial(bedwars.cfgmoney.getString("material."+tapple));
					ItemStack itapple = new ItemStack(mtapple);
					ItemMeta itmapple = itapple.getItemMeta();
					itmapple.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tapple));
					itapple.setItemMeta(itmapple);
					itapple.setAmount(tapplesamount);
					ItemStack apple = new ItemStack(Material.APPLE);
					ItemMeta applem = apple.getItemMeta();
					applem.setDisplayName("Apfel");
					apple.setItemMeta(applem);
					apple.setAmount(tappleamount);
					e.getClickedInventory().setItem(1, apple);
					e.getClickedInventory().setItem(10, itapple);
					String tsteak = bedwars.cfgmoney.getString("food.steak.typ");
					int tsteakamount = bedwars.cfgmoney.getInt("food.steak.shopamount");
					int tsteaksamount = bedwars.cfgmoney.getInt("food.steak.amount");
					Material mtsteak = Material.getMaterial(bedwars.cfgmoney.getString("material."+tsteak));
					ItemStack itsteak = new ItemStack(mtsteak);
					ItemMeta itmsteak = itsteak.getItemMeta();
					itmsteak.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tsteak));
					itsteak.setItemMeta(itmsteak);
					itsteak.setAmount(tsteaksamount);
					ItemStack steak = new ItemStack(Material.COOKED_BEEF);
					ItemMeta steakm = steak.getItemMeta();
					steakm.setDisplayName("Steak");
					steak.setItemMeta(steakm);
					steak.setAmount(tsteakamount);
					e.getClickedInventory().setItem(2, steak);
					e.getClickedInventory().setItem(11, itsteak);
					String tgoldenapple = bedwars.cfgmoney.getString("food.goldenapple.typ");
					int tgoldenappleamount = bedwars.cfgmoney.getInt("food.goldenapple.shopamount");
					int tgoldenapplesamount = bedwars.cfgmoney.getInt("food.goldenapple.amount");
					Material mtgoldenapple = Material.getMaterial(bedwars.cfgmoney.getString("material."+tgoldenapple));
					ItemStack itgoldenapple = new ItemStack(mtgoldenapple);
					ItemMeta itmgoldenapple = itgoldenapple.getItemMeta();
					itmgoldenapple.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tgoldenapple));
					itgoldenapple.setItemMeta(itmgoldenapple);
					itgoldenapple.setAmount(tgoldenapplesamount);
					ItemStack goldenapple = new ItemStack(Material.GOLDEN_APPLE);
					ItemMeta goldenapplem = goldenapple.getItemMeta();
					goldenapplem.setDisplayName("Apfel");
					goldenapple.setItemMeta(goldenapplem);
					goldenapple.setAmount(tgoldenappleamount);
					e.getClickedInventory().setItem(3, goldenapple);
					e.getClickedInventory().setItem(12, itgoldenapple);
					
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else
				if(m==Material.DIAMOND_PICKAXE&&ms.contains("Spitzhacke")) {
					e.getWhoClicked().setCustomName("Spitzhacke");
					e.getClickedInventory().clear();
					String twood = bedwars.cfgmoney.getString("pickaxe.wood.typ");
					int twoodamount = bedwars.cfgmoney.getInt("pickaxe.wood.shopamount");
					int twoodsamount = bedwars.cfgmoney.getInt("pickaxe.wood.amount");
					Material mtwood = Material.getMaterial(bedwars.cfgmoney.getString("material."+twood));
					ItemStack itwood = new ItemStack(mtwood);
					ItemMeta itmwood = itwood.getItemMeta();
					itmwood.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+twood));
					itwood.setItemMeta(itmwood);
					itwood.setAmount(twoodsamount);
					ItemStack wood = new ItemStack(Material.WOOD_PICKAXE);
					ItemMeta woodm = wood.getItemMeta();
//					woodm.setDisplayName("HolzSpitzhacke");

					if(manipulator.modifyenchantinglist.containsKey("WoodPickAxe")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("WoodPickAxe");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							woodm.addEnchant(ench, level, true);
						}
					}else {
//						woodm.addEnchant(EFFICIENCY, 1, true);
						
					}
					wood.setItemMeta(woodm);
					wood.setAmount(twoodamount);
					e.getClickedInventory().setItem(0, wood);
					e.getClickedInventory().setItem(9, itwood);
					String tstonetier1 = bedwars.cfgmoney.getString("pickaxe.stonetier1.typ");
					int tstonetier1amount = bedwars.cfgmoney.getInt("pickaxe.stonetier1.shopamount");
					int tstonetier1samount = bedwars.cfgmoney.getInt("pickaxe.stonetier1.amount");
					Material mtstonetier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tstonetier1));
					ItemStack itstonetier1 = new ItemStack(mtstonetier1);
					ItemMeta itmstonetier1 = itstonetier1.getItemMeta();
					itmstonetier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tstonetier1));
					itstonetier1.setItemMeta(itmstonetier1);
					itstonetier1.setAmount(tstonetier1samount);
					ItemStack stonetier1 = new ItemStack(Material.STONE_PICKAXE);
					ItemMeta stonetier1m = stonetier1.getItemMeta();
					stonetier1m.setDisplayName("SteinSpitzhacke I");
					if(manipulator.modifyenchantinglist.containsKey("StonePickAxe I")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("StonePickAxe I");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							stonetier1m.addEnchant(ench, level, true);
						}
					}else {
						stonetier1m.addEnchant(EFFICIENCY, 1, true);
						
					}
					stonetier1.setItemMeta(stonetier1m);
					stonetier1.setAmount(tstonetier1amount);
					e.getClickedInventory().setItem(1, stonetier1);
					e.getClickedInventory().setItem(10, itstonetier1);
					String tstonetier2 = bedwars.cfgmoney.getString("pickaxe.stonetier2.typ");
					int tstonetier2amount = bedwars.cfgmoney.getInt("pickaxe.stonetier2.shopamount");
					int tstonetier2samount = bedwars.cfgmoney.getInt("pickaxe.stonetier2.amount");
					Material mtstonetier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tstonetier2));
					ItemStack itstonetier2 = new ItemStack(mtstonetier2);
					ItemMeta itmstonetier2 = itstonetier2.getItemMeta();
					itmstonetier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tstonetier2));
					itstonetier2.setItemMeta(itmstonetier2);
					itstonetier2.setAmount(tstonetier2samount);
					ItemStack stonetier2 = new ItemStack(Material.STONE_PICKAXE);
					ItemMeta stonetier2m = stonetier2.getItemMeta();
					stonetier2m.setDisplayName("SteinSpitzhacke II");
					if(manipulator.modifyenchantinglist.containsKey("StonePickAxe II")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("StonePickAxe II");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							stonetier2m.addEnchant(ench, level, true);
						}
					}else {
						stonetier2m.addEnchant(EFFICIENCY, 2, true);
						
					}
					stonetier2.setItemMeta(stonetier2m);
					stonetier2.setAmount(tstonetier2amount);
					e.getClickedInventory().setItem(2, stonetier2);
					e.getClickedInventory().setItem(11, itstonetier2);
					String tiron = bedwars.cfgmoney.getString("pickaxe.iron.typ");
					int tironamount = bedwars.cfgmoney.getInt("pickaxe.iron.shopamount");
					int tironsamount = bedwars.cfgmoney.getInt("pickaxe.iron.amount");
					Material mtiron = Material.getMaterial(bedwars.cfgmoney.getString("material."+tiron));
					ItemStack itiron = new ItemStack(mtiron);
					ItemMeta itmiron = itiron.getItemMeta();
					itmiron.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tiron));
					itiron.setItemMeta(itmiron);
					itiron.setAmount(tironsamount);
					ItemStack iron = new ItemStack(Material.IRON_PICKAXE);
					ItemMeta ironm = iron.getItemMeta();

					if(manipulator.modifyenchantinglist.containsKey("IronPickAxe")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("IronPickAxe II");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							ironm.addEnchant(ench, level, true);
						}
					}else {
						ironm.addEnchant(EFFICIENCY, 1, true);
						
					}
//					ironm.setDisplayName("EisenSpitzhacke");
					iron.setItemMeta(ironm);
					iron.setAmount(tironamount);
					e.getClickedInventory().setItem(3, iron);
					e.getClickedInventory().setItem(12, itiron);
					
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else
				if(m==Material.EYE_OF_ENDER&&ms.contains("Extra")) {
					e.getWhoClicked().setCustomName("Extra");
					e.getClickedInventory().clear();
					String tenderpearl = bedwars.cfgmoney.getString("extra.enderpearl.typ");
					int tenderpearlamount = bedwars.cfgmoney.getInt("extra.enderpearl.shopamount");
					int tenderpearlsamount = bedwars.cfgmoney.getInt("extra.enderpearl.amount");
					Material mtenderpearl = Material.getMaterial(bedwars.cfgmoney.getString("material."+tenderpearl));
					ItemStack itenderpearl = new ItemStack(mtenderpearl);
					ItemMeta itmenderpearl = itenderpearl.getItemMeta();
					itmenderpearl.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tenderpearl));
					itenderpearl.setItemMeta(itmenderpearl);
					itenderpearl.setAmount(tenderpearlsamount);
					ItemStack enderpearl = new ItemStack(Material.ENDER_PEARL);
					ItemMeta enderpearlm = enderpearl.getItemMeta();
					enderpearlm.setDisplayName("Enderperle");
					enderpearl.setItemMeta(enderpearlm);
					enderpearl.setAmount(tenderpearlamount);
					e.getClickedInventory().setItem(0, enderpearl);
					e.getClickedInventory().setItem(9, itenderpearl);
					String tfishingrod = bedwars.cfgmoney.getString("extra.fishingrod.typ");
					int tfishingrodamount = bedwars.cfgmoney.getInt("extra.fishingrod.shopamount");
					int tfishingrodsamount = bedwars.cfgmoney.getInt("extra.fishingrod.amount");
					Material mtfishingrod = Material.getMaterial(bedwars.cfgmoney.getString("material."+tfishingrod));
					ItemStack itfishingrod = new ItemStack(mtfishingrod);
					ItemMeta itmfishingrod = itfishingrod.getItemMeta();
					itmfishingrod.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tfishingrod));
					itfishingrod.setItemMeta(itmfishingrod);
					itfishingrod.setAmount(tfishingrodsamount);
					ItemStack fishingrod = new ItemStack(Material.FISHING_ROD);
					ItemMeta fishingrodm = fishingrod.getItemMeta();
					fishingrodm.setDisplayName("Angel");
					fishingrod.setItemMeta(fishingrodm);
					fishingrod.setAmount(tfishingrodamount);
					e.getClickedInventory().setItem(1, fishingrod);
					e.getClickedInventory().setItem(10, itfishingrod);
					String ttnt = bedwars.cfgmoney.getString("extra.tnt.typ");
					int ttntamount = bedwars.cfgmoney.getInt("extra.tnt.shopamount");
					int ttntsamount = bedwars.cfgmoney.getInt("extra.tnt.amount");
					Material mttnt = Material.getMaterial(bedwars.cfgmoney.getString("material."+ttnt));
					ItemStack ittnt = new ItemStack(mttnt);
					ItemMeta itmtnt = ittnt.getItemMeta();
					itmtnt.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+ttnt));
					ittnt.setItemMeta(itmtnt);
					ittnt.setAmount(ttntsamount);
					ItemStack tnt = new ItemStack(Material.TNT);
					ItemMeta tntm = tnt.getItemMeta();
					tntm.setDisplayName("Granate");
					tnt.setItemMeta(tntm);
					tnt.setAmount(ttntamount);
					e.getClickedInventory().setItem(2, tnt);
					e.getClickedInventory().setItem(11, ittnt);
					String tcobweb = bedwars.cfgmoney.getString("extra.cobweb.typ");
					int tcobwebamount = bedwars.cfgmoney.getInt("extra.cobweb.shopamount");
					int tcobwebsamount = bedwars.cfgmoney.getInt("extra.cobweb.amount");
					Material mtcobweb = Material.getMaterial(bedwars.cfgmoney.getString("material."+tcobweb));
					ItemStack itcobweb = new ItemStack(mtcobweb);
					ItemMeta itmcobweb = itcobweb.getItemMeta();
					itmcobweb.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tcobweb));
					itcobweb.setItemMeta(itmcobweb);
					itcobweb.setAmount(tcobwebsamount);
					ItemStack cobweb = new ItemStack(Material.WEB);
					ItemMeta cobwebm = cobweb.getItemMeta();
					cobwebm.setDisplayName("Spinnennetz");
					cobweb.setItemMeta(cobwebm);
					cobweb.setAmount(tcobwebamount);
					e.getClickedInventory().setItem(3, cobweb);
					e.getClickedInventory().setItem(12, itcobweb);
					String tflintandsteel = bedwars.cfgmoney.getString("extra.flintandsteel.typ");
					int tflintandsteelamount = bedwars.cfgmoney.getInt("extra.flintandsteel.shopamount");
					int tflintandsteelsamount = bedwars.cfgmoney.getInt("extra.flintandsteel.amount");
					Material mtflintandsteel = Material.getMaterial(bedwars.cfgmoney.getString("material."+tflintandsteel));
					ItemStack itflintandsteel = new ItemStack(mtflintandsteel);
					ItemMeta itmflintandsteel = itflintandsteel.getItemMeta();
					itmflintandsteel.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tflintandsteel));
					itflintandsteel.setItemMeta(itmflintandsteel);
					itflintandsteel.setAmount(tflintandsteelsamount);
					ItemStack flintandsteel = new ItemStack(Material.FLINT_AND_STEEL);
					ItemMeta flintandsteelm = flintandsteel.getItemMeta();
					flintandsteelm.setDisplayName("Feuerzeug");
					flintandsteel.setItemMeta(flintandsteelm);
					flintandsteel.setAmount(tflintandsteelamount);
					e.getClickedInventory().setItem(4, flintandsteel);
					e.getClickedInventory().setItem(13, itflintandsteel);
					String tegg = bedwars.cfgmoney.getString("extra.egg.typ");
					int teggamount = bedwars.cfgmoney.getInt("extra.egg.shopamount");
					int teggsamount = bedwars.cfgmoney.getInt("extra.egg.amount");
					Material mtegg = Material.getMaterial(bedwars.cfgmoney.getString("material."+tegg));
					ItemStack itegg = new ItemStack(mtegg);
					ItemMeta itmegg = itegg.getItemMeta();
					itmegg.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tegg));
					itegg.setItemMeta(itmegg);
					itegg.setAmount(teggsamount);
					ItemStack egg = new ItemStack(Material.EGG);
					ItemMeta eggm = egg.getItemMeta();
					eggm.setDisplayName("Ei");
					egg.setItemMeta(eggm);
					egg.setAmount(teggamount);
					e.getClickedInventory().setItem(5, egg);
					e.getClickedInventory().setItem(14, itegg);
					String talertstring = bedwars.cfgmoney.getString("extra.alertstring.typ");
					int talertstringamount = bedwars.cfgmoney.getInt("extra.alertstring.shopamount");
					int talertstringsamount = bedwars.cfgmoney.getInt("extra.alertstring.amount");
					Material mtalertstring = Material.getMaterial(bedwars.cfgmoney.getString("material."+talertstring));
					ItemStack italertstring = new ItemStack(mtalertstring);
					ItemMeta itmalertstring = italertstring.getItemMeta();
					itmalertstring.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+talertstring));
					italertstring.setItemMeta(itmalertstring);
					italertstring.setAmount(talertstringsamount);
					ItemStack alertstring = new ItemStack(Material.STRING);
					ItemMeta alertstringm = alertstring.getItemMeta();
					alertstringm.setDisplayName("Alarm Faden");
					alertstring.setItemMeta(alertstringm);
					alertstring.setAmount(talertstringamount);
					e.getClickedInventory().setItem(6, alertstring);
					e.getClickedInventory().setItem(15, italertstring);
					String thomepowder = bedwars.cfgmoney.getString("extra.homepowder.typ");
					int thomepowderamount = bedwars.cfgmoney.getInt("extra.homepowder.shopamount");
					int thomepowdersamount = bedwars.cfgmoney.getInt("extra.homepowder.amount");
					Material mthomepowder = Material.getMaterial(bedwars.cfgmoney.getString("material."+thomepowder));
					ItemStack ithomepowder = new ItemStack(mthomepowder);
					ItemMeta itmhomepowder = ithomepowder.getItemMeta();
					itmhomepowder.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+thomepowder));
					ithomepowder.setItemMeta(itmhomepowder);
					ithomepowder.setAmount(thomepowdersamount);
					ItemStack homepowder = new ItemStack(Material.SULPHUR);
					ItemMeta homepowderm = homepowder.getItemMeta();
					homepowderm.setDisplayName("Nachhause Pulver");
					homepowder.setItemMeta(homepowderm);
					homepowder.setAmount(thomepowderamount);
					e.getClickedInventory().setItem(7, homepowder);
					e.getClickedInventory().setItem(16, ithomepowder);
					
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else
				if(m==Material.DIAMOND_SWORD&&ms.contains("Waffen")) {
					e.getWhoClicked().setCustomName("Waffen");
					e.getClickedInventory().clear();
					String tstick = bedwars.cfgmoney.getString("weapon.stick.typ");
					int tstickamount = bedwars.cfgmoney.getInt("weapon.stick.shopamount");
					int tsticksamount = bedwars.cfgmoney.getInt("weapon.stick.amount");
					Material mtstick = Material.getMaterial(bedwars.cfgmoney.getString("material."+tstick));
					ItemStack itstick = new ItemStack(mtstick);
					ItemMeta itmstick = itstick.getItemMeta();
					itmstick.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tstick));
					itstick.setItemMeta(itmstick);
					itstick.setAmount(tsticksamount);
					ItemStack stick = new ItemStack(Material.STICK);
					ItemMeta stickm = stick.getItemMeta();
					stickm.setDisplayName("KnockBackStick");
					if(manipulator.modifyenchantinglist.containsKey("KnockBackStick")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("KnockBackStick");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							stickm.addEnchant(ench, level, true);
						}
					}else {
						stickm.addEnchant(Enchantment.KNOCKBACK, 2, true);
						
					}
					stick.setItemMeta(stickm);
					stick.setAmount(tstickamount);
					e.getClickedInventory().setItem(0, stick);
					e.getClickedInventory().setItem(9, itstick);
					String tgoldswordtier1 = bedwars.cfgmoney.getString("weapon.goldswordtier1.typ");
					int tgoldswordtier1amount = bedwars.cfgmoney.getInt("weapon.goldswordtier1.shopamount");
					int tgoldswordtier1samount = bedwars.cfgmoney.getInt("weapon.goldswordtier1.amount");
					Material mtgoldswordtier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tgoldswordtier1));
					ItemStack itgoldswordtier1 = new ItemStack(mtgoldswordtier1);
					ItemMeta itmgoldswordtier1 = itgoldswordtier1.getItemMeta();
					itmgoldswordtier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tgoldswordtier1));
					itgoldswordtier1.setItemMeta(itmgoldswordtier1);
					itgoldswordtier1.setAmount(tgoldswordtier1samount);
					ItemStack goldswordtier1 = new ItemStack(Material.GOLD_SWORD);
					ItemMeta goldswordtier1m = goldswordtier1.getItemMeta();
					goldswordtier1m.setDisplayName("Goldschwert I");

					if(manipulator.modifyenchantinglist.containsKey("GoldSword I")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("GoldSword I");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							goldswordtier1m.addEnchant(ench, level, true);
						}
					}else {
						goldswordtier1m.addEnchant(SHARPNESS, 1, true);
						
					}
					goldswordtier1.setItemMeta(goldswordtier1m);
					goldswordtier1.setAmount(tgoldswordtier1amount);
					e.getClickedInventory().setItem(1, goldswordtier1);
					e.getClickedInventory().setItem(10, itgoldswordtier1);
					String tgoldswordtier2 = bedwars.cfgmoney.getString("weapon.goldswordtier2.typ");
					int tgoldswordtier2amount = bedwars.cfgmoney.getInt("weapon.goldswordtier2.shopamount");
					int tgoldswordtier2samount = bedwars.cfgmoney.getInt("weapon.goldswordtier2.amount");
					Material mtgoldswordtier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tgoldswordtier2));
					ItemStack itgoldswordtier2 = new ItemStack(mtgoldswordtier2);
					ItemMeta itmgoldswordtier2 = itgoldswordtier2.getItemMeta();
					itmgoldswordtier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tgoldswordtier2));
					itgoldswordtier2.setItemMeta(itmgoldswordtier2);
					itgoldswordtier2.setAmount(tgoldswordtier2samount);
					ItemStack goldswordtier2 = new ItemStack(Material.GOLD_SWORD);
					ItemMeta goldswordtier2m = goldswordtier2.getItemMeta();
					goldswordtier2m.setDisplayName("Goldschwert II");

					if(manipulator.modifyenchantinglist.containsKey("GoldSword II")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("GoldSword II");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							goldswordtier2m.addEnchant(ench, level, true);
						}
					}else {
						goldswordtier2m.addEnchant(SHARPNESS, 2, true);
						
					}
					goldswordtier2.setItemMeta(goldswordtier2m);
					goldswordtier2.setAmount(tgoldswordtier2amount);
					e.getClickedInventory().setItem(2, goldswordtier2);
					e.getClickedInventory().setItem(11, itgoldswordtier2);
					String tironsword = bedwars.cfgmoney.getString("weapon.ironsword.typ");
					int tironswordamount = bedwars.cfgmoney.getInt("weapon.ironsword.shopamount");
					int tironswordsamount = bedwars.cfgmoney.getInt("weapon.ironsword.amount");
					Material mtironsword = Material.getMaterial(bedwars.cfgmoney.getString("material."+tironsword));
					ItemStack itironsword = new ItemStack(mtironsword);
					ItemMeta itmironsword = itironsword.getItemMeta();
					itmironsword.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tironsword));
					itironsword.setItemMeta(itmironsword);
					itironsword.setAmount(tironswordsamount);
					ItemStack ironsword = new ItemStack(Material.IRON_SWORD);
					ItemMeta ironswordm = ironsword.getItemMeta();
//					ironswordm.setDisplayName("Eisenschwert");

					if(manipulator.modifyenchantinglist.containsKey("IronSword")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("IronSword");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							ironswordm.addEnchant(ench, level, true);
						}
					}else {
						ironswordm.addEnchant(SHARPNESS, 1, true);
						
					}
					ironsword.setItemMeta(ironswordm);
					ironsword.setAmount(tironswordamount);
					e.getClickedInventory().setItem(3, ironsword);
					e.getClickedInventory().setItem(12, itironsword);
					String tarrow = bedwars.cfgmoney.getString("weapon.arrow.typ");
					int tarrowamount = bedwars.cfgmoney.getInt("weapon.arrow.shopamount");
					int tarrowsamount = bedwars.cfgmoney.getInt("weapon.arrow.amount");
					Material mtarrow = Material.getMaterial(bedwars.cfgmoney.getString("material."+tarrow));
					ItemStack itarrow = new ItemStack(mtarrow);
					ItemMeta itmarrow = itarrow.getItemMeta();
					itmarrow.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tarrow));
					itarrow.setItemMeta(itmarrow);
					itarrow.setAmount(tarrowsamount);
					ItemStack arrow = new ItemStack(Material.ARROW);
					ItemMeta arrowm = arrow.getItemMeta();
//					arrowm.addEnchant(Enchantment.KNOCKBACK, 3, true);
//					arrowm.setDisplayName("Pfeil");
					arrow.setItemMeta(arrowm);
					arrow.setAmount(tarrowamount);
					e.getClickedInventory().setItem(4, arrow);
					e.getClickedInventory().setItem(13, itarrow);
					String tbowtier1 = bedwars.cfgmoney.getString("weapon.bowtier1.typ");
					int tbowtier1amount = bedwars.cfgmoney.getInt("weapon.bowtier1.shopamount");
					int tbowtier1samount = bedwars.cfgmoney.getInt("weapon.bowtier1.amount");
					Material mtbowtier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tbowtier1));
					ItemStack itbowtier1 = new ItemStack(mtbowtier1);
					ItemMeta itmbowtier1 = itbowtier1.getItemMeta();
					itmbowtier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tbowtier1));
					itbowtier1.setItemMeta(itmbowtier1);
					itbowtier1.setAmount(tbowtier1samount);
					ItemStack bowtier1 = new ItemStack(Material.BOW);
					ItemMeta bowtier1m = bowtier1.getItemMeta();
					
					bowtier1m.setDisplayName("Bogen I");


					if(manipulator.modifyenchantinglist.containsKey("Bow I")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("Bow I");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							bowtier1m.addEnchant(ench, level, true);
						}
					}else {
						bowtier1m.addEnchant(POWER, 1, true);
						bowtier1m.addEnchant(Enchantment.DURABILITY, 1, true);
						
					}
					bowtier1.setItemMeta(bowtier1m);
					bowtier1.setAmount(tbowtier1amount);
					e.getClickedInventory().setItem(5, bowtier1);
					e.getClickedInventory().setItem(14, itbowtier1);
					String tbowtier2 = bedwars.cfgmoney.getString("weapon.bowtier2.typ");
					int tbowtier2amount = bedwars.cfgmoney.getInt("weapon.bowtier2.shopamount");
					int tbowtier2samount = bedwars.cfgmoney.getInt("weapon.bowtier2.amount");
					Material mtbowtier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tbowtier2));
					ItemStack itbowtier2 = new ItemStack(mtbowtier2);
					ItemMeta itmbowtier2 = itbowtier2.getItemMeta();
					itmbowtier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tbowtier2));
					itbowtier2.setItemMeta(itmbowtier2);
					itbowtier2.setAmount(tbowtier2samount);
					ItemStack bowtier2 = new ItemStack(Material.BOW);
					ItemMeta bowtier2m = bowtier2.getItemMeta();
					bowtier2m.setDisplayName("Bogen II");


					if(manipulator.modifyenchantinglist.containsKey("Bow II")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("Bow II");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							bowtier2m.addEnchant(ench, level, true);
						}
					}else {
						bowtier2m.addEnchant(POWER, 2, true);
						bowtier2m.addEnchant(PUNCH, 1, true);
						bowtier2m.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
						bowtier2m.addEnchant(Enchantment.DURABILITY, 1, true);
						
					}
					bowtier2.setItemMeta(bowtier2m);
					bowtier2.setAmount(tbowtier2amount);
					e.getClickedInventory().setItem(6, bowtier2);
					e.getClickedInventory().setItem(15, itbowtier2);
					String tbowtier3 = bedwars.cfgmoney.getString("weapon.bowtier3.typ");
					int tbowtier3amount = bedwars.cfgmoney.getInt("weapon.bowtier3.shopamount");
					int tbowtier3samount = bedwars.cfgmoney.getInt("weapon.bowtier3.amount");
					Material mtbowtier3 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tbowtier3));
					ItemStack itbowtier3 = new ItemStack(mtbowtier3);
					ItemMeta itmbowtier3 = itbowtier3.getItemMeta();
					itmbowtier3.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tbowtier3));
					itbowtier3.setItemMeta(itmbowtier3);
					itbowtier3.setAmount(tbowtier3samount);
					ItemStack bowtier3 = new ItemStack(Material.BOW);
					ItemMeta bowtier3m = bowtier3.getItemMeta();
					bowtier3m.setDisplayName("Bogen III");


					if(manipulator.modifyenchantinglist.containsKey("Bow III")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("Bow III");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							bowtier3m.addEnchant(ench, level, true);
						}
					}else {
						bowtier3m.addEnchant(POWER, 2, true);
						bowtier3m.addEnchant(PUNCH, 1, true);
						bowtier3m.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
						bowtier3m.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
						bowtier3m.addEnchant(Enchantment.DURABILITY, 1, true);
						
					}
					bowtier3.setItemMeta(bowtier3m);
					bowtier3.setAmount(tbowtier3amount);
					e.getClickedInventory().setItem(7, bowtier3);
					e.getClickedInventory().setItem(16, itbowtier3);
//					String tbowtier1fire = bedwars.cfgmoney.getString("weapon.bowtier1fire.typ");
//					int tbowtier1fireamount = bedwars.cfgmoney.getInt("weapon.bowtier1fire.shopamount");
//					int tbowtier1firesamount = bedwars.cfgmoney.getInt("weapon.bowtier1fire.amount");
//					Material mtbowtier1fire = Material.getMaterial(bedwars.cfgmoney.getString("material."+tbowtier1fire));
//					ItemStack itbowtier1fire = new ItemStack(mtbowtier1fire);
//					ItemMeta itmbowtier1fire = itbowtier1fire.getItemMeta();
//					itmbowtier1fire.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tbowtier1fire));
//					itbowtier1fire.setItemMeta(itmbowtier1fire);
//					itbowtier1fire.setAmount(tbowtier1firesamount);
//					ItemStack bowtier1fire = new ItemStack(Material.BOW);
//					ItemMeta bowtier1firem = bowtier1fire.getItemMeta();
//					bowtier1firem.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
//					bowtier1firem.addEnchant(Enchantment.DURABILITY, 1, true);
//					bowtier1firem.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
//					bowtier1firem.setDisplayName("Bogen I F");
//					bowtier1fire.setItemMeta(bowtier1firem);
//					bowtier1fire.setAmount(tbowtier1fireamount);
//					e.getClickedInventory().setItem(5, bowtier1fire);
//					e.getClickedInventory().setItem(14, itbowtier1fire);
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
					
				}else

				if(m==Material.LEATHER_CHESTPLATE&&ms.contains("Rüstung")) {
					e.getWhoClicked().setCustomName("Rüstung");
					e.getClickedInventory().clear();
					String tlederhelmet = bedwars.cfgmoney.getString("armor.lederhelmet.typ");
					int tlederhelmetamount = bedwars.cfgmoney.getInt("armor.lederhelmet.shopamount");
					int tlederhelmetsamount = bedwars.cfgmoney.getInt("armor.lederhelmet.amount");
					Material mtlederhelmet = Material.getMaterial(bedwars.cfgmoney.getString("material."+tlederhelmet));
					ItemStack itlederhelmet = new ItemStack(mtlederhelmet);
					ItemMeta itmlederhelmet = itlederhelmet.getItemMeta();
					itmlederhelmet.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tlederhelmet));
					itlederhelmet.setItemMeta(itmlederhelmet);
					itlederhelmet.setAmount(tlederhelmetsamount);
					ItemStack lederhelmet = new ItemStack(Material.LEATHER_HELMET);
					LeatherArmorMeta lederhelmetm = (LeatherArmorMeta) lederhelmet.getItemMeta();
//					lederhelmetm.setDisplayName("Leder Helm");
					lederhelmetm.setColor(teams.getteamleathercolor(p));


					if(manipulator.modifyenchantinglist.containsKey("LeatherHelmet")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("LeatherHelmet");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							lederhelmetm.addEnchant(ench, level, true);
						}
					}else {
//						lederhelmetm.addEnchant(EFFICIENCY, 1, true);
						
					}
					lederhelmet.setItemMeta(lederhelmetm);
					lederhelmet.setAmount(tlederhelmetamount);
					e.getClickedInventory().setItem(0, lederhelmet);
					e.getClickedInventory().setItem(9, itlederhelmet);
					String tlederboots = bedwars.cfgmoney.getString("armor.lederboots.typ");
					int tlederbootsamount = bedwars.cfgmoney.getInt("armor.lederboots.shopamount");
					int tlederbootssamount = bedwars.cfgmoney.getInt("armor.lederboots.amount");
					Material mtlederboots = Material.getMaterial(bedwars.cfgmoney.getString("material."+tlederboots));
					ItemStack itlederboots = new ItemStack(mtlederboots);
					ItemMeta itmlederboots = itlederboots.getItemMeta();
					itmlederboots.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tlederboots));
					itlederboots.setItemMeta(itmlederboots);
					itlederboots.setAmount(tlederbootssamount);
					ItemStack lederboots = new ItemStack(Material.LEATHER_BOOTS);
					LeatherArmorMeta lederbootsm = (LeatherArmorMeta) lederboots.getItemMeta();
//					lederbootsm.setDisplayName("Leder Schuhe");
					lederbootsm.setColor(teams.getteamleathercolor(p));


					if(manipulator.modifyenchantinglist.containsKey("LeatherBoots")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("LeatherBoots");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							lederbootsm.addEnchant(ench, level, true);
						}
					}else {
//						lederbootsm.addEnchant(EFFICIENCY, 1, true);
						
					}
					lederboots.setItemMeta(lederbootsm);
					lederboots.setAmount(tlederbootsamount);
					e.getClickedInventory().setItem(1, lederboots);
					e.getClickedInventory().setItem(10, itlederboots);
					String tlederleggings = bedwars.cfgmoney.getString("armor.lederleggings.typ");
					int tlederleggingsamount = bedwars.cfgmoney.getInt("armor.lederleggings.shopamount");
					int tlederleggingssamount = bedwars.cfgmoney.getInt("armor.lederleggings.amount");
					Material mtlederleggings = Material.getMaterial(bedwars.cfgmoney.getString("material."+tlederleggings));
					ItemStack itlederleggings = new ItemStack(mtlederleggings);
					ItemMeta itmlederleggings = itlederleggings.getItemMeta();
					itmlederleggings.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tlederleggings));
					itlederleggings.setItemMeta(itmlederleggings);
					itlederleggings.setAmount(tlederleggingssamount);
					ItemStack lederleggings = new ItemStack(Material.LEATHER_LEGGINGS);
					LeatherArmorMeta lederleggingsm = (LeatherArmorMeta) lederleggings.getItemMeta();
					lederleggingsm.setColor(teams.getteamleathercolor(p));
//					System.out.println(lederbootsm.getColor().getRed()+lederbootsm.getColor().getBlue()+lederbootsm.getColor().getGreen());
//					lederleggingsm.setDisplayName("Leder Hose");


					if(manipulator.modifyenchantinglist.containsKey("LeatherLeggings")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("LeatherLeggings");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							lederleggingsm.addEnchant(ench, level, true);
						}
					}else {
//						lederleggingsm.addEnchant(EFFICIENCY, 1, true);
						
					}
					lederleggings.setItemMeta(lederleggingsm);
					lederleggings.setAmount(tlederleggingsamount);
					e.getClickedInventory().setItem(2, lederleggings);
					e.getClickedInventory().setItem(11, itlederleggings);
					String tchainchestplatetier1 = bedwars.cfgmoney.getString("armor.chainchestplatetier1.typ");
					int tchainchestplatetier1amount = bedwars.cfgmoney.getInt("armor.chainchestplatetier1.shopamount");
					int tchainchestplatetier1samount = bedwars.cfgmoney.getInt("armor.chainchestplatetier1.amount");
					Material mtchainchestplatetier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tchainchestplatetier1));
					ItemStack itchainchestplatetier1 = new ItemStack(mtchainchestplatetier1);
					ItemMeta itmchainchestplatetier1 = itchainchestplatetier1.getItemMeta();
					itmchainchestplatetier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tchainchestplatetier1));
					itchainchestplatetier1.setItemMeta(itmchainchestplatetier1);
					itchainchestplatetier1.setAmount(tchainchestplatetier1samount);
					ItemStack chainchestplatetier1 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
					ItemMeta chainchestplatetier1m = chainchestplatetier1.getItemMeta();
					chainchestplatetier1m.setDisplayName("Ketten Brustplatte I");


					if(manipulator.modifyenchantinglist.containsKey("ChainChestplate I")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("ChainChestplate I");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							chainchestplatetier1m.addEnchant(ench, level, true);
						}
					}else {
						chainchestplatetier1m.addEnchant(PROTECTION, 1, true);
						
					}
					chainchestplatetier1.setItemMeta(chainchestplatetier1m);
					chainchestplatetier1.setAmount(tchainchestplatetier1amount);
					e.getClickedInventory().setItem(3, chainchestplatetier1);
					e.getClickedInventory().setItem(12, itchainchestplatetier1);
					String tchainchestplatetier2 = bedwars.cfgmoney.getString("armor.chainchestplatetier2.typ");
					int tchainchestplatetier2amount = bedwars.cfgmoney.getInt("armor.chainchestplatetier2.shopamount");
					int tchainchestplatetier2samount = bedwars.cfgmoney.getInt("armor.chainchestplatetier2.amount");
					Material mtchainchestplatetier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tchainchestplatetier2));
					ItemStack itchainchestplatetier2 = new ItemStack(mtchainchestplatetier2);
					ItemMeta itmchainchestplatetier2 = itchainchestplatetier2.getItemMeta();
					itmchainchestplatetier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tchainchestplatetier2));
					itchainchestplatetier2.setItemMeta(itmchainchestplatetier2);
					itchainchestplatetier2.setAmount(tchainchestplatetier2samount);
					ItemStack chainchestplatetier2 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
					ItemMeta chainchestplatetier2m = chainchestplatetier2.getItemMeta();
					chainchestplatetier2m.addEnchant(PROTECTION, 2, true);
					chainchestplatetier2m.setDisplayName("Ketten Brustplatte II");


					if(manipulator.modifyenchantinglist.containsKey("ChainChestplate II")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("ChainChestplate II");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							chainchestplatetier2m.addEnchant(ench, level, true);
						}
					}else {
						chainchestplatetier2m.addEnchant(PROTECTION, 2, true);
						
					}
					chainchestplatetier2.setItemMeta(chainchestplatetier2m);
					chainchestplatetier2.setAmount(tchainchestplatetier2amount);
					e.getClickedInventory().setItem(4, chainchestplatetier2);
					e.getClickedInventory().setItem(13, itchainchestplatetier2);
					String tchainchestplatetier3 = bedwars.cfgmoney.getString("armor.chainchestplatetier3.typ");
					int tchainchestplatetier3amount = bedwars.cfgmoney.getInt("armor.chainchestplatetier3.shopamount");
					int tchainchestplatetier3samount = bedwars.cfgmoney.getInt("armor.chainchestplatetier3.amount");
					Material mtchainchestplatetier3 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tchainchestplatetier3));
					ItemStack itchainchestplatetier3 = new ItemStack(mtchainchestplatetier3);
					ItemMeta itmchainchestplatetier3 = itchainchestplatetier3.getItemMeta();
					itmchainchestplatetier3.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tchainchestplatetier3));
					itchainchestplatetier3.setItemMeta(itmchainchestplatetier3);
					itchainchestplatetier3.setAmount(tchainchestplatetier3samount);
					ItemStack chainchestplatetier3 = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
					ItemMeta chainchestplatetier3m = chainchestplatetier3.getItemMeta();
					chainchestplatetier3m.setDisplayName("Ketten Brustplatte III");


					if(manipulator.modifyenchantinglist.containsKey("ChainChestplate III")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("ChainChestplate III");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							chainchestplatetier3m.addEnchant(ench, level, true);
						}
					}else {
						chainchestplatetier3m.addEnchant(PROTECTION, 3, true);
						
					}
					chainchestplatetier3.setItemMeta(chainchestplatetier3m);
					chainchestplatetier3.setAmount(tchainchestplatetier3amount);
					e.getClickedInventory().setItem(5, chainchestplatetier3);
					e.getClickedInventory().setItem(14, itchainchestplatetier3);
					String tfederfallboots = bedwars.cfgmoney.getString("armor.federfallboots.typ");
					int tfederfallbootsamount = bedwars.cfgmoney.getInt("armor.federfallboots.shopamount");
					int tfederfallbootssamount = bedwars.cfgmoney.getInt("armor.federfallboots.amount");
					Material mtfederfallboots = Material.getMaterial(bedwars.cfgmoney.getString("material."+tfederfallboots));
					ItemStack itfederfallboots = new ItemStack(mtfederfallboots);
					ItemMeta itmfederfallboots = itfederfallboots.getItemMeta();
					itmfederfallboots.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tfederfallboots));
					itfederfallboots.setItemMeta(itmfederfallboots);
					itfederfallboots.setAmount(tfederfallbootssamount);
					ItemStack federfallboots = new ItemStack(Material.GOLD_BOOTS);
					ItemMeta federfallbootsm = federfallboots.getItemMeta();


					if(manipulator.modifyenchantinglist.containsKey("FeatherfallBoots")) {
						ArrayList<String> list=manipulator.modifyenchantinglist.get("FeatherfallBoots");
						for(String entry:list) {
							int level = 0;
							Enchantment ench = null;
							for(String str:entry.split(";")) {
								try {
									level=Integer.parseInt(str);
								}catch(NumberFormatException en) {
									ench=Enchantment.getByName(str);
								}
							}
							federfallbootsm.addEnchant(ench, level, true);
						}
					}else {
						federfallbootsm.addEnchant(Enchantment.PROTECTION_FALL, 4, true);
						
					}
					federfallbootsm.setDisplayName("Fallschaden Schuhe");
					federfallboots.setItemMeta(federfallbootsm);
					federfallboots.setAmount(tfederfallbootsamount);
					e.getClickedInventory().setItem(6, federfallboots);
					e.getClickedInventory().setItem(15, itfederfallboots);
					String tbridgeboots = bedwars.cfgmoney.getString("armor.bridgeboots.typ");
					int tbridgebootsamount = bedwars.cfgmoney.getInt("armor.bridgeboots.shopamount");
					int tbridgebootssamount = bedwars.cfgmoney.getInt("armor.bridgeboots.amount");
					Material mtbridgeboots = Material.getMaterial(bedwars.cfgmoney.getString("material."+tbridgeboots));
					ItemStack itbridgeboots = new ItemStack(mtbridgeboots);
					ItemMeta itmbridgeboots = itbridgeboots.getItemMeta();
					itmbridgeboots.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tbridgeboots));
					itbridgeboots.setItemMeta(itmbridgeboots);
					itbridgeboots.setAmount(tbridgebootssamount);
					ItemStack bridgeboots = new ItemStack(Material.GOLD_BOOTS);
					ItemMeta bridgebootsm = bridgeboots.getItemMeta();
					bridgebootsm.setDisplayName("Brücken Schuhe");
					bridgeboots.setItemMeta(bridgebootsm);
					bridgeboots.setAmount(tbridgebootsamount);
					e.getClickedInventory().setItem(7, bridgeboots);
					e.getClickedInventory().setItem(16, itbridgeboots);
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else

				if(m==Material.EXP_BOTTLE&&ms.contains("Tränke")) {
					e.getClickedInventory().clear();
					e.getWhoClicked().setCustomName("Tränke");
					String tspeedtier1 = bedwars.cfgmoney.getString("potion.speedtier1.typ");
					int tspeedtier1amount = bedwars.cfgmoney.getInt("potion.speedtier1.shopamount");
					int tspeedtier1samount = bedwars.cfgmoney.getInt("potion.speedtier1.amount");
					Material mtspeedtier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tspeedtier1));
					ItemStack itspeedtier1 = new ItemStack(mtspeedtier1);
					ItemMeta itmspeedtier1 = itspeedtier1.getItemMeta();
					itmspeedtier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tspeedtier1));
					itspeedtier1.setItemMeta(itmspeedtier1);
					itspeedtier1.setAmount(tspeedtier1samount);
					ItemStack speedtier1 = new ItemStack(Material.POTION,1, (short) 8194);
					ItemMeta speedtier1m = speedtier1.getItemMeta();
//					speedtier1m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					speedtier1m.setDisplayName("Schnelligkeit I");
					speedtier1.setItemMeta(speedtier1m);
					speedtier1.setAmount(tspeedtier1amount);
					e.getClickedInventory().setItem(0, speedtier1);
					e.getClickedInventory().setItem(9, itspeedtier1);
					String tspeedtier2 = bedwars.cfgmoney.getString("potion.speedtier2.typ");
					int tspeedtier2amount = bedwars.cfgmoney.getInt("potion.speedtier2.shopamount");
					int tspeedtier2samount = bedwars.cfgmoney.getInt("potion.speedtier2.amount");
					Material mtspeedtier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tspeedtier2));
					ItemStack itspeedtier2 = new ItemStack(mtspeedtier2);
					ItemMeta itmspeedtier2 = itspeedtier2.getItemMeta();
					itmspeedtier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tspeedtier2));
					itspeedtier2.setItemMeta(itmspeedtier2);
					itspeedtier2.setAmount(tspeedtier2samount);
					ItemStack speedtier2 = new ItemStack(Material.POTION,1, (short) 8226);
					ItemMeta speedtier2m = speedtier2.getItemMeta();
//					speedtier2m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					speedtier2m.setDisplayName("Schnelligkeit II");
					speedtier2.setItemMeta(speedtier2m);
					speedtier2.setAmount(tspeedtier2amount);
					e.getClickedInventory().setItem(1, speedtier2);
					e.getClickedInventory().setItem(10, itspeedtier2);
					String tregenerationtier1 = bedwars.cfgmoney.getString("potion.regenerationtier1.typ");
					int tregenerationtier1amount = bedwars.cfgmoney.getInt("potion.regenerationtier1.shopamount");
					int tregenerationtier1samount = bedwars.cfgmoney.getInt("potion.regenerationtier1.amount");
					Material mtregenerationtier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tregenerationtier1));
					ItemStack itregenerationtier1 = new ItemStack(mtregenerationtier1);
					ItemMeta itmregenerationtier1 = itregenerationtier1.getItemMeta();
					itmregenerationtier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tregenerationtier1));
					itregenerationtier1.setItemMeta(itmregenerationtier1);
					itregenerationtier1.setAmount(tregenerationtier1samount);
					ItemStack regenerationtier1 = new ItemStack(Material.POTION,1, (short) 8193);
					ItemMeta regenerationtier1m = regenerationtier1.getItemMeta();
//					regenerationtier1m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					regenerationtier1m.setDisplayName("Regeneration I");
					regenerationtier1.setItemMeta(regenerationtier1m);
					regenerationtier1.setAmount(tregenerationtier1amount);
					e.getClickedInventory().setItem(2, regenerationtier1);
					e.getClickedInventory().setItem(11, itregenerationtier1);
					String tregenerationtier2 = bedwars.cfgmoney.getString("potion.regenerationtier2.typ");
					int tregenerationtier2amount = bedwars.cfgmoney.getInt("potion.regenerationtier2.shopamount");
					int tregenerationtier2samount = bedwars.cfgmoney.getInt("potion.regenerationtier2.amount");
					Material mtregenerationtier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tregenerationtier2));
					ItemStack itregenerationtier2 = new ItemStack(mtregenerationtier2);
					ItemMeta itmregenerationtier2 = itregenerationtier2.getItemMeta();
					itmregenerationtier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tregenerationtier2));
					itregenerationtier2.setItemMeta(itmregenerationtier2);
					itregenerationtier2.setAmount(tregenerationtier2samount);
					ItemStack regenerationtier2 = new ItemStack(Material.POTION,1, (short) 8225);
					ItemMeta regenerationtier2m = regenerationtier2.getItemMeta();
//					regenerationtier2m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					regenerationtier2m.setDisplayName("Regeneration II");
					regenerationtier2.setItemMeta(regenerationtier2m);
					regenerationtier2.setAmount(tregenerationtier2amount);
					e.getClickedInventory().setItem(3, regenerationtier2);
					e.getClickedInventory().setItem(12, itregenerationtier2);
					String tjumptier1 = bedwars.cfgmoney.getString("potion.jumptier1.typ");
					int tjumptier1amount = bedwars.cfgmoney.getInt("potion.jumptier1.shopamount");
					int tjumptier1samount = bedwars.cfgmoney.getInt("potion.jumptier1.amount");
					Material mtjumptier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tjumptier1));
					ItemStack itjumptier1 = new ItemStack(mtjumptier1);
					ItemMeta itmjumptier1 = itjumptier1.getItemMeta();
					itmjumptier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tjumptier1));
					itjumptier1.setItemMeta(itmjumptier1);
					itjumptier1.setAmount(tjumptier1samount);
					ItemStack jumptier1 = new ItemStack(Material.POTION,1, (short) 8267);
					ItemMeta jumptier1m = jumptier1.getItemMeta();
//					jumptier1m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					jumptier1m.setDisplayName("Sprungkraft I");
					jumptier1.setItemMeta(jumptier1m);
					jumptier1.setAmount(tjumptier1amount);
					e.getClickedInventory().setItem(4, jumptier1);
					e.getClickedInventory().setItem(13, itjumptier1);
					String tjumptier2 = bedwars.cfgmoney.getString("potion.jumptier2.typ");
					int tjumptier2amount = bedwars.cfgmoney.getInt("potion.jumptier2.shopamount");
					int tjumptier2samount = bedwars.cfgmoney.getInt("potion.jumptier2.amount");
					Material mtjumptier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tjumptier2));
					ItemStack itjumptier2 = new ItemStack(mtjumptier2);
					ItemMeta itmjumptier2 = itjumptier2.getItemMeta();
					itmjumptier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tjumptier2));
					itjumptier2.setItemMeta(itmjumptier2);
					itjumptier2.setAmount(tjumptier2samount);
					ItemStack jumptier2 = new ItemStack(Material.POTION,1, (short) 8235);
					ItemMeta jumptier2m = jumptier2.getItemMeta();
//					jumptier2m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					jumptier2m.setDisplayName("Sprungkraft II");
					jumptier2.setItemMeta(jumptier2m);
					jumptier2.setAmount(tjumptier2amount);
					e.getClickedInventory().setItem(5, jumptier2);
					e.getClickedInventory().setItem(14, itjumptier2);
					String tfireprotectiontier1 = bedwars.cfgmoney.getString("potion.fireprotectiontier1.typ");
					int tfireprotectiontier1amount = bedwars.cfgmoney.getInt("potion.fireprotectiontier1.shopamount");
					int tfireprotectiontier1samount = bedwars.cfgmoney.getInt("potion.fireprotectiontier1.amount");
					Material mtfireprotectiontier1 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tfireprotectiontier1));
					ItemStack itfireprotectiontier1 = new ItemStack(mtfireprotectiontier1);
					ItemMeta itmfireprotectiontier1 = itfireprotectiontier1.getItemMeta();
					itmfireprotectiontier1.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tfireprotectiontier1));
					itfireprotectiontier1.setItemMeta(itmfireprotectiontier1);
					itfireprotectiontier1.setAmount(tfireprotectiontier1samount);
					ItemStack fireprotectiontier1 = new ItemStack(Material.POTION,1, (short) 8227);
					ItemMeta fireprotectiontier1m = fireprotectiontier1.getItemMeta();
//					fireprotectiontier1m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					fireprotectiontier1m.setDisplayName("Feuerschutz kurz");
					fireprotectiontier1.setItemMeta(fireprotectiontier1m);
					fireprotectiontier1.setAmount(tfireprotectiontier1amount);
					e.getClickedInventory().setItem(6, fireprotectiontier1);
					e.getClickedInventory().setItem(15, itfireprotectiontier1);
					String tfireprotectiontier2 = bedwars.cfgmoney.getString("potion.fireprotectiontier2.typ");
					int tfireprotectiontier2amount = bedwars.cfgmoney.getInt("potion.fireprotectiontier2.shopamount");
					int tfireprotectiontier2samount = bedwars.cfgmoney.getInt("potion.fireprotectiontier2.amount");
					Material mtfireprotectiontier2 = Material.getMaterial(bedwars.cfgmoney.getString("material."+tfireprotectiontier2));
					ItemStack itfireprotectiontier2 = new ItemStack(mtfireprotectiontier2);
					ItemMeta itmfireprotectiontier2 = itfireprotectiontier2.getItemMeta();
					itmfireprotectiontier2.setDisplayName(bedwars.cfgmoney.getString("moneytyp."+tfireprotectiontier2));
					itfireprotectiontier2.setItemMeta(itmfireprotectiontier2);
					itfireprotectiontier2.setAmount(tfireprotectiontier2samount);
					ItemStack fireprotectiontier2 = new ItemStack(Material.POTION,1, (short) 8259);
					ItemMeta fireprotectiontier2m = fireprotectiontier2.getItemMeta();
//					fireprotectiontier2m.addEnchant(Enchantment.PROTECTION_FALL, 3, true);
					fireprotectiontier2m.setDisplayName("Feuerschutz lang");
					fireprotectiontier2.setItemMeta(fireprotectiontier2m);
					fireprotectiontier2.setAmount(tfireprotectiontier2amount);
					e.getClickedInventory().setItem(7, fireprotectiontier2);
					e.getClickedInventory().setItem(16, itfireprotectiontier2);
					
					ItemStack back = api.getItemHead("10678");
					ItemMeta backm = back.getItemMeta();
					backm.setDisplayName("Zurück");
					ArrayList<String> backl = new ArrayList<String>();
					backl.add(" ");
					backm.setLore(backl);
					back.setItemMeta(backm);
					e.getClickedInventory().setItem(8, back);
					e.getClickedInventory().setItem(17, back);
					
				}else
				if(e.getWhoClicked().getCustomName().equalsIgnoreCase(e.getWhoClicked().getName())) {
					
				}else if(m==Material.SKULL||m==Material.SKULL_ITEM){
					
				}else {
					if(e.getWhoClicked().getCustomName().equalsIgnoreCase("Blöcke")||e.getWhoClicked().getCustomName().equalsIgnoreCase("Essen")) {
						if(e.isShiftClick()) {
							int slot = e.getSlot();
							int slotitem = slot+9;
							int stack = 64;
							int shopvalue = e.getClickedInventory().getItem(slot).getAmount();
							int multiplyer = stack/shopvalue;
							int shopcostraw = e.getClickedInventory().getItem(slotitem).getAmount();
							int shopcostfinal = 0;
							ItemStack money=e.getClickedInventory().getItem(slotitem);
							if(isFull(p.getInventory())==true) {
								p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
								return;
							}
							for(ItemStack item : p.getInventory().getContents()) {
								try {
								if(item.getType()==money.getType()) {
									int hasamount = item.getAmount();
									int testvaluenew = hasamount*shopvalue/shopcostraw;
										shopcostfinal=testvaluenew;
									
											p.getInventory().removeItem(item);
//										p.getInventory().remove(money);
										ItemStack stacker = e.getCurrentItem().clone();
										ItemMeta stackern = e.getCurrentItem().getItemMeta();
										stacker.setItemMeta(stackern);
										
										stacker.setAmount((hasamount*shopvalue/shopcostraw));
										p.getInventory().addItem(stacker);
										p.updateInventory();
										return;
									
								}}catch(NullPointerException en) {
									
								}
							}
							p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
							
						}else if(e.isLeftClick()||e.isRightClick()) {
							try {
						int slot = e.getSlot();
						int slotitem = slot+9;
						ItemStack money=e.getClickedInventory().getItem(slotitem);
						if(isFull(p.getInventory())==true) {
							p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
							return;
						}
						for(ItemStack item : p.getInventory().getContents()) {
							try {
							if(item.getType()==money.getType()) {
								if(item.getAmount()==money.getAmount()||item.getAmount()>=money.getAmount()) {
									if(item.getAmount()==money.getAmount()) {
										p.getInventory().removeItem(item);
									}
									item.setAmount(item.getAmount()-money.getAmount());
//									p.getInventory().remove(money);
									p.updateInventory();
									p.getInventory().addItem(e.getCurrentItem());
									return;
								}
							}}catch(NullPointerException en) {
								
							}
						}
						p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
							}catch(IndexOutOfBoundsException ean) {
								
							}
						}
					}else {
						try {
						int slot = e.getSlot();
						int slotitem = slot+9;
						ItemStack money=e.getClickedInventory().getItem(slotitem);
						if(isFull(p.getInventory())==true) {
							p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
							return;
						}
						for(ItemStack item : p.getInventory().getContents()) {
							try {
							if(item.getType()==money.getType()) {
								if(item.getAmount()==money.getAmount()||item.getAmount()>=money.getAmount()) {
									if(item.getAmount()==money.getAmount()) {
										p.getInventory().removeItem(item);
									}
									item.setAmount(item.getAmount()-money.getAmount());
//									p.getInventory().remove(money);
									p.updateInventory();
									p.getInventory().addItem(e.getCurrentItem());
									return;
								}
							}}catch(NullPointerException en) {
								
							}
						}
						p.playSound(e.getWhoClicked().getLocation(),Sound.ZOMBIE_METAL,1,1);
						}catch(IndexOutOfBoundsException eoa) {
							
						}
					}
					
					
				}
			}else if(teams.spectator.hasEntry(p.getName())) {
				String displayname = e.getCurrentItem().getItemMeta().getDisplayName();
				if(displayname.equalsIgnoreCase("§7>> §fRandom Spieler")) {
					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {

						@Override
						public void run() {
							p.setSpectatorTarget(rtp);
							bedwars.sendTitle(p, "Du bist in", entr);
						}
						
					}.runTaskLater(plugin, 5);
				}else if(e.getSlot()==0||e.getSlot()==8||e.getSlot()<=8){
					Team ttt = teams.getTeamFromDisplayName(displayname);
					if(ttt!=null) {
						ArrayList<String> plays = new ArrayList<String>();
						for(String entr:ttt.getEntries()) {
							plays.add(entr);
						}
						ItemStack remover = new ItemStack(Material.AIR,1);
						for(int i=8;i<35;i++) {
							p.getInventory().setItem(i, remover);
							p.updateInventory();
						}
						ItemStack spawn = new ItemStack(Material.EMERALD,1);
						ItemMeta spawnm = spawn.getItemMeta();
						spawnm.setDisplayName("Team "+displayname+" §rBasis");
						spawn.setItemMeta(spawnm);
						p.getInventory().setItem(31, spawn);
						for(int i=0;i<plays.size();i++) {
							ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
					        SkullMeta meta = (SkullMeta)skull.getItemMeta();
					          meta.setOwner(plays.get(i));
					          meta.setDisplayName(ttt.getPrefix()+plays.get(i));
					          skull.setItemMeta(meta);
					          p.getInventory().setItem(i+9, skull);
					          p.updateInventory();
						}
					}
				}else if(displayname.contains("Basis")){
					p.setSpectatorTarget(null);
					String nobasestr=displayname.replace(" §rBasis", "");
					String search = nobasestr;
					if(spectator.specstands.containsKey(search)) {
						p.teleport(spectator.specstands.get(search));
					}
				}else {
					p.setSpectatorTarget(null);
					String rawname = ChatColor.stripColor(displayname);
					Player ptt = Bukkit.getPlayer(rawname);
					p.teleport(ptt.getLocation());
					new BukkitRunnable() {

						@Override
						public void run() {
							p.setSpectatorTarget(ptt);
							bedwars.sendTitle(p, "Du bist in", rawname);
						}
						
					}.runTaskLater(plugin, 5);
				}
			}}catch(NullPointerException en) {
				
			}
		}
	}
	@EventHandler
	public void onShot(ProjectileLaunchEvent event) {
		Entity entity = event.getEntity();
		if(entity instanceof Arrow) {
			Entity shooter = (Entity) ((Arrow) entity).getShooter();
			if(shooter instanceof Player) {
				Player player = (Player) shooter;
				String arrowname = player.getItemInHand().getItemMeta().getDisplayName();
				entity.setCustomName(arrowname);
			}
		}
	}
	@EventHandler
	public void onVillager(PlayerInteractAtEntityEvent e) {
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			
			Player p = e.getPlayer();
			if(teams.spectator.hasEntry(p.getName())) {
				e.setCancelled(true);
				return;
			}
			Entity et = e.getRightClicked();
			if(et instanceof Villager) {
				e.getPlayer().setCustomName(e.getPlayer().getName());
				e.setCancelled(true);
				Inventory ses;
				ses = p.getServer().createInventory(null,18, "§8>>§3SHOP");
				ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
				ItemMeta swordm = sword.getItemMeta();
				swordm.setDisplayName("Waffen");
				ArrayList<String> swordl = new ArrayList<String>();
				swordl.add(" ");
				swordm.setLore(swordl);
				sword.setItemMeta(swordm);
				sword.setAmount(1);
				ses.setItem(1, sword);
				ItemStack block = new ItemStack(Material.SANDSTONE);
				ItemMeta blockm = block.getItemMeta();
				blockm.setDisplayName("Blöcke");
				ArrayList<String> blockl = new ArrayList<String>();
				blockl.add(" ");
				blockm.setLore(blockl);
				block.setItemMeta(blockm);
				block.setAmount(1);
				ses.setItem(3, block);
				ItemStack food = new ItemStack(Material.COOKED_CHICKEN);
				ItemMeta foodm = food.getItemMeta();
				foodm.setDisplayName("Essen");
				ArrayList<String> foodl = new ArrayList<String>();
				foodl.add(" ");
				foodm.setLore(foodl);
				food.setItemMeta(foodm);
				food.setAmount(1);
				ses.setItem(5, food);
				ItemStack pickaxe = new ItemStack(Material.DIAMOND_PICKAXE);
				ItemMeta pickaxem = pickaxe.getItemMeta();
				pickaxem.setDisplayName("Spitzhacke");
				ArrayList<String> pickaxel = new ArrayList<String>();
				pickaxel.add(" ");
				pickaxem.setLore(pickaxel);
				pickaxe.setItemMeta(pickaxem);
				pickaxe.setAmount(1);
				ses.setItem(7, pickaxe);
				ItemStack extra = new ItemStack(Material.EYE_OF_ENDER);
				ItemMeta extram = extra.getItemMeta();
				extram.setDisplayName("Extra");
				ArrayList<String> extral = new ArrayList<String>();
				extral.add(" ");
				extram.setLore(extral);
				extra.setItemMeta(extram);
				extra.setAmount(1);
				ses.setItem(13, extra);
				ItemStack potion = new ItemStack(Material.EXP_BOTTLE);
				ItemMeta potionm = potion.getItemMeta();
				potionm.setDisplayName("Tränke");
				ArrayList<String> potionl = new ArrayList<String>();
				potionl.add(" ");
				potionm.setLore(potionl);
				potion.setItemMeta(potionm);
				potion.setAmount(1);
				ses.setItem(11, potion);
				ItemStack armor = new ItemStack(Material.LEATHER_CHESTPLATE);
				ItemMeta armorm = armor.getItemMeta();
				armorm.setDisplayName("Rüstung");
				ArrayList<String> armorl = new ArrayList<String>();
				armorl.add(" ");
				armorm.setLore(armorl);
				armor.setItemMeta(armorm);
				armor.setAmount(1);
				ses.setItem(15, armor);
				new BukkitRunnable() {

					@Override
					public void run() {
						p.openInventory(ses);
						
					}
					
				}.runTaskLater(plugin, 2);
			}
		}
		
	}
	public boolean isFull(Inventory inv) {
	    for (ItemStack i : inv.getContents()) {
	        if (i == null || i.getType() == Material.AIR) return false;
	    }
	    return true;
	}
}
