package me.eliteSchwein.bedwars;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import me.eliteSCHW31N.bedwars.randomevents.manipulator;
import me.eliteSchwein.PRcore.PRcore;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_8_R3.WorldBorder;

public class event_bed implements Listener{
	public static ArrayList<Player> antideathspam = new ArrayList<Player>();
	public static ArrayList<Player> beeberlist = new ArrayList<Player>();
	public static int respawntime = 5;
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    private bedwars plugin;
	public event_bed(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	@EventHandler
	public void stringkill(BlockBreakEvent e) {
		if(e.getBlock().getType()==Material.STRING) {
			e.getBlock().getDrops().clear();
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);
			Location loc = e.getBlock().getLocation();
			int x = loc.getBlockX();
			int y = loc.getBlockY();
			int z = loc.getBlockZ();
			event_extra.alertstring.set("X."+x+".Y."+y+".Z."+z, null);
			try {
				event_extra.alertstring.save(event_extra.filealertstring);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else if(e.getBlock().getType()==Material.STRING) {
			e.getBlock().getDrops().clear();
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);
		}
	}
	@EventHandler
	public void chat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if(teams.spectator.hasEntry(p.getName())) {
			if(timer.runningtimer.equalsIgnoreCase("game")) {
				String message = e.getMessage();
				e.setCancelled(true);
				for(String entrys: teams.spectator.getEntries()) {
					Player ep = Bukkit.getPlayer(entrys);
					ep.sendMessage("§7[SPEC]§r "+p.getDisplayName()+" §3>>§r§7 "+message.replace("%", "Prozent"));
				}
				
			}
		}else if(teams.red.hasEntry(p.getName())&&teams.red.getSize()!=1&&teams.red.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.red.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.blue.hasEntry(p.getName())&&teams.blue.getSize()!=1&&teams.blue.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.blue.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.yellow.hasEntry(p.getName())&&teams.yellow.getSize()!=1&&teams.yellow.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.yellow.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.green.hasEntry(p.getName())&&teams.green.getSize()!=1&&teams.green.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.green.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.purple.hasEntry(p.getName())&&teams.purple.getSize()!=1&&teams.purple.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.purple.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.magenta.hasEntry(p.getName())&&teams.magenta.getSize()!=1&&teams.magenta.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.magenta.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.white.hasEntry(p.getName())&&teams.white.getSize()!=1&&teams.white.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.white.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}else if(teams.black.hasEntry(p.getName())&&teams.black.getSize()!=1&&teams.black.getSize()!=0&&timer.runningtimer.equalsIgnoreCase("game")) {
			String message = e.getMessage();
			if(String.valueOf(message.charAt(0)).equals("#")) {
				String message1 =message.substring(1, message.length());
				e.setMessage(message1);
				return;
			}
			e.setCancelled(true);
			for(String entry:teams.black.getEntries()) {
				Player ep = Bukkit.getPlayer(entry);
				ep.sendMessage("§6[Team]§r "+p.getDisplayName()+" §3>>§r§6 "+message.replace("%", "Prozent"));
			}
		}
	}
	@EventHandler
	public void spawned(CreatureSpawnEvent e) {
		Entity et = e.getEntity();
		if(et instanceof Villager) {
			return;
		}
		if(et instanceof ArmorStand) {
			return;
		}
		e.setCancelled(true);
	}
	@EventHandler
	public void nolobbyfalldamage(EntityDamageEvent e) {
		if(timer.runningtimer.equalsIgnoreCase("Lobby")) {
			if(e.getCause()==DamageCause.FALL) {
				e.setCancelled(true);
			}
		}
	}
	public static void beeber() {
		new BukkitRunnable(){

			@Override
			public void run() {
				for(Player p : beeberlist) {
					Location ploc = p.getLocation();
					p.playSound(ploc, Sound.NOTE_BASS_DRUM, (float)0.5, (float)0.7);
					new BukkitRunnable() {

						@Override
						public void run() {
							p.playSound(ploc, Sound.NOTE_BASS_DRUM, (float)0.5, (float)0.2);
							
						}
						
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 5);
				}
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 20);
		
	}
	@EventHandler
	public void leavearea(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		World world = player.getWorld();
		Location location = player.getLocation();
		Location locationxp = player.getLocation().add(3, 0, 0);
		Location locationxn = player.getLocation().add(-3, 0, 0);
		Location locationyp = player.getLocation().add(0, 3, 0);
		Location locationyn = player.getLocation().add(0, -3, 0);
//		Location locationyn1 = player.getLocation().add(0, -3, 0);
		Location locationzp = player.getLocation().add(0, 0, 3);
		Location locationzn = player.getLocation().add(0, 0, -3);

		if(teams.spectator.hasEntry(player.getName())) {

			if(bedwars.checkEntryfalse(player, world, locationyn)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-Y)");
				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(0, 1, 0));
				}
			
			}else if(bedwars.checkEntryfalse(player, world, locationyp)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+Y)");
				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(0, -1, 0));
				}
			
			}else if(bedwars.checkEntryfalse(player, world, locationxn)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-X)");

				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(1, 0, 0));
				}
				
			}else if(bedwars.checkEntryfalse(player, world, locationxp)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+X)");
				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(-1, 0, 0));
				}
				
//				player.sendMessage("Border near at X+");
			}else if(bedwars.checkEntryfalse(player, world, locationzn)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-Z)");
				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(0, 0, 1));
				}
				
			}else if(bedwars.checkEntryfalse(player, world, locationzp)==false) {
				bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+Z)");
				if(!beeberlist.contains(player)) {
					beeberlist.add(player);
				}
				if(player.getGameMode()==GameMode.SPECTATOR) {
					player.teleport(player.getLocation().clone().add(0, 0, -1));
				}
				
			}else {
				bedwars.sendActionBar(player, "§cInventar Öffnen für Menü / Sneaken zum verlassen");
			}
		}else
		if(bedwars.checkEntryfalse(player, world, location)==false) {
			


			if(beeberlist.contains(player)) {
				beeberlist.remove(player);
			}
			if(player.getGameMode()!=GameMode.SPECTATOR&&!antideathspam.contains(player)) {
				player.setHealth(0);
				UUID uuid = player.getUniqueId();
				if(gametimer.slowdamagelist.containsKey(uuid)) {
					gametimer.slowdamagelist.remove(uuid);
				}
				bedwars.sendTitle(player, "§cAchtung!", "§ces gibt eine Grenze");
			}else {
				if(bedwars.checkEntryfalse(player, world, locationyn)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-Y)");
					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(0, 1, 0));
					}
				
				}else if(bedwars.checkEntryfalse(player, world, locationyp)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+Y)");
					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(0, -1, 0));
					}
				
				}else if(bedwars.checkEntryfalse(player, world, locationxn)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-X)");

					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(1, 0, 0));
					}
					
				}else if(bedwars.checkEntryfalse(player, world, locationxp)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+X)");
					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(-1, 0, 0));
					}
					
//					player.sendMessage("Border near at X+");
				}else if(bedwars.checkEntryfalse(player, world, locationzn)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (-Z)");
					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(0, 0, 1));
					}
					
				}else if(bedwars.checkEntryfalse(player, world, locationzp)==false) {
					bedwars.sendActionBar(player, "§cAchtung! zunah an Grenze (+Z)");
					if(!beeberlist.contains(player)) {
						beeberlist.add(player);
					}
					if(player.getGameMode()==GameMode.SPECTATOR) {
						player.teleport(player.getLocation().clone().add(0, 0, -1));
					}
					
				}else {
					if(beeberlist.contains(player)) {
						beeberlist.remove(player);
					}
				}
			}
			
		}else if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationxn)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (-X)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
			
		}else
		if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationxp)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (+X)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
		}else if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationyn)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (-Y)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
//			player.sendMessage("Border near at Y-");
			
		}else
		if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationyp)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (+Y)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
			
		}else if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationzn)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (-Z)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
			
		}else
		if(player.getGameMode()!=GameMode.SPECTATOR&&bedwars.checkEntryfalse(player, world, locationzp)==false) {
			UUID uuid = player.getUniqueId();
			if(gametimer.slowdamagelist.containsKey(uuid)) {
				gametimer.slowdamagelist.replace(uuid, 2);
			}else {
				gametimer.slowdamagelist.put(uuid, 2);
			}
			bedwars.sendTitle(player, "§cAchtung!", "§czunah an Grenze (+Z)");
			if(!beeberlist.contains(player)) {
				beeberlist.add(player);
			}
			
		}else {

			if(beeberlist.contains(player)) {
				beeberlist.remove(player);
			}
		}
	}
	@EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p_murder = (Player) e.getEntity().getKiller();
        Player p_opfer = (Player) e.getEntity();
        e.setKeepInventory(true);
        if(antideathspam.contains(p_opfer)) {
        	e.setDeathMessage("");
        	return;
        }
        	if(timer.runningtimer.equalsIgnoreCase("game")) {
        		if(teams.spectator.hasEntry(p_opfer.getName())) {

                	e.setDeathMessage("");
        			return;
        		}
        		antideathspam.add(p_opfer);
                if(p_murder != null) {
                    e.setDeathMessage("§c " +p_opfer.getDisplayName()+ " §7wurde von §c"+p_murder.getDisplayName()+" §7getötet");
                }else {
                    e.setDeathMessage("§c " +p_opfer.getDisplayName() + " §7begang Selbstmord");
               }
                return;
        	}else {
            	e.setDeathMessage("");
                return;
        		
        	}
    }
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			Player p = e.getPlayer();
			antideathspam.remove(p);
		}
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		try {
			ItemStack teststack = api.getItemHead("7856");
			ItemMeta testmeta = teststack.getItemMeta();
			testmeta.setDisplayName("useless");
			teststack.setItemMeta(testmeta);
		}catch(NullPointerException en) {
			PRcore.hubonep("§cBedwars lädt noch", e.getPlayer());
		}
	}
	@EventHandler
	public void placeblock(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			Material m = e.getBlock().getType();
			if(m==Material.LADDER) {
				Material ma = e.getBlockAgainst().getType();
				for(Material mall:bedwars.resetblocks) {
					if(mall==ma) {
						return;
					}
				}
				p.sendMessage(bedwars.MessageNoPerm+"Du darfst an diesen Block keine Leiter platzieren");
				e.setCancelled(true);
			}else if(bedwars.resetblocks.contains(m)) {
				e.setBuild(true);
				e.setCancelled(false);
			}else {
				e.setBuild(false);
				e.setCancelled(true);
			}
		}else {
			e.setCancelled(true);
			e.setBuild(false);
		}
	}
	 @EventHandler
	    public void onCreatureSpawn(CreatureSpawnEvent event)
	    {
	        if (event.getSpawnReason() == SpawnReason.EGG)
	        {
	            event.setCancelled(true);
	        }
	    }
	@EventHandler
	public void egg(PlayerEggThrowEvent e) {
		e.setHatching(false);
		
	}
	@EventHandler
	public void breakblock(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			Material m = e.getBlock().getType();
			if(m==Material.TRIPWIRE) {
				Location loc = e.getBlock().getLocation();
				int x = loc.getBlockX();
				int y = loc.getBlockY();
				int z = loc.getBlockZ();
				event_extra.alertstring.set("X."+x+".Y."+y+".Z."+z, null);
				try {
					event_extra.alertstring.save(event_extra.filealertstring);
				} catch (IOException en) {
					// TODO Auto-generated catch block
					en.printStackTrace();
				}
			}
			if(bedwars.resetblocks.contains(m)) {
				e.setCancelled(false);
				if(m==Material.BED||m==Material.BED_BLOCK) {
					Location loc = e.getBlock().getLocation();
					double xloc = loc.getX();
					double yloc = loc.getY();
					double zloc = loc.getZ();
					
					double redx = mapengine.redbed1.getX();
					double redy = mapengine.redbed1.getY();
					double redz = mapengine.redbed1.getZ();
					double red1x = mapengine.redbed2.getX();
					double red1y = mapengine.redbed2.getY();
					double red1z = mapengine.redbed2.getZ();
					
					double bluex = mapengine.bluebed1.getX();
					double bluey = mapengine.bluebed1.getY();
					double bluez = mapengine.bluebed1.getZ();
					double blue1x = mapengine.bluebed2.getX();
					double blue1y = mapengine.bluebed2.getY();
					double blue1z = mapengine.bluebed2.getZ();
					
					double greenx = mapengine.greenbed1.getX();
					double greeny = mapengine.greenbed1.getY();
					double greenz = mapengine.greenbed1.getZ();
					double green1x = mapengine.greenbed2.getX();
					double green1y = mapengine.greenbed2.getY();
					double green1z = mapengine.greenbed2.getZ();
					
					double yellowx = mapengine.yellowbed1.getX();
					double yellowy = mapengine.yellowbed1.getY();
					double yellowz = mapengine.yellowbed1.getZ();
					double yellow1x = mapengine.yellowbed2.getX();
					double yellow1y = mapengine.yellowbed2.getY();
					double yellow1z = mapengine.yellowbed2.getZ();
					
					double magentax = mapengine.magentabed1.getX();
					double magentay = mapengine.magentabed1.getY();
					double magentaz = mapengine.magentabed1.getZ();
					double magenta1x = mapengine.magentabed2.getX();
					double magenta1y = mapengine.magentabed2.getY();
					double magenta1z = mapengine.magentabed2.getZ();
					
					double purplex = mapengine.purplebed1.getX();
					double purpley = mapengine.purplebed1.getY();
					double purplez = mapengine.purplebed1.getZ();
					double purple1x = mapengine.purplebed2.getX();
					double purple1y = mapengine.purplebed2.getY();
					double purple1z = mapengine.purplebed2.getZ();
					
					double whitex = mapengine.whitebed1.getX();
					double whitey = mapengine.whitebed1.getY();
					double whitez = mapengine.whitebed1.getZ();
					double white1x = mapengine.whitebed2.getX();
					double white1y = mapengine.whitebed2.getY();
					double white1z = mapengine.whitebed2.getZ();
					
					double blackx = mapengine.blackbed1.getX();
					double blacky = mapengine.blackbed1.getY();
					double blackz = mapengine.blackbed1.getZ();
					double black1x = mapengine.blackbed2.getX();
					double black1y = mapengine.blackbed2.getY();
					double black1z = mapengine.blackbed2.getZ();

					new BukkitRunnable() {
						
						@Override
						public void run() {
							for(Entity et:p.getWorld().getEntities()) {
								if(et instanceof Item) {
									Item it = (Item) et;
									if(it.getItemStack().getType()==Material.BED||it.getItemStack().getType()==Material.BED_BLOCK) {
										et.remove();
									}
								}
							}
							
						}
					}.runTaskLater(plugin, 1);
					if(xloc==redx&&yloc==redy&&zloc==redz) {
						if(teams.red.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
						}else {
						bedwars.chatconsole("§5Bed destroyed : "+teams.red.getDisplayName());
						gametimer.redteam=false;
						event_stats.setbedbreak(p);
						for(Player pa:Bukkit.getOnlinePlayers()) {
							pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.red.getDisplayName()+"§r§7 wurde zerstört");
							centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
						}
						for(String entry:teams.red.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.red);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==red1x&&yloc==red1y&&zloc==red1z) {
						if(teams.red.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							bedwars.chatconsole("§5Bed destroyed : "+teams.red.getDisplayName());
						gametimer.redteam=false;
						event_stats.setbedbreak(p);
						for(Player pa:Bukkit.getOnlinePlayers()) {
							pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.red.getDisplayName()+"§r§7 wurde zerstört");
							centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
						}
						for(String entry:teams.red.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.red);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==bluex&&yloc==bluey&&zloc==bluez) {
						if(teams.blue.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							bedwars.chatconsole("§5Bed destroyed : "+teams.blue.getDisplayName());
						gametimer.blueteam=false;
						event_stats.setbedbreak(p);
						for(Player pa:Bukkit.getOnlinePlayers()) {
							pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.blue.getDisplayName()+"§r§7 wurde zerstört");
							centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
						}
						for(String entry:teams.blue.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.blue);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==blue1x&&yloc==blue1y&&zloc==blue1z) {
						if(teams.blue.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							bedwars.chatconsole("§5Bed destroyed : "+teams.blue.getDisplayName());
						gametimer.blueteam=false;

						event_stats.setbedbreak(p);
						for(Player pa:Bukkit.getOnlinePlayers()) {
							pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.blue.getDisplayName()+"§r§7 wurde zerstört");
							centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
							centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
						}
						for(String entry:teams.blue.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.blue);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==greenx&&yloc==greeny&&zloc==greenz) {
						if(teams.green.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.green.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.green.getDisplayName());
							event_stats.setbedbreak(p);
						gametimer.greenteam=false;
						for(String entry:teams.green.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.green);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==green1x&&yloc==green1y&&zloc==green1z) {
						if(teams.green.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.green.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.green.getDisplayName());
							event_stats.setbedbreak(p);
						gametimer.greenteam=false;
						for(String entry:teams.green.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.green);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==yellowx&&yloc==yellowy&&zloc==yellowz) {
						if(teams.yellow.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.yellow.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.yellow.getDisplayName());
						gametimer.yellowteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.yellow.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.yellow);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==yellow1x&&yloc==yellow1y&&zloc==yellow1z) {
						if(teams.yellow.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.yellow.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.yellow.getDisplayName());
						gametimer.yellowteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.yellow.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.yellow);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==purplex&&yloc==purpley&&zloc==purplez) {
						if(teams.purple.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.purple.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.purple.getDisplayName());
						gametimer.purpleteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.purple.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.purple);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==purple1x&&yloc==purple1y&&zloc==purple1z) {
						if(teams.purple.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.purple.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.purple.getDisplayName());
						gametimer.purpleteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.purple.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.purple);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==magentax&&yloc==magentay&&zloc==magentaz) {
						if(teams.magenta.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.magenta.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.magenta.getDisplayName());
						gametimer.magentateam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.magenta.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.magenta);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==magenta1x&&yloc==magenta1y&&zloc==magenta1z) {
						if(teams.magenta.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.magenta.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.magenta.getDisplayName());
						gametimer.magentateam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.magenta.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.magenta);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==whitex&&yloc==whitey&&zloc==whitez) {
						if(teams.white.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.white.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.white.getDisplayName());
						gametimer.whiteteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.white.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.white);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==white1x&&yloc==white1y&&zloc==white1z) {
						if(teams.white.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.white.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.white.getDisplayName());
						gametimer.whiteteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.white.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.white);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==blackx&&yloc==blacky&&zloc==blackz) {
						if(teams.black.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.black.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.black.getDisplayName());
						gametimer.blackteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.black.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.black);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
					if(xloc==black1x&&yloc==black1y&&zloc==black1z) {
						if(teams.black.hasEntry(p.getName())) {
							e.setCancelled(true);
							bedwars.sendTitle(p, null, "§cDas ist dein Bett");
							return;
							
						}else {
							for(Player pa:Bukkit.getOnlinePlayers()) {
								pa.playSound(pa.getLocation(), Sound.WITHER_DEATH, 1, 1);
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								centralmessage.sendCenteredMessage(pa, "§7Bed von Team §r"+teams.black.getDisplayName()+"§r§7 wurde zerstört");
								centralmessage.sendCenteredMessage(pa, "§7von: §6§l"+p.getDisplayName());
								centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
							}
							bedwars.chatconsole("§5Bed destroyed : "+teams.black.getDisplayName());
						gametimer.blackteam=false;
						event_stats.setbedbreak(p);
						for(String entry:teams.black.getEntries()) {
							Player pt = Bukkit.getPlayer(entry);
							gametimer.liveteams.remove(teams.black);
							bedwars.sendTitle(pt, null, "§cDein Bett wurde zerstört");
							return;
						}}
					}
				}
			}else {
				e.setCancelled(true);
			}
		}
	else {
		e.setCancelled(true);
	}
		
	}
	@EventHandler
	public void damageVillager(EntityDamageEvent e) {
		if(e.getEntity() instanceof Villager) {
			e.setCancelled(true);
		}
		if(e.getEntity() instanceof ArmorStand) {
			e.setCancelled(true);
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void respawn(PlayerRespawnEvent e) {
		Location respawnloc = null;
		Player p = e.getPlayer();
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			p.setGameMode(GameMode.SPECTATOR);
			if(teams.red.hasEntry(p.getName())) {
				respawnloc=mapengine.redspawn;
				//e.setRespawnLocation(mapengine.redspawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.redteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.red.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.red.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.blue.hasEntry(p.getName())) {
				respawnloc=mapengine.bluespawn;
				//e.setRespawnLocation(mapengine.bluespawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.blueteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.blue.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.blue.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.green.hasEntry(p.getName())) {
				respawnloc=mapengine.greenspawn;
				//e.setRespawnLocation(mapengine.greenspawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.greenteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.green.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.green.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.yellow.hasEntry(p.getName())) {
				respawnloc=mapengine.yellowspawn;
				//e.setRespawnLocation(mapengine.yellowspawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.yellowteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.yellow.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.yellow.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.purple.hasEntry(p.getName())) {
				respawnloc=mapengine.purplespawn;
				//e.setRespawnLocation(mapengine.purplespawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.purpleteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.purple.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.purple.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.magenta.hasEntry(p.getName())) {
				respawnloc=mapengine.magentaspawn;
				//e.setRespawnLocation(mapengine.magentaspawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.magentateam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.magenta.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.magenta.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.white.hasEntry(p.getName())) {
				respawnloc=mapengine.whitespawn;
				//e.setRespawnLocation(mapengine.whitespawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.whiteteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.white.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.white.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else
			if(teams.black.hasEntry(p.getName())) {
				respawnloc=mapengine.blackspawn;
				//e.setRespawnLocation(mapengine.blackspawn);
				e.setRespawnLocation(p.getLocation());
				if(gametimer.blackteam==false) {
					teams.spectator.addEntry(p.getName());
					p.setGameMode(GameMode.SPECTATOR);
					teams.players.remove(p.getUniqueId());
					respawnloc=null;

					teams.spectator.addEntry(p.getName());
					bedwars.sendTitle(p, "§4§l§o0", "Du schaust nun zu");
					p.setGameMode(GameMode.SPECTATOR);

					for(String str:spectator.specstands.keySet()) {
						Location specloc = spectator.specstands.get(str);
						spectator.addArmorStand(p, specloc, spectator.entityIds, str);
					}

					ArrayList<String> plays = new ArrayList<String>();
					for(Team rt:gametimer.restteams) {
						for(String entr:rt.getEntries()) {
							plays.add(entr);
						}
					}
					p.setSpectatorTarget(null);
					String entr = plays.get((new Random()).nextInt(plays.size()));
					Player rtp=Bukkit.getPlayer(entr);
					p.teleport(rtp.getLocation());
					new BukkitRunnable() {
						
						@Override
						public void run() {
							if(teams.black.getEntries().size()==0) {

								for(Player pa:Bukkit.getOnlinePlayers()) {
									pa.playSound(pa.getLocation(), Sound.ENDERMAN_SCREAM, 1, 1);
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
									centralmessage.sendCenteredMessage(pa, "§7Team §r"+teams.black.getDisplayName()+"§r§7 wurde ausradiert");
									centralmessage.sendCenteredMessage(pa, "§4§m§l-----]---------[-------]---------[-----");
								}
							}
							
						}
					}.runTaskLater(plugin, 2);
				}
			}else {

				p.setGameMode(GameMode.SPECTATOR);
				String world = plugin.cfgPRcore_join.getString(".world");
		        int x = plugin.cfgPRcore_join.getInt(".x");
		        int y = plugin.cfgPRcore_join.getInt(".y");
		        int z = plugin.cfgPRcore_join.getInt(".z");
		        double xp = plugin.cfgPRcore_join.getInt(".x")+0.5;
		        double yp = plugin.cfgPRcore_join.getInt(".y")+0.5;
		        double zp = plugin.cfgPRcore_join.getInt(".z")+0.5;
		        int yaw = plugin.cfgPRcore_join.getInt(".yaw");
		        int pitch = plugin.cfgPRcore_join.getInt(".pitch");
		        Location loc = new Location(Bukkit.getWorld(world), x, y, z);
		        Location locp = new Location(Bukkit.getWorld(world), xp, yp, zp);
		        loc.setPitch((float)pitch);
		        loc.setYaw((float)yaw);
		        locp.setPitch((float)pitch);
		        locp.setYaw((float)yaw);
		        p.teleport(locp);
		        p.playEffect(loc, Effect.ENDER_SIGNAL, 1000);
			}
			if(respawnloc!=null) {
				final Location respawnloc1 = respawnloc.clone();
				new BukkitRunnable() {
					int time = respawntime;
					@Override
					public void run() {
						if(time==3) {
							bedwars.sendTitle(p, "§c§o3", null);
							time--;
						}else if(time==2) {
							bedwars.sendTitle(p, "§e§n2", null);
							
							time--;
						}else if(time==1) {
							bedwars.sendTitle(p, "§a§l1", null);
							if(!manipulator.respawneffects.isEmpty()) {
								for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
									p.removePotionEffect(rdmeventeffect.getType());
								}
							}
							time--;
						}else if(time==0) {
							bedwars.sendTitle(p, "  ", null);
							
							p.setGameMode(GameMode.SURVIVAL);
							p.teleport(respawnloc1.clone());
							if(!manipulator.respawneffects.isEmpty()) {
								for(PotionEffect rdmeventeffect:manipulator.respawneffects) {
									p.addPotionEffect(rdmeventeffect);
								}
							}
							cancel();
						} else {
							time--;
						}
						
					}
					
				}.runTaskTimer(plugin, 20, 20);
			}
		}
		
	}
	@EventHandler
    public void craftItem(PrepareItemCraftEvent e) {
		e.getInventory().setResult(new ItemStack(Material.AIR));
	}
	protected void removeWorldBorderPacket(Player player) {
		CraftPlayer cp = (CraftPlayer) player;
		WorldBorder ww = new WorldBorder();
		ww.setSize(30_000_000);
		ww.setCenter(player.getLocation().getX(), player.getLocation().getZ());
		cp.getHandle().playerConnection.sendPacket(new PacketPlayOutWorldBorder(ww, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE));

	}
	protected void sendWorldBorderPacket(Player player, int warningBlocks) {
        EntityPlayer nmsPlayer = ((CraftPlayer) player).getHandle();
        WorldBorder playerWorldBorder = nmsPlayer.world.getWorldBorder();
        PacketPlayOutWorldBorder worldBorder = new PacketPlayOutWorldBorder(playerWorldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_WARNING_BLOCKS);
        try {
            Field field = worldBorder.getClass().getDeclaredField("i");
            field.setAccessible(true);
            field.setInt(worldBorder, warningBlocks);
            field.setAccessible(!field.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        }
        nmsPlayer.playerConnection.sendPacket(worldBorder);
//		CraftPlayer cp = (CraftPlayer) player;
//
//		WorldBorder w = new WorldBorder();
//		w.setSize(1);
//		w.setCenter(player.getLocation().getX() + 10_000, player.getLocation().getZ() + 10_000);
//		cp.getHandle().playerConnection.sendPacket(new PacketPlayOutWorldBorder(w, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE));

    }
}
