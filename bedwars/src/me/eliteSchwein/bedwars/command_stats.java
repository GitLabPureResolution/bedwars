package me.eliteSchwein.bedwars;


import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class command_stats implements CommandExecutor{

    HeadDatabaseAPI api = new HeadDatabaseAPI();
	@SuppressWarnings("unused")
	private bedwars plugin;

	public command_stats(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(args.length==1) {
				try {
				OfflinePlayer po = Bukkit.getOfflinePlayer(args[0]);
				UUID uuid = po.getUniqueId();
				ArrayList<String> test = me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars");
				if(test.size()==0) {
					p.sendMessage(bedwars.MessageNoPerm+"Noch keine Stats vorhanden");
					
					return true;
				}

				Inventory ses;
				ses = p.getServer().createInventory(null,9, "§a"+po.getName()+"'s Stats");
				

				ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
	            SkullMeta meta = (SkullMeta)skull.getItemMeta();
	            meta.setOwner(p.getName());
	            meta.setDisplayName("§cGlobale §7Stats");
	            meta.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Global"));
	            skull.setItemMeta(meta);
					ses.setItem(6, skull);

					 ItemStack leaveno = new ItemStack(Material.WEB);
	    			 ItemMeta leavenom = leaveno.getItemMeta();
						leavenom.setDisplayName("§cExit");
						leaveno.setItemMeta(leavenom);
						ses.setItem(8, leaveno);

						 ItemStack killsno = new ItemStack(Material.WOOL);
		    			 ItemMeta killsnom = killsno.getItemMeta();
							killsnom.setDisplayName("§cKills");
							killsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Kills"));
							killsno.setItemMeta(killsnom);
							ses.setItem(1, killsno);

							 ItemStack todesno = new ItemStack(Material.WOOL);
			    			 ItemMeta todesnom = todesno.getItemMeta();
								todesnom.setDisplayName("§cTode");
								todesnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Tode"));
								todesno.setItemMeta(todesnom);
								ses.setItem(2, todesno);

								 ItemStack KDsno = new ItemStack(Material.WOOL);
				    			 ItemMeta KDsnom = KDsno.getItemMeta();
									KDsnom.setDisplayName("§cKDs");
									KDsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.KD"));
									KDsno.setItemMeta(KDsnom);
									ses.setItem(3, KDsno);

									 ItemStack bedsno = new ItemStack(Material.WOOL);
					    			 ItemMeta bedsnom = bedsno.getItemMeta();
										bedsnom.setDisplayName("§cBetten zerstört");
										bedsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Bed_Zerstoert"));
										bedsno.setItemMeta(bedsnom);
										ses.setItem(4, bedsno);
				p.openInventory(ses);}catch(NullPointerException en) {
					p.sendMessage(bedwars.MessageNoPerm+"Noch keine Stats vorhanden");
				}
			}else {
				
				
				UUID uuid = p.getUniqueId();

				Inventory ses;
				ses = p.getServer().createInventory(null,9, "§a"+p.getName()+"'s Stats");
				

				ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
	            SkullMeta meta = (SkullMeta)skull.getItemMeta();
	            meta.setOwner(p.getName());
	            meta.setDisplayName("§cGlobale §7Stats");
	            meta.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Global"));
	            skull.setItemMeta(meta);
					ses.setItem(6, skull);

					 ItemStack leaveno = new ItemStack(Material.WEB);
	    			 ItemMeta leavenom = leaveno.getItemMeta();
						leavenom.setDisplayName("§cExit");
						leaveno.setItemMeta(leavenom);
						ses.setItem(8, leaveno);

						 ItemStack killsno = new ItemStack(Material.WOOL);
		    			 ItemMeta killsnom = killsno.getItemMeta();
							killsnom.setDisplayName("§cKills");
							killsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Kills"));
							killsno.setItemMeta(killsnom);
							ses.setItem(1, killsno);

							 ItemStack todesno = new ItemStack(Material.WOOL);
			    			 ItemMeta todesnom = todesno.getItemMeta();
								todesnom.setDisplayName("§cTode");
								todesnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Tode"));
								todesno.setItemMeta(todesnom);
								ses.setItem(2, todesno);

								 ItemStack KDsno = new ItemStack(Material.WOOL);
				    			 ItemMeta KDsnom = KDsno.getItemMeta();
									KDsnom.setDisplayName("§cKDs");
									KDsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.KD"));
									KDsno.setItemMeta(KDsnom);
									ses.setItem(3, KDsno);

									 ItemStack bedsno = new ItemStack(Material.WOOL);
					    			 ItemMeta bedsnom = bedsno.getItemMeta();
										bedsnom.setDisplayName("§cBetten zerstört");
										bedsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(uuid, "Bedwars.Bed_Zerstoert"));
										bedsno.setItemMeta(bedsnom);
										ses.setItem(4, bedsno);
				p.openInventory(ses);
			}
			
		}else {
			bedwars.chatconsole("§cPlayer only!");
		}
		return false;
	}

}
