package me.eliteSchwein.bedwars;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.scoreboard.Team;

import me.arcaniax.hdb.api.HeadDatabaseAPI;

public class event_lobby implements Listener {
	HeadDatabaseAPI api = new HeadDatabaseAPI();
    @SuppressWarnings("unused")
	private bedwars plugin;
	public event_lobby(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	@SuppressWarnings("unused")
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(!teams.saver.hasEntry(p.getName())) {
		if(timer.runningtimer==null) {
			e.setCancelled(true);
			try {
			String itemname = e.getCurrentItem().getItemMeta().getDisplayName();
			Material itemmaterial = e.getCurrentItem().getType();
			if(itemname!=null&&itemmaterial!=Material.AIR) {
				if(itemname.contains("Team")) {
					e.getWhoClicked().closeInventory();
					((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.WOOD_CLICK, 1, 1);
				}else {
					for(Team team:teams.activeteams) {
						if(team.getDisplayName().equalsIgnoreCase(itemname)) {
							String checkteam1=gameengine.setTeam(p, team);
							gameengine.removeTeam(p);
							String checkteam=gameengine.setTeam(p, team);
							if(checkteam.equalsIgnoreCase("inside")) {
								p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
								p.sendMessage(bedwars.MessageNoPerm+"Du bist bereits im Team");
							}else if(checkteam.equalsIgnoreCase("full")) {
								p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
								p.sendMessage(bedwars.MessageNoPerm+"Das Team ist voll");
								
							}else {
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast das Team gewechselt");
								
							}
						}
					}
				}
			}}catch(NullPointerException en) {
				
			}
		}else if(timer.runningtimer.equalsIgnoreCase("Lobby")||timer.runningtimer.equalsIgnoreCase("End")) {
			if(!teams.saver.hasEntry(p.getName())) {
			e.setCancelled(true);
			
			try {

				String itemname = e.getCurrentItem().getItemMeta().getDisplayName();
				Material itemmaterial = e.getCurrentItem().getType();
				if(itemname!=null&&itemmaterial!=Material.AIR) {
				String inventoryname = e.getClickedInventory().getTitle();
				if(inventoryname.equalsIgnoreCase("§8>> §3Team-Auswahl")) {
				if(itemname.contains("Team")) {
					e.getWhoClicked().closeInventory();
					((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.WOOD_CLICK, 1, 1);
				}else {
					for(Team team:teams.activeteams) {
						if(team.getDisplayName().equalsIgnoreCase(itemname)) {
							String enginetest = gameengine.setTeam(p, team);
							if(enginetest.equalsIgnoreCase("full")) {
								p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
								p.sendMessage(bedwars.MessageNoPerm+"Das Team ist voll");
								
							} else
							if(enginetest.equalsIgnoreCase("inside")) {
								p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
								p.sendMessage(bedwars.MessageNoPerm+"Du bist bereits im Team");
							}else  {
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast das Team gewechselt");
								
							}
						}
					}
				}
				}else if(inventoryname.equalsIgnoreCase("§8>>§3Map-Vote")) {
					if(itemname.equalsIgnoreCase("§cMap-Vote schließen?")) {

						e.getWhoClicked().closeInventory();
						((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.WOOD_CLICK, 1, 1);
					}else {
						String mapvote = itemname.replaceAll("§c", "").replace("§c", "");
						boolean check = gameengine.mapvoteblacklist.contains(p.getUniqueId());
						if(check==true) {
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
							p.sendMessage(bedwars.MessageNoPerm+"Du hast bereits gevoted!");
						}else {
							p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
							p.sendMessage(bedwars.MessagePerm+"Du hast für §l"+mapvote+" §r§2gevoted");
							gameengine.addVote(e.getWhoClicked().getUniqueId(), mapvote);
							
						}
							
						
						
					}
					
					
				}else if(inventoryname.equalsIgnoreCase("§8>>§3Random-Vote")) {
					if(itemname.equalsIgnoreCase("§cRandom Event-Vote schließen?")) {

						e.getWhoClicked().closeInventory();
						((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.WOOD_CLICK, 1, 1);
					}else {
						String mapvote = itemname.replaceAll("§c", "").replace("§c", "");
						boolean check = mapengine.randomevnvotedn.contains(p.getUniqueId());
						boolean check1 = mapengine.randomevnvotedy.contains(p.getUniqueId());
						if(check==true||check1==true) {
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
							p.sendMessage(bedwars.MessageNoPerm+"Du hast bereits gevoted!");
						}else {
							if(mapvote.contains("abschalten")) {
								mapengine.randomevnvotedn.add(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast dich für abschalten entschieden");
								
							}else {
								mapengine.randomevnvotedy.add(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast dich für anschalten entschieden");
								
							}
							p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
							
						}
							
						
						
					}
					
					
				}else if(inventoryname.equalsIgnoreCase("§8>>§3"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote")) {
					if(itemname.equalsIgnoreCase("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote schließen?")) {

						e.getWhoClicked().closeInventory();
						((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.WOOD_CLICK, 1, 1);
					}else {
						String mapvote = itemname.replaceAll("§c", "").replace("§c", "");
						boolean check = mapengine.tier3votedn.contains(p.getUniqueId());
						boolean check1 = mapengine.tier3votedy.contains(p.getUniqueId());
						if(check==true||check1==true) {
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
							p.sendMessage(bedwars.MessageNoPerm+"Du hast bereits gevoted!");
						}else {
							if(mapvote.contains("abschalten")) {
								mapengine.tier3votedn.add(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast dich für abschalten entschieden");
								
							}else {
								mapengine.tier3votedy.add(p.getUniqueId());
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
								p.sendMessage(bedwars.MessagePerm+"Du hast dich für anschalten entschieden");
								
							}
							p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
							
						}
							
						
						
					}
					
					
				}}}catch(NullPointerException en) {
				
			}}
		}}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onArmorStand(PlayerInteractAtEntityEvent e) {
		Player p = e.getPlayer();
		if(!teams.saver.hasEntry(p.getName())) {
		if(timer.runningtimer==null) {
			e.setCancelled(true);
		}else if(timer.runningtimer.equalsIgnoreCase("Lobby")||timer.runningtimer.equalsIgnoreCase("End")) {
			e.setCancelled(true);
			Entity et = e.getRightClicked();
			if(et instanceof ArmorStand) {
				ArmorStand am = (ArmorStand) et;
				String NameTag = am.getCustomName();
				if(NameTag.contains("Frei")) {
					for(Team team:teams.activeteams) {
						String checkprefix = team.getPrefix();
						if(NameTag.contains(checkprefix)) {
							if(gameengine.setTeam(p, team).equalsIgnoreCase("inside")) {
								p.sendMessage(bedwars.MessageNoPerm+"Du bist schon in dem Team");
								p.playSound(et.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
								p.playEffect(et.getLocation(), Effect.LAVA_POP, 100);
							}
						}
					}
				}
			}
			
		}}
	}
}
