package me.eliteSchwein.bedwars;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import me.arcaniax.hdb.api.HeadDatabaseAPI;
import me.eliteSchwein.PRcore.PRcore;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;

public class event_hotbar implements Listener{
	HeadDatabaseAPI api = new HeadDatabaseAPI();

	private bedwars plugin;
	public event_hotbar(bedwars plugin) {
		// TODO Auto-generated constructor stub
		this.plugin = plugin;
	}
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if(!teams.saver.hasEntry(p.getName())) {
		if(timer.runningtimer==null) {
			e.setCancelled(true);
		}else if(timer.runningtimer.equalsIgnoreCase("Lobby")||timer.runningtimer.equalsIgnoreCase("End")) {
			e.setCancelled(true);
		}
			
		}
		
	}
	@SuppressWarnings("unchecked")
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(!teams.saver.hasEntry(p.getName())&&!teams.saverlobby.hasEntry(p.getName())) {
		if(timer.runningtimer==null) {
			int itemslot = p.getInventory().getHeldItemSlot();
			if(p.getItemInHand().getType()!=Material.AIR) {
				if(itemslot==0) {
					if(gameengine.allowmapvote==true) {
						ArrayList<String> maps  = new ArrayList<String>();
						maps=(ArrayList<String>) bedwars.cfgmain.getList("maps");
						Inventory ses;
						ses = p.getServer().createInventory(null,27, "§8>>§3Map-Vote");
						for(int i = 0;i<maps.size();i++) {

			    			 ItemStack teamexit = new ItemStack(Material.PAPER);
			    			 ItemMeta teamexit1 = teamexit.getItemMeta();
								teamexit1.setDisplayName("§c"+maps.get(i));
								ArrayList<String> Lore1 = new ArrayList<String>();
								Lore1.add("§aKlick, zum voten");
								teamexit1.setLore(Lore1);
								teamexit.setItemMeta(teamexit1);
								ses.setItem(i, teamexit);
						}

		    			 ItemStack teamexit = new ItemStack(Material.BARRIER);
		    			 ItemMeta teamexit1 = teamexit.getItemMeta();
							teamexit1.setDisplayName("§cMap-Vote schließen?");
							ArrayList<String> Lore1 = new ArrayList<String>();
							Lore1.add("§aKlick, zum schließen");
							teamexit1.setLore(Lore1);
							teamexit.setItemMeta(teamexit1);
							ses.setItem(26, teamexit);
						p.openInventory(ses);
//						p.updateInventory();
					}else {

						p.sendMessage(bedwars.MessageNoPerm+"Map-Vote wurde gesperrt!");
						p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
					}
					
				}
				if(itemslot==1) {
					if(mapengine.allowtier3vote==true) {
						Inventory ses;
						ses = p.getServer().createInventory(null,9, "§8>>§3"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote");
						 ItemStack teamexit = new ItemStack(Material.BARRIER);
		    			 ItemMeta teamexit1 = teamexit.getItemMeta();
							teamexit1.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote schließen?");
							ArrayList<String> Lore1 = new ArrayList<String>();
							Lore1.add("§aKlick, zum schließen");
							teamexit1.setLore(Lore1);
							teamexit.setItemMeta(teamexit1);
							ses.setItem(8, teamexit);

							 ItemStack teamyes = api.getItemHead("844");
			    			 ItemMeta teamyesm = teamyes.getItemMeta();
								teamyesm.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+" abschalten?");
								ArrayList<String> Loreyes = new ArrayList<String>();
								Loreyes.add("§aKlick, zum voten");
								teamyesm.setLore(Loreyes);
								teamyes.setItemMeta(teamyesm);
								ses.setItem(1, teamyes);

								 ItemStack teamno = api.getItemHead("845");
				    			 ItemMeta teamnom = teamno.getItemMeta();
									teamnom.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+" anschalten?");
									ArrayList<String> Loreno = new ArrayList<String>();
									Loreno.add("§aKlick, zum voten");
									teamnom.setLore(Loreno);
									teamno.setItemMeta(teamnom);
									ses.setItem(0, teamno);
						p.openInventory(ses);
					}else {
						p.sendMessage(bedwars.MessageNoPerm+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote wurde gesperrt!");
						p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
					}
					
				}
				if(itemslot==2) {
					if(timer.lobbytimersec==5||timer.lobbytimersec<=5) {
						p.sendMessage(bedwars.MessageNoPerm+"Countdown zu niedrig");
						
					}else {
						timer.manlobby(0, 6);
					}
					
				}
				if(itemslot==4) {
					PRcore.hubonep("§cVerlassen", p);
					
				}
				if(itemslot==6) {
					if(mapengine.allowrandomevnvote==true) {
						Inventory ses;
						ses = p.getServer().createInventory(null,9, "§8>>§3Random-Vote");
						 ItemStack teamexit = new ItemStack(Material.BARRIER);
		    			 ItemMeta teamexit1 = teamexit.getItemMeta();
							teamexit1.setDisplayName("§cRandom Event-Vote schließen?");
							ArrayList<String> Lore1 = new ArrayList<String>();
							Lore1.add("§aKlick, zum schließen");
							teamexit1.setLore(Lore1);
							teamexit.setItemMeta(teamexit1);
							ses.setItem(8, teamexit);

							 ItemStack teamyes = api.getItemHead("844");
			    			 ItemMeta teamyesm = teamyes.getItemMeta();
								teamyesm.setDisplayName("§cRandom Event-Vote abschalten?");
								ArrayList<String> Loreyes = new ArrayList<String>();
								Loreyes.add("§aKlick, zum voten");
								teamyesm.setLore(Loreyes);
								teamyes.setItemMeta(teamyesm);
								ses.setItem(1, teamyes);

								 ItemStack teamno = api.getItemHead("845");
				    			 ItemMeta teamnom = teamno.getItemMeta();
									teamnom.setDisplayName("§cRandom Event-Vote anschalten?");
									ArrayList<String> Loreno = new ArrayList<String>();
									Loreno.add("§aKlick, zum voten");
									teamnom.setLore(Loreno);
									teamno.setItemMeta(teamnom);
									ses.setItem(0, teamno);
						p.openInventory(ses);
					}else {
						p.sendMessage(bedwars.MessageNoPerm+"Random Event-Vote wurde gesperrt!");
						p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
					}
					
				}
				if(itemslot==7) {
					if(gameengine.allowteamchange==true) {
						Inventory ses;
						ses = p.getServer().createInventory(null,9, "§8>> §3Team-Auswahl");
						p.openInventory(ses);
//						p.updateInventory();
						}else {

							p.sendMessage(bedwars.MessageNoPerm+"Team-Wechsel wurde gesperrt!");
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
							
						}
					
				}
				if(itemslot==8) {
					event_stats.kdsett(p.getUniqueId());
					Inventory ses;
					ses = p.getServer().createInventory(null,9, "§a"+p.getName()+"'s Stats");
					

					ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
		            SkullMeta meta = (SkullMeta)skull.getItemMeta();
		            meta.setOwner(p.getName());
		            meta.setDisplayName("§cGlobale §7Stats");
		            meta.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Global"));
		            skull.setItemMeta(meta);
						ses.setItem(6, skull);

						 ItemStack leaveno = new ItemStack(Material.WEB);
		    			 ItemMeta leavenom = leaveno.getItemMeta();
							leavenom.setDisplayName("§cExit");
							leaveno.setItemMeta(leavenom);
							ses.setItem(8, leaveno);

							 ItemStack killsno = new ItemStack(Material.WOOL);
			    			 ItemMeta killsnom = killsno.getItemMeta();
								killsnom.setDisplayName("§cKills");
								killsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Kills"));
								killsno.setItemMeta(killsnom);
								ses.setItem(1, killsno);

								 ItemStack todesno = new ItemStack(Material.WOOL);
				    			 ItemMeta todesnom = todesno.getItemMeta();
									todesnom.setDisplayName("§ctodes");
									todesnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Tode"));
									todesno.setItemMeta(todesnom);
									ses.setItem(2, todesno);

									 ItemStack KDsno = new ItemStack(Material.WOOL);
					    			 ItemMeta KDsnom = KDsno.getItemMeta();
										KDsnom.setDisplayName("§cKDs");
										KDsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.KD"));
										KDsno.setItemMeta(KDsnom);
										ses.setItem(3, KDsno);

										 ItemStack bedsno = new ItemStack(Material.WOOL);
						    			 ItemMeta bedsnom = bedsno.getItemMeta();
											bedsnom.setDisplayName("§cBetten zerstört");
											bedsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Bed_Zerstoert"));
											bedsno.setItemMeta(bedsnom);
											ses.setItem(4, bedsno);
					p.openInventory(ses);
					
				}
			}
			e.setCancelled(true);
		}else if(timer.runningtimer.equalsIgnoreCase("Lobby")||timer.runningtimer.equalsIgnoreCase("End")) {
			if(!teams.saver.hasEntry(p.getName())&&!teams.saverlobby.hasEntry(p.getName())) {
				int itemslot = p.getInventory().getHeldItemSlot();
				if(p.getItemInHand().getType()!=Material.AIR) {
					if(itemslot==0) {
						if(gameengine.allowmapvote==true) {
							ArrayList<String> maps  = new ArrayList<String>();
							maps=(ArrayList<String>) bedwars.cfgmain.getList("maps");
							Inventory ses;
							ses = p.getServer().createInventory(null,27, "§8>>§3Map-Vote");
							for(int i = 0;i<maps.size();i++) {

				    			 ItemStack teamexit = new ItemStack(Material.PAPER);
				    			 ItemMeta teamexit1 = teamexit.getItemMeta();
									teamexit1.setDisplayName("§c"+maps.get(i));
									ArrayList<String> Lore1 = new ArrayList<String>();
									Lore1.add("§aKlick, zum voten");
									teamexit1.setLore(Lore1);
									teamexit.setItemMeta(teamexit1);
									ses.setItem(i, teamexit);
							}

			    			 ItemStack teamexit = new ItemStack(Material.BARRIER);
			    			 ItemMeta teamexit1 = teamexit.getItemMeta();
								teamexit1.setDisplayName("§cMap-Vote schließen?");
								ArrayList<String> Lore1 = new ArrayList<String>();
								Lore1.add("§aKlick, zum schließen");
								teamexit1.setLore(Lore1);
								teamexit.setItemMeta(teamexit1);
								ses.setItem(26, teamexit);
							p.openInventory(ses);
//							p.updateInventory();
						}else {

							p.sendMessage(bedwars.MessageNoPerm+"Mapvote wurde gesperrt!");
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
						}
						
					}
					if(itemslot==1) {
						if(mapengine.allowtier3vote==true) {
							Inventory ses;
							ses = p.getServer().createInventory(null,9, "§8>>§3"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote");
							 ItemStack teamexit = new ItemStack(Material.BARRIER);
			    			 ItemMeta teamexit1 = teamexit.getItemMeta();
								teamexit1.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote schließen?");
								ArrayList<String> Lore1 = new ArrayList<String>();
								Lore1.add("§aKlick, zum schließen");
								teamexit1.setLore(Lore1);
								teamexit.setItemMeta(teamexit1);
								ses.setItem(8, teamexit);

								 ItemStack teamyes1 = api.getItemHead("844");
				    			 ItemMeta teamyesm = teamyes1.getItemMeta();
									teamyesm.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+" abschalten?");
									ArrayList<String> Loreyes = new ArrayList<String>();
									Loreyes.add("§aKlick, zum voten");
									teamyesm.setLore(Loreyes);
									teamyes1.setItemMeta(teamyesm);
									ses.setItem(1, teamyes1);

									 ItemStack teamno = api.getItemHead("845");
					    			 ItemMeta teamnom = teamno.getItemMeta();
										teamnom.setDisplayName("§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+" anschalten?");
										ArrayList<String> Loreno = new ArrayList<String>();
										Loreno.add("§aKlick, zum voten");
										teamnom.setLore(Loreno);
										teamno.setItemMeta(teamnom);
										ses.setItem(0, teamno);
							p.openInventory(ses);
						}else {
							p.sendMessage(bedwars.MessageNoPerm+bedwars.cfgmoney.getString("moneytyp.tier3")+"-Vote wurde gesperrt!");
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
						}
					}
					if(itemslot==2) {
						if(timer.lobbytimersec==5||timer.lobbytimersec<=5) {
							p.sendMessage(bedwars.MessageNoPerm+"Countdown zu niedrig");
							
						}else {
							timer.manlobby(0, 6);
						}
						
					}
					if(itemslot==3) {
						
					}
					if(itemslot==4) {
						PRcore.hubonep("§cVerlassen", p);
						
					}
					if(itemslot==6) {
						if(mapengine.allowrandomevnvote==true) {
							Inventory ses;
							ses = p.getServer().createInventory(null,9, "§8>>§3Random-Vote");
							 ItemStack teamexit = new ItemStack(Material.BARRIER);
			    			 ItemMeta teamexit1 = teamexit.getItemMeta();
								teamexit1.setDisplayName("§cRandom Event-Vote schließen?");
								ArrayList<String> Lore1 = new ArrayList<String>();
								Lore1.add("§aKlick, zum schließen");
								teamexit1.setLore(Lore1);
								teamexit.setItemMeta(teamexit1);
								ses.setItem(8, teamexit);

								 ItemStack teamyes = api.getItemHead("844");
				    			 ItemMeta teamyesm = teamyes.getItemMeta();
									teamyesm.setDisplayName("§cRandom Event-Vote abschalten?");
									ArrayList<String> Loreyes = new ArrayList<String>();
									Loreyes.add("§aKlick, zum voten");
									teamyesm.setLore(Loreyes);
									teamyes.setItemMeta(teamyesm);
									ses.setItem(1, teamyes);

									 ItemStack teamno = api.getItemHead("845");
					    			 ItemMeta teamnom = teamno.getItemMeta();
										teamnom.setDisplayName("§cRandom Event-Vote anschalten?");
										ArrayList<String> Loreno = new ArrayList<String>();
										Loreno.add("§aKlick, zum voten");
										teamnom.setLore(Loreno);
										teamno.setItemMeta(teamnom);
										ses.setItem(0, teamno);
							p.openInventory(ses);
						}else {
							p.sendMessage(bedwars.MessageNoPerm+"Random Event-Vote wurde gesperrt!");
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
						}
						
					}
					if(itemslot==7) {
						if(gameengine.allowteamchange==true) {
						Inventory ses;
						ses = p.getServer().createInventory(null,9, "§8>> §3Team-Auswahl");
						p.openInventory(ses);
//						p.updateInventory();
						}else {

							p.sendMessage(bedwars.MessageNoPerm+"Team-Wechsel wurde gesperrt!");
							p.playSound(p.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
							
						}
						
					}
					if(itemslot==8) {
						event_stats.kdsett(p.getUniqueId());
						Inventory ses;
						ses = p.getServer().createInventory(null,9, "§a"+p.getName()+"'s Stats");
						

						ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			            SkullMeta meta = (SkullMeta)skull.getItemMeta();
			            meta.setOwner(p.getName());
			            meta.setDisplayName("§cGlobale §7Stats");
			            meta.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Global"));
			            skull.setItemMeta(meta);
							ses.setItem(6, skull);

							 ItemStack leaveno = new ItemStack(Material.WEB);
			    			 ItemMeta leavenom = leaveno.getItemMeta();
								leavenom.setDisplayName("§cExit");
								leaveno.setItemMeta(leavenom);
								ses.setItem(8, leaveno);

								 ItemStack killsno = new ItemStack(Material.WOOL);
				    			 ItemMeta killsnom = killsno.getItemMeta();
									killsnom.setDisplayName("§cKills");
									killsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Kills"));
									killsno.setItemMeta(killsnom);
									ses.setItem(1, killsno);

									 ItemStack todesno = new ItemStack(Material.WOOL);
					    			 ItemMeta todesnom = todesno.getItemMeta();
										todesnom.setDisplayName("§cTode");
										todesnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Tode"));
										todesno.setItemMeta(todesnom);
										ses.setItem(2, todesno);

										 ItemStack KDsno = new ItemStack(Material.WOOL);
						    			 ItemMeta KDsnom = KDsno.getItemMeta();
											KDsnom.setDisplayName("§cKDs");
											KDsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.KD"));
											KDsno.setItemMeta(KDsnom);
											ses.setItem(3, KDsno);

											 ItemStack bedsno = new ItemStack(Material.WOOL);
							    			 ItemMeta bedsnom = bedsno.getItemMeta();
												bedsnom.setDisplayName("§cBetten zerstört");
												bedsnom.setLore(me.eliteSchwein.PRcore_addon.stats.getstatsMessageArrayList(p.getUniqueId(), "Bedwars.Bed_Zerstoert"));
												bedsno.setItemMeta(bedsnom);
												ses.setItem(4, bedsno);
						p.openInventory(ses);
						
					}
				}else if(e.getAction()==Action.RIGHT_CLICK_AIR&&teams.spectator.hasEntry(e.getPlayer().getName())) {
					spectator.SpecTest(e.getPlayer());
				}
				e.setCancelled(true);
			}
			
		}}
	}
	@SuppressWarnings("unused")
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.getPlayer().setHealth(20);
		e.getPlayer().setSaturation(20);
		for(PotionEffect pt : e.getPlayer().getActivePotionEffects()) {
			e.getPlayer().removePotionEffect(pt.getType());
		}
		if(timer.runningtimer.equalsIgnoreCase("Lobby")) {
			Player p = e.getPlayer();
			for (Player allplayer :  Bukkit.getOnlinePlayers()) {

				try {

					String teamname = teams.board.getEntryTeam(allplayer.getName()).getDisplayName();
				}catch(NullPointerException en) {
					teams.lobbyuser.addEntry(p.getName());
				
			}
			}
			ItemStack MapVote = new ItemStack(Material.PAPER);
			ItemMeta MapVote_meta = MapVote.getItemMeta();
			ArrayList<String> MapVote_Lore = new ArrayList<>();
			MapVote_Lore.add("§aKlick, um eine §6Map §azu wählen");
			MapVote_meta.setDisplayName("§6MapVote");
			MapVote_meta.setLore(MapVote_Lore);
			MapVote.setItemMeta(MapVote_meta);
			
			ItemStack RandomEvnVote = new ItemStack(Material.WATCH);
			ItemMeta RandomEvnVote_meta = RandomEvnVote.getItemMeta();
			ArrayList<String> RandomEvnVote_Lore = new ArrayList<>();
			RandomEvnVote_Lore.add("§aKlick, für Random Event");
			RandomEvnVote_meta.setDisplayName("§6Random Event?");
			RandomEvnVote_meta.setLore(RandomEvnVote_Lore);
			RandomEvnVote.setItemMeta(RandomEvnVote_meta);
			
			ItemStack Hub = new ItemStack(Material.WEB);
			ItemMeta Hub_meta = Hub.getItemMeta();
			ArrayList<String> Hub_Lore = new ArrayList<>();
			Hub_Lore.add("§aKlick, um zu §cverlassen!");
			Hub_meta.setDisplayName("§cLeave");
			Hub_meta.setLore(Hub_Lore);
			Hub.setItemMeta(Hub_meta);
			
			ItemStack goldvote = new ItemStack(Material.getMaterial(bedwars.cfgmoney.getString("material.tier3")));
			ItemMeta goldvote_meta = goldvote.getItemMeta();
			ArrayList<String> goldvote_Lore = new ArrayList<>();
			goldvote_Lore.add("§aKlick, für kein "+bedwars.cfgmoney.getString("moneytyp.tier3"));
			goldvote_meta.setDisplayName("§6Kein "+bedwars.cfgmoney.getString("moneytyp.tier3")+"?");
			goldvote_meta.setLore(goldvote_Lore);
			goldvote.setItemMeta(goldvote_meta);
			
			ItemStack SchnellStart = new ItemStack(Material.DIAMOND);
			ItemMeta SchnellStart_meta = SchnellStart.getItemMeta();
			ArrayList<String> SchnellStart_Lore = new ArrayList<>();
			SchnellStart_Lore.add("§aKlick, um den §6Countdown §azu verkürzen");
			SchnellStart_meta.setDisplayName("§6Schnellstart");
			SchnellStart_meta.setLore(SchnellStart_Lore);
			SchnellStart.setItemMeta(SchnellStart_meta);
			
			ItemStack Stats = new ItemStack(Material.GHAST_TEAR);
			ItemMeta Stats_meta = Stats.getItemMeta();
			ArrayList<String> Stats_Lore = new ArrayList<>();
			Stats_Lore.add("§aKlick, um deine §6Stats §azu sehen");
			Stats_meta.setDisplayName("§6Stats");
			Stats_meta.setLore(Stats_Lore);
			Stats.setItemMeta(Stats_meta);
			
			ItemStack teams = new ItemStack(Material.BED);
			ItemMeta teams_meta = teams.getItemMeta();
			ArrayList<String> teams_Lore = new ArrayList<>();
			teams_Lore.add("§aKlick, für Team");
			teams_meta.setDisplayName("§6Teams");
			teams_meta.setLore(teams_Lore);
			teams.setItemMeta(teams_meta);
			
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			
			p.getInventory().setItem(0, MapVote);
			p.getInventory().setItem(1, goldvote);
			if(p.hasPermission("bedwars.SchnellStart")) {
				p.getInventory().setItem(2, SchnellStart);
			}
			p.getInventory().setItem(4, Hub);
			p.getInventory().setItem(6, RandomEvnVote);
			p.getInventory().setItem(7, teams);
			p.getInventory().setItem(8, Stats);
			
//			p.updateInventory();
			p.setGameMode(GameMode.ADVENTURE);
		}else if(timer.runningtimer.equalsIgnoreCase("game")) {
			Player p = e.getPlayer();
			p.getInventory().clear();
			teams.spectator.addEntry(p.getName());
			p.setGameMode(GameMode.SPECTATOR);
			teams.players.remove(p.getUniqueId());
//			for(String str:spectator.specstands.keySet()) {
//				Location specloc = spectator.specstands.get(str);
//				spectator.addArmorStand(p, specloc, spectator.entityIds, str);
//			}
			ArrayList<String> plays = new ArrayList<String>();
			for(Team rt:gametimer.restteams) {
				for(String entr:rt.getEntries()) {
					plays.add(entr);
				}
			}
			p.setSpectatorTarget(null);
			String entr = plays.get((new Random()).nextInt(plays.size()));
			Player rtp=Bukkit.getPlayer(entr);
			new BukkitRunnable() {

				@Override
				public void run() {
					p.teleport(rtp.getLocation());

					new BukkitRunnable() {

						@Override
						public void run() {
							for(String str:spectator.specstands.keySet()) {
								Location specloc = spectator.specstands.get(str);
								spectator.addArmorStand(p, specloc, spectator.entityIds, str);
							}
						}
					}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 3);
				}
			}.runTaskLater(plugin, 3);
			
		}
		
	}
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if(spectator.entityIds.containsValue(p.getUniqueId().toString())) {
			spectator.removeArmorStand(p, spectator.entityIds);
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			for(ItemStack item:p.getInventory().getContents()) {
				if(item==null) {
					
				}else if(item.getType()==Material.AIR) {
					
				}else {
//					p.getWorld().dropItem(p.getLocation(), item);
					
				}
			}
			for(ItemStack item:p.getInventory().getArmorContents()) {
				if(item==null) {
					
				}else if(item.getType()==Material.AIR) {
					
				}else {
//					p.getWorld().dropItem(p.getLocation(), item);
					
				}
				
			}
			p.getInventory().clear();
			p.getInventory().setBoots(new ItemStack(Material.AIR));
			p.getInventory().setHelmet(new ItemStack(Material.AIR));
			p.getInventory().setLeggings(new ItemStack(Material.AIR));
			p.getInventory().setChestplate(new ItemStack(Material.AIR));
			p.updateInventory();
			}
		new BukkitRunnable() {

			@Override
			public void run() {
				((CraftPlayer)p).getHandle().playerConnection.a(new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN));
				
			}
			
		}.runTaskLater(plugin, 3);
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		try {
		Player player = event.getPlayer();
		ItemStack boots = player.getInventory().getBoots();
		ItemMeta bootsmeta = boots.getItemMeta();
		String bootsname = bootsmeta.getDisplayName();
		Location playerlocation = player.getLocation().clone();
		World world = playerlocation.getWorld();
//		Location fromlocation = event.getFrom();
//		if(fromlocation.getX()==playerlocation.getX()&&fromlocation.getY()==playerlocation.getY()&&fromlocation.getZ()==playerlocation.getZ()) {
//			return;
//		}
		if(bootsname!=null) {
			if(bootsname.equalsIgnoreCase("Brücken Schuhe")) {
				
				Location locationxp = player.getLocation().add(3, 0, 0);
				Location locationxn = player.getLocation().add(-3, 0, 0);
				Location locationyp = player.getLocation().add(0, 3, 0);
				Location locationyn = player.getLocation().add(0, -3, 0);
//				Location locationyn1 = player.getLocation().add(0, -3, 0);
				Location locationzp = player.getLocation().add(0, 0, 3);
				Location locationzn = player.getLocation().add(0, 0, -3);
				if(bedwars.checkEntryfalse(player, world, locationyn)==false) {
					return;
				}
				if(bedwars.checkEntryfalse(player, world, locationyp)==false) {
					return;
				}
				if(bedwars.checkEntryfalse(player, world, locationxn)==false) {
					return;
				}
				if(bedwars.checkEntryfalse(player, world, locationxp)==false) {
					return;
				}
				if(bedwars.checkEntryfalse(player, world, locationzn)==false) {
					return;
				}
				if(bedwars.checkEntryfalse(player, world, locationzp)==false) {
					return;
				}
				ItemStack handitem = player.getItemInHand();
				if(bedwars.buildblocks.contains(handitem.getType())) {
					playerlocation.setPitch(5);
					Location placelocation = playerlocation.clone();
					Vector vector = playerlocation.getDirection();
					placelocation.add(vector.normalize());
					if(placelocation.getBlock().getType()==Material.AIR) {
						placelocation.getBlock().setType(handitem.getType());
						placelocation.getBlock().setData((byte) handitem.getDurability());
						placelocation.getWorld().playSound(placelocation, Sound.PISTON_EXTEND, 1, 1);
						int amount = handitem.getAmount();
						if(amount>=11) {
//							manipulator.hearthsoundcancel(player);
						}else {
							bedwars.sendActionBar(player, "§cAchtung!!! wenige Blöcke!!!");
//							manipulator.hearthsound(player);
						}
						if(amount==1) {
							ItemStack remover = new ItemStack(Material.AIR,1);
							player.setItemInHand(remover);
							player.updateInventory();
						}else {
							int newamount = amount-1;
							handitem.setAmount(newamount);
							player.setItemInHand(handitem);
							player.updateInventory();
						}
					}
				}else {
//					manipulator.hearthsoundcancel(player);
				}
			}
		}}catch(NullPointerException en) {
			
		}
	}
	@EventHandler
	public void spectest(PlayerInteractEvent e) {
		if(e.getAction()==Action.RIGHT_CLICK_AIR&&teams.spectator.hasEntry(e.getPlayer().getName())) {
			spectator.SpecTest(e.getPlayer());
		}
	}
	@EventHandler
	public void specopeninv(InventoryCloseEvent e) {
		if(teams.spectator.hasEntry(e.getPlayer().getName())) {
			e.getPlayer().getInventory().clear();
		}
	}
	

}
