package me.eliteSchwein.bedwars;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

import me.eliteSCHW31N.bedwars.randomevents.manager;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PlayerConnection;


public class bedwars extends JavaPlugin implements Listener{
	
	public static ArrayList<Material> resetblocks = new ArrayList<Material>();
	public static ArrayList<Material> tntblocks = new ArrayList<Material>();
	public static ArrayList<Material> buildblocks = new ArrayList<Material>();
	
	public static String MessageNoPerm ="[§r§cbedwars§r]§r§4>>§r§c";
	public static String MessagePerm ="[§r§abedwars§r]§r§7>>§r§2";
	public static String KillMessageStart ="";
	public static String KillMessageMiddle ="";

	public static File filemoney = new File("plugins/bedwars", "shop.yml");
	public static FileConfiguration cfgmoney = YamlConfiguration.loadConfiguration(filemoney);

	public static File filemain = new File("plugins/bedwars", "serverconfig.yml");
	public static FileConfiguration cfgmain = YamlConfiguration.loadConfiguration(filemain);

	public static File filemapdata = new File("plugins/bedwars", "mapdata.yml");
	public static FileConfiguration cfgmapdata = YamlConfiguration.loadConfiguration(filemapdata);

	public static File filelobbydata = new File("plugins/bedwars", "lobbydata.yml");
	public static FileConfiguration cfglobbydata = YamlConfiguration.loadConfiguration(filelobbydata);
	public File filePRcore_join = new File("plugins/PRcore", "join.yml");
	public FileConfiguration cfgPRcore_join = YamlConfiguration.loadConfiguration(this.filePRcore_join);
	public static boolean restartlock = true;
	
	 
	public static void enableresetblocks() {
		resetblocks.add(Material.STAINED_CLAY);
		resetblocks.add(Material.ICE);
		resetblocks.add(Material.PACKED_ICE);
		resetblocks.add(Material.IRON_BLOCK);
		resetblocks.add(Material.GLASS);
		resetblocks.add(Material.BED);
		resetblocks.add(Material.BED_BLOCK);
		resetblocks.add(Material.CHEST);
		resetblocks.add(Material.ENDER_CHEST);
		resetblocks.add(Material.TNT);
		resetblocks.add(Material.WEB);
		resetblocks.add(Material.FIRE);
		resetblocks.add(Material.STRING);
		resetblocks.add(Material.TRIPWIRE);
		tntblocks.add(Material.STAINED_CLAY);
		tntblocks.add(Material.ICE);
		tntblocks.add(Material.PACKED_ICE);
		tntblocks.add(Material.IRON_BLOCK);
		tntblocks.add(Material.GLASS);
		tntblocks.add(Material.CHEST);
		tntblocks.add(Material.ENDER_CHEST);
		tntblocks.add(Material.WEB);
		tntblocks.add(Material.FIRE);
		buildblocks.add(Material.STAINED_CLAY);
		buildblocks.add(Material.ICE);
		buildblocks.add(Material.PACKED_ICE);
		buildblocks.add(Material.IRON_BLOCK);
		buildblocks.add(Material.GLASS);
	}
	
	public void endisabledMessage(boolean enable) {
		
		if(enable==true) {
			chatconsole("§b  _                _                            ");
			chatconsole("§b | |              | |                           ");
			chatconsole("§b | |__    ___   __| |__      __ __ _  _ __  ___");
			chatconsole("§b | '_ \\  / _ \\ / _` |\\ \\ /\\ / // _` || '__|/ __|");
			chatconsole("§b | |_) ||  __/| (_| | \\ V  V /| (_| || |   \\__ \\");
			chatconsole("§b |_.__/  \\___| \\__,_|  \\_/\\_/  \\__,_||_|   |___/");
			chatconsole("§b-->> Version "+Bukkit.getPluginManager().getPlugin("bedwars").getDescription().getVersion());
			chatconsole("§3 ____  _  _    __    ____  __    ____  ____  ");
			chatconsole("§3( ___)( \\( )  /__\\  (  _ \\(  )  ( ___)(  _ \\ ");
			chatconsole("§3 )__)  )  (  /(__)\\  ) _ < )(__  )__)  )(_) )");
			chatconsole("§3(____)(_)\\_)(__)(__)(____/(____)(____)(____/ ");
		}else{
			chatconsole("§c  _                _                            ");
			chatconsole("§c | |              | |                           ");
			chatconsole("§c | |__    ___   __| |__      __ __ _  _ __  ___");
			chatconsole("§c | '_ \\  / _ \\ / _` |\\ \\ /\\ / // _` || '__|/ __|");
			chatconsole("§c | |_) ||  __/| (_| | \\ V  V /| (_| || |   \\__ \\");
			chatconsole("§c |_.__/  \\___| \\__,_|  \\_/\\_/  \\__,_||_|   |___/");
			chatconsole("§c-->> Version "+Bukkit.getPluginManager().getPlugin("bedwars").getDescription().getVersion());
			chatconsole("§4 ____  ____  ___    __    ____  __    ____  ____  ");
			chatconsole("§4(  _ \\(_  _)/ __)  /__\\  (  _ \\(  )  ( ___)(  _ \\ ");
			chatconsole("§4 )(_) )_)(_ \\__ \\ /(__)\\  ) _ < )(__  )__)  )(_) )");
			chatconsole("§4(____/(____)(___/(__)(__)(____/(____)(____)(____/ ");
			
		}
	}
	public static void chatconsole(String message) {
		Bukkit.getConsoleSender().sendMessage(message);
	}

	@Override
	public void onEnable(){

		endisabledMessage(true);
		for(World w:Bukkit.getWorlds()) {
			w.setDifficulty(Difficulty.EASY);
			w.setStorm(false);
			w.setThundering(false);
			w.setTime(2000);
		}
		boolean checkmoney = cfgmoney.getBoolean("enabled?");
		if(checkmoney==false) {
			try {
				makedefaultconfigs.makeitmoney();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		boolean checkmain = cfgmain.getBoolean("enabled?");
		if(checkmain==false) {
			try {
				makedefaultconfigs.makeitmain();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		teams.enableTeams();
		
		 getServer().getPluginManager().registerEvents(new event_makemap(this), this);
		 getServer().getPluginManager().registerEvents(new event_hotbar(this), this);
		 getServer().getPluginManager().registerEvents(new event_makelobby(this), this);
		 getServer().getPluginManager().registerEvents(new event_lobby(this), this);
		 getServer().getPluginManager().registerEvents(new event_bed(this), this);
		 getServer().getPluginManager().registerEvents(new event_extra(this), this);
		 getServer().getPluginManager().registerEvents(new event_stats(this), this);
		 getServer().getPluginManager().registerEvents(new event_inventory_shop(this), this);
		 getServer().getPluginManager().registerEvents(new gameengine(this), this);
		 getServer().getPluginManager().registerEvents(new mapengine(this), this);
		 
		 registerEvents();
		 registerCommands();
		 for(World w : Bukkit.getWorlds()) {
			 for(Entity et : w.getEntities()) {
					if(et instanceof ArmorStand) {
						
//						String nametag = et.getCustomName();
//						if(nametag.contains("shop")||nametag.contains("teleport")||nametag.contains("bed")||nametag.contains("Pos")||nametag.contains("Spawner")) {
							et.remove();
//						}
					}
					
				}
		 }
//		 timer.onEnable();
		 armorstandmanager.onEnable();
		 enableresetblocks();
		 event_bed.beeber();
		 save.startchunkloader();
		 manager.onEnable();
		 new BukkitRunnable() {

			@Override
			public void run() {
				for(Player p:Bukkit.getOnlinePlayers()) {
					if(teams.spectator.hasEntry(p.getName())) {
						if(p.getGameMode()==GameMode.SPECTATOR) {
							
						}else {
							p.setGameMode(GameMode.SPECTATOR);
						}
					}
				}
				
			}
			 
		 }.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 1);
		
	}
	private void registerEvents() {
		new event_makemap(this);
		new event_hotbar(this);
		new event_makelobby(this);
		new event_lobby(this);
		new event_bed(this);
		new event_extra(this);
		new event_stats(this);
		new event_inventory_shop(this);
		new gameengine(this);
		new mapengine(this);
	}
	@Override
	public void onDisable(){
		endisabledMessage(false);
		for(Player pa:Bukkit.getOnlinePlayers()) {
			pa.kickPlayer("§cBEDWARS neustart");
		}
		event_extra.alertstring.set("X", null);
		try {
			event_extra.alertstring.save(event_extra.filealertstring);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 manager.onDisable();
		try {
		((CraftServer)Bukkit.getServer()).getServer().setMotd("§8>>§4RELOAD§8<<");
		teams.disableTeams();
		mapengine.resetmap();
		gametimer.killVillager();
		gametimer.endgame=true;
		teams.board.clearSlot(DisplaySlot.SIDEBAR);
		teams.board.getObjective("sidebar").unregister();
		
		
		}catch(NullPointerException en) {
			
		}
	}
	private void registerCommands() {
		command_makemap makemap = new command_makemap(this);
		getCommand("savemap").setExecutor(makemap);
		command_makelobby makelobby = new command_makelobby(this);
		getCommand("savelobby").setExecutor(makelobby);
		command_stats stats = new command_stats(this);
		getCommand("stats").setExecutor(stats);
		command_force_randomevent force_randomevent = new command_force_randomevent(this);
		getCommand("forcerandomevent").setExecutor(force_randomevent);
		
	}
	public static void randomeventfixer(boolean enable,String which) {
		
	}
	public static void sendTitle(Player player, String title, String subtitle) {
        try{
            CraftPlayer craftplayer = (CraftPlayer) player;
            PlayerConnection connection = craftplayer.getHandle().playerConnection;
            IChatBaseComponent titleJSON = ChatSerializer.a("{'text': '" + title + "'}");
            IChatBaseComponent subtitleJSON = ChatSerializer.a("{'text': '" + subtitle + "'}");
            IChatBaseComponent titleJSONNull = ChatSerializer.a("{'text': '"  +" "+ "'}");
            IChatBaseComponent subtitleJSONNull = ChatSerializer.a("{'text': '" +" " + "'}");
            PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleJSON, 0, 0, 0);
            PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, subtitleJSON);
            PacketPlayOutTitle titlePacketNull = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleJSONNull, 0, 0, 0);
            PacketPlayOutTitle subtitlePacketNull = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, subtitleJSONNull);
            if(title!=null) {
                connection.sendPacket(titlePacket);
            	
            }else {
                connection.sendPacket(titlePacketNull);
            	
            }
            if(subtitle!=null) {
                connection.sendPacket(subtitlePacket);
            	
            }else {
                connection.sendPacket(subtitlePacketNull);
            	
            }
        }catch (NoClassDefFoundError e1){
//            Main.console.sendMessage(Main.RM_Console_Prefix+"Falsche Bukkit Serverversion! §c[Needed: 1.8_R3]");
        	chatconsole("Wrong Server Version!");
        }
    }
	public static void setSquare(final Location location1, final Location location2)
    {
        final World world = location1.getWorld();
        int highestX = Math.max(location2.getBlockX(), location1.getBlockX());
        int lowestX = Math.min(location2.getBlockX(), location1.getBlockX());
 
        int highestY = Math.max(location2.getBlockY(), location1.getBlockY());
        int lowestY = Math.min(location2.getBlockY(), location1.getBlockY());
 
        int highestZ = Math.max(location2.getBlockZ(), location1.getBlockZ());
        int lowestZ = Math.min(location2.getBlockZ(), location1.getBlockZ());
 
       
                for(int x = lowestX; x <= highestX; x++)
                {
                    for(int z = lowestZ; z <= highestZ; z++)
                    {
                        for(int y = lowestY; y <= highestY; y++)
                        {
                            Location location = new Location(world, x, y, z);
                           
                                	if(resetblocks.contains(location.getBlock().getType())){
                                        location.getBlock().setType(Material.AIR);
                                		
                                	}
                                
                           
                        }
                    }
                }
    }
	public static void sendActionBar(Player player,String text) {
		PacketPlayOutChat packet = new PacketPlayOutChat(ChatSerializer.a("{\"text\":\""+text+"\"}"), (byte) 2);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
	}
	@SuppressWarnings("deprecation")
	public static boolean checkEntryfalse(Player player,World world,Location location) {

		ApplicableRegionSet set = WGBukkit.getRegionManager(world).getApplicableRegions(location);
		
		return set.allows(DefaultFlag.CHEST_ACCESS);
		
	}
//	public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
//        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
//
//        PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
//        connection.sendPacket(packetPlayOutTimes);
//        if (subtitle != null) {
//            IChatBaseComponent titleSub = ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
//            PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, titleSub);
//            connection.sendPacket(packetPlayOutSubTitle);
//        }
//        if (!title.equalsIgnoreCase("notuse")) {
//            IChatBaseComponent titleMain = ChatSerializer.a("{\"text\": \"" + title + "\"}");
//            PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleMain);
//            connection.sendPacket(packetPlayOutTitle);
//        }
//    }
	
}
