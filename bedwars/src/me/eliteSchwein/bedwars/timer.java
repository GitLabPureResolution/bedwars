package me.eliteSchwein.bedwars;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import me.eliteSCHW31N.api.PRcoreProAPI;
import me.eliteSCHW31N.bedwars.randomevents.manager;
import me.eliteSchwein.PRcore.PRcore;

public class timer {
	static PRcoreProAPI prapi = new PRcoreProAPI();
	public static String runningtimer = null;
	 
	public static int lobbytimersec = 0;
	public static int lobbytimermin = 0;
	public static int gametimersec = 0;
	public static int gametimermin = 0;
	public static int endtimersec = 0;
	public static int endtimermin = 0;
	
	public static int manlobbytimersec = 0;
	public static int manlobbytimermin = 0;
	public static int mangametimersec = 0;
	public static int mangametimermin = 0;
	public static int manendtimersec = 0;
	public static int manendtimermin = 0;
	
	public static boolean ismanlobby = false;
	public static boolean ismangame = false;
	public static boolean ismanend = false;
	public static boolean killmanlobby = false;
	
	public static void manlobby(int min,int sec) {
		manlobbytimermin=min;
		manlobbytimersec=sec;
		ismanlobby=true;
	}
	
	public static void mangame(int min,int sec) {
		mangametimermin=min;
		mangametimersec=sec;
		ismangame=true;
	}
	
	public static void manend(int min,int sec) {
		manendtimermin=min;
		manendtimersec=sec;
		ismanend=true;
	}
	public static void onEnable() {
		lobbytimermin=bedwars.cfgmain.getInt("lobbytimer.minutes");
		lobbytimersec=bedwars.cfgmain.getInt("lobbytimer.seconds");
		gametimermin=bedwars.cfgmain.getInt("gametimer.minutes");
		gametimersec=bedwars.cfgmain.getInt("gametimer.seconds");
		endtimermin=bedwars.cfgmain.getInt("endtimer.minutes");
		endtimersec=bedwars.cfgmain.getInt("endtimer.seconds");
		runningtimer=null;
		LobbyTimer();
		prapi.updateGameState(true);
	}
	
	public static void LobbyTimer() {
		runningtimer="Lobby";
//		"&b&l5&r&6&o4&r&e3&r&a&n&l2&r&c&m1&r&4&l&o0";
		new BukkitRunnable() {

			@Override
			public void run() {
				if(killmanlobby==true) {
					cancel();
				}
				for(World w:Bukkit.getWorlds()) {
					w.setTime(2000);
				}
				Date date = new Date();
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.setTime(date);
				int hour = calendar.get(Calendar.HOUR_OF_DAY);
				if(hour==0&&bedwars.restartlock==false) {
					bedwars.chatconsole("§cSHUTDOWN");
					PRcore.hub("§cBedwars Restart");
					Bukkit.shutdown();
				}else if(hour>=1&&bedwars.restartlock==true) {
					bedwars.restartlock=false;
					bedwars.chatconsole("§cDISABLED RESTARTLOCK");
				}
				bedwars.chatconsole("§3Lobby Timer "+Integer.toString(lobbytimersec) + "Sec");
				bedwars.chatconsole("§3Lobby Timer "+Integer.toString(lobbytimermin) + "Min");
				if(ismanlobby==true) {
					bedwars.chatconsole("§cLobby Timer manipulated");
					lobbytimermin=manlobbytimermin;
					lobbytimersec=manlobbytimersec;
					ismanlobby=false;
				}
				teams.checkteamsusage();
				bedwars.chatconsole("§dUsed Teams "+Integer.toString(teams.teams.size()));
				if(lobbytimermin==0) {
					if(lobbytimersec==0) {
						cancel();
							if(teams.teams.size()!=2&&teams.teams.size()<=2) {
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
						     
								bedwars.sendTitle(allplayer, null, "§cMindestens Zwei Teams Belegen!");
								gameengine.allowmapvote=true;
								gameengine.allowteamchange=true;
								mapengine.allowtier3vote=true;
								mapengine.allowrandomevnvote=true;
								onEnable();
						    
							}
							
						}else {
							PRcore.Motdstate(true);
							prapi.updateGameState(false);
//							mapengine.resetmap();
							gameengine.gamestarted=true;
							gametimer.startgame();
							for (Player allplayer :  Bukkit.getOnlinePlayers()) {
								if(teams.lobbyuser.hasEntry(allplayer.getName())) {
									teams.spectator.addEntry(allplayer.getName());
									bedwars.sendTitle(allplayer, "§4§l§o0", "Du schaust nun zu");
									allplayer.setGameMode(GameMode.SPECTATOR);
									ArrayList<String> plays = new ArrayList<String>();
									for(Team rt:gametimer.restteams) {
										for(String entr:rt.getEntries()) {
											plays.add(entr);
										}
									}
									allplayer.setSpectatorTarget(null);
									String entr = plays.get((new Random()).nextInt(plays.size()));
									Player rtp=Bukkit.getPlayer(entr);
									allplayer.teleport(rtp.getLocation());
									new BukkitRunnable() {

										@Override
										public void run() {
											for(String str:spectator.specstands.keySet()) {
												Location specloc = spectator.specstands.get(str);
												spectator.addArmorStand(allplayer, specloc, spectator.entityIds, str);
											}
										}
									}.runTaskLater(Bukkit.getPluginManager().getPlugin("bedwars"), 20);

									
								}else {
								try {
								bedwars.sendTitle(allplayer, "§4§l§o0", "Dein Team : "+teams.board.getEntryTeam(allplayer.getName()).getDisplayName());
								String teamname = teams.board.getEntryTeam(allplayer.getName()).getDisplayName();
								PRcore.setNick(allplayer, "§7 ["+teamname+"§7]");
								}catch(NullPointerException en) {
									teams.spectator.addEntry(allplayer.getName());
									bedwars.sendTitle(allplayer, "§4§l§o0", "Du schaust nun zu");
									allplayer.setGameMode(GameMode.SPECTATOR);
									for(String str:spectator.specstands.keySet()) {
										Location specloc = spectator.specstands.get(str);
										spectator.addArmorStand(allplayer, specloc, spectator.entityIds, str);
									}

									ArrayList<String> plays = new ArrayList<String>();
									for(Team rt:gametimer.restteams) {
										for(String entr:rt.getEntries()) {
											plays.add(entr);
										}
									}
									allplayer.setSpectatorTarget(null);
									String entr = plays.get((new Random()).nextInt(plays.size()));
									Player rtp=Bukkit.getPlayer(entr);
									allplayer.teleport(rtp.getLocation());

								}
								}}
						}
					}
					for (Player allplayer :  Bukkit.getOnlinePlayers()) {
						allplayer.setExp(0.99f);
						allplayer.setLevel(lobbytimersec);
					}
					if(lobbytimersec==5||lobbytimersec<=5) {
						if(teams.teams.size()!=2&&teams.teams.size()<=2) {
							cancel();
							for (Player allplayer :  Bukkit.getOnlinePlayers()) {
							     
								bedwars.sendTitle(allplayer, null, "§cMindestens Zwei Teams Belegen!");
								gameengine.allowmapvote=true;
								gameengine.allowteamchange=true;
								mapengine.allowtier3vote=true;
								mapengine.allowrandomevnvote=true;
								    
								}
							onEnable();
							
							
							
						}else {

							for (Player allplayer :  Bukkit.getOnlinePlayers()) {
								float exp = allplayer.getExp();
								float removeexp = (float)1/lobbytimersec;
								float newexp = exp-removeexp;
								allplayer.setExp(newexp);
								allplayer.setLevel(lobbytimersec);
							}
							if(lobbytimersec==5) {
								gameengine.allowmapvote=false;
								mapengine.allowtier3vote=false;
								mapengine.allowrandomevnvote=false;
								mapengine.tier3finalvote();
								mapengine.randomevnfinalvote();
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									bedwars.sendTitle(allplayer, "§b§l5", "§cVotes ist nun gesperrt");
									    
									}
							}
							if(lobbytimersec==4) {
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									gameengine.allowteamchange=false;
									bedwars.sendTitle(allplayer, "§6§o4", "§cTeamwechsel ist nun gesperrt");
									    
									}
							}
							if(lobbytimersec==3) {
								String randomevnvote = null;
								if(mapengine.randomevnvote==true) {
									String rmd=manager.select();
									randomevnvote="§aRandom Event: §2"+rmd;
								}else {
									randomevnvote="§cRandom Event deaktiviert";
									
								}
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									bedwars.sendTitle(allplayer, "§e3", randomevnvote);
									
									    
									}
							}
							if(lobbytimersec==2) {
								String tier3vote = null;
								if(mapengine.tier3vote==false) {
									tier3vote="§a"+bedwars.cfgmoney.getString("moneytyp.tier3")+" aktiviert";
								}else {
									tier3vote="§c"+bedwars.cfgmoney.getString("moneytyp.tier3")+" deaktiviert";
									
								}
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									String votedmap = gameengine.mapvoted();
									gameengine.votedmap= votedmap;
									bedwars.sendTitle(allplayer, "§a§n§l2", tier3vote);
									    
									}
								mapengine.loadmap(gameengine.votedmap);
							}
							if(lobbytimersec==1) {
								for (Player allplayer :  Bukkit.getOnlinePlayers()) {
									bedwars.sendTitle(allplayer, "§c§n§l1", "§bMap: "+gameengine.votedmap);
									    
									}
							}
							
							
						}
					}
					lobbytimersec--;
				}else {
					for (Player allplayer :  Bukkit.getOnlinePlayers()) {
						allplayer.setExp(0.99f);
						allplayer.setLevel(lobbytimermin);
					}
					if(lobbytimersec==0) {
						lobbytimersec=60;
						lobbytimermin=lobbytimermin-1;
					}else {
						lobbytimersec--;
					}
				}
				
			}
			
		}.runTaskTimer(Bukkit.getPluginManager().getPlugin("bedwars"), 0, 20);
	}
	
	public void setTotalExperience(Player player,int xp) {
	     //Levels 0 through 15
	     if(xp >= 0 && xp < 351) {
	       //Calculate Everything
	       int a = 1; int b = 6; int c = -xp;
	       int level = (int) (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
	       int xpForLevel = (int) (Math.pow(level, 2) + (6 * level));
	       int remainder = xp - xpForLevel;
	       int experienceNeeded = (2 * level) + 7;
	       float experience = (float) remainder / (float) experienceNeeded;
	       experience = round(experience, 2);
	       
	       //Set Everything
	       player.setLevel(level);
	       player.setExp(experience);
	     //Levels 16 through 30
	     } else if(xp >= 352 && xp < 1507) {
	       //Calculate Everything
	       double a = 2.5; double b = -40.5; int c = -xp + 360;
	       double dLevel = (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
	       int level = (int) Math.floor(dLevel);
	       int xpForLevel = (int) (2.5 * Math.pow(level, 2) - (40.5 * level) + 360);
	       int remainder = xp - xpForLevel;
	       int experienceNeeded = (5 * level) - 38;
	       float experience = (float) remainder / (float) experienceNeeded;
	       experience = round(experience, 2);
	       
	       //Set Everything
	       player.setLevel(level);
	       player.setExp(experience);     
	     //Level 31 and greater
	     } else {
	       //Calculate Everything
	       double a = 4.5; double b = -162.5; int c = -xp + 2220;
	       double dLevel = (-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a);
	       int level = (int) Math.floor(dLevel);
	       int xpForLevel = (int) (4.5 * Math.pow(level, 2) - (162.5 * level) + 2220);
	       int remainder = xp - xpForLevel;
	       int experienceNeeded = (9 * level) - 158;
	       float experience = (float) remainder / (float) experienceNeeded;
	       experience = round(experience, 2);
	       
	       //Set Everything
	       player.setLevel(level);
	       player.setExp(experience);       
	     }
	   }
	   
	  private float round(float d, int decimalPlace) {
	  BigDecimal bd = new BigDecimal(Float.toString(d));
	  bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_DOWN);
	  return bd.floatValue();
	  }

}
