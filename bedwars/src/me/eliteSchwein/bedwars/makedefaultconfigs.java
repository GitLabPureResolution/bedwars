package me.eliteSchwein.bedwars;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Material;

public class makedefaultconfigs {
	
	
	public static void makeitmoney() throws IOException {
		bedwars.cfgmoney.set("enabled?", true);
		bedwars.cfgmoney.set("moneytyp.tier1", "BRONZE");
		bedwars.cfgmoney.set("moneytyp.tier2", "EISEN");
		bedwars.cfgmoney.set("moneytyp.tier3", "GOLD");
		bedwars.cfgmoney.set("material.tier1", Material.CLAY_BRICK.toString());
		bedwars.cfgmoney.set("material.tier2", Material.IRON_INGOT.toString());
		bedwars.cfgmoney.set("material.tier3", Material.GOLD_INGOT.toString());
		bedwars.cfgmoney.set("subid.tier1", 0);
		bedwars.cfgmoney.set("subid.tier2", 0);
		bedwars.cfgmoney.set("subid.tier3", 0);
		bedwars.cfgmoney.set("block.stainedclay.typ", "tier1");
		bedwars.cfgmoney.set("block.stainedclay.amount", 1);
		bedwars.cfgmoney.set("block.stainedclay.shopamount", 2);

		bedwars.cfgmoney.set("block.iceblock.typ", "tier2");
		bedwars.cfgmoney.set("block.iceblock.amount", 1);
		bedwars.cfgmoney.set("block.iceblock.shopamount", 1);

		bedwars.cfgmoney.set("block.ironblock.typ", "tier2");
		bedwars.cfgmoney.set("block.ironblock.amount", 2);
		bedwars.cfgmoney.set("block.ironblock.shopamount", 1);

		bedwars.cfgmoney.set("block.ladder.typ", "tier3");
		bedwars.cfgmoney.set("block.ladder.amount", 1);
		bedwars.cfgmoney.set("block.ladder.shopamount", 1);

		bedwars.cfgmoney.set("block.glassblock.typ", "tier2");
		bedwars.cfgmoney.set("block.glassblock.amount", 16);
		bedwars.cfgmoney.set("block.glassblock.shopamount", 2);

		bedwars.cfgmoney.set("block.chest.typ", "tier2");
		bedwars.cfgmoney.set("block.chest.amount", 3);
		bedwars.cfgmoney.set("block.chest.shopamount", 1);

		bedwars.cfgmoney.set("block.enderchest.typ", "tier3");
		bedwars.cfgmoney.set("block.enderchest.amount", 1);
		bedwars.cfgmoney.set("block.enderchest.shopamount", 1);

		bedwars.cfgmoney.set("weapon.stick.typ", "tier1");
		bedwars.cfgmoney.set("weapon.stick.amount", 6);
		bedwars.cfgmoney.set("weapon.stick.shopamount", 1);

		bedwars.cfgmoney.set("weapon.goldswordtier1.typ", "tier2");
		bedwars.cfgmoney.set("weapon.goldswordtier1.amount", 1);
		bedwars.cfgmoney.set("weapon.goldswordtier1.shopamount", 1);

		bedwars.cfgmoney.set("weapon.goldswordtier2.typ", "tier2");
		bedwars.cfgmoney.set("weapon.goldswordtier2.amount", 2);
		bedwars.cfgmoney.set("weapon.goldswordtier2.shopamount", 1);

		bedwars.cfgmoney.set("weapon.ironsword.typ", "tier3");
		bedwars.cfgmoney.set("weapon.ironsword.amount", 4);
		bedwars.cfgmoney.set("weapon.ironsword.shopamount", 1);

		bedwars.cfgmoney.set("weapon.arrow.typ", "tier3");
		bedwars.cfgmoney.set("weapon.arrow.amount", 1);
		bedwars.cfgmoney.set("weapon.arrow.shopamount", 1);

		bedwars.cfgmoney.set("weapon.bowtier1.typ", "tier3");
		bedwars.cfgmoney.set("weapon.bowtier1.amount", 3);
		bedwars.cfgmoney.set("weapon.bowtier1.shopamount", 1);

		bedwars.cfgmoney.set("weapon.bowtier2.typ", "tier3");
		bedwars.cfgmoney.set("weapon.bowtier2.amount", 6);
		bedwars.cfgmoney.set("weapon.bowtier2.shopamount", 1);

		bedwars.cfgmoney.set("weapon.bowtier3.typ", "tier3");
		bedwars.cfgmoney.set("weapon.bowtier3.amount", 12);
		bedwars.cfgmoney.set("weapon.bowtier3.shopamount", 1);

		bedwars.cfgmoney.set("weapon.bowtier1fire.typ", "tier3");
		bedwars.cfgmoney.set("weapon.bowtier1fire.amount", 16);
		bedwars.cfgmoney.set("weapon.bowtier1fire.shopamount", 1);

		bedwars.cfgmoney.set("food.fish.typ", "tier1");
		bedwars.cfgmoney.set("food.fish.amount", 1);
		bedwars.cfgmoney.set("food.fish.shopamount", 1);

		bedwars.cfgmoney.set("food.apple.typ", "tier1");
		bedwars.cfgmoney.set("food.apple.amount", 1);
		bedwars.cfgmoney.set("food.apple.shopamount", 2);

		bedwars.cfgmoney.set("food.steak.typ", "tier1");
		bedwars.cfgmoney.set("food.steak.amount", 2);
		bedwars.cfgmoney.set("food.steak.shopamount", 1);

		bedwars.cfgmoney.set("food.goldenapple.typ", "tier3");
		bedwars.cfgmoney.set("food.goldenapple.amount", 1);
		bedwars.cfgmoney.set("food.goldenapple.shopamount", 1);

		bedwars.cfgmoney.set("pickaxe.wood.typ", "tier1");
		bedwars.cfgmoney.set("pickaxe.wood.amount", 4);
		bedwars.cfgmoney.set("pickaxe.wood.shopamount", 1);

		bedwars.cfgmoney.set("pickaxe.stonetier1.typ", "tier2");
		bedwars.cfgmoney.set("pickaxe.stonetier1.amount", 1);
		bedwars.cfgmoney.set("pickaxe.stonetier1.shopamount", 1);

		bedwars.cfgmoney.set("pickaxe.stonetier2.typ", "tier2");
		bedwars.cfgmoney.set("pickaxe.stonetier2.amount", 3);
		bedwars.cfgmoney.set("pickaxe.stonetier2.shopamount", 1);

		bedwars.cfgmoney.set("pickaxe.iron.typ", "tier3");
		bedwars.cfgmoney.set("pickaxe.iron.amount", 1);
		bedwars.cfgmoney.set("pickaxe.iron.shopamount", 1);

		bedwars.cfgmoney.set("extra.enderpearl.typ", "tier3");
		bedwars.cfgmoney.set("extra.enderpearl.amount", 5);
		bedwars.cfgmoney.set("extra.enderpearl.shopamount", 1);

		bedwars.cfgmoney.set("extra.fishingrod.typ", "tier2");
		bedwars.cfgmoney.set("extra.fishingrod.amount", 5);
		bedwars.cfgmoney.set("extra.fishingrod.shopamount", 1);

		bedwars.cfgmoney.set("extra.tnt.typ", "tier2");
		bedwars.cfgmoney.set("extra.tnt.amount", 16);
		bedwars.cfgmoney.set("extra.tnt.shopamount", 1);

		bedwars.cfgmoney.set("extra.cobweb.typ", "tier1");
		bedwars.cfgmoney.set("extra.cobweb.amount", 16);
		bedwars.cfgmoney.set("extra.cobweb.shopamount", 1);

		bedwars.cfgmoney.set("extra.flintandsteel.typ", "tier2");
		bedwars.cfgmoney.set("extra.flintandsteel.amount", 3);
		bedwars.cfgmoney.set("extra.flintandsteel.shopamount", 1);

		bedwars.cfgmoney.set("extra.egg.typ", "tier1");
		bedwars.cfgmoney.set("extra.egg.amount", 16);
		bedwars.cfgmoney.set("extra.egg.shopamount", 1);

		bedwars.cfgmoney.set("extra.alertstring.typ", "tier2");
		bedwars.cfgmoney.set("extra.alertstring.amount", 2);
		bedwars.cfgmoney.set("extra.alertstring.shopamount", 1);

		bedwars.cfgmoney.set("extra.homepowder.typ", "tier2");
		bedwars.cfgmoney.set("extra.homepowder.amount", 3);
		bedwars.cfgmoney.set("extra.homepowder.shopamount", 1);

		bedwars.cfgmoney.set("potion.speedtier1.typ", "tier2");
		bedwars.cfgmoney.set("potion.speedtier1.amount", 4);
		bedwars.cfgmoney.set("potion.speedtier1.shopamount", 1);

		bedwars.cfgmoney.set("potion.speedtier2.typ", "tier2");
		bedwars.cfgmoney.set("potion.speedtier2.amount", 6);
		bedwars.cfgmoney.set("potion.speedtier2.shopamount", 2);

		bedwars.cfgmoney.set("potion.regenerationtier1.typ", "tier2");
		bedwars.cfgmoney.set("potion.regenerationtier1.amount", 4);
		bedwars.cfgmoney.set("potion.regenerationtier1.shopamount", 1);

		bedwars.cfgmoney.set("potion.regenerationtier2.typ", "tier2");
		bedwars.cfgmoney.set("potion.regenerationtier2.amount", 6);
		bedwars.cfgmoney.set("potion.regenerationtier2.shopamount", 2);

		bedwars.cfgmoney.set("potion.jumptier1.typ", "tier2");
		bedwars.cfgmoney.set("potion.jumptier1.amount", 4);
		bedwars.cfgmoney.set("potion.jumptier1.shopamount", 1);

		bedwars.cfgmoney.set("potion.jumptier2.typ", "tier2");
		bedwars.cfgmoney.set("potion.jumptier2.amount", 6);
		bedwars.cfgmoney.set("potion.jumptier2.shopamount", 2);

		bedwars.cfgmoney.set("potion.fireprotectiontier1.typ", "tier2");
		bedwars.cfgmoney.set("potion.fireprotectiontier1.amount", 4);
		bedwars.cfgmoney.set("potion.fireprotectiontier1.shopamount", 1);

		bedwars.cfgmoney.set("potion.fireprotectiontier2.typ", "tier2");
		bedwars.cfgmoney.set("potion.fireprotectiontier2.amount", 6);
		bedwars.cfgmoney.set("potion.fireprotectiontier2.shopamount", 2);

		bedwars.cfgmoney.set("armor.lederhelmet.typ", "tier1");
		bedwars.cfgmoney.set("armor.lederhelmet.amount", 3);
		bedwars.cfgmoney.set("armor.lederhelmet.shopamount", 1);

		bedwars.cfgmoney.set("armor.lederboots.typ", "tier1");
		bedwars.cfgmoney.set("armor.lederboots.amount", 3);
		bedwars.cfgmoney.set("armor.lederboots.shopamount", 1);

		bedwars.cfgmoney.set("armor.lederleggings.typ", "tier1");
		bedwars.cfgmoney.set("armor.lederleggings.amount", 3);
		bedwars.cfgmoney.set("armor.lederleggings.shopamount", 1);

		bedwars.cfgmoney.set("armor.chainchestplatetier1.typ", "tier2");
		bedwars.cfgmoney.set("armor.chainchestplatetier1.amount", 1);
		bedwars.cfgmoney.set("armor.chainchestplatetier1.shopamount", 1);

		bedwars.cfgmoney.set("armor.chainchestplatetier2.typ", "tier2");
		bedwars.cfgmoney.set("armor.chainchestplatetier2.amount", 2);
		bedwars.cfgmoney.set("armor.chainchestplatetier2.shopamount", 1);

		bedwars.cfgmoney.set("armor.chainchestplatetier3.typ", "tier2");
		bedwars.cfgmoney.set("armor.chainchestplatetier3.amount", 3);
		bedwars.cfgmoney.set("armor.chainchestplatetier3.shopamount", 1);

		bedwars.cfgmoney.set("armor.federfallboots.typ", "tier3");
		bedwars.cfgmoney.set("armor.federfallboots.amount", 3);
		bedwars.cfgmoney.set("armor.federfallboots.shopamount", 1);

		bedwars.cfgmoney.set("armor.bridgeboots.typ", "tier3");
		bedwars.cfgmoney.set("armor.bridgeboots.amount", 16);
		bedwars.cfgmoney.set("armor.bridgeboots.shopamount", 1);
		
		bedwars.cfgmoney.save(bedwars.filemoney);



		

		
		
		
	}
	public static void makeitmain() throws IOException {
		bedwars.cfgmain.set("enabled?", true);
		bedwars.cfgmain.set("lobbytimer.minutes", 0);
		bedwars.cfgmain.set("lobbytimer.seconds", 30);
		bedwars.cfgmain.set("endtimer.minutes", 0);
		bedwars.cfgmain.set("endtimer.seconds", 5);
		bedwars.cfgmain.set("gametimer.minutes", 60);
		bedwars.cfgmain.set("gametimer.seconds", 0);
		bedwars.cfgmain.set("teamamount", 8);
		bedwars.cfgmain.set("teamplayersize", 1);
		ArrayList<String> makedefault = new ArrayList<String>();
		makedefault.add("example1");
		makedefault.add("example2");
		makedefault.add("example3");
		makedefault.add("example4");
		bedwars.cfgmain.set("maps", makedefault);
		bedwars.cfgmain.set("winmoney", "25");
		bedwars.cfgmain.set("losemoney", "1");
		bedwars.cfgmain.set("undecidedmoney", "10");
		bedwars.cfgmain.set("time", "30");
		bedwars.cfgmain.set("timeunit", "MINUTES");
		bedwars.cfgmain.set("timetier1", "1");
		bedwars.cfgmain.set("timetier1unit", "SECONDS");
		bedwars.cfgmain.set("timetier2", "3");
		bedwars.cfgmain.set("timetier2unit", "SECONDS");
		bedwars.cfgmain.set("timetier3", "6");
		bedwars.cfgmain.set("timetier3unit", "SECONDS");
		bedwars.cfgmain.set("redname", "&cRot");
		bedwars.cfgmain.set("bluename", "&9Blau");
		bedwars.cfgmain.set("greenname", "&aGr&uuml;n");
		bedwars.cfgmain.set("yellowname", "&eGelb");
		bedwars.cfgmain.set("purplename", "&dLila");
		bedwars.cfgmain.set("magentaname", "&5Magenta");
		bedwars.cfgmain.set("whitename", "&fWei&szlig;");
		bedwars.cfgmain.set("blackname", "&0Schwarz");
		bedwars.cfgmain.set("redprefix", "&c");
		bedwars.cfgmain.set("blueprefix", "&9");
		bedwars.cfgmain.set("greenprefix", "&a");
		bedwars.cfgmain.set("yellowprefix", "&e");
		bedwars.cfgmain.set("purpleprefix", "&d");
		bedwars.cfgmain.set("magentaprefix", "&5");
		bedwars.cfgmain.set("whiteprefix", "&f");
		bedwars.cfgmain.set("blackprefix", "&0");
		bedwars.cfgmain.set("redclaycolor", 14);
		bedwars.cfgmain.set("blueclaycolor", 11);
		bedwars.cfgmain.set("greenclaycolor", 5);
		bedwars.cfgmain.set("yellowclaycolor", 4);
		bedwars.cfgmain.set("purpleclaycolor", 10);
		bedwars.cfgmain.set("magentaclaycolor", 2);
		bedwars.cfgmain.set("whiteclaycolor", 0);
		bedwars.cfgmain.set("blackclaycolor", 15);
		bedwars.cfgmain.set("redleathercolor", "r:255;g:0;b:0");
		bedwars.cfgmain.set("blueleathercolor", "r:0;g:0;b:255");
		bedwars.cfgmain.set("greenleathercolor", "r:0;g:255;b:0");
		bedwars.cfgmain.set("yellowleathercolor", "r:255;g:255;b:0");
		bedwars.cfgmain.set("purpleleathercolor", "r:255;g:0;b:138");
		bedwars.cfgmain.set("magentaleathercolor", "r:255;g:0;b:247");
		bedwars.cfgmain.set("whiteleathercolor", "r:255;g:255;b:255");
		bedwars.cfgmain.set("blackleathercolor", "r:0;g:0;b:0");
		
		bedwars.cfgmain.save(bedwars.filemain);



		

		
		
		
	}

}
