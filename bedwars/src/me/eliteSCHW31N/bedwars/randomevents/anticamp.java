package me.eliteSCHW31N.bedwars.randomevents;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import me.eliteSchwein.bedwars.teams;

public class anticamp implements Listener{
	static int timer1=0;
	static BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	static ConcurrentHashMap<Player, Integer> camplist = new ConcurrentHashMap<Player, Integer>();

	public static void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new anticamp(), plugin);
		for(Player player:Bukkit.getOnlinePlayers()) {
			if(!teams.spectator.hasEntry(player.getName())&&!teams.build.hasEntry(player.getName())&&manipulator.gamestartet()) {
				camplist.put(player, 25);
			}
		}
		timer1=scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				for(Player player:camplist.keySet()) {
					int oldtime = camplist.get(player);
					if(oldtime==0) {
						manipulator.hearthsoundcancel(player);
						player.setHealth(0);
						camplist.remove(player);
					}else if(oldtime<=11) {
						int xptime = oldtime-1;
						manipulator.setXPbar(false, player, xptime);
						manipulator.hearthsound(player);
						camplist.replace(player, oldtime-1);
					}else {
						int xptime = oldtime-1;
						manipulator.setXPbar(true, player, xptime);
						camplist.replace(player, oldtime-1);
					}
				}
			}
			
		}, 0, 20);
		
	}

	public static void onDisable() {
		scheduler.cancelTask(timer1);
		
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		Player player = e.getPlayer();
		manipulator.hearthsoundcancel(player);
		if(!teams.spectator.hasEntry(player.getName())&&!teams.build.hasEntry(player.getName())&&manipulator.gamestartet()) {
			if(camplist.contains(player)) {
				camplist.replace(player, 35);
			}else {
				camplist.put(player, 35);
			}
		}else {
			camplist.remove(player);
		}
	}
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		Location loc = player.getLocation();
		Location toloc = e.getTo();
		if ((loc.getBlockZ() != toloc.getBlockZ()) || (loc.getBlockX() != toloc.getBlockX()) || (loc.getBlockY() != toloc.getBlockY())){

			manipulator.hearthsoundcancel(player);
			if(!teams.spectator.hasEntry(player.getName())&&!teams.build.hasEntry(player.getName())&&manipulator.gamestartet()) {
				if(camplist.contains(player)) {
					camplist.replace(player, 20);
				}else {
					camplist.put(player, 20);
				}
			}else {
				camplist.remove(player);
			}
	    }
	}

}
