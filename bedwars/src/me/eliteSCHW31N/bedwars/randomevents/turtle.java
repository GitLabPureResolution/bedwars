package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class turtle {
	public static void onEnable() {
		ArrayList<PotionEffect> respawneffectslist = new ArrayList<PotionEffect>();
		respawneffectslist.add(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 2, false, false));
		respawneffectslist.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1, false, false));
		respawneffectslist.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 2, false, false));
		manipulator.setrespawnpotions(respawneffectslist);
	}
	public static void onDisable() {
		manipulator.resetrespawnpotions();
	}

}
