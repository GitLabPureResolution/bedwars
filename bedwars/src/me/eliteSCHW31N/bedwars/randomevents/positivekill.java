package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class positivekill implements Listener{
	static ArrayList<PotionEffect> potions = new ArrayList<PotionEffect>();
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new positivekill(), plugin);
		potions.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.SATURATION, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.ABSORPTION, 20*15, 2));
		potions.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*25, 1));
		potions.add(new PotionEffect(PotionEffectType.FAST_DIGGING, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, 20*25, 1));
		potions.add(new PotionEffect(PotionEffectType.INVISIBILITY, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.JUMP, 20*25, 1));
		potions.add(new PotionEffect(PotionEffectType.NIGHT_VISION, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.REGENERATION, 20*10, 2));
		potions.add(new PotionEffect(PotionEffectType.SPEED, 20*10, 2));

		
	}
	public static void onDisable() {
		potions.clear();
		
	}
	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		Player pk = e.getEntity().getKiller();
		if(pk!=null) {
			int sizeof = potions.size();
			PotionEffect rmdpotion = potions.get((new Random()).nextInt(sizeof));
			pk.addPotionEffect(rmdpotion);
		}
	}

}
