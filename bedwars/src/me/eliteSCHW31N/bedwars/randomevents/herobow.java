package me.eliteSCHW31N.bedwars.randomevents;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import me.eliteSchwein.bedwars.event_extra;

public class herobow implements Listener{
	public static ConcurrentHashMap<Arrow,Integer> arrowlist = new ConcurrentHashMap<Arrow,Integer>();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	public static void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new herobow(), plugin);
		
	}
	public static void onDisable() {
		arrowlist.clear();
	}
	@EventHandler
	public void onArrowHit(ProjectileHitEvent event) {
		Entity projectile = event.getEntity();
		if(projectile.getType()==EntityType.ARROW) {
			Entity shooter = (Entity) event.getEntity().getShooter();
			Arrow arrow = (Arrow) projectile;
			ProjectileSource shootersc = event.getEntity().getShooter();
			if(arrowlist.containsKey(arrow)) {
				int dupli = arrowlist.get(arrow);
				if(dupli==1) {
					arrowlist.remove(arrow);
				}else {
					int duplinew = dupli-1;
					arrowlist.remove(arrow);
					Location loc = arrow.getLocation();
					Location locc1 = loc.clone().add(1, 2, 1);
					Location locc2 = loc.clone().add(1, 2, -1);
					Location locc3 = loc.clone().add(-1, 2, 1);
					Location locc4 = loc.clone().add(-1, 2, -1);
					World world = loc.getWorld();
					Arrow arrowc1 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc2 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc3 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc4 = (Arrow)world.spawn(loc, Arrow.class);
					arrowc1.setShooter(shootersc);
					arrowc2.setShooter(shootersc);
					arrowc3.setShooter(shootersc);
					arrowc4.setShooter(shootersc);
					Vector vectorc1 = locc1.toVector().subtract(loc.toVector());
					vectorc1.normalize().multiply(0.3D);
					arrowc1.setVelocity(vectorc1);
					Vector vectorc2 = locc2.toVector().subtract(loc.toVector());
					vectorc2.normalize().multiply(0.3D);
					arrowc2.setVelocity(vectorc2);
					Vector vectorc3 = locc3.toVector().subtract(loc.toVector());
					vectorc3.normalize().multiply(0.3D);
					arrowc3.setVelocity(vectorc3);
					Vector vectorc4 = locc4.toVector().subtract(loc.toVector());
					vectorc4.normalize().multiply(0.3D);
					arrowc4.setVelocity(vectorc4);
					arrowlist.put(arrowc1, duplinew);
					arrowlist.put(arrowc2, duplinew);
					arrowlist.put(arrowc3, duplinew);
					arrowlist.put(arrowc4, duplinew);
				}
				return;
			}
			if(shooter instanceof Player) {
				if(arrow.getCustomName()==null) {
					arrowlist.remove(arrow);
				}else
				if(arrow.getCustomName().contains("Bogen")) {
					String arrowtier=arrow.getCustomName().replace("Bogen ", "");
					Location loc = arrow.getLocation();
					World world = loc.getWorld();
					for(Entity et:world.getNearbyEntities(loc, 0.2, 0.2, 0.2)) {
						if(et instanceof TNTPrimed) {
							return;
						}else if(et instanceof Arrow&&et!=projectile) {
							return;
						}
					}
					Location locc1 = loc.clone().add(1, 2, 1);
					Location locc2 = loc.clone().add(1, 2, -1);
					Location locc3 = loc.clone().add(-1, 2, 1);
					Location locc4 = loc.clone().add(-1, 2, -1);
					Arrow arrowc1 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc2 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc3 = (Arrow)world.spawn(loc, Arrow.class);
					Arrow arrowc4 = (Arrow)world.spawn(loc, Arrow.class);
					arrowc1.setShooter(shootersc);
					arrowc2.setShooter(shootersc);
					arrowc3.setShooter(shootersc);
					arrowc4.setShooter(shootersc);
					Vector vectorc1 = locc1.toVector().subtract(loc.toVector());
					vectorc1.normalize().multiply(0.3D);
					arrowc1.setVelocity(vectorc1);
					Vector vectorc2 = locc2.toVector().subtract(loc.toVector());
					vectorc2.normalize().multiply(0.3D);
					arrowc2.setVelocity(vectorc2);
					Vector vectorc3 = locc3.toVector().subtract(loc.toVector());
					vectorc3.normalize().multiply(0.3D);
					arrowc3.setVelocity(vectorc3);
					Vector vectorc4 = locc4.toVector().subtract(loc.toVector());
					vectorc4.normalize().multiply(0.3D);
					arrowc4.setVelocity(vectorc4);
					if(arrowtier.equals("III")) {
						arrowlist.put(arrowc1, 3);
						arrowlist.put(arrowc2, 3);
						arrowlist.put(arrowc3, 3);
						arrowlist.put(arrowc4, 3);
						world.playEffect(loc, Effect.SPELL, 1000);
						world.strikeLightningEffect(loc);
						for(Entity et:world.getNearbyEntities(loc, 6, 6, 6)) {
							if(et instanceof Player) {
								Location playerloc = et.getLocation();
								world.strikeLightningEffect(playerloc);
								world.strikeLightningEffect(playerloc);
								world.strikeLightningEffect(playerloc);
								((Player) et).damage(8);
								et.setFireTicks(20*3);
							}
						}
					}else if(arrowtier.equals("II")) {
						arrowlist.put(arrowc1, 2);
						arrowlist.put(arrowc2, 2);
						arrowlist.put(arrowc3, 2);
						arrowlist.put(arrowc4, 2);
						TNTPrimed tnt = (TNTPrimed)world.spawn(loc.clone().add(0, 2, 0), TNTPrimed.class);
					    tnt.setCustomName("§cGRANATE!!!!");
					    tnt.setCustomNameVisible(true);
					    tnt.setYield(event_extra.tntpower);
					    tnt.setFuseTicks(20*2);
					    manipulator.putlaunchedTnt(tnt);
					}else if(arrowtier.equals("I")) {
						arrowlist.put(arrowc1, 1);
						arrowlist.put(arrowc2, 1);
						arrowlist.put(arrowc3, 1);
						arrowlist.put(arrowc4, 1);
					}
				}
			}
		}
	}
}
