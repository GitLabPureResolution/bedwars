package me.eliteSCHW31N.bedwars.randomevents;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.Plugin;


public class blizzardgranade implements Listener{
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new blizzardgranade(), plugin);
		manipulator.changetntpower((float) 1.5);
		
	}

	public static void onDisable() {
		manipulator.changetntpower(3);
		
	}

	@EventHandler
	public void onTNT(EntityExplodeEvent e) {
		if(e.getEntityType()==EntityType.PRIMED_TNT) {
			Location loc = e.getLocation();
			World w = loc.getWorld();
			w.playEffect(loc, Effect.SPELL, 1000);
			w.strikeLightningEffect(loc);
			for(Entity et:w.getNearbyEntities(loc, 6, 6, 6)) {
				if(et instanceof Player) {
					Location playerloc = et.getLocation();
					w.strikeLightningEffect(playerloc);
					w.strikeLightningEffect(playerloc);
					w.strikeLightningEffect(playerloc);
					((Player) et).damage(8);
					et.setFireTicks(20*3);
				}
			}
			
		}
	}

}
