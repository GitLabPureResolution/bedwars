package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import me.eliteSchwein.bedwars.event_extra;

public class clustergranade implements Listener{
	public static ArrayList<TNTPrimed> clustergranade = new ArrayList<TNTPrimed>();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	public static void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new clustergranade(), plugin);
		manipulator.changetntpower(2);
		
	}
	public static void onDisable() {
		manipulator.changetntpower(3);
		
	}

	@EventHandler
	public void onTNT(EntityExplodeEvent e) {
		if(e.getEntityType()==EntityType.PRIMED_TNT) {
			TNTPrimed tnt = (TNTPrimed) e.getEntity();
			if(clustergranade.contains(tnt)) {
				clustergranade.remove(tnt);
				return;
			}
			Location loc = tnt.getLocation();
			Location locc1 = tnt.getLocation().clone().add(2, 0, 2);
			Location locc2 = tnt.getLocation().clone().add(2, 0, -2);
			Location locc3 = tnt.getLocation().clone().add(-2, 0, 2);
			Location locc4 = tnt.getLocation().clone().add(-2, 0, -2);
			World world = loc.getWorld();

			TNTPrimed tntc1 = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			TNTPrimed tntc2 = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			TNTPrimed tntc3 = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			TNTPrimed tntc4 = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
			
			clustergranade.add(tntc1);
			clustergranade.add(tntc2);
			clustergranade.add(tntc3);
			clustergranade.add(tntc4);
			
			manipulator.putlaunchedTnt(tntc1);
			manipulator.putlaunchedTnt(tntc2);
			manipulator.putlaunchedTnt(tntc3);
			manipulator.putlaunchedTnt(tntc4);
			Vector vectorc1 = locc1.toVector().subtract(loc.toVector());
			vectorc1.normalize().multiply(1.95D);
			tntc1.setVelocity(vectorc1);
		    tntc1.setCustomName("§cGRANATE!!!!");
		    tntc1.setCustomNameVisible(true);
		    tntc1.setYield(event_extra.tntpower);
		    tntc1.setFuseTicks(event_extra.tnttime);
			
			manipulator.putlaunchedTnt(tntc2);
			manipulator.putlaunchedTnt(tntc2);
			manipulator.putlaunchedTnt(tntc3);
			manipulator.putlaunchedTnt(tntc4);
			Vector vectorc2 = locc2.toVector().subtract(loc.toVector());
			vectorc2.normalize().multiply(1.95D);
			tntc2.setVelocity(vectorc2);
		    tntc2.setCustomName("§cGRANATE!!!!");
		    tntc2.setCustomNameVisible(true);
		    tntc2.setYield(event_extra.tntpower);
		    tntc2.setFuseTicks(event_extra.tnttime);
			
			manipulator.putlaunchedTnt(tntc3);
			manipulator.putlaunchedTnt(tntc2);
			manipulator.putlaunchedTnt(tntc3);
			manipulator.putlaunchedTnt(tntc4);
			Vector vectorc3 = locc3.toVector().subtract(loc.toVector());
			vectorc3.normalize().multiply(1.95D);
			tntc3.setVelocity(vectorc3);
		    tntc3.setCustomName("§cGRANATE!!!!");
		    tntc3.setCustomNameVisible(true);
		    tntc3.setYield(event_extra.tntpower);
		    tntc3.setFuseTicks(event_extra.tnttime);
			
			manipulator.putlaunchedTnt(tntc4);
			manipulator.putlaunchedTnt(tntc2);
			manipulator.putlaunchedTnt(tntc3);
			manipulator.putlaunchedTnt(tntc4);
			Vector vectorc4 = locc4.toVector().subtract(loc.toVector());
			vectorc4.normalize().multiply(1.95D);
			tntc4.setVelocity(vectorc4);
		    tntc4.setCustomName("§cGRANATE!!!!");
		    tntc4.setCustomNameVisible(true);
		    tntc4.setYield(event_extra.tntpower);
		    tntc4.setFuseTicks(event_extra.tnttime);
		}
	}

}
