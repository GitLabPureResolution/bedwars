package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.Collections;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;


public class invkill implements Listener{
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new invkill(), plugin);

	}
	public static void onDisable() {
		
	}
	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		Player pk = e.getEntity().getKiller();
		if(pk!=null) {
			ArrayList<ItemStack> pinv = new ArrayList<ItemStack>();
			for(ItemStack items: pk.getInventory().getContents()) {
				pinv.add(items);
			}
			Collections.shuffle(pinv);
			pk.getInventory().clear();
			for(int i =0;i<pinv.size();i++) {
				pk.getInventory().setItem(i, pinv.get(i));
			}
			pk.updateInventory();
		}
		
	}
}
