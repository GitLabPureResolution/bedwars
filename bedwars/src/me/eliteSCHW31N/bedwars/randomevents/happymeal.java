package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class happymeal implements Listener{
	static ArrayList<Material> foods = new ArrayList<Material>();
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new happymeal(), plugin);
		foods.add(Material.COOKED_FISH);
		foods.add(Material.APPLE);
		foods.add(Material.COOKED_BEEF);
		foods.add(Material.GOLDEN_APPLE);
	}
	public static void onDisable() {
		foods.clear();
	}
	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		Material m = e.getItem().getType();
		Player p = e.getPlayer();
		if(foods.contains(m)) {
			PotionEffect nausea = new PotionEffect(PotionEffectType.CONFUSION, 20*15, 1);
			p.addPotionEffect(nausea);
		}
		
	}

}
