package me.eliteSCHW31N.bedwars.randomevents;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import me.eliteSchwein.bedwars.teams;

public class breathless  implements Listener{
	static int timer1=0;
	static BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	static ConcurrentHashMap<Player, Integer> breathlist = new ConcurrentHashMap<Player, Integer>();
	public static void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new breathless(), plugin);
		for(Player player:Bukkit.getOnlinePlayers()) {
			if(!teams.spectator.hasEntry(player.getName())&&!teams.build.hasEntry(player.getName())&&manipulator.gamestartet()) {
				breathlist.put(player, 300);
			}
		}
		timer1=scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				for(Player player:breathlist.keySet()) {
					int oldtime = breathlist.get(player);
					if(oldtime==0) {
						manipulator.hearthsoundcancel(player);
						player.setHealth(0);
						breathlist.remove(player);
					}else if(oldtime<=11) {
						int xptime = oldtime-1;
						manipulator.setXPbar(false, player, xptime);
						manipulator.hearthsound(player);
						breathlist.replace(player, oldtime-1);
					}else {
						int xptime = oldtime-1;
						manipulator.setXPbar(true, player, xptime);
						breathlist.replace(player, oldtime-1);
					}
				}
			}
			
		}, 0, 20);
	}
	public static void onDisable() {
		scheduler.cancelTask(timer1);
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		Player player = e.getPlayer();
		manipulator.hearthsoundcancel(player);
		if(!teams.spectator.hasEntry(player.getName())&&!teams.build.hasEntry(player.getName())&&manipulator.gamestartet()) {
			if(breathlist.contains(player)) {
				breathlist.replace(player, 309);
			}else {
				breathlist.put(player, 309);
			}
		}else {
			breathlist.remove(player);
		}
	}

}
