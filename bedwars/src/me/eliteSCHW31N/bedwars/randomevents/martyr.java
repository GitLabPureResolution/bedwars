package me.eliteSCHW31N.bedwars.randomevents;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;

public class martyr implements Listener{
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	
	public static void onEnable(){
		Bukkit.getServer().getPluginManager().registerEvents(new martyr(), plugin);
		
	}
	
	public static void onDisable(){
		
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if(manipulator.isingameteam(player)) {

			World world = player.getWorld();
			Location loc = new Location(player.getWorld(),player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ());
			loc.setDirection(player.getLocation().getDirection());
			TNTPrimed tnt = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
		      tnt.setCustomName("§cGRANATE!!!!");
		      tnt.setCustomNameVisible(true);
		      tnt.setYield((float) 2);
		      tnt.setFuseTicks(20);
		      manipulator.putlaunchedTnt(tnt);
		}
	}
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		Player player = e.getPlayer();
		if(manipulator.isvalidteambed(block)) {
			if(manipulator.getteambed(block)!=null) {
				if(manipulator.getteambed(block).hasEntry(player.getName())) {
					return;
				}
			}
			Location loc = block.getLocation().clone().add(0, 0.5, 0);
			World world = loc.getWorld();
			TNTPrimed tnt = (TNTPrimed)world.spawn(loc, TNTPrimed.class);
		    tnt.setCustomName("§cGRANATE!!!!");
		    tnt.setCustomNameVisible(true);
		    tnt.setYield((float) 5);
		    tnt.setFuseTicks(20);
		    manipulator.putlaunchedTnt(tnt);
		}
	}
}
