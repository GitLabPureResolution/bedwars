package me.eliteSCHW31N.bedwars.randomevents;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import me.eliteSchwein.bedwars.bedwars;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.PacketPlayOutBlockBreakAnimation;

public class chinablock implements Listener {
	static int timer1=0;
	static BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	static ConcurrentHashMap<Location, Integer> blocklist = new ConcurrentHashMap<Location, Integer>();
	static ConcurrentHashMap<Location, Integer> artstate = new ConcurrentHashMap<Location, Integer>();

	public static void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new chinablock(), plugin);
		manipulator.changeDropAmount(2, 1);
		timer1=scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for(Location loc:blocklist.keySet()) {
					int oldtime = blocklist.get(loc);
					if(oldtime==-1) {
						blocklist.remove(loc);
					}else
					if(oldtime==0) {
						blocklist.remove(loc);
						Block b = loc.getBlock();
						b.getDrops().clear();
						if(b.getType()==Material.STRING||b.getType()==Material.WEB) {
							b.setType(Material.AIR);;
						}else {
							b.breakNaturally();
						}
						b.getWorld().playSound(loc, Sound.ZOMBIE_WOODBREAK, 1, 1);
						b.getWorld().playEffect(loc, Effect.STEP_SOUND, b.getTypeId());
						int id = artstate.get(loc);
						PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(id, new BlockPosition(b.getX(), b.getY(), b.getZ()), 10);
						for(Player playerall:Bukkit.getOnlinePlayers()) {
							((CraftPlayer)playerall).getHandle().playerConnection.sendPacket(packet);
							playerall.playEffect(loc, Effect.STEP_SOUND, b.getTypeId());
						}
					}else if(oldtime<=10) {
						int newtime = oldtime-1;
						blocklist.replace(loc, newtime);
						Block b = loc.getBlock();
						int id = artstate.get(loc);
						PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(id, new BlockPosition(b.getX(), b.getY(), b.getZ()), 9-oldtime);
						for(Player playerall:Bukkit.getOnlinePlayers()) {
							((CraftPlayer)playerall).getHandle().playerConnection.sendPacket(packet);
						}
						
					}else {
						int newtime = oldtime-1;
						blocklist.replace(loc, newtime);
						
					}
					

//					PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(0, new BlockPosition(e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ()), 5);
					
				}
			}
			
		}, 0, 20);
		
	}
	public static void onDisable() {
		manipulator.changeDropAmount(1, 1);
		scheduler.cancelTask(timer1);
		
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Block b = e.getBlock();
		Location loc = b.getLocation();
		Random rand = new Random();
		int id = rand.nextInt(Integer.MAX_VALUE);
		PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(id, new BlockPosition(b.getX(), b.getY(), b.getZ()), 10);
		for(Player playerall:Bukkit.getOnlinePlayers()) {
			((CraftPlayer)playerall).getHandle().playerConnection.sendPacket(packet);
		}
		if(bedwars.resetblocks.contains(b.getType())) {
			if(blocklist.contains(loc)) {
				blocklist.replace(loc, 15);
				artstate.replace(loc, id);
			}else {
				blocklist.put(loc, 15);
				artstate.put(loc, id);
			}
		}
	}
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Block b = e.getBlock();
		Location loc = b.getLocation();
		if(blocklist.containsKey(loc)) {
			blocklist.remove(loc);
			int id = artstate.get(loc);
			PacketPlayOutBlockBreakAnimation packet = new PacketPlayOutBlockBreakAnimation(id, new BlockPosition(b.getX(), b.getY(), b.getZ()), 10);
			for(Player playerall:Bukkit.getOnlinePlayers()) {
				((CraftPlayer)playerall).getHandle().playerConnection.sendPacket(packet);
			}
			artstate.remove(loc);
		}
	}

}
