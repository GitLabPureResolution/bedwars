package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Material;

import me.eliteSchwein.bedwars.gametimer;

public class dropswitch {
	static ArrayList<String> resetlist = new ArrayList<String>();
	public static void onEnable() {
		Material t1materialo = gametimer.t1material;
		Material t2materialo = gametimer.t2material;
		Material t3materialo = gametimer.t3material;
		int t1materialsub = gametimer.t1materialsub;
		int t2materialsub = gametimer.t2materialsub;
		int t3materialsub = gametimer.t3materialsub;
		String t1materialname = gametimer.t1materialname;
		String t2materialname = gametimer.t2materialname;
		String t3materialname = gametimer.t3materialname;
		Random rand = new Random();
		int min=1;
		int max=5;
		int randomNum = rand.nextInt((max - min) + 1) + min;
		if(randomNum==1) {
			resetlist.add(manipulator.changeDropItem(t1materialo, t1materialname, t1materialsub, 1));
			resetlist.add(manipulator.changeDropItem(t3materialo, t3materialname, t3materialsub, 2));
			resetlist.add(manipulator.changeDropItem(t2materialo, t2materialname, t2materialsub, 3));
		}else if(randomNum==2) {
			resetlist.add(manipulator.changeDropItem(t3materialo, t3materialname, t3materialsub, 1));
			resetlist.add(manipulator.changeDropItem(t1materialo, t1materialname, t1materialsub, 2));
			resetlist.add(manipulator.changeDropItem(t2materialo, t2materialname, t2materialsub, 3));
		}else if(randomNum==3) {
			resetlist.add(manipulator.changeDropItem(t3materialo, t3materialname, t3materialsub, 1));
			resetlist.add(manipulator.changeDropItem(t2materialo, t2materialname, t2materialsub, 2));
			resetlist.add(manipulator.changeDropItem(t1materialo, t1materialname, t1materialsub, 3));
		}else if(randomNum==4) {
			resetlist.add(manipulator.changeDropItem(t2materialo, t2materialname, t2materialsub, 1));
			resetlist.add(manipulator.changeDropItem(t1materialo, t1materialname, t1materialsub, 2));
			resetlist.add(manipulator.changeDropItem(t3materialo, t3materialname, t3materialsub, 3));
		}else if(randomNum==5) {
			resetlist.add(manipulator.changeDropItem(t2materialo, t2materialname, t2materialsub, 1));
			resetlist.add(manipulator.changeDropItem(t3materialo, t3materialname, t3materialsub, 2));
			resetlist.add(manipulator.changeDropItem(t1materialo, t1materialname, t1materialsub, 3));
		}
	}
	public static void onDisable() {
		manipulator.resetDropItem(resetlist);
		
	}

}
