package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.Random;

import me.eliteSchwein.bedwars.bedwars;
import me.eliteSchwein.bedwars.mapengine;

public class manager {
	public static String selected = null;
	public static ArrayList<String> rmds = new ArrayList<String>();
	public static void onEnable() {
		bedwars.chatconsole("§2§m»»§r§aRandom Event Manager Enabled");
		rmds.add("PositivKill");
		rmds.add("NegativKill");
		rmds.add("Randomize");
		rmds.add("DropX5");
		rmds.add("DropSwitch");
		rmds.add("DropTausch");
		rmds.add("MensaEssen");
		rmds.add("Atemzug");
		rmds.add("Tollpatsch");
		rmds.add("FastRespawn");
		rmds.add("EierGranate");
		rmds.add("AntiCamp");
		rmds.add("ChinaBlock");
		rmds.add("StarkeGranate");
		rmds.add("BlitzGranate");
		rmds.add("GravGranate");
		rmds.add("Märtyrer");
		rmds.add("ClusterGranate");
		rmds.add("Mond");
		rmds.add("Schildkröte");
		rmds.add("Hosenloch");
		rmds.add("HeldenBogen");
		rmds.add("Schlagstock");
		rmds.add("NutzloserStock");
		manipulator.onEnable();
	}
	public static void onDisable() {
		bedwars.chatconsole("§4§m»»§r§cRandom Event Manager Disabled");
		useRDM(selected,false);
		selected=null;
		rmds.clear();
		manipulator.onDisable();
	}
	public static String select() {
		if(mapengine.randomevnvote&&mapengine.forcerandomevnwhich!=null) {
			selected=mapengine.forcerandomevnwhich;
			return mapengine.forcerandomevnwhich;
		}
		int rmdssize = rmds.size();
		String rt = rmds.get((new Random()).nextInt(rmdssize));
		selected=rt;
		return rt;
	}
	public static void useRDM(String selecto,boolean enable) {
		if(selecto!=null) {
			if(enable==true) {
				bedwars.chatconsole("§a╒§2Random event enabled");
				bedwars.chatconsole("§a╘§2Name: §a"+selecto);
			}else {
				bedwars.chatconsole("§c╒§4Random event disabled");
				bedwars.chatconsole("§c╘§4Name: §c"+selecto);
			}
//			System.out.println("RandomEvent "+selecto);
			if(selecto.equals("PositivKill")) {
				if(enable==true) {
					positivekill.onEnable();
				}else {
					positivekill.onDisable();
				}
			}
			if(selecto.equals("NegativKill")) {
				if(enable==true) {
					negativekill.onEnable();
				}else {
					negativekill.onDisable();
				}
			}
			if(selecto.equals("Randomize")) {
				if(enable==true) {
					invkill.onEnable();
				}else {
					invkill.onDisable();
				}
			}
			if(selecto.equals("DropX5")) {
				if(enable==true) {
					dropx5.onEnable();
				}else {
					dropx5.onDisable();
				}
			}
			if(selecto.equals("DropSwitch")) {
				if(enable==true) {
					dropswitch.onEnable();
				}else {
					dropswitch.onDisable();
				}
			}
			if(selecto.equals("DropTausch")) {
				if(enable==true) {
					dropswitchnew.onEnable();
				}else {
					dropswitchnew.onDisable();
				}
			}
			if(selecto.equals("MensaEssen")) {
				if(enable==true) {
					happymeal.onEnable();
				}else {
					happymeal.onDisable();
				}
			}
			if(selecto.equals("Atemzug")) {
				if(enable==true) {
					breathless.onEnable();
				}else {
					breathless.onDisable();
				}
			}
			if(selecto.equals("Tollpatsch")) {
				if(enable==true) {
					hobbledehoy.onEnable();
				}else {
					hobbledehoy.onDisable();
				}
			}
			if(selecto.equals("FastRespawn")) {
				if(enable==true) {
					fastrespawn.onEnable();
				}else {
					fastrespawn.onDisable();
				}
			}
			if(selecto.equals("EierGranate")) {
				if(enable==true) {
					egggranate.onEnable();
				}else {
					egggranate.onDisable();
				}
			}
			if(selecto.equals("AntiCamp")) {
				if(enable==true) {
					anticamp.onEnable();
				}else {
					anticamp.onDisable();
				}
			}
			if(selecto.equals("ChinaBlock")) {
				if(enable==true) {
					chinablock.onEnable();
				}else {
					chinablock.onDisable();
				}
			}
			if(selecto.equals("StarkeGranate")) {
				if(enable==true) {
					powerfulgranade.onEnable();
				}else {
					powerfulgranade.onDisable();
				}
			}
			if(selecto.equals("BlitzGranate")) {
				if(enable==true) {
					blizzardgranade.onEnable();
				}else {
					blizzardgranade.onDisable();
				}
			}
			if(selecto.equals("GravGranate")) {
				if(enable==true) {
					gravitygranade.onEnable();
				}else {
					gravitygranade.onDisable();
				}
			}
			if(selecto.equals("Märtyrer")) {
				if(enable==true) {
					martyr.onEnable();
				}else {
					martyr.onDisable();
				}
			}
			if(selecto.equals("ClusterGranate")) {
				if(enable==true) {
					clustergranade.onEnable();
				}else {
					clustergranade.onDisable();
				}
			}
			if(selecto.equals("Mond")) {
				if(enable==true) {
					moon.onEnable();
				}else {
					moon.onDisable();
				}
			}
			if(selecto.equals("Schildkröte")) {
				if(enable==true) {
					turtle.onEnable();
				}else {
					turtle.onDisable();
				}
			}
			if(selecto.equals("Hosenloch")) {
				if(enable==true) {
					holepants.onEnable();
				}else {
					holepants.onDisable();
				}
			}
			if(selecto.equals("HeldenBogen")) {
				if(enable==true) {
					herobow.onEnable();
				}else {
					herobow.onDisable();
				}
			}
			if(selecto.equals("Schlagstock")) {
				if(enable==true) {
					baton.onEnable();
				}else {
					baton.onDisable();
				}
			}
			if(selecto.equals("NutzloserStock")) {
				if(enable==true) {
					uselessstick.onEnable();
				}else {
					uselessstick.onDisable();
				}
			}
		}
	}

}
