package me.eliteSCHW31N.bedwars.randomevents;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class egggranate implements Listener{
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new egggranate(), plugin);
		
	}
	public static void onDisable() {
		
	}
	@EventHandler
    public void onGranate(PlayerEggThrowEvent e){
		Egg egg = e.getEgg();
		for(Entity entity : egg.getNearbyEntities(3, 3, 3)) {
            if(entity instanceof Player) {
                Player target = (Player) entity;
               
                double Ax = egg.getLocation().getX();
                double Ay = egg.getLocation().getY();
                double Az = egg.getLocation().getZ();
               
                double Bx = target.getLocation().getX();
                double By = target.getLocation().getY();
                double Bz = target.getLocation().getZ();
               
                double x = Bx - Ax;
                double y = By - Ay;
                double z = Bz - Az;
                Vector v = new Vector(x, y, z).normalize().multiply(1.4D).setY(0.6D);
                target.setVelocity(v);
               
            }
        }
       
        egg.getWorld().playEffect(egg.getLocation(), Effect.EXPLOSION_HUGE, 3);
        egg.getWorld().playSound(egg.getLocation(), Sound.EXPLODE, 1, 1);
        e.setHatching(false);
	}

}
