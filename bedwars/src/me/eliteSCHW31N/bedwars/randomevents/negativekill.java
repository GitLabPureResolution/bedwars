package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class negativekill implements Listener{
	static ArrayList<PotionEffect> potions = new ArrayList<PotionEffect>();
	public static void onEnable() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
		Bukkit.getServer().getPluginManager().registerEvents(new negativekill(), plugin);
//		potions.add(new PotionEffect(PotionEffectType.HARM, 20*5, 1));
		potions.add(new PotionEffect(PotionEffectType.CONFUSION, 20*15, 1));
		potions.add(new PotionEffect(PotionEffectType.HUNGER, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.POISON, 20*5, 1));
		potions.add(new PotionEffect(PotionEffectType.SLOW, 20*15, 1));
		potions.add(new PotionEffect(PotionEffectType.WEAKNESS, 20*25, 2));
		potions.add(new PotionEffect(PotionEffectType.WITHER, 20*5, 1));
		potions.add(new PotionEffect(PotionEffectType.BLINDNESS, 20*15, 1));

	}
	public static void onDisable() {
		potions.clear();
		
	}
	@EventHandler
	public void onKill(PlayerDeathEvent e) {
		Player pk = e.getEntity().getKiller();
		if(pk!=null) {
			int sizeof = potions.size();
			PotionEffect rmdpotion = potions.get((new Random()).nextInt(sizeof));
			pk.addPotionEffect(rmdpotion);
		}
		
	}
}
