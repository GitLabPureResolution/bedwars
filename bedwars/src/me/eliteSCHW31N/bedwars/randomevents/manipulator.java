package me.eliteSCHW31N.bedwars.randomevents;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.Team;

import me.eliteSchwein.bedwars.bedwars;
import me.eliteSchwein.bedwars.event_bed;
import me.eliteSchwein.bedwars.event_extra;
import me.eliteSchwein.bedwars.gametimer;
import me.eliteSchwein.bedwars.mapengine;
import me.eliteSchwein.bedwars.teams;
import me.eliteSchwein.bedwars.timer;

public class manipulator {

	 
    public static Enchantment PROTECTION = Enchantment.PROTECTION_ENVIRONMENTAL;
    public static Enchantment FIRE_PROTECTION = Enchantment.PROTECTION_FIRE;
    public static Enchantment FEATHER_FALLING = Enchantment.PROTECTION_FALL;
    public static Enchantment BLAST_PROTECTION = Enchantment.PROTECTION_EXPLOSIONS;
    public static Enchantment PROJECTILE_PROTECTION = Enchantment.PROTECTION_PROJECTILE;
    public static Enchantment RESPIRATION = Enchantment.OXYGEN;
    public static Enchantment AQUA_AFFINITY = Enchantment.WATER_WORKER;
    public static Enchantment SHARPNESS = Enchantment.DAMAGE_ALL;
    public static Enchantment SMITE = Enchantment.DAMAGE_UNDEAD;
    public static Enchantment BANE = Enchantment.DAMAGE_ARTHROPODS;
    public static Enchantment KNOCKBACK = Enchantment.KNOCKBACK;
    public static Enchantment FIRE_ASPECT = Enchantment.FIRE_ASPECT;
    public static Enchantment LOOTING = Enchantment.LOOT_BONUS_MOBS;
    public static Enchantment EFFICIENCY = Enchantment.DIG_SPEED;
    public static Enchantment SILK_TOUCH = Enchantment.SILK_TOUCH;
    public static Enchantment UNBREAKING = Enchantment.DURABILITY;
    public static Enchantment FORTUNE = Enchantment.LOOT_BONUS_BLOCKS;
    public static Enchantment POWER = Enchantment.ARROW_DAMAGE;
    public static Enchantment PUNCH = Enchantment.ARROW_KNOCKBACK;
    public static Enchantment FLAME = Enchantment.ARROW_FIRE;
    public static Enchantment INFINITE = Enchantment.ARROW_INFINITE;
	public static ArrayList<PotionEffect> respawneffects = new ArrayList<PotionEffect>();
	public static ArrayList<TNTPrimed> launchedtnt = new ArrayList<TNTPrimed>();
	static int timer1=0;
	static BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
	static Plugin plugin = Bukkit.getPluginManager().getPlugin("bedwars");
	static ArrayList<Player> hearthlist = new ArrayList<Player>();
	public static ConcurrentHashMap<String,ArrayList<String>> modifyenchantinglist = new ConcurrentHashMap<String,ArrayList<String>>();
	
	public static void onEnable() {
		timer1=scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				
			}
			
		}, 0, 20);
	}
	public static void onDisable() {
		scheduler.cancelTask(timer1);
		hearthlist.clear();
		launchedtnt.clear();
		respawneffects.clear();
		modifyenchantinglist.clear();
	}
	public static boolean isingameteam(Player player) {
		for(Team team:teams.activeteams) {
			if(team.hasEntry(player.getName())) {
				return true;
			}
		}
		return false;
	}
	public static void modifyenchantment(String itemname,Enchantment enchantment,Integer level) {
		ArrayList<String> list = new ArrayList<String>();
		if(modifyenchantinglist.containsKey(itemname)) {
			list=modifyenchantinglist.get(itemname);
		}
		String entry=enchantment.getName()+";"+Integer.toString(level);
		list.add(entry);
		if(modifyenchantinglist.containsKey(itemname)) {
			modifyenchantinglist.replace(itemname, list);
		}else {
			modifyenchantinglist.put(itemname, list);
		}
	}
	public static void clearenchantment(String itemname) {
		if(modifyenchantinglist.containsKey(itemname)) {
			modifyenchantinglist.remove(itemname);
		}
	}
	public static void resetrespawnpotions() {
		respawneffects.clear();
	}
	public static void setrespawnpotions(ArrayList<PotionEffect> respawneffectslist) {
		respawneffects=respawneffectslist;
	}
	public static boolean isvalidteambed(Block block) {
		Location loc = block.getLocation();
		Material m = block.getType();

		if(m!=Material.BED&&m!=Material.BED_BLOCK) {
			return false;
		}
		
		double xloc = loc.getX();
		double yloc = loc.getY();
		double zloc = loc.getZ();
		
		double redx = mapengine.redbed1.getX();
		double redy = mapengine.redbed1.getY();
		double redz = mapengine.redbed1.getZ();
		double red1x = mapengine.redbed2.getX();
		double red1y = mapengine.redbed2.getY();
		double red1z = mapengine.redbed2.getZ();
		
		double bluex = mapengine.bluebed1.getX();
		double bluey = mapengine.bluebed1.getY();
		double bluez = mapengine.bluebed1.getZ();
		double blue1x = mapengine.bluebed2.getX();
		double blue1y = mapengine.bluebed2.getY();
		double blue1z = mapengine.bluebed2.getZ();
		
		double greenx = mapengine.greenbed1.getX();
		double greeny = mapengine.greenbed1.getY();
		double greenz = mapengine.greenbed1.getZ();
		double green1x = mapengine.greenbed2.getX();
		double green1y = mapengine.greenbed2.getY();
		double green1z = mapengine.greenbed2.getZ();
		
		double yellowx = mapengine.yellowbed1.getX();
		double yellowy = mapengine.yellowbed1.getY();
		double yellowz = mapengine.yellowbed1.getZ();
		double yellow1x = mapengine.yellowbed2.getX();
		double yellow1y = mapengine.yellowbed2.getY();
		double yellow1z = mapengine.yellowbed2.getZ();
		
		double magentax = mapengine.magentabed1.getX();
		double magentay = mapengine.magentabed1.getY();
		double magentaz = mapengine.magentabed1.getZ();
		double magenta1x = mapengine.magentabed2.getX();
		double magenta1y = mapengine.magentabed2.getY();
		double magenta1z = mapengine.magentabed2.getZ();
		
		double purplex = mapengine.purplebed1.getX();
		double purpley = mapengine.purplebed1.getY();
		double purplez = mapengine.purplebed1.getZ();
		double purple1x = mapengine.purplebed2.getX();
		double purple1y = mapengine.purplebed2.getY();
		double purple1z = mapengine.purplebed2.getZ();
		
		double whitex = mapengine.whitebed1.getX();
		double whitey = mapengine.whitebed1.getY();
		double whitez = mapengine.whitebed1.getZ();
		double white1x = mapengine.whitebed2.getX();
		double white1y = mapengine.whitebed2.getY();
		double white1z = mapengine.whitebed2.getZ();
		
		double blackx = mapengine.blackbed1.getX();
		double blacky = mapengine.blackbed1.getY();
		double blackz = mapengine.blackbed1.getZ();
		double black1x = mapengine.blackbed2.getX();
		double black1y = mapengine.blackbed2.getY();
		double black1z = mapengine.blackbed2.getZ();
		if(xloc==redx&&yloc==redy&&zloc==redz) {
			return true;
		}
		if(xloc==red1x&&yloc==red1y&&zloc==red1z) {
			return true;
		}
		if(xloc==bluex&&yloc==bluey&&zloc==bluez) {
			return true;
		}
		if(xloc==blue1x&&yloc==blue1y&&zloc==blue1z) {
			return true;
		}
		if(xloc==yellowx&&yloc==yellowy&&zloc==yellowz) {
			return true;
		}
		if(xloc==yellow1x&&yloc==yellow1y&&zloc==yellow1z) {
			return true;
		}
		if(xloc==greenx&&yloc==greeny&&zloc==greenz) {
			return true;
		}
		if(xloc==green1x&&yloc==green1y&&zloc==green1z) {
			return true;
		}
		if(xloc==magentax&&yloc==magentay&&zloc==magentaz) {
			return true;
		}
		if(xloc==magenta1x&&yloc==magenta1y&&zloc==magenta1z) {
			return true;
		}
		if(xloc==purplex&&yloc==purpley&&zloc==purplez) {
			return true;
		}
		if(xloc==purple1x&&yloc==purple1y&&zloc==purple1z) {
			return true;
		}
		if(xloc==whitex&&yloc==whitey&&zloc==whitez) {
			return true;
		}
		if(xloc==white1x&&yloc==white1y&&zloc==white1z) {
			return true;
		}
		if(xloc==blackx&&yloc==blacky&&zloc==blackz) {
			return true;
		}
		if(xloc==black1x&&yloc==black1y&&zloc==black1z) {
			return true;
		}
		return false;
	}
	public static void changeTntEngine(boolean alternate) {
		event_extra.alternatetnt=alternate;
	}
	public static void changeDropAmount(Integer i,Integer Tier) {
		if(Tier==1) {
			gametimer.dropamounttier1=i;
		}else if(Tier==2) {
			gametimer.dropamounttier2=i;
		}else if(Tier==3) {
			gametimer.dropamounttier3=i;
		}else if(Tier==4) {
			gametimer.dropamounttier1=i;
			gametimer.dropamounttier2=i;
			gametimer.dropamounttier3=i;
		}else {
			throw new ArithmeticException("This Function goes only from tier 1-3 and 4 for all");
		}
		
	}
	public static Team getteambed(Block block) {
		Location loc = block.getLocation();
		Material m = block.getType();

		if(m!=Material.BED&&m!=Material.BED_BLOCK) {
			return null;
		}
		
		double xloc = loc.getX();
		double yloc = loc.getY();
		double zloc = loc.getZ();
		
		double redx = mapengine.redbed1.getX();
		double redy = mapengine.redbed1.getY();
		double redz = mapengine.redbed1.getZ();
		double red1x = mapengine.redbed2.getX();
		double red1y = mapengine.redbed2.getY();
		double red1z = mapengine.redbed2.getZ();
		
		double bluex = mapengine.bluebed1.getX();
		double bluey = mapengine.bluebed1.getY();
		double bluez = mapengine.bluebed1.getZ();
		double blue1x = mapengine.bluebed2.getX();
		double blue1y = mapengine.bluebed2.getY();
		double blue1z = mapengine.bluebed2.getZ();
		
		double greenx = mapengine.greenbed1.getX();
		double greeny = mapengine.greenbed1.getY();
		double greenz = mapengine.greenbed1.getZ();
		double green1x = mapengine.greenbed2.getX();
		double green1y = mapengine.greenbed2.getY();
		double green1z = mapengine.greenbed2.getZ();
		
		double yellowx = mapengine.yellowbed1.getX();
		double yellowy = mapengine.yellowbed1.getY();
		double yellowz = mapengine.yellowbed1.getZ();
		double yellow1x = mapengine.yellowbed2.getX();
		double yellow1y = mapengine.yellowbed2.getY();
		double yellow1z = mapengine.yellowbed2.getZ();
		
		double magentax = mapengine.magentabed1.getX();
		double magentay = mapengine.magentabed1.getY();
		double magentaz = mapengine.magentabed1.getZ();
		double magenta1x = mapengine.magentabed2.getX();
		double magenta1y = mapengine.magentabed2.getY();
		double magenta1z = mapengine.magentabed2.getZ();
		
		double purplex = mapengine.purplebed1.getX();
		double purpley = mapengine.purplebed1.getY();
		double purplez = mapengine.purplebed1.getZ();
		double purple1x = mapengine.purplebed2.getX();
		double purple1y = mapengine.purplebed2.getY();
		double purple1z = mapengine.purplebed2.getZ();
		
		double whitex = mapengine.whitebed1.getX();
		double whitey = mapengine.whitebed1.getY();
		double whitez = mapengine.whitebed1.getZ();
		double white1x = mapengine.whitebed2.getX();
		double white1y = mapengine.whitebed2.getY();
		double white1z = mapengine.whitebed2.getZ();
		
		double blackx = mapengine.blackbed1.getX();
		double blacky = mapengine.blackbed1.getY();
		double blackz = mapengine.blackbed1.getZ();
		double black1x = mapengine.blackbed2.getX();
		double black1y = mapengine.blackbed2.getY();
		double black1z = mapengine.blackbed2.getZ();
		if(xloc==redx&&yloc==redy&&zloc==redz) {
			return teams.red;
		}
		if(xloc==red1x&&yloc==red1y&&zloc==red1z) {
			return teams.red;
		}
		if(xloc==bluex&&yloc==bluey&&zloc==bluez) {
			return teams.blue;
		}
		if(xloc==blue1x&&yloc==blue1y&&zloc==blue1z) {
			return teams.blue;
		}
		if(xloc==yellowx&&yloc==yellowy&&zloc==yellowz) {
			return teams.yellow;
		}
		if(xloc==yellow1x&&yloc==yellow1y&&zloc==yellow1z) {
			return teams.yellow;
		}
		if(xloc==greenx&&yloc==greeny&&zloc==greenz) {
			return teams.green;
		}
		if(xloc==green1x&&yloc==green1y&&zloc==green1z) {
			return teams.green;
		}
		if(xloc==magentax&&yloc==magentay&&zloc==magentaz) {
			return teams.magenta;
		}
		if(xloc==magenta1x&&yloc==magenta1y&&zloc==magenta1z) {
			return teams.magenta;
		}
		if(xloc==purplex&&yloc==purpley&&zloc==purplez) {
			return teams.purple;
		}
		if(xloc==purple1x&&yloc==purple1y&&zloc==purple1z) {
			return teams.purple;
		}
		if(xloc==whitex&&yloc==whitey&&zloc==whitez) {
			return teams.white;
		}
		if(xloc==white1x&&yloc==white1y&&zloc==white1z) {
			return teams.white;
		}
		if(xloc==blackx&&yloc==blacky&&zloc==blackz) {
			return teams.black;
		}
		if(xloc==black1x&&yloc==black1y&&zloc==black1z) {
			return teams.black;
		}
		
		return null;
	}
	public static void putlaunchedTnt(TNTPrimed tnt) {
		launchedtnt.add(tnt);
	}
	public static void removelaunchedTnt(TNTPrimed tnt) {
		launchedtnt.remove(tnt);
	}
	public static void changetnttime(int time) {
		event_extra.tnttime=time;
	}
	public static void changetntpower(float power) {
		event_extra.tntpower=power;
	}
	public static void changerespawntime(Integer newtime) {
		event_bed.respawntime=newtime;
	}
	public static boolean gamestartet() {
		if(timer.runningtimer.equalsIgnoreCase("game")) {
			return true;
		}
		return false;
	}
	public static void resetDropItem(ArrayList<String> resetlist) {
		for(String str:resetlist) {
			ArrayList<String> strlist = new ArrayList<String>();
			for(String str1:str.split(";")) {
				strlist.add(str1.replace(";", ""));
			}
			bedwars.cfgmoney.set("moneytyp."+strlist.get(0), strlist.get(1));
			bedwars.cfgmoney.set("material."+strlist.get(0), strlist.get(2));
			bedwars.cfgmoney.set("subid."+strlist.get(0), Integer.parseInt(strlist.get(3)));
		}
	}
	public static void hearthsound(Player p) {
		if(!event_bed.beeberlist.contains(p)) {
			event_bed.beeberlist.add(p);
		}
		
	}
	public static void hearthsoundcancel(Player p) {
		if(event_bed.beeberlist.contains(p)) {
			event_bed.beeberlist.remove(p);
		}
	}
	public static void setXPbar(boolean fullbar,Player player,Integer value) {
		if(fullbar==true) {
			player.setExp(0.99F);
			player.setLevel(value);
		}else {
			float exp = player.getExp();
			float removeexp = (float)1/value;
			float newexp = exp-removeexp;
			player.setExp(newexp);
			player.setLevel(value);
		}
		
	}
	public static String changeDropItem(Material material,String name,Integer subid,Integer tier) {
		if(tier==1) {
			String name1 = bedwars.cfgmoney.getString("moneytyp.tier1");
			String material1 = bedwars.cfgmoney.getString("material.tier1");
			int subid1 =bedwars.cfgmoney.getInt("subid.tier1");
			String feedback = "tier1;"+name1+";"+material1+";"+Integer.toString(subid1);
			gametimer.t1material=material;
			gametimer.t1materialname=name;
			gametimer.t1materialsub=subid;
			bedwars.cfgmoney.set("moneytyp.tier1", name);
			bedwars.cfgmoney.set("material.tier1", material);
			bedwars.cfgmoney.set("subid.tier1", subid);
			return feedback;
		}else if(tier==2) {
			String name1 = bedwars.cfgmoney.getString("moneytyp.tier2");
			String material1 = bedwars.cfgmoney.getString("material.tier2");
			int subid1 =bedwars.cfgmoney.getInt("subid.tier2");
			String feedback = "tier2;"+name1+";"+material1+";"+Integer.toString(subid1);
			gametimer.t2material=material;
			gametimer.t2materialname=name;
			gametimer.t2materialsub=subid;
			bedwars.cfgmoney.set("moneytyp.tier2", name);
			bedwars.cfgmoney.set("material.tier2", material);
			bedwars.cfgmoney.set("subid.tier2", subid);
			return feedback;
		}else if(tier==3) {
			String name1 = bedwars.cfgmoney.getString("moneytyp.tier3");
			String material1 = bedwars.cfgmoney.getString("material.tier3");
			int subid1 =bedwars.cfgmoney.getInt("subid.tier3");
			String feedback = "tier3;"+name1+";"+material1+";"+Integer.toString(subid1);
			gametimer.t3material=material;
			gametimer.t3materialname=name;
			gametimer.t3materialsub=subid;
			bedwars.cfgmoney.set("moneytyp.tier3", name);
			bedwars.cfgmoney.set("material.tier3", material);
			bedwars.cfgmoney.set("subid.tier3", subid);
			return feedback;
		}else {
			throw new ArithmeticException("This Function goes only from tier 1-3");
		}
	}

}
